unittest-xml-reporting==2.2.0
grpcio-tools==1.18.0
pymongo==3.7.2
argcomplete==1.9.4
redis==3.4.1
ray[debug,tune]==0.8.6
git+https://gitlab.com/datadrivendiscovery/automl-rpc.git@6e1f01f44be485c5850c75e70c0042599b5a5f54#egg=d3m_automl_rpc
# The following should be kept in sync with packages from the base Docker image.
pandas==1.0.3
PyYAML==5.1.2
numpy==1.18.2
nltk==3.5
python-prctl==1.7
-e git+https://gitlab.com/datadrivendiscovery/d3m.git@82da2ecf41c87590d5ad18bca699042180b12172#egg=d3m
-e git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@627b3b533c3c9ec1fab53bdd54e77b7170712578#egg=sklearn_wrap
-e git+https://gitlab.com/datadrivendiscovery/common-primitives.git@c63c1788e5ca192be80ff368be79206b17612ff5#egg=common_primitives
-e git+https://gitlab.com/datadrivendiscovery/tests-data.git@753d974b322ccae79f674432a3ca79f2f734d4c2#egg=test_primitives&subdirectory=primitives
