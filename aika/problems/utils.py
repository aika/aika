from d3m.metadata import problem as problem_module

from aika import problems
from aika.utils import exceptions


def get_problem(problem_id: str, *, strict_digest: bool = False, datasets_dir: str = None, handle_score_split: bool = True) -> problem_module.Problem:
    """
    Loads a problem description identified by ``problem_id``.

    Parameters
    ----------
    problem_id:
        A problem ID, a path to the problem description, or problem description URI.
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    datasets_dir:
        Used by D3M core package. Not supported by Aika.
    handle_score_split:
        Used by D3M core package. Not supported by Aika.

    Returns
    -------
    A problem description.
    """

    # In Aika codebase this parameter is never passed.
    assert datasets_dir is None

    try:
        return problem_module.get_problem(problem_id, strict_digest=strict_digest)
    except (exceptions.ProblemNotFoundError, exceptions.ProblemUriNotSupportedError):
        pass

    problems.store.populate(strict_digest=strict_digest)

    if not problems.store.has(problem_id):  # pylint: disable=no-member
        raise exceptions.ProblemNotFoundError("Problem description with ID '{problem_id}' cannot be found.".format(problem_id=problem_id))

    return problems.store.get(problem_id)  # pylint: disable=no-member
