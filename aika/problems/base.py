import logging
import os
import os.path
import typing

from d3m.metadata import problem as problem_module

from aika.utils import exceptions, store

__all__ = ('ProblemsStore',)

logger = logging.getLogger(__name__)

# It should match "DATASETS_DIRECTORY" and "PROBLEMS_DIRECTORY" in "aika.datasets.base" and "aika.cli".
PROBLEMS_DIRECTORY = os.path.join(os.path.dirname(__file__), '..', '..', '..', 'datasets')


class ProblemsStore(store.Store[problem_module.Problem]):
    """
    Store of all problems.

    Parameters
    ----------
    directories:
        Under which directories to search for problems.
    """

    def __init__(self, directories: typing.Union[str, typing.Sequence[str]] = (PROBLEMS_DIRECTORY,)) -> None:
        if isinstance(directories, str):
            directories = (directories,)
        self.directories = directories

        super().__init__()

    def set_directories(self, directories: typing.Union[str, typing.Sequence[str]]) -> None:
        """
        Set different directories instead of using defaults.

        Parameters
        ----------
        directories:
            Under which directories to search for problems.
        """

        with self._populating_lock:
            if self.is_populated():
                raise exceptions.InvalidStateError("Store is already populated.")

            if isinstance(directories, str):
                directories = (directories,)

            logger.info("Setting problem description search paths: %(directories)s", {'directories': list(directories)})

            self.directories = directories

    def populate(self, strict_digest: bool = False) -> None:  # pylint: disable=arguments-differ
        """
        Populates the store with values.

        Parameters
        ----------
        strict_digest:
            If computed digest does not match the one provided in metadata, raise an exception?
        """

        with self._populating_lock:
            if self.is_populated():
                return

            logger.info("Populating problems.")

            for base_directory in self.directories:
                for dirpath, dirnames, filenames in os.walk(base_directory, followlinks=True):
                    if 'datasetDoc.json' in filenames:
                        # Do not traverse further this directory to not parse problemDoc.json
                        # if it exists in a dataset as a raw data filename. This also speeds
                        # traversal because we do not go over all files in all datasets.
                        dirnames[:] = []

                        continue

                    if 'problemDoc.json' not in filenames:
                        continue

                    problem_path = os.path.join(os.path.abspath(dirpath), 'problemDoc.json')

                    try:
                        logger.debug("Loading problem from '%(problem_path)s'.", {'problem_path': problem_path})
                        problem_description = problem_module.get_problem(problem_path, strict_digest=strict_digest)
                        self._add_problem(problem_description)
                        logger.debug("Problem loaded as '%(problem_id)s'.", {'problem_id': problem_description['id']})
                    # pylint: disable=broad-except
                    except Exception:
                        logger.exception("Error indexing a problem '%(problem_path)s'.", {'problem_path': problem_path})

            logger.info("Populating problems done.")

            self._is_populated.set()

    def _add_problem(self, problem_description: problem_module.Problem) -> None:
        problem_id = problem_description['id']
        if self.has(problem_id):
            logger.warning(
                "Duplicate problem '%(problem_id)s'.",
                {
                    'problem_id': problem_id,
                },
            )
        else:
            self._index[problem_id] = problem_description
