import os.path
import typing

from d3m.container import dataset as dataset_module

from aika import datasets
from aika.utils import exceptions, uri


def get_dataset_value(dataset_id: str, *, compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING, strict_digest: bool = False) -> typing.Dict:
    """
    Constructs a value at the *intermediate level* identified
    by ``dataset_id``.

    Parameters
    ----------
    dataset_id:
        A dataset ID, a path to the dataset, or dataset URI.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?

    Returns
    -------
    A dict with ``type`` and ``value`` fields. ``type`` is
    always ``dataset_uri`` and ``value`` is the dataset URI.
    """

    if uri.is_uri(dataset_id):
        return {
            'type': 'dataset_uri',
            'value': dataset_id,
        }
    elif os.path.exists(dataset_id):
        return {
            'type': 'dataset_uri',
            'value': uri.fix_uri(dataset_id),
        }
    else:
        datasets.store.populate(compute_digest=compute_digest, strict_digest=strict_digest)
        dataset = datasets.store.get(dataset_id)  # pylint: disable=no-member
        return {
            'type': 'dataset_uri',
            'value': dataset.metadata.query(())['location_uris'][0],
        }


def get_dataset(
    dataset_id: str,
    *,
    compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING,
    strict_digest: bool = False,
    lazy: bool = False,
    datasets_dir: str = None,
    handle_score_split: bool = True,
) -> dataset_module.Dataset:
    """
    Loads a dataset identified by ``dataset_id``.

    Parameters
    ----------
    dataset_id:
        A dataset ID, a path to the dataset, or dataset URI.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    lazy:
        If ``True``, load only top-level metadata and not whole dataset.
    datasets_dir:
        Used by D3M core package. Not supported by Aika.
    handle_score_split:
        Used by D3M core package. Not supported by Aika.

    Returns
    -------
    Non-lazy Dataset instance itself.
    """

    # In Aika codebase this parameter is never passed.
    assert datasets_dir is None

    try:
        return dataset_module.get_dataset(dataset_id, compute_digest=compute_digest, strict_digest=strict_digest, lazy=lazy)
    except (exceptions.DatasetNotFoundError, exceptions.DatasetUriNotSupportedError):
        pass

    datasets.store.populate(compute_digest=compute_digest, strict_digest=strict_digest)

    if not datasets.store.has(dataset_id):  # pylint: disable=no-member
        raise exceptions.DatasetNotFoundError("Dataset with ID '{dataset_id}' cannot be found.".format(dataset_id=dataset_id))

    dataset = datasets.store.get(dataset_id)  # pylint: disable=no-member
    if dataset.is_lazy() and not lazy:
        dataset.load_lazy()
    return dataset
