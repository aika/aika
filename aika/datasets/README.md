Datasets store provides datasets available to Aika. Datasets can come from various sources
and be in various formats and Aika unifies all those sources and formats together
into a generic internal representation which allows then later primitives to convert
data to useful formats for future processing.
