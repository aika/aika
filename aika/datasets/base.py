import logging
import os
import os.path
import typing

from d3m.container import dataset as dataset_module

from aika.utils import exceptions, store

__all__ = ('DatasetsStore',)

logger = logging.getLogger(__name__)

# It should match "DATASETS_DIRECTORY" and "PROBLEMS_DIRECTORY" in "aika.cli" and "aika.problems.base".
DATASETS_DIRECTORY = os.path.join(os.path.dirname(__file__), '..', '..', '..', 'datasets')


class DatasetsStore(store.Store[dataset_module.Dataset]):
    """
    Store of all datasets.

    Parameters
    ----------
    directories:
        Under which directories to search for datasets.
    """

    def __init__(self, directories: typing.Union[str, typing.Sequence[str]] = (DATASETS_DIRECTORY,)) -> None:
        if isinstance(directories, str):
            directories = (directories,)
        self.directories = directories

        super().__init__()

    def set_directories(self, directories: typing.Union[str, typing.Sequence[str]]) -> None:
        """
        Set different directories instead of using defaults.

        Parameters
        ----------
        directories:
            Under which directories to search for datasets.
        """

        with self._populating_lock:
            if self.is_populated():
                raise exceptions.InvalidStateError("Store is already populated.")

            if isinstance(directories, str):
                directories = (directories,)

            logger.info("Setting dataset search paths: %(directories)s", {'directories': list(directories)})

            self.directories = directories

    def populate(self, compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING, strict_digest: bool = False) -> None:  # pylint: disable=arguments-differ
        """
        Populates the store with values.

        Parameters
        ----------
        compute_digest:
            When loading datasets, compute a digest over the data?
        strict_digest:
            If computed digest does not match the one provided in metadata, raise an exception?
        """

        with self._populating_lock:
            if self.is_populated():
                return

            logger.info("Populating datasets.")

            for name in ('boston', 'iris', 'diabetes', 'digits', 'linnerud', 'breast_cancer'):
                logger.debug("Loading sklearn dataset '%(name)s'.", {'name': name})
                dataset = dataset_module.get_dataset('sklearn://{name}'.format(name=name), lazy=True, compute_digest=compute_digest, strict_digest=strict_digest)
                logger.debug("Dataset loaded as %(dataset)s.", {'dataset': dataset})
                self._add_dataset(dataset)

            for base_directory in self.directories:
                for dirpath, dirnames, filenames in os.walk(base_directory, followlinks=True):
                    if 'datasetDoc.json' not in filenames:
                        continue

                    # Do not traverse further (to not parse datasetDoc.json if it exists in raw data filename).
                    dirnames[:] = []

                    dataset_path = os.path.join(os.path.abspath(dirpath), 'datasetDoc.json')

                    try:
                        logger.debug("Loading dataset from '%(dataset_path)s'.", {'dataset_path': dataset_path})
                        dataset = dataset_module.get_dataset('file://{dataset_path}'.format(dataset_path=dataset_path), lazy=True, compute_digest=compute_digest, strict_digest=strict_digest)
                        self._add_dataset(dataset)
                        logger.debug("Dataset loaded as %(dataset)s.", {'dataset': dataset})
                    # pylint: disable=broad-except
                    except Exception:
                        logger.exception("Error indexing a dataset '%(dataset_path)s'.", {'dataset_path': dataset_path})

            logger.info("Populating datasets done.")

            self._is_populated.set()

    def _add_dataset(self, dataset: dataset_module.Dataset) -> None:
        dataset_id = dataset.metadata.query(())['id']
        if self.has(dataset_id):
            logger.warning(
                "Duplicate dataset '%(dataset_id)s': %(old_dataset)s and %(dataset)s.",
                {
                    'dataset_id': dataset_id,
                    'dataset': dataset,
                    'old_dataset': self.get(dataset_id),
                },
            )
        else:
            self._index[dataset_id] = dataset
