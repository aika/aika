import copy
import datetime
import json
import os
import os.path
import typing

import yaml

# Non-module imports are made to make things available directly from this module.
from d3m import deprecate
from d3m.metadata import pipeline as pipeline_module
from d3m.metadata.base import ArgumentType, PipelineStepType  # pylint: disable=import-only-modules,unused-import
from d3m.metadata.pipeline import PlaceholderStep, PrimitiveStep, StepBase, SubpipelineStep  # pylint: disable=import-only-modules,unused-import
from d3m.primitive_interfaces import base

from aika import primitives
from aika.primitives import base as primitives_base
from aika.utils import exceptions

EVALUATION_DIRECTORY = os.path.join(os.path.dirname(__file__), '..', 'contrib', 'pipelines', 'evaluation')
SNIPPETS_DIRECTORY = os.path.join(os.path.dirname(__file__), '..', 'contrib', 'pipelines', 'snippets')
TEMPLATES_DIRECTORY = os.path.join(os.path.dirname(__file__), '..', 'contrib', 'pipelines', 'templates')


class Resolver(pipeline_module.Resolver):
    """
    Resolver which uses our primitives store to load primitives. Moreover, it resolves pipelines
    to our ``Pipeline`` class.
    """

    @classmethod
    def get_pipeline_class(cls) -> 'typing.Type[Pipeline]':
        """
        Returns a pipeline class to use when resolving pipelines.

        Returns
        -------
        A pipeline class to use.
        """

        return Pipeline

    def _get_primitive(self, primitive_description: typing.Dict) -> typing.Optional[typing.Type[base.PrimitiveBase]]:
        # We have to also check for blacklisted primitives here, because otherwise
        # they might still be loaded even if primitives store has not loaded them.
        if primitive_description['python_path'] in primitives_base.BLOCKLIST:  # pylint: disable=no-member
            raise exceptions.InvalidArgumentValueError("Primitive '{python_path}' is blacklisted.".format(python_path=primitive_description['python_path']))

        # We always first populate local primitives so that they can be resolved using just path
        # and that trying to resolve them does not trigger loading of all primitives.
        # We can call "populate_local" multiple times and it will populate primitives only once.
        primitives.store.populate_local()

        return super()._get_primitive(primitive_description)

    def _load_primitives(self) -> None:
        if not self._load_all_primitives:
            return

        if self._primitives_loaded:
            return
        self._primitives_loaded = True

        # We can call "populate" multiple times and it will populate primitives only once.
        primitives.store.populate()


class NoPrimitiveResolver(Resolver):
    """
    A resolver which does not resolve primitives.
    """

    def _get_primitive(self, primitive_description: typing.Dict) -> typing.Optional[typing.Type[base.PrimitiveBase]]:  # pylint: disable=useless-return
        return None


class NoPipelineResolver(Resolver):
    """
    A resolver which does not resolve pipelines from files, but it does
    resolve them when nested.
    """

    def _from_file(self, pipeline_description: typing.Dict) -> 'typing.Optional[Pipeline]':  # pylint: disable=useless-return
        return None


class NoResolver(Resolver):
    """
    A resolver which does not resolve primitives and pipelines from files.
    But it does resolve pipelines when nested.
    """

    def _get_primitive(self, primitive_description: typing.Dict) -> typing.Optional[typing.Type[base.PrimitiveBase]]:  # pylint: disable=useless-return
        return None

    def _from_file(self, pipeline_description: typing.Dict) -> 'typing.Optional[Pipeline]':  # pylint: disable=useless-return
        return None


P = typing.TypeVar('P', bound='Pipeline')  # pylint: disable=invalid-name


class Pipeline(pipeline_module.Pipeline):
    """
    Pipeline representation with Aika additions.

    It has an additional attribute ``aika`` with additional metadata.
    Moreover, it allows disabling checking for pipeline correctness.
    """

    @deprecate.arguments('context', message="argument ignored")
    def __init__(
        self,
        pipeline_id: str = None,
        *,
        context: typing.Any = None,
        created: datetime.datetime = None,
        source: typing.Dict = None,
        name: str = None,
        description: str = None,
        aika: typing.Dict = None,
        disable_check: bool = False,
    ) -> None:  # noqa: D102
        super().__init__(pipeline_id, created=created, source=source, name=name, description=description)

        if aika is None:
            self.aika = {}
        else:
            self.aika = aika

        self._disable_check = disable_check

    @classmethod
    def from_json_structure(  # pylint: disable=arguments-differ
        cls: typing.Type[P],
        pipeline_description: typing.Dict,
        *,
        resolver: typing.Optional[pipeline_module.Resolver] = None,
        strict_digest: bool = False,
        strict_resolving: bool = False,
        pipeline_search_paths: typing.Sequence[str] = None,
        respect_environment_variable: bool = True,
        use_internal_pipeline_search_paths: bool = False,
        no_primitive_resolving: bool = False,
        no_pipeline_resolving: bool = False,
    ) -> P:  # noqa: D102
        if resolver is None:
            # Importing here to prevent import cycle.
            from aika.pipelines import utils as pipelines_utils  # pylint: disable=cyclic-import

            if use_internal_pipeline_search_paths:
                pipeline_search_paths = pipelines_utils.get_internal_pipeline_search_paths(pipeline_search_paths)

            if no_primitive_resolving and no_pipeline_resolving:
                resolver_class: typing.Type[Resolver] = NoResolver
            elif no_primitive_resolving:
                resolver_class = NoPrimitiveResolver
            elif no_pipeline_resolving:
                resolver_class = NoPipelineResolver
            else:
                resolver_class = Resolver

            resolver = resolver_class(strict_resolving=strict_resolving, pipeline_search_paths=pipeline_search_paths, respect_environment_variable=respect_environment_variable)

        pipeline = super().from_json_structure(pipeline_description, resolver=resolver, strict_digest=strict_digest)

        pipeline.aika = pipeline_description.get('aika', {})

        return pipeline

    @classmethod
    def _canonical_pipeline_description(cls, pipeline_description: typing.Dict) -> typing.Dict:
        pipeline_description = super()._canonical_pipeline_description(pipeline_description)

        if 'aika' in pipeline_description:
            pipeline_description = copy.deepcopy(pipeline_description)
            del pipeline_description['aika']

        return pipeline_description

    def to_json_structure(self, *, nest_subpipelines: bool = False, canonical: bool = False) -> typing.Dict:  # noqa: D102
        pipeline_description = super().to_json_structure(nest_subpipelines=nest_subpipelines, canonical=canonical)

        if self.aika and not canonical:
            pipeline_description['aika'] = self.aika

        return pipeline_description

    def export_to_json(self, output_dir: str, subpipelines_dir: str, *, pipeline_rank: float = None) -> None:
        """
        Exports the pipeline as JSON to a directory ``output_dir``, including all sub-pipelines
        to a ``subpipelines_dir`` directory.

        Pipeline filenames are based on their pipeline IDs. Existing files are overridden.

        Directories are created if they are missing.

        Parameters
        ----------
        output_dir:
            A path to the output directory to store pipelines in.
        subpipelines_dir:
            A path to the directory to store sub-pipelines in.
        pipeline_rank:
            Optional pipeline rank to include in the pipeline output.
        """

        self._export(output_dir, subpipelines_dir, 'json', pipeline_rank)

    def export_to_yaml(self, output_dir: str, subpipelines_dir: str, *, pipeline_rank: float = None) -> None:
        """
        Exports the pipeline as YAML to a directory ``output_dir``, including all sub-pipelines
        to a ``subpipelines_dir`` directory.

        Pipeline filenames are based on their pipeline IDs. Existing files are overridden.

        Directories are created if they are missing.

        Parameters
        ----------
        output_dir:
            A path to the output directory to store pipelines in.
        subpipelines_dir:
            A path o the directory to store sub-pipelines in.
        pipeline_rank:
            Optional pipeline rank to include in the pipeline output.
        """

        self._export(output_dir, subpipelines_dir, 'yml', pipeline_rank)

    def _export(self, output_dir: str, subpipelines_dir: str, file_extension: str, pipeline_rank: typing.Optional[float]) -> None:
        os.makedirs(output_dir, mode=0o755, exist_ok=True)
        os.makedirs(subpipelines_dir, mode=0o755, exist_ok=True)

        if pipeline_rank is not None:
            with open(os.path.join(output_dir, '{pipeline_id}.rank'.format(pipeline_id=self.id)), 'w', encoding='utf8') as file:
                file.write(str(pipeline_rank))

        obj = self.to_json_structure(nest_subpipelines=False, canonical=True)

        self._export_obj(obj, output_dir, file_extension)

        for step in self.steps:
            if isinstance(step, pipeline_module.SubpipelineStep):
                # All sub-pipelines and their sub-pipelines go to "subpipelines_dir".
                if step.pipeline is None:
                    assert step.pipeline_description is not None
                    self._export_obj(step.pipeline_description, subpipelines_dir, file_extension)
                else:
                    typing.cast(Pipeline, step.pipeline)._export(subpipelines_dir, subpipelines_dir, file_extension, None)  # pylint: disable=protected-access

    def _export_obj(self, obj: typing.Dict, output_dir: str, file_extension: str) -> None:
        with open(os.path.join(output_dir, '{pipeline_id}.{file_extension}'.format(pipeline_id=self.id, file_extension=file_extension)), 'w', encoding='utf8') as file:
            if file_extension == 'json':
                json.dump(obj, file, allow_nan=False)
            elif file_extension == 'yml':
                yaml.dump(obj, stream=file, default_flow_style=False)
            else:
                exceptions.InvalidArgumentValueError("Invalid file extension: {file_extension}".format(file_extension=file_extension))

    def check(self, *, allow_placeholders: bool = False, standard_pipeline: bool = True, input_types: typing.Dict[str, type] = None) -> None:  # noqa: D102
        if self._disable_check:
            return

        super().check(allow_placeholders=allow_placeholders, standard_pipeline=standard_pipeline, input_types=input_types)
