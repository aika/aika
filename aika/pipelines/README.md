Pipeline is a data-flow program with [primitives](../primitives/) as basic building
blocks.

Pipelines have inputs, outputs, and free hyper-parameters.
Pipelines are general, and inputs and outputs can be of any type, but commonly
inputs are datasets and outputs are predictions.

Pipeline is a directed acyclic graph of primitives. Correct construction
of pipelines, including being well-typed, has to be assured at a
construction time of a pipeline.
