import typing

from d3m import runtime
from d3m.metadata import base as metadata_base, problem as problem_module
from d3m.primitive_interfaces import base

from aika.pipelines import pipeline as pipeline_module, utils as pipelines_utils
from aika.utils import exceptions


class IncrementalRuntime(runtime.Runtime):
    """
    A runtime which supports incrementally building and fitting a pipeline, primitive by primitive.

    Once pipeline is finalized (which finishes its fitting), it can be normally used to produce on new data.

    Only one pipeline input is supported.

    Parameters
    ----------
    fit_input:
        Pipeline input to be used during incremental building and fitting of the pipeline.
    problem_description:
        A parsed problem description in standard problem description schema.
    random_seed:
        A random seed to use for this run. This control all randomness during the run.
    volumes_dir:
        Path to a directory with static files required by primitives.
        In the standard directory structure (as obtained running ``python3 -m d3m.index download``).
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    """

    def __init__(self, fit_input: typing.Any, *, problem_description: problem_module.Problem = None, random_seed: int = 0, volumes_dir: str = None, scratch_dir: str = None) -> None:
        self._fit_input = fit_input
        self._resolver = pipeline_module.Resolver(strict_resolving=True, pipeline_search_paths=[], respect_environment_variable=False)

        pipeline = pipeline_module.Pipeline(disable_check=True)

        pipeline.add_input('input dataset')

        # TODO: Correctly set context.
        super().__init__(
            pipeline,
            None,
            problem_description=problem_description,
            random_seed=random_seed,
            volumes_dir=volumes_dir,
            is_standard_pipeline=False,
            context=metadata_base.Context.TESTING,
            environment=pipelines_utils.get_runtime_environment(),
            scratch_dir=scratch_dir,
        )

    def add_and_fit_primitive(self, primitive: typing.Type[base.PrimitiveBase], hyperparams: typing.Dict = None, arguments: typing.Dict = None) -> typing.Any:
        """
        Adds a new primitive the pipeline and fits it.

        If there is any error fitting the primitive, an exception is thrown.

        Only the ``produce`` output of the primitive is used.

        Parameters
        ----------
        primitive:
            A primitive class to add.
        hyperparams:
            Any hyper-parameter values for this primitive.
        arguments:
            Any argument data references for this primitive. By default all arguments are connected
            to the output of the current last step. Here you can specify if a different data reference
            should be used for a particular argument.

        Returns
        -------
        Produced value from fitting the added primitive.
        """

        if hyperparams is None:
            hyperparams = {}
        if arguments is None:
            arguments = {}

        self._add_primitive(primitive, hyperparams, arguments)
        return self._fit_added_primitive()

    def _get_previous_step_data_reference(self) -> str:
        if not self.pipeline.steps:
            return 'inputs.0'
        else:
            return 'steps.{i}.produce'.format(i=len(self.pipeline.steps) - 1)

    def get_last_result(self) -> typing.Any:
        """
        Returns the produce value from the last added primitive.

        Returns
        -------
        The last produced value from fitting the last added primitive.
        """

        if self.phase is not metadata_base.PipelineRunPhase.FIT:
            raise exceptions.InvalidStateError("Runtime is not in the fitting phase.")

        previous_step_data_reference = self._get_previous_step_data_reference()

        return self.data_values[previous_step_data_reference]

    # TODO: Remove also primitive from the pipeline run record.
    def remove_last_primitive(self) -> None:
        """
        Undo adding the primitive added last.

        It is removed from the pipeline and runtime state is restored like
        this primitive has never been fitted.
        """

        if self.phase is not metadata_base.PipelineRunPhase.FIT:
            raise exceptions.InvalidStateError("Runtime is not in the fitting phase.")
        if not self.pipeline.steps:
            raise exceptions.InvalidStateError("Pipeline contains on steps.")

        previous_step_data_reference = self._get_previous_step_data_reference()
        if previous_step_data_reference in self.data_values:
            del self.data_values[previous_step_data_reference]

        self.pipeline.steps.pop()
        self.steps_state.pop()
        self.current_step -= 1

    def _add_primitive(self, primitive: typing.Type[base.PrimitiveBase], hyperparams: typing.Dict, arguments: typing.Dict) -> None:
        # pylint: disable=too-many-locals

        step = pipeline_module.PrimitiveStep(primitive=primitive, resolver=self._resolver)

        previous_step_data_reference = self._get_previous_step_data_reference()

        primitive_metadata = primitive.metadata.query()

        primitive_arguments = self._get_primitive_arguments(primitive_metadata)

        primitive_arguments_set = set(primitive_arguments)
        arguments_keys_set = set(arguments.keys())

        if arguments_keys_set - primitive_arguments_set:
            raise exceptions.InvalidArgumentValueError(
                "\"arguments\" contains arguments not available by the primitive: {extra}".format(
                    extra=sorted(arguments_keys_set - primitive_arguments_set),
                ),
            )

        for argument_name in primitive_arguments:
            if argument_name in arguments:
                step.add_argument(argument_name, pipeline_module.ArgumentType.CONTAINER, arguments[argument_name])
            else:
                step.add_argument(argument_name, pipeline_module.ArgumentType.CONTAINER, previous_step_data_reference)

        primitive_hyperparams_set = set(primitive_metadata['primitive_code']['hyperparams'].keys())
        hyperparams_keys_set = set(hyperparams.keys())

        if hyperparams_keys_set - primitive_hyperparams_set:
            raise exceptions.InvalidArgumentValueError(
                "\"hyperparams\" contains hyper-parameters not available by the primitive: {extra}".format(
                    extra=sorted(hyperparams_keys_set - primitive_hyperparams_set),
                ),
            )

        for hyperparameter_name, hyperparameter_value in hyperparams.items():
            step.add_hyperparameter(hyperparameter_name, pipeline_module.ArgumentType.VALUE, hyperparameter_value)

        step.add_output('produce')

        self.pipeline.add_step(step)
        self.steps_state.append(None)

    def _get_primitive_arguments(self, primitive_metadata: typing.Dict) -> typing.Sequence[str]:
        arguments = set()

        for argument in primitive_metadata['primitive_code']['instance_methods']['set_training_data']['arguments'] + primitive_metadata['primitive_code']['instance_methods']['produce']['arguments']:
            if primitive_metadata['primitive_code']['arguments'][argument]['kind'] == metadata_base.PrimitiveArgumentKind.PIPELINE:
                arguments.add(argument)

        return sorted(arguments)

    def _fit_added_primitive(self) -> typing.Any:
        if self.phase is None:
            self._initialize_run_state([self._fit_input], metadata_base.PipelineRunPhase.FIT, [])
            self.current_step = -1  # pylint: disable=attribute-defined-outside-init
        elif self.phase is not metadata_base.PipelineRunPhase.FIT:
            raise exceptions.InvalidStateError("Runtime is not in the fitting phase.")

        self.current_step += 1

        assert self.current_step == len(self.pipeline.steps) - 1

        step = self.pipeline.steps[self.current_step]

        self._do_run_step(step)

        return self.data_values['steps.{i}.produce'.format(i=self.current_step)]

    def finish_incremental_fit(self) -> runtime.Result:
        """
        After all primitives have been added and fitted, call this method to finish
        the incremental building and fitting of the pipeline.

        It finishes with the "fit" phase and returns the pipeline run associated
        with fitting. Because exceptions are thrown during incremental fitting,
        returned ``Result`` never has exception set. Only the the output values
        of the whole pipeline are returned.

        Returns
        -------
        A result object with kept values, pipeline run description, and any exception.
        """

        if self.phase is not metadata_base.PipelineRunPhase.FIT:
            raise exceptions.InvalidStateError("Runtime is not in the fitting phase.")
        if not self.pipeline.steps:
            raise exceptions.InvalidPipelineError("Pipeline contains on steps.")

        previous_step_data_reference = self._get_previous_step_data_reference()

        self.pipeline.add_output(previous_step_data_reference, 'predictions')

        pipeline_output = self.data_values[previous_step_data_reference]

        assert self.pipeline_run is not None

        pipeline_run = self.pipeline_run

        self._clear_run_state()

        return runtime.Result(pipeline_run, {'outputs.0': pipeline_output}, None)

    def _after_step_run(self) -> None:
        # We do not do anything, by which we do not delete unnecessary values.
        # TODO: We could remove all values from the environment except for the last value and construct predictions reference.
        #       Or we could store the construct predictions reference dataframe in the gym and pass it somehow back later on again.
        #       We should make sure we leave the current last result around though.
        pass
