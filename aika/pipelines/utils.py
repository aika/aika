import logging
import os
import os.path
import typing

from d3m import container
from d3m.metadata import pipeline as d3m_pipeline_module, pipeline_run as pipeline_run_module

from aika.utils import exceptions

from . import pipeline as pipeline_module

logger = logging.getLogger(__name__)


def get_pipeline(
    pipeline_id: str,
    *,
    allow_pipeline_path: bool = False,
    strict_resolving: bool = False,
    strict_digest: bool = False,
    pipeline_search_paths: typing.Sequence[str] = None,
    respect_environment_variable: bool = True,
) -> typing.Optional[pipeline_module.Pipeline]:
    """
    Load a pipeline.

    Parameters
    ----------
    pipeline_id:
        A pipeline ID or a path to a pipeline file.
    allow_pipeline_path:
        Allow ``pipeline_id`` to be a path to a pipeline or require it to be an ID?
    strict_resolving:
        If resolved pipeline or primitive does not fully match specified reference, raise an exception?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    pipeline_search_paths:
        Use these pipeline search paths when resolving pipelines.
    respect_environment_variable:
        Use also (colon separated) pipeline search paths from ``PIPELINES_PATH`` environment variable?

    Returns
    -------
    Pipeline object itself.
    """

    if allow_pipeline_path:
        # "d3m_pipeline_module.get_pipeline" can access files outside of allowed data directories so it should be used only with trusted "pipeline_id".
        return d3m_pipeline_module.get_pipeline(
            pipeline_id,
            strict_resolving=strict_resolving,
            strict_digest=strict_digest,
            pipeline_search_paths=pipeline_search_paths,
            respect_environment_variable=respect_environment_variable,
            resolver_class=pipeline_module.Resolver,
            pipeline_class=pipeline_module.Pipeline,
        )
    else:
        resolver = pipeline_module.Resolver(
            strict_resolving=strict_resolving,
            strict_digest=strict_digest,
            pipeline_search_paths=pipeline_search_paths,
            respect_environment_variable=respect_environment_variable,
        )

        return typing.cast(typing.Optional[pipeline_module.Pipeline], resolver.get_pipeline({'id': pipeline_id}))


def get_internal_pipeline_search_paths(pipeline_search_paths: typing.Sequence[str] = None) -> typing.Sequence[str]:
    """
    Generates a list of internal pipeline search paths from pipeline directories bundled
    with Aika and their sub-directories. These include pipeline templates.

    Attributes
    ----------
    pipeline_search_paths:
        A pipeline search paths to add to.

    Returns
    -------
    A list of directories.
    """

    if pipeline_search_paths is None:
        pipeline_search_paths = []
    else:
        # Make a copy.
        pipeline_search_paths = list(pipeline_search_paths)

    for directory in [pipeline_module.SNIPPETS_DIRECTORY, pipeline_module.EVALUATION_DIRECTORY, pipeline_module.TEMPLATES_DIRECTORY]:
        pipeline_search_paths.append(directory)

        for dirpath, dirnames, filenames in os.walk(directory, followlinks=True):  # pylint: disable=unused-variable
            for dirname in dirnames:
                pipeline_search_paths.append(os.path.abspath(os.path.join(dirpath, dirname)))

    return pipeline_search_paths


def compute_normalized_score(scores: container.DataFrame) -> float:
    """
    Computes normalized average score across all folds and scores.

    Parameters
    ----------
    scores:
        A container DataFrame with scores and folds.

    Returns
    -------
    A normalized average score across all folds and scores.
    """

    normalized_values = []
    for normalized, random_seed, fold in scores.loc[:, ['normalized', 'randomSeed', 'fold']].itertuples(index=False, name=None):  # pylint: disable=unused-variable
        normalized_values.append(normalized)

    return sum(normalized_values) / len(normalized_values)


_environment: typing.Optional[pipeline_run_module.RuntimeEnvironment] = None  # pylint: disable=invalid-name


# TODO: Might not be necessary anymore in the future. See: https://gitlab.com/datadrivendiscovery/d3m/issues/280
# TODO: Allow this to be configured.
def get_runtime_environment() -> pipeline_run_module.RuntimeEnvironment:
    """
    Creates a description of the current runtime environment for use in pipeline run records.

    Description is created only once per process.

    Returns
    -------
    A runtime environment description.
    """

    global _environment  # pylint: disable=global-statement,invalid-name

    if _environment is None:
        _environment = pipeline_run_module.RuntimeEnvironment()

    return _environment


def last_step_is_placeholder(pipeline: pipeline_module.Pipeline) -> bool:
    """
    Checks that the pipeline has exactly one placeholder and that it is the last step in the pipeline.

    Parameters
    ----------
    pipeline:
        A pipeline to check.

    Returns
    -------
    ``True` if and only if the last step is a placeholder.
    """

    if not pipeline.steps:
        return False

    if not isinstance(pipeline.steps[-1], pipeline_module.PlaceholderStep):
        return False

    if any(_step_is_placeholder(s) for s in pipeline.steps[:-1]):
        return False

    return True


def _step_is_placeholder(step: pipeline_module.StepBase) -> bool:
    """
    Returns ``True`` if the step is a placeholder or if it contains any placeholder
    steps in its sub-pipelines.

    Parameters
    ----------
    step:
        A step to check.

    Returns
    -------
    ``True`` if and only if the step is a placeholder step or it contains
    a placeholder step in its sub-pipelines.
    """

    if isinstance(step, pipeline_module.PlaceholderStep):
        return True

    if isinstance(step, pipeline_module.SubpipelineStep):
        if step.pipeline is None:
            raise exceptions.InvalidStateError("Pipeline has not been resolved.")

        if any(_step_is_placeholder(s) for s in step.pipeline.steps):
            return True

    return False
