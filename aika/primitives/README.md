Primitives are functions available to Aika for use in its pipelines.

Primitives are generally provided by 3rd parties, but we also have [some ours](../contrib/primitives/).
Aika expects all their dependencies (requirements) to be available on the system it runs.
