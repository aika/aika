import logging
import os.path
import pkgutil
import sys
import threading
import typing

from d3m import index
from d3m.primitive_interfaces import base

import aika
from aika.contrib import primitives
from aika.utils import store

__all__ = ('PrimitivesStore',)

logger = logging.getLogger(__name__)

PRIMITIVES_DIRECTORY = os.path.join(os.path.dirname(__file__), '..', 'contrib', 'primitives')

BLOCKLIST: typing.List[str] = []


class PrimitivesStore(store.Store[typing.Type[base.PrimitiveBase]]):
    """
    Store of all primitives.

    Parameters
    ----------
    directories:
        Under which directories to load additional primitives.
    """

    _is_local_populated: threading.Event
    _local_populating_lock: threading.Lock

    def __init__(self, directories: typing.Union[str, typing.Sequence[str]] = ()) -> None:
        if isinstance(directories, str):
            directories = (directories,)
        self.directories = directories

        super().__init__()

        self._is_local_populated = threading.Event()
        self._local_populating_lock = threading.Lock()

    def populate_local(self) -> None:
        """
        Populates the store with local primitives.
        """

        with self._local_populating_lock:
            if self.is_local_populated():
                return

            logger.info("Populating local primitives.")

            # Load all primitives in contrib package. By loading a module we
            # give any primitive in it a chance to register itself.
            for loader, name, ispkg in pkgutil.walk_packages(primitives.__path__, primitives.__name__ + '.'):  # type: ignore # pylint: disable=unused-variable
                try:
                    # These modules should not have any relative imports.
                    loader.find_module(name).load_module(name)
                except Exception:  # pylint: disable=broad-except
                    logger.exception("Could not load the module: %(name)s", {'name': name})

            for loader, name, ispkg in pkgutil.walk_packages(self.directories):  # pylint: disable=unused-variable
                try:
                    # These modules should not have any relative imports.
                    loader.find_module(name).load_module(name)
                except Exception:  # pylint: disable=broad-except
                    logger.exception("Could not load the module: %(name)s", {'name': name})

            for primitive in index.get_loaded_primitives():
                # We check directly in values and not by primitive ID to catch duplicate primitive IDs.
                if primitive not in self.list():
                    self._add_primitive(primitive)

            logger.info("Populating local primitives done.")

            self._is_local_populated.set()

    def populate(self) -> None:
        """
        Populates the store with values.
        """

        with self._populating_lock:
            if self.is_populated():
                return

            logger.info("Populating primitives.")

            # We remove aika itself from Python's path. It sometimes ends up there. It is not necessary and it can
            # introduce conflicts if another package has a generic name like "primitives" and we try to import it.
            aika_path = aika.__path__[0]  # type: ignore
            sys.path = [path for path in sys.path if path != aika_path]

            self.populate_local()

            index.load_all(BLOCKLIST)

            for primitive in index.get_loaded_primitives():
                # We check directly in values and not by primitive ID to catch duplicate primitive IDs.
                if primitive not in self.list():
                    self._add_primitive(primitive)

            logger.info("Populating primitives done.")

            self._is_populated.set()

    def _add_primitive(self, primitive: typing.Type[base.PrimitiveBase]) -> None:
        primitive_id = primitive.metadata.query()['id']
        if self.has(primitive_id):
            logger.warning(
                "Duplicate primitive '%(primitive_id)s': %(old_primitive)s and %(primitive)s.",
                {
                    'primitive_id': primitive_id,
                    'primitive': primitive,
                    'old_primitive': self.get(primitive_id),
                },
            )
        else:
            self._index[primitive_id] = primitive

    def is_local_populated(self, block: bool = False, timeout: float = None) -> bool:
        """
        Returns ``True`` if store has been populated with local primitives.

        Parameters
        ----------
        block:
            If store has not been populated, block until it is?
        timeout:
            If ``block`` is ``True``, how long to wait before returning, in seconds?
        """

        if block:
            self._is_local_populated.wait(timeout)

        return self._is_local_populated.is_set()
