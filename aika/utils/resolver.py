import importlib
import typing


def from_path(path: str) -> typing.Any:
    """
    Return a resolved Python path.

    Parameters
    ----------
    path:
        A dot-delimited Python path to a symbol to resolve, for example ``aika.types.model.Model``.

    Returns
    -------
    Symbol found at ``path``.
    """

    (module_path, _, function_name) = path.rpartition('.')
    module_ = importlib.import_module(module_path)
    return getattr(module_, function_name)
