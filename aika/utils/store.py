import abc
import threading
import typing

T = typing.TypeVar('T')  # pylint: disable=invalid-name


# TODO: In the future we can implement this to be backed by a database (like MongoDB).
class Store(typing.Generic[T]):
    """
    A simple store for accessing a collection of values identified by IDs.
    """

    _index: typing.Dict[str, T]
    _is_populated: threading.Event
    # Lock should be obtained always when setting "_is_populated".
    # Especially if it is first tested and then set.
    _populating_lock: threading.Lock

    def __init__(self) -> None:
        self._index = {}
        self._is_populated = threading.Event()
        self._populating_lock = threading.Lock()

    def get(self, id_: str) -> T:
        """
        Retrieves the value with ID `id_`.

        Parameters
        ----------
        id_:
            ID of the value to retrieve.

        Returns
        -------
        A value stored.
        """

        return self._index[id_]

    def has(self, id_: str) -> bool:
        """
        Check if the value with ID `id_` is in the store.

        Parameters
        ----------
        id_:
            ID of the value to check.

        Returns
        -------
        ``True`` if the value with an ID ``id_`` is in the store.
        """

        return id_ in self._index

    def list(self) -> typing.Iterable[T]:
        """
        Lists all values in the store.

        Returns
        -------
        An iterator over all values stored.
        """

        return self._index.values()

    @abc.abstractmethod
    def populate(self) -> None:
        """
        Populates the store with values.
        """

    def is_populated(self, block: bool = False, timeout: float = None) -> bool:
        """
        Returns ``True`` if store has been populated.

        Parameters
        ----------
        block:
            If store has not been populated, block until it is?
        timeout:
            If ``block`` is ``True``, how long to wait before returning, in seconds?
        """

        if block:
            self._is_populated.wait(timeout)

        return self._is_populated.is_set()
