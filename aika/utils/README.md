Reusable (and potentially reusable) generic code. If something could be seen
as a potential public package for others to use in their applications, then this
is the place to put that code.

One guideline to understand what is generic code is that the code does import
from other `aika` modules.
