import copy
import logging
import types
import typing
from logging import config

FORMATTERS = {
    'worker': {
        'class': 'aika.utils.logging.Formatter',
        'format': '[%(levelname)s %(asctime)s %(hostname)s/%(process)s/%(task_name)s %(name)s] %(message)s',
    },
    'verbose': {
        'class': 'aika.utils.logging.Formatter',
        'format': '[%(levelname)s %(asctime)s %(name)s] %(message)s',
    },
    'simple': {
        'class': 'aika.utils.logging.Formatter',
        'format': '%(message)s',
    },
}


class LogRecord(logging.LogRecord):
    """
    A ``LogRecord`` class with ``getMessage`` method which reuses existing formatted
    message, if it exists, instead of formatting it every time it is called.

    We use this because we format the message before storing a ``LogRecord`` object
    into MongoDB and we do not want that after we read it from MongoDB the message is
    attempted to be formatted again, because it might lack necessary ``args`` attribute
    which has not been stored into MongoDB because values might not be compatible with MongoDB.

    Moreover, we allow ``exc_info`` to be a tuple of one element, a string, in which case we
    set it as ``exc_text``. This allows one to pass a pre-formatted traceback. It has to
    be a tuple and not just a string to bypass the logic in `logging` module itself which would
    otherwise modify ``exc_info`` before reaching the code here.
    """

    def __init__(
        self,
        name: str,
        level: int,
        pathname: str,
        lineno: int,
        msg: typing.Any,
        args: 'typing.Union[typing.Tuple[typing.Any, ...], typing.Mapping[str, typing.Any]]',
        exc_info: typing.Optional[typing.Union[typing.Tuple[type, BaseException, typing.Optional[types.TracebackType]], typing.Tuple[None, None, None], typing.Tuple[str]]],
        func: typing.Optional[str] = None,
        sinfo: typing.Optional[str] = None,
    ) -> None:
        if isinstance(exc_info, tuple) and len(exc_info) == 1 and isinstance(exc_info[0], str):
            exc_text: typing.Optional[str] = exc_info[0]
            exc_info = None
        else:
            exc_text = None

        exc_info = typing.cast(typing.Optional[typing.Union[typing.Tuple[type, BaseException, typing.Optional[types.TracebackType]], typing.Tuple[None, None, None]]], exc_info)

        super().__init__(name, level, pathname, lineno, msg, args, exc_info, func, sinfo)

        if exc_text is not None:
            self.exc_text = exc_text

    def getMessage(self) -> str:  # noqa: N802
        """
        Returns the message for this ``LogRecord`` object, but reuses existing
        formatted message, if it exists.

        Returns
        -------
        The message.
        """

        message = getattr(self, 'message', None)

        if message is not None:
            return message

        return super().getMessage()


class Formatter(logging.Formatter):
    """
    A logging formatter which allows for format string to reference nonexistent keys.
    They are replaced with empty strings.
    """

    def __init__(self, *args: typing.Any, **kwargs: typing.Any) -> None:
        super().__init__(*args, **kwargs)

        assert self._fmt is not None

        self._placeholders = get_placeholders(self._fmt)

    def formatMessage(self, record: logging.LogRecord) -> str:  # noqa: N802
        """
        Formats the message itself.

        Parameters
        ----------
        record:
            A record to format message for.

        Returns
        -------
        A formatted message.
        """

        values = copy.copy(record.__dict__)

        for placeholder in self._placeholders:
            if placeholder not in values:
                values[placeholder] = ''

        assert self._fmt is not None

        return self._fmt % values


def configure(stream: typing.TextIO = None, initial_logging_level: str = 'INFO') -> None:
    """
    Configure logging.

    Parameters
    ----------
    stream:
        Output stream.
    initial_logging_level:
        Level at which to show messages.
    """

    try:
        import pip.utils.logging  # pylint: disable=unused-import

        console_class = 'pip.utils.logging.ColorizedStreamHandler'
    except ModuleNotFoundError:
        import pip._internal.utils.logging  # pylint: disable=unused-import

        console_class = 'pip._internal.utils.logging.ColorizedStreamHandler'

    console: typing.Dict[str, typing.Any] = {
        'class': console_class,
        'formatter': 'verbose',
        'level': initial_logging_level,
    }

    if stream is not None:
        console['stream'] = stream

    config.dictConfig(
        {
            'version': 1,
            'disable_existing_loggers': False,
            'formatters': FORMATTERS,
            'handlers': {
                'console': console,
            },
            'root': {
                # We configure DEBUG level so that at the root logger we capture everything.
                # We limit levels through handlers.
                'level': 'DEBUG',
                'handlers': [
                    'console',
                ],
            },
        },
    )

    logging.captureWarnings(True)

    logging.setLogRecordFactory(LogRecord)


def get_console_handler() -> logging.Handler:
    """
    Returns Python logging handler instance named ``console``.

    Returns
    -------
    A Python logging handler instance.
    """

    for handler in logging.getLogger().handlers:
        if handler.name == 'console':
            return handler

    raise KeyError("Console handler not found.")


# Based on: https://stackoverflow.com/a/19048272/252025
def get_placeholders(string: str) -> typing.Sequence[str]:
    """
    Extract names of string interpolation placeholders in ``string``.

    Parameters
    ----------
    string:
        A string to extract placeholders for.

    Returns
    -------
    A list of placeholder names.
    """

    values: typing.Dict[str, int] = {}

    while True:
        try:
            string % values
        except KeyError as error:
            values[error.args[0]] = 0
        else:
            break

    return list(values.keys())
