import binascii


def hex_to_binary(hex_value: str) -> bytes:
    """
    Converts hex-encoded string to bytes.
    """

    return binascii.unhexlify(hex_value)


def binary_to_hex(value: bytes) -> str:
    """
    Converts bytes to hex-encoded string.
    """

    return binascii.hexlify(value).decode()
