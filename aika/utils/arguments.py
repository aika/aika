import argparse
import typing


class ArgumentParser(argparse.ArgumentParser):
    """
    A version of argument parser which supports lazy initialization.

    We use this parser for command parsers so that commands can populate
    their arguments lazily. For example, they might have to do expensive
    imports.
    """

    def __init__(self, *args: typing.Any, **kwargs: typing.Any) -> None:
        super().__init__(*args, **kwargs)

        self.lazy_init = None

    def parse_known_args(self, args: typing.Any = None, namespace: argparse.Namespace = None) -> typing.Any:
        """
        This method is called by the parent parser, so we use it to
        initialize what has been lazily postponed.
        """

        if self.lazy_init is not None:
            self.lazy_init()
            self.lazy_init = None

        return super().parse_known_args(args, namespace)

    def __getattribute__(self, name: str) -> typing.Any:
        # "argcomplete" calls "isinstance" on the parser instance before patching the instance.
        # By intercepting access to "__class__" we can make sure parser is initialized before
        # patching happens.
        if name == '__class__':
            if self.lazy_init is not None:
                self.lazy_init()
                self.lazy_init = None

        return super().__getattribute__(name)
