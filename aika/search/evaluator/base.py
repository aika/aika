import abc
import datetime
import logging
import typing

from d3m import container, utils as d3m_utils  # pylint: disable=unused-import
from d3m.metadata import problem as problem_module

from aika.pipelines import pipeline as pipeline_module

logger = logging.getLogger(__name__)


class EvaluatorBase(metaclass=d3m_utils.AbstractMetaclass):
    """
    Evaluator scores solutions.

    Evaluator instances should be picklable and should not keep/rely on the local state
    of previous solution evaluations because instances can be copied and reused in a
    non-deterministic manner.

    Parameters
    ----------
    strict_resolving:
        If resolved primitive does not fully match specified primitive reference, raise an exception?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    volumes_dir:
        Path to a directory with static files required by primitives.
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    pipelines_scored_dir:
        Where to store all successfully evaluated pipelines?
    subpipelines_dir:
        Where to store sub-pipelines? If not specified, a corresponding pipeline
        output directory is used for sub-pipelines as well.
    """

    def __init__(self, *, strict_resolving: bool, strict_digest: bool, volumes_dir: str = None, scratch_dir: str, pipelines_scored_dir: str = None, subpipelines_dir: str = None) -> None:
        self.strict_resolving = strict_resolving
        self.strict_digest = strict_digest
        self.volumes_dir = volumes_dir
        self.scratch_dir = scratch_dir
        self.pipelines_scored_dir = pipelines_scored_dir
        self.subpipelines_dir = subpipelines_dir

    @abc.abstractmethod
    def evaluate_solution(
        self,
        pipeline: pipeline_module.Pipeline,
        problem_description: problem_module.Problem,
        inputs: typing.Sequence[container.Dataset],
        *,
        hyperparameter_values: typing.Sequence = None,
        random_seed: int = 0,
        deadline: datetime.datetime = None,
    ) -> typing.Optional[typing.Tuple[float, typing.Sequence[typing.Dict]]]:
        """
        Evaluate a pipeline with hyper-parameter values.

        Returns ``None`` if evaluation has errored or deadline has reached.

        Parameters
        ----------
        pipeline:
            A pipeline to evaluate.
        problem_description:
            A problem description to use.
        inputs:
            A list of input values to the pipeline.
        hyperparameter_values:
            A list of dicts of hyper-parameter name to raw Python value mappings for each step in the pipeline.
            If not provided, default pipeline hyper-parameters are used.
        random_seed:
            Random seed to use.
        deadline:
            Deadline before which the evaluation should finish.

        Returns
        -------
        A tuple with: a normalized score for this solutions, and a list of
        descriptions of how scores were computed and their raw values.
        """
