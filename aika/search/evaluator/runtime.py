import datetime
import logging
import typing

from d3m import container, runtime as runtime_module
from d3m.metadata import base as metadata_base, problem as problem_module

from aika.automl import base as automl_base
from aika.pipelines import pipeline as pipeline_module, utils as pipelines_utils

from . import base

logger = logging.getLogger(__name__)


class RuntimeEvaluator(base.EvaluatorBase):
    """
    Evaluate a pipeline with hyper-parameters using a 0.75 train-test holdout and
    metrics from the problem description.
    """

    def evaluate_solution(
        self,
        pipeline: pipeline_module.Pipeline,
        problem_description: problem_module.Problem,
        inputs: typing.Sequence[container.Dataset],
        *,
        hyperparameter_values: typing.Sequence = None,
        random_seed: int = 0,
        # TODO: Respect the deadline.
        deadline: datetime.datetime = None,
    ) -> typing.Optional[typing.Tuple[float, typing.Sequence[typing.Dict]]]:  # noqa: D102
        # pylint: disable=too-many-locals

        logger.info(
            "Preparing for evaluation of the pipeline '%(pipeline_id)s' and hyper-parameters.",
            {
                'pipeline_id': pipeline.id,
            },
        )

        try:
            # We see the following pipelines as part of the code of this task, just stored in a pipeline form.
            # So we do not respect custom pipeline search paths to get these pipelines to resolve to something else.

            pipeline_search_paths = pipelines_utils.get_internal_pipeline_search_paths()

            data_pipeline = pipelines_utils.get_pipeline(
                'train-test-tabular-split',
                strict_resolving=self.strict_resolving,
                strict_digest=self.strict_digest,
                pipeline_search_paths=pipeline_search_paths,
                respect_environment_variable=False,
            )
            assert data_pipeline is not None

            data_params = {
                # Values have to be JSON-serialized.
                'train_score_ratio': '0.75',
                'shuffle': 'true',
            }

            scoring_pipeline = pipelines_utils.get_pipeline(
                runtime_module.DEFAULT_SCORING_PIPELINE_PATH,
                strict_resolving=self.strict_resolving,
                strict_digest=self.strict_digest,
                allow_pipeline_path=True,
                respect_environment_variable=False,
            )
            assert scoring_pipeline is not None

            metrics = runtime_module.get_metrics_from_problem_description(problem_description)

            logger.info(
                "Evaluating the pipeline '%(pipeline_id)s' and hyper-parameters.",
                {
                    'pipeline_id': pipeline.id,
                },
            )

            # TODO: Correctly set context.
            scores_list, results_list = runtime_module.evaluate(
                pipeline,
                inputs,
                data_pipeline=data_pipeline,
                scoring_pipeline=scoring_pipeline,
                problem_description=problem_description,
                data_params=data_params,
                metrics=metrics,
                hyperparams=hyperparameter_values,
                random_seed=random_seed,
                volumes_dir=self.volumes_dir,
                scratch_dir=self.scratch_dir,
                context=metadata_base.Context.TESTING,
                runtime_environment=pipelines_utils.get_runtime_environment(),
            )

            if results_list.has_error():
                logger.info(
                    "Evaluation of the pipeline '%(pipeline_id)s' and hyper-parameters finished with error.",
                    {
                        'pipeline_id': pipeline.id,
                    },
                )
            else:
                logger.info(
                    "Evaluation of the pipeline '%(pipeline_id)s' and hyper-parameters finished without error.",
                    {
                        'pipeline_id': pipeline.id,
                    },
                )

            # TODO: Use "results_list" to save pipeline runs.
            results_list.check_success()

            assert scores_list
            scores = runtime_module.combine_folds(scores_list)

            normalized_score = pipelines_utils.compute_normalized_score(scores)

            output_scores = automl_base.construct_scores(scores, metrics)

            scores_description = [
                {
                    'configuration': {
                        'method': automl_base.EvaluationMethod.HOLDOUT,
                        'folds': 1,
                        'train_test_ratio': 0.75,
                        'shuffle': True,
                        'random_seed': random_seed,
                        'stratified': False,
                    },
                    'scores': output_scores,
                },
            ]

            logger.info(
                "Pipeline '%(pipeline_id)s' evaluated with normalized score: %(normalized_score)s",
                {
                    'pipeline_id': pipeline.id,
                    'normalized_score': normalized_score,
                },
            )

            return normalized_score, scores_description

        except Exception:  # pylint: disable=broad-except
            logger.exception(
                "Pipeline '%(pipeline_id)s' evaluation errored.",
                {
                    'pipeline_id': pipeline.id,
                },
            )

            return None
