import abc
import typing

from d3m import container, utils as d3m_utils  # pylint: disable=unused-import
from d3m.metadata import problem as problem_module

from aika.pipelines import pipeline as pipeline_module


class PipelineRewriterBase(metaclass=d3m_utils.AbstractMetaclass):
    """
    Pipeline rewriter rewrites a given pipeline. For example, to rewrite its primitive
    hyper-parameters configuration.

    Parameters
    ----------
    problem_description:
        A problem description to use.
    datasets:
        A sequence of datasets to use.
    """

    def __init__(self, problem_description: problem_module.Problem, datasets: typing.Sequence[container.Dataset]) -> None:
        self.problem_description = problem_description
        self.datasets = datasets

    @abc.abstractmethod
    def rewrite(self, pipeline: pipeline_module.Pipeline) -> typing.Tuple[pipeline_module.Pipeline, typing.Sequence]:
        """
        Returns a rewritten pipeline and free hyper-parameters for the pipeline.

        Input pipeline is not modified.

        Parameters
        ----------
        pipeline:
            A pipeline to rewrite.

        Returns
        -------
        A rewritten pipeline and a hyper-parameters configuration for free hyper-parameters.
        """
