import collections
import copy
import typing

import frozendict

from d3m.metadata import hyperparams as hyperparams_module

from aika.pipelines import pipeline as pipeline_module
from aika.utils import exceptions

from . import base

BOUNDED_HYPERPARAMS = (hyperparams_module.Bounded, hyperparams_module.UniformInt, hyperparams_module.Uniform, hyperparams_module.LogUniform)

N_ESTIMATORS_DEFAULT = 100
N_ESTIMATORS_UPPER = 100000
MIN_IMPURITY_DECREASE_UPPER = 0.05


class RulesBasedRewriter(base.PipelineRewriterBase):
    """
    Rewrites pipeline's hyper-parameters based on fixed rules.
    """

    def _rewrite_hyperparams(self, hyperparams: typing.Sequence) -> typing.Sequence:
        # pylint: disable=protected-access

        assert isinstance(hyperparams, typing.Sequence)

        # Make mutable.
        hyperparams = list(hyperparams)

        for step_index, hyperparams_for_step in enumerate(hyperparams):
            if isinstance(hyperparams_for_step, (dict, frozendict.frozendict)):
                # Make mutable.
                hyperparams_for_step = collections.OrderedDict(hyperparams_for_step)

                # We iterate over a list so that we can change dict while iterating.
                for hyperparameter_name, hyperparameter in list(hyperparams_for_step.items()):
                    hyperparameter = copy.copy(hyperparameter)

                    if hyperparameter_name == 'n_estimators':
                        if isinstance(hyperparameter, BOUNDED_HYPERPARAMS) and hyperparameter.upper is None:
                            hyperparameter.upper = N_ESTIMATORS_UPPER
                        if hyperparameter._default == 10:
                            hyperparameter._default = N_ESTIMATORS_DEFAULT

                    elif hyperparameter_name == 'min_impurity_decrease':
                        if isinstance(hyperparameter, BOUNDED_HYPERPARAMS) and hyperparameter.upper is None:
                            hyperparameter.upper = MIN_IMPURITY_DECREASE_UPPER  # pylint: disable=redefined-variable-type

                    hyperparams_for_step[hyperparameter_name] = hyperparameter

                hyperparams[step_index] = hyperparams_for_step

            elif isinstance(hyperparams_for_step, typing.Sequence):
                hyperparams[step_index] = self._rewrite_hyperparams(hyperparams_for_step)

            else:
                raise exceptions.UnexpectedValueError("Unknown hyper-parameters type: {hyperparams_type}".format(hyperparams_type=type(hyperparams_for_step)))

        return hyperparams

    def rewrite(self, pipeline: pipeline_module.Pipeline) -> typing.Tuple[pipeline_module.Pipeline, typing.Sequence]:  # noqa: D102
        free_hyperparams = pipeline.get_free_hyperparams()

        return pipeline, self._rewrite_hyperparams(free_hyperparams)
