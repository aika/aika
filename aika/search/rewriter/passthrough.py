import typing

from aika.pipelines import pipeline as pipeline_module

from . import base


class PassthroughPipelineRewriter(base.PipelineRewriterBase):
    """
    A pipeline rewriter that just returns the provided pipeline and its free hyper-parameters.
    """

    def rewrite(self, pipeline: pipeline_module.Pipeline) -> typing.Tuple[pipeline_module.Pipeline, typing.Sequence]:  # noqa: D102
        return pipeline, pipeline.get_free_hyperparams()
