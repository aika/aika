import datetime
import typing
import uuid

import frozendict

from d3m import container
from d3m.metadata import problem as problem_module

from aika.pipelines import pipeline as pipeline_module
from aika.utils import exceptions

from . import base


class DefaultValuesPipelineTuner(base.PipelineTunerBase):
    """
    A pipeline tuner which just returns default values for free hyper-parameters.

    This tuner ignores the ``deadline`` argument.
    """

    _pending_configurations: typing.Dict[str, typing.Sequence]

    def __init__(
        self,
        problem_description: problem_module.Problem,
        datasets: typing.Sequence[container.Dataset],
        *,
        random_seed: int,
        strict_resolving: bool,
        strict_digest: bool,
        volumes_dir: str = None,
        scratch_dir: str,
        deadline: datetime.datetime = None,
    ) -> None:
        super().__init__(
            problem_description,
            datasets,
            random_seed=random_seed,
            strict_resolving=strict_resolving,
            strict_digest=strict_digest,
            volumes_dir=volumes_dir,
            scratch_dir=scratch_dir,
            deadline=deadline,
        )

        self._pending_configurations = {}

    # We ignore "pipeline" argument.
    def add_hyperparams(self, pipeline: pipeline_module.Pipeline, hyperparams: typing.Sequence) -> str:  # noqa: D102
        tuning_id = str(uuid.uuid4())

        self._pending_configurations[tuning_id] = hyperparams

        return tuning_id

    @classmethod
    def get_default_values(cls, hyperparams: typing.Sequence) -> typing.Sequence:
        """
        Generates a list of default hyper-parameter values for all provided hyper-parameters.

        Parameters
        ----------
        hyperparams:
            A hyper-parameters configuration.

        Returns
        -------
        A list of dicts of hyper-parameter name to raw Python value mappings for all provided hyper-parameters.
        """

        hyperparameter_values = []

        for hyperparams_for_step in hyperparams:
            if isinstance(hyperparams_for_step, (dict, frozendict.frozendict)):
                values = {}
                for name, hyperparameter in hyperparams_for_step.items():
                    values[name] = hyperparameter.get_default()
                hyperparameter_values.append(values)
            elif isinstance(hyperparams_for_step, typing.Sequence):
                hyperparameter_values.append(cls.get_default_values(hyperparams_for_step))
            else:
                raise exceptions.UnexpectedValueError("Unknown hyper-parameters type: {hyperparams_type}".format(hyperparams_type=type(hyperparams_for_step)))

        return hyperparameter_values

    def get_next_values(self) -> typing.Optional[typing.Sequence[typing.Tuple[str, str, typing.Sequence]]]:  # noqa: D102
        if self._pending_configurations:
            tunind_id, hyperparams = self._pending_configurations.popitem()

            return [(str(uuid.uuid4()), tunind_id, self.get_default_values(hyperparams))]

        return None

    def on_values_result(self, values_id: str, score: typing.Optional[float]) -> None:  # noqa: D102
        pass
