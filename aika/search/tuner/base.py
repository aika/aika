import abc
import datetime
import typing

from d3m import container, utils as d3m_utils  # pylint: disable=unused-import
from d3m.metadata import problem as problem_module

from aika.pipelines import pipeline as pipeline_module


class PipelineTunerBase(metaclass=d3m_utils.AbstractMetaclass):
    """
    Pipeline tuner tunes a pipeline and a configuration of free hyper-parameters
    for best performance for metrics listed in the problem description.

    Provided free hyper-parameters must match pipeline's free hyper-parameters, but
    their configuration might have been modified to use the tuner to search inside
    a modified hyper-parameters configuration.

    It can tune multiple pipelines and configurations at the same time to
    potentially tune across them.

    Found hyper-parameter values might not even combine with the pipeline
    into a runnable pipeline.

    Parameters
    ----------
    problem_description:
        A problem description to use.
    datasets:
        A sequence of datasets to use.
    random_seed:
        Random seed to use.
    strict_resolving:
        If resolved pipeline or primitive does not fully match specified reference, raise an exception?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    volumes_dir:
        Path to a directory with static files required by primitives.
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    deadline:
        Deadline before which the whole tuning should finish.
    """

    def __init__(
        self,
        problem_description: problem_module.Problem,
        datasets: typing.Sequence[container.Dataset],
        *,
        random_seed: int,
        strict_resolving: bool,
        strict_digest: bool,
        volumes_dir: str = None,
        scratch_dir: str,
        deadline: datetime.datetime = None,
    ) -> None:
        self.problem_description = problem_description
        self.datasets = datasets
        self.random_seed = random_seed
        self.strict_resolving = strict_resolving
        self.strict_digest = strict_digest
        self.volumes_dir = volumes_dir
        self.scratch_dir = scratch_dir
        self.deadline = deadline

    @abc.abstractmethod
    def add_hyperparams(self, pipeline: pipeline_module.Pipeline, hyperparams: typing.Sequence) -> str:
        """
        Add a new pipeline or at least a new configuration of hyper-parameters for the pipeline.

        Can be called multiple times with the same pipeline but different hyper-parameter configurations.

        Parameters
        ----------
        pipeline:
            A pipeline to tune free hyper-parameters for.
        hyperparams:
            A hyper-parameters configuration for free hyper-parameters of the pipeline.

        Returns
        -------
        An arbitrary tuning ID corresponding to this pipeline and configuration of hyper-parameters
        combination.
        """

    @abc.abstractmethod
    def get_next_values(self) -> typing.Optional[typing.Sequence[typing.Tuple[str, str, typing.Sequence]]]:
        """
        Searchers for the next hyper-parameter values for previously added
        pipelines and configuration of hyper-parameters.

        Returns ``None`` if no more hyper-parameter values can be found
        for any of currently added pipeline and configuration of hyper-parameters
        combinations. If returns an empty sequence if for now no more hyper-parameter
        values can be found, because the tuner is waiting for results to come in.

        Returns
        -------
        A sequence of tuples with: an arbitrary values ID, a tuning ID,
        and hyper-parameter values. Hyper-parameter values are represented as
        a list of dicts of hyper-parameter name to raw Python value mappings
        for each step in the pipeline.
        """

    @abc.abstractmethod
    def on_values_result(self, values_id: str, score: typing.Optional[float]) -> None:
        """
        Called when hyper-parameter values with ID ``values_id`` have been scored,
        or when scoring has failed.

        Can be called multiple times for same ``values_id`` if scoring has been done multiple times
        (e.g., scoring could be done for multiple combination of folds of datasets).

        Parameters
        ----------
        values_id:
            An ID of hyper-parameter values as returned from `get_next_values`.
        score:
            A number between 0 and 1, inclusive, where 1 is better. ``None`` if scoring has errored.
        """
