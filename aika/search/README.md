## Solution search

Solution search consists of the following three stages:

* Pipeline generation.
* Pipeline rewriting.
* Hyper-parameters tuning.

We have base classes for all these three stages which define interfaces
for them. Moreover, there is a base class for the solution search
itself. The default solution search runs these three stages in order,
but other solution search approaches can do some or all of them as
part of the overall search.

## Runners

Runners evaluate candidate solutions (pipelines and hyper-parameters values)
and provide results back to solution searcher. They use evaluators to
evaluate them.
