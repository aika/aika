import datetime
import glob
import logging
import os.path
import typing
import uuid

import numpy

from d3m import container
from d3m.metadata import problem as problem_module

from aika.pipelines import pipeline as pipeline_module, utils as pipelines_utils
from aika.utils import exceptions

from . import base
from .. import utils as search_utils

PIPELINES_BASE_DIRECTORY = os.path.join(os.path.dirname(__file__), '..', '..', 'contrib', 'pipelines', 'primitives')
NAMESPACE = uuid.UUID('fe4008b9-6b09-4edf-bdc2-1fa6e6295379')

logger = logging.getLogger(__name__)


class PrimitivePipelinesPipelineGenerator(base.PipelineGeneratorBase):
    """
    A pipeline generator which uses primitive pipelines.
    """

    def __init__(
        self,
        problem_description: problem_module.Problem,
        datasets: typing.Sequence[container.Dataset],
        *,
        random_seed: int,
        strict_resolving: bool,
        strict_digest: bool,
        volumes_dir: str = None,
        scratch_dir: str,
        template: pipeline_module.Pipeline = None,
        deadline: datetime.datetime = None,
    ) -> None:
        super().__init__(
            problem_description,
            datasets,
            random_seed=random_seed,
            strict_resolving=strict_resolving,
            strict_digest=strict_digest,
            volumes_dir=volumes_dir,
            scratch_dir=scratch_dir,
            template=template,
            deadline=deadline,
        )

        self._populate_pipelines()

    def _populate_pipelines(self) -> None:
        self._pipelines = []

        pipeline_paths = []
        pipeline_paths.extend(glob.glob('{base}/primitives/*/*/*/pipelines/*.json'.format(base=PIPELINES_BASE_DIRECTORY)))
        pipeline_paths.extend(glob.glob('{base}/primitives/*/*/*/pipelines/*.yml'.format(base=PIPELINES_BASE_DIRECTORY)))
        pipeline_paths.extend(glob.glob('{base}/primitives/*/*/*/pipelines/*.yaml'.format(base=PIPELINES_BASE_DIRECTORY)))

        # Sorting to assure reproducible traversal.
        pipeline_paths.sort()

        for pipeline_path in pipeline_paths:
            # Has time to generate pipelines expired?
            if self.deadline is not None and datetime.datetime.now(datetime.timezone.utc) > self.deadline:
                return

            try:
                pipeline = pipelines_utils.get_pipeline(pipeline_path, allow_pipeline_path=True, strict_resolving=self.strict_resolving, strict_digest=self.strict_digest)
            except Exception:  # pylint: disable=broad-except
                logger.exception(
                    "Cannot load pipeline %(pipeline_path)s.",
                    {
                        'pipeline_path': pipeline_path,
                    },
                )
                continue

            self._pipelines.append(pipeline)

        random_state = numpy.random.RandomState(self.random_seed)

        # We shuffle based on the random seed.
        random_state.shuffle(self._pipelines)

    def get_next_pipelines(self) -> typing.Optional[typing.Sequence[pipeline_module.Pipeline]]:  # noqa: D102
        # Has time to generate pipelines expired?
        if self.deadline is not None and datetime.datetime.now(datetime.timezone.utc) > self.deadline:
            return None

        if not self._pipelines:
            return None

        pipeline = self._pipelines.pop()

        assert pipeline is not None

        # We want new pipeline ID to be the same for the same pipeline.
        pipeline.id = str(uuid.uuid5(NAMESPACE, pipeline.id))

        # Just to make names of inputs and outputs more uniform and not depending on the pipeline.
        if len(pipeline.inputs) == 1:
            pipeline.inputs[0]['name'] = 'dataset'
        if len(pipeline.outputs) == 1:
            pipeline.outputs[0]['name'] = 'predictions'

        if self.template:
            pipeline = search_utils.merge_template(self.template, pipeline)

        # We use the same pipeline ID for the same pipeline, but we are changing the
        # timestamp here every time. Not sure if this is OK.
        self._update_pipeline_metadata(pipeline, datetime.datetime.now(datetime.timezone.utc))

        return [pipeline]

    def _update_pipeline_metadata(self, pipeline: pipeline_module.Pipeline, timestamp: datetime.datetime = None) -> None:
        if timestamp is not None:
            pipeline.created = timestamp
        pipeline.name = None
        pipeline.description = None

        if pipeline.source is not None:
            # We iterate over a list so that we can change dict while iterating.
            for source_key in list(pipeline.source.keys()):
                # We remove pipeline author name and other source metadata,
                # but we keep the metadata about source pipeline.
                if source_key == 'from':
                    continue
                del pipeline.source[source_key]

            if not pipeline.source:
                pipeline.source = None

        for step in pipeline.steps:
            if isinstance(step, pipeline_module.SubpipelineStep):
                if step.pipeline is None:
                    raise exceptions.InvalidStateError("Pipeline has not been resolved.")

                self._update_pipeline_metadata(typing.cast(pipeline_module.Pipeline, step.pipeline))

    def on_pipeline_result(self, pipeline_id: str, score: typing.Optional[float]) -> None:  # noqa: D102
        pass
