import copy
import datetime
import logging
import typing

import dateutil.parser

from d3m import container
from d3m.metadata import problem as problem_module

from aika.pipelines import pipeline as pipeline_module
from aika.search.generator import base as generator_base
from aika.utils import exceptions, resolver as resolver_utils

from . import base

logger = logging.getLogger(__name__)


class ChainGeneratorsPipelineGenerator(base.PipelineGeneratorBase):
    """
    A pipeline generator which chains generators listed in ``generators``
    argument and uses them to generate pipelines, in order they are listed.

    This generator ignores the ``deadline`` argument and leave to generators in
    ``generators`` to handle it.
    """

    _generators_classes: typing.Sequence[typing.Type[base.PipelineGeneratorBase]]
    _generators_arguments: typing.Sequence[typing.Dict[str, typing.Any]]
    _generators: typing.Sequence[base.PipelineGeneratorBase]
    _pipeline_to_generator: typing.Dict[str, base.PipelineGeneratorBase]

    def __init__(
        self,
        problem_description: problem_module.Problem,
        datasets: typing.Sequence[container.Dataset],
        *,
        random_seed: int,
        strict_resolving: bool,
        strict_digest: bool,
        volumes_dir: str = None,
        scratch_dir: str,
        template: pipeline_module.Pipeline = None,
        deadline: datetime.datetime = None,
        generators: typing.Sequence[str] = (
            'aika.search.generator.templates.TemplatesBasedPipelineGenerator',
            'aika.search.generator.primitives.PrimitivePipelinesPipelineGenerator',
        ),
        generators_arguments: typing.Sequence[typing.Dict[str, typing.Any]] = None,
    ) -> None:
        super().__init__(
            problem_description,
            datasets,
            random_seed=random_seed,
            strict_resolving=strict_resolving,
            strict_digest=strict_digest,
            volumes_dir=volumes_dir,
            scratch_dir=scratch_dir,
            template=template,
            deadline=deadline,
        )

        if generators_arguments is None:
            generators_arguments = [{}] * len(generators)
        else:
            # Make mutable.
            generators_arguments = list(generators_arguments)

        if len(generators) != len(generators_arguments):
            raise exceptions.InvalidArgumentValueError("'generators' and 'generators_arguments' arguments do not match in length.")

        for i, generator_arguments in enumerate(generators_arguments):
            generator_arguments = copy.copy(generator_arguments)

            # We pass "deadline" through kwargs and not directly as other arguments
            # to allow one to configure deadlines differently for different generators.
            # Arguments are JSON encoded and while we decode them, timestamps will
            # stay strings. So we have to parse them here.
            if 'deadline' in generator_arguments:
                generator_arguments['deadline'] = dateutil.parser.parse(generator_arguments['deadline'], fuzzy=True)
            else:
                generator_arguments['deadline'] = self.deadline

            generators_arguments[i] = generator_arguments

        self._generators_classes = [typing.cast(typing.Type[generator_base.PipelineGeneratorBase], resolver_utils.from_path(generator)) for generator in generators]
        self._generators_arguments = generators_arguments

        self._generators = [
            generator(
                self.problem_description,
                self.datasets,
                random_seed=self.random_seed,
                strict_resolving=self.strict_resolving,
                strict_digest=self.strict_digest,
                volumes_dir=self.volumes_dir,
                scratch_dir=self.scratch_dir,
                template=self.template,
                **generator_arguments,
            )
            for generator, generator_arguments in zip(self._generators_classes, self._generators_arguments)
        ]

        self._pipeline_to_generator = {}

    def get_next_pipelines(self) -> typing.Optional[typing.Sequence[pipeline_module.Pipeline]]:  # noqa: D102
        pipelines = None

        for generator in self._generators:
            generated_pipelines = generator.get_next_pipelines()

            if generated_pipelines is None:
                continue

            if pipelines is None:
                pipelines = []

            for pipeline in generated_pipelines:
                if pipeline.id in self._pipeline_to_generator:
                    logger.warning("Duplicate pipeline generated: %(pipeline_id)s", {'pipeline_id': pipeline.id})
                    continue

                self._pipeline_to_generator[pipeline.id] = generator
                pipelines.append(pipeline)

        return pipelines

    def on_pipeline_result(self, pipeline_id: str, score: typing.Optional[float]) -> None:  # noqa: D102
        if pipeline_id not in self._pipeline_to_generator:
            logger.warning("Result for unknown pipeline: %(pipeline_id)s", {'pipeline_id': pipeline_id})
            return

        self._pipeline_to_generator[pipeline_id].on_pipeline_result(pipeline_id, score)
