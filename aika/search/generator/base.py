import abc
import datetime
import logging
import typing

from d3m import container, utils as d3m_utils  # pylint: disable=unused-import
from d3m.metadata import problem as problem_module

from aika.pipelines import pipeline as pipeline_module

logger = logging.getLogger(__name__)


class PipelineGeneratorBase(metaclass=d3m_utils.AbstractMetaclass):
    """
    Pipeline generator takes a problem description and a set of datasets and
    returns potential pipelines to solve the problem given the datasets.

    Found pipelines might not even run for given datasets.

    Parameters
    ----------
    problem_description:
        A problem description to use.
    datasets:
        A sequence of datasets to use.
    random_seed:
        Random seed to use.
    strict_resolving:
        If resolved primitive does not fully match specified primitive reference, raise an exception?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    volumes_dir:
        Path to a directory with static files required by primitives.
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    template:
        A pipeline template instance.
    deadline:
        Deadline before which generation of all pipelines should finish.
    """

    def __init__(
        self,
        problem_description: problem_module.Problem,
        datasets: typing.Sequence[container.Dataset],
        *,
        random_seed: int,
        strict_resolving: bool,
        strict_digest: bool,
        volumes_dir: str = None,
        scratch_dir: str,
        template: pipeline_module.Pipeline = None,
        deadline: datetime.datetime = None,
    ) -> None:
        self.problem_description = problem_description
        self.datasets = datasets
        self.random_seed = random_seed
        self.strict_resolving = strict_resolving
        self.strict_digest = strict_digest
        self.volumes_dir = volumes_dir
        self.scratch_dir = scratch_dir
        self.template = template
        self.deadline = deadline

    @abc.abstractmethod
    def get_next_pipelines(self) -> typing.Optional[typing.Sequence[pipeline_module.Pipeline]]:
        """
        Searches for next pipelines to solve the problem given datasets.

        Returns ``None`` if no more pipelines can be found.
        If returns an empty sequence if for now no more pipelines can be found,
        because the generator is waiting for results to come in.

        Returns
        -------
        A list of pipeline descriptions.
        """

    @abc.abstractmethod
    def on_pipeline_result(self, pipeline_id: str, score: typing.Optional[float]) -> None:
        """
        Called when a pipeline with ID ``pipeline_id`` has been scored, or when scoring has failed.

        Can be called multiple times for same ``pipeline_id`` if scoring has been done multiple times
        (e.g., scoring could be done for multiple sets of hyper-parameter values).

        Note: the pipeline which has been scored might have been rewritten before being
        scored and is different from what has been generated.

        Parameters
        ----------
        pipeline_id:
            A pipeline ID.
        score:
            A number between 0 and 1, inclusive, where 1 is better. ``None`` if scoring has errored.
        """
