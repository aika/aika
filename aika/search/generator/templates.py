import datetime
import logging
import os
import os.path
import typing
import uuid

import numpy

from d3m import container
from d3m.metadata import problem as problem_module

from aika.pipelines import pipeline as pipeline_module, utils as pipelines_utils
from aika.utils import exceptions

from . import base
from .. import utils as search_utils

NAMESPACE = uuid.UUID('1a5ad9e2-fbdb-4c9c-85ec-602ed9e4def1')

logger = logging.getLogger(__name__)


class TemplatesBasedPipelineGenerator(base.PipelineGeneratorBase):
    """
    A pipeline generator which uses pipeline templates.
    """

    def __init__(
        self,
        problem_description: problem_module.Problem,
        datasets: typing.Sequence[container.Dataset],
        *,
        random_seed: int,
        strict_resolving: bool,
        strict_digest: bool,
        volumes_dir: str = None,
        scratch_dir: str,
        template: pipeline_module.Pipeline = None,
        deadline: datetime.datetime = None,
    ) -> None:
        super().__init__(
            problem_description,
            datasets,
            random_seed=random_seed,
            strict_resolving=strict_resolving,
            strict_digest=strict_digest,
            volumes_dir=volumes_dir,
            scratch_dir=scratch_dir,
            template=template,
            deadline=deadline,
        )

        self._task_keywords = set(self.problem_description['problem']['task_keywords'])
        self._populate_templates()

    def _populate_templates(self) -> None:
        self._templates = []

        for dirpath, dirnames, filenames in os.walk(pipeline_module.TEMPLATES_DIRECTORY, followlinks=True):  # pylint: disable=unused-variable
            # Sorting to assure reproducible traversal.
            dirnames.sort()

            # Sorting to assure reproducible traversal.
            for filename in sorted(filenames):
                # Has time to generate pipelines expired?
                if self.deadline is not None and datetime.datetime.now(datetime.timezone.utc) > self.deadline:
                    return

                template_path = os.path.join(dirpath, filename)

                try:
                    if template_path.endswith('.yaml') or template_path.endswith('.yml') or template_path.endswith('.json'):
                        template = pipelines_utils.get_pipeline(template_path, allow_pipeline_path=True, strict_resolving=self.strict_resolving, strict_digest=self.strict_digest)
                        assert template is not None
                    else:
                        continue
                except Exception:  # pylint: disable=broad-except
                    logger.exception(
                        "Cannot load pipeline template %(template_path)s.",
                        {
                            'template_path': template_path,
                        },
                    )
                    continue

                # "aika" is our extension to the pipeline object.
                if self._task_keywords & set(template.aika.get('task_keywords', [])):  # pylint: disable=no-member
                    self._templates.append(template)

        random_state = numpy.random.RandomState(self.random_seed)

        # We shuffle based on the random seed.
        random_state.shuffle(self._templates)

    def get_next_pipelines(self) -> typing.Optional[typing.Sequence[pipeline_module.Pipeline]]:  # noqa: D102
        # Has time to generate pipelines expired?
        if self.deadline is not None and datetime.datetime.now(datetime.timezone.utc) > self.deadline:
            return None

        if not self._templates:
            return None

        pipeline = self._templates.pop()

        if pipeline.has_placeholder():
            # TODO: Once implemented, resulting pipeline ID should be based on how placeholders were fulfilled.
            #       So that for same fulfillment of placeholders, the same pipeline ID should be generated.
            raise NotImplementedError("Templates with placeholders are not yet supported.")

        template_id = pipeline.id
        # We want new pipeline ID to be the same for the same pipeline.
        pipeline.id = str(uuid.uuid5(NAMESPACE, template_id))

        # Just to make names of inputs and outputs more uniform and not depending on the template.
        if len(pipeline.inputs) == 1:
            pipeline.inputs[0]['name'] = 'dataset'
        if len(pipeline.outputs) == 1:
            pipeline.outputs[0]['name'] = 'predictions'

        if self.template:
            pipeline = search_utils.merge_template(self.template, pipeline)

        # TODO: We are generating the same pipeline ID for the same pipeline, but we are changing the
        #       timestamp here every time. Not sure if this is OK.
        self._update_pipeline_metadata(pipeline, datetime.datetime.now(datetime.timezone.utc))

        return [pipeline]

    def _update_pipeline_metadata(self, pipeline: pipeline_module.Pipeline, timestamp: datetime.datetime = None) -> None:
        if timestamp is not None:
            pipeline.created = timestamp
        pipeline.name = None
        pipeline.description = None
        pipeline.source = None
        pipeline.aika = {}

        for step in pipeline.steps:
            if isinstance(step, pipeline_module.SubpipelineStep):
                if step.pipeline is None:
                    raise exceptions.InvalidStateError("Pipeline has not been resolved.")

                self._update_pipeline_metadata(typing.cast(pipeline_module.Pipeline, step.pipeline))

    def on_pipeline_result(self, pipeline_id: str, score: typing.Optional[float]) -> None:  # noqa: D102
        pass
