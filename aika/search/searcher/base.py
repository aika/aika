import abc
import datetime
import typing

from d3m import container, utils as d3m_utils  # pylint: disable=unused-import
from d3m.metadata import problem as problem_module

from aika.pipelines import pipeline as pipeline_module


class SolutionSearcherBase(metaclass=d3m_utils.AbstractMetaclass):
    """
    Solution searcher takes a problem description and a set of datasets and
    returns potential solutions to solve the problem given the datasets.

    Solutions are pipelines and values for their free hyper-parameters.

    Found solutions might not even run for a particular dataset.

    Parameters
    ----------
    problem_description:
        A problem description to use.
    datasets:
        A sequence of datasets to use.
    random_seed:
        Random seed to use.
    strict_resolving:
        If resolved pipeline or primitive does not fully match specified reference, raise an exception?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    volumes_dir:
        Path to a directory with static files required by primitives.
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    template:
        A pipeline template instance.
    deadline:
        Deadline before which the whole search should finish.
    """

    def __init__(
        self,
        problem_description: problem_module.Problem,
        datasets: typing.Sequence[container.Dataset],
        *,
        random_seed: int,
        strict_resolving: bool,
        strict_digest: bool,
        volumes_dir: str = None,
        scratch_dir: str,
        template: pipeline_module.Pipeline = None,
        deadline: datetime.datetime = None,
    ) -> None:
        self.problem_description = problem_description
        self.datasets = datasets
        self.random_seed = random_seed
        self.strict_resolving = strict_resolving
        self.strict_digest = strict_digest
        self.volumes_dir = volumes_dir
        self.scratch_dir = scratch_dir
        self.template = template
        self.deadline = deadline

    @abc.abstractmethod
    def get_next_solutions(self) -> typing.Optional[typing.Sequence[typing.Tuple[str, pipeline_module.Pipeline, typing.Sequence]]]:
        """
        Searches for next solutions to solve the problem given datasets.

        Returns ``None`` if no more solutions can be found.
        If returns an empty sequence if for now no more solutions can be found,
        because the searcher is waiting for results to come in.

        Returns
        -------
        A sequence of tuples with: an arbitrary solution ID, a pipeline
        description, and hyper-parameter values. Hyper-parameter values are
        represented as a list of dicts of hyper-parameter name to raw Python
        value mappings for each step in the pipeline.
        """

    @abc.abstractmethod
    def on_solution_result(self, solution_id: str, score: typing.Optional[float]) -> None:
        """
        Called when a solution with ID ``solution_id`` has been scored,
        or when scoring has failed.

        Can be called multiple times for same ``solution_id`` if scoring has been done multiple times
        (e.g., scoring could be done for multiple combination of folds of datasets).

        Parameters
        ----------
        solution_id:
            A solution ID as returned from `get_next_solutions`.
        score:
            A number between 0 and 1, inclusive, where 1 is better. ``None`` if scoring has errored.
        """
