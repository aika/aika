import copy
import datetime
import logging
import typing

import dateutil.parser

from d3m import container
from d3m.metadata import problem as problem_module

from aika.pipelines import pipeline as pipeline_module
from aika.search.generator import base as generator_base
from aika.search.rewriter import base as rewriter_base
from aika.search.tuner import base as tuner_base
from aika.utils import resolver as resolver_utils

from . import base

logger = logging.getLogger(__name__)


class ExhaustiveSolutionSearcher(base.SolutionSearcherBase):
    """
    A solution (pipeline + hyper-parameter values) searcher which uses, by default, a `ChainGeneratorsPipelineGenerator`
    to find pipelines and then tunes them using `DefaultValuesPipelineTuner`.
    """

    _pipelines: typing.Dict[str, pipeline_module.Pipeline]
    _tuning_to_pipelines: typing.Dict[str, pipeline_module.Pipeline]
    _values_to_pipelines: typing.Dict[str, pipeline_module.Pipeline]

    def __init__(
        self,
        problem_description: problem_module.Problem,
        datasets: typing.Sequence[container.Dataset],
        *,
        random_seed: int,
        strict_resolving: bool,
        strict_digest: bool,
        volumes_dir: str = None,
        scratch_dir: str,
        template: pipeline_module.Pipeline = None,
        deadline: datetime.datetime = None,
        generator_class: str = 'aika.search.generator.chain.ChainGeneratorsPipelineGenerator',
        generator_arguments: typing.Dict[str, typing.Any] = None,
        rewriter_class: str = 'aika.search.rewriter.passthrough.PassthroughPipelineRewriter',
        rewriter_arguments: typing.Dict[str, typing.Any] = None,
        tuner_class: str = 'aika.search.tuner.default.DefaultValuesPipelineTuner',
        tuner_arguments: typing.Dict[str, typing.Any] = None,
    ) -> None:
        # pylint: disable=too-many-locals

        super().__init__(
            problem_description,
            datasets,
            random_seed=random_seed,
            strict_resolving=strict_resolving,
            strict_digest=strict_digest,
            volumes_dir=volumes_dir,
            scratch_dir=scratch_dir,
            template=template,
            deadline=deadline,
        )

        if generator_arguments is None:
            generator_arguments = {}
        else:
            generator_arguments = copy.copy(generator_arguments)
        if rewriter_arguments is None:
            rewriter_arguments = {}
        if tuner_arguments is None:
            tuner_arguments = {}
        else:
            tuner_arguments = copy.copy(tuner_arguments)

        # We pass "deadline" through kwargs and not directly as other arguments
        # to allow one to configure deadlines differently for different phases.
        # Arguments are JSON encoded and while we decode them, timestamps will
        # stay strings. So we have to parse them here.
        if 'deadline' in generator_arguments:
            generator_arguments['deadline'] = dateutil.parser.parse(generator_arguments['deadline'], fuzzy=True)
        else:
            generator_arguments['deadline'] = self.deadline
        if 'deadline' in tuner_arguments:
            tuner_arguments['deadline'] = dateutil.parser.parse(tuner_arguments['deadline'], fuzzy=True)
        else:
            tuner_arguments['deadline'] = self.deadline

        self._generator_class = typing.cast(typing.Type[generator_base.PipelineGeneratorBase], resolver_utils.from_path(generator_class))
        self._generator_arguments = generator_arguments
        self._rewriter_class = typing.cast(typing.Type[rewriter_base.PipelineRewriterBase], resolver_utils.from_path(rewriter_class))
        self._rewriter_arguments = rewriter_arguments
        self._tuner_class = typing.cast(typing.Type[tuner_base.PipelineTunerBase], resolver_utils.from_path(tuner_class))
        self._tuner_arguments = tuner_arguments

        self._rewriter = self._rewriter_class(self.problem_description, self.datasets, **self._rewriter_arguments)  # type: ignore
        self._generator = self._generator_class(
            self.problem_description,
            self.datasets,
            random_seed=self.random_seed,
            strict_resolving=self.strict_resolving,
            strict_digest=self.strict_digest,
            volumes_dir=self.volumes_dir,
            scratch_dir=self.scratch_dir,
            template=self.template,
            **self._generator_arguments,
        )
        self._tuner = self._tuner_class(
            self.problem_description,
            self.datasets,
            random_seed=self.random_seed,
            strict_resolving=self.strict_resolving,
            strict_digest=self.strict_digest,
            volumes_dir=self.volumes_dir,
            scratch_dir=self.scratch_dir,
            **self._tuner_arguments,
        )

        self._pipelines = {}
        self._tuning_to_pipelines = {}
        self._values_to_pipelines = {}

    def get_next_solutions(self) -> typing.Optional[typing.Sequence[typing.Tuple[str, pipeline_module.Pipeline, typing.Sequence]]]:  # noqa: D102
        generated_pipelines = self._generator.get_next_pipelines()

        if generated_pipelines is not None:
            for pipeline in generated_pipelines:
                if pipeline.id in self._pipelines:
                    logger.warning("Duplicate pipeline generated: %(pipeline_id)s", {'pipeline_id': pipeline.id})
                    continue

                rewritten_pipeline, free_hyperparams = self._rewriter.rewrite(pipeline)

                tuning_id = self._tuner.add_hyperparams(rewritten_pipeline, free_hyperparams)
                self._pipelines[pipeline.id] = rewritten_pipeline
                self._tuning_to_pipelines[tuning_id] = rewritten_pipeline

        suggested_values = self._tuner.get_next_values()

        if generated_pipelines is None and suggested_values is None:
            return None

        solutions = []

        if suggested_values is not None:
            for values_id, tuning_id, values in suggested_values:
                pipeline = self._tuning_to_pipelines[tuning_id]
                # We reuse "values_id" here as a solution ID.
                solutions.append((values_id, pipeline, values))
                self._values_to_pipelines[values_id] = pipeline

        return solutions

    def on_solution_result(self, solution_id: str, score: typing.Optional[float]) -> None:  # noqa: D102
        # We reused "values_id" as a solution ID.
        self._generator.on_pipeline_result(self._values_to_pipelines[solution_id].id, score)
        self._tuner.on_values_result(solution_id, score)
