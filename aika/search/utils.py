import copy
import logging
import typing
import uuid

from aika.pipelines import pipeline as pipeline_module, utils as pipelines_utils

from .tuner import default as default_tuner

logger = logging.getLogger(__name__)


def merge_template(template: pipeline_module.Pipeline, pipeline: pipeline_module.Pipeline) -> pipeline_module.Pipeline:
    """
    Replaces the last step in the ``template`` (which has to be a placeholder) with
    a sub-pipeline step with ``pipeline`` as step's pipeline.

    Parameters
    ----------
    template:
        A template to merge ``pipeline`` into.
    pipeline:
        A pipeline to merge.

    Returns
    -------
    A merged pipeline.
    """

    assert pipelines_utils.last_step_is_placeholder(template)

    # TODO: Is there a simpler way?
    output_pipeline = copy.copy(template)
    output_pipeline.steps = copy.copy(output_pipeline.steps)

    output_pipeline.id = str(uuid.uuid4())

    if output_pipeline.source is None:
        output_pipeline.source = {}

    output_pipeline.source['from'] = {
        'type': 'PIPELINE',
        'pipelines': [
            {
                'id': template.id,
                'digest': template.get_digest(),
            },
        ],
    }

    template_last_step = template.steps[-1]

    assert isinstance(template_last_step, pipeline_module.PlaceholderStep), type(template_last_step)

    subpipeline_step = pipeline_module.SubpipelineStep(pipeline=pipeline, resolver=template_last_step.resolver)

    for data_reference in template_last_step.inputs:
        subpipeline_step.add_input(data_reference)
    for output_id in template_last_step.outputs:
        subpipeline_step.add_output(output_id)

    output_pipeline.replace_step(len(output_pipeline.steps) - 1, subpipeline_step)

    return output_pipeline


def merge_hyperparameter_values(template: pipeline_module.Pipeline, hyperparameter_values: typing.Sequence) -> typing.Sequence:
    """
    Generates default hyper-parameter values for ``template`` and replaces the
    hyper-parameter values for the last step (which has to be a placeholder)
    with hyper-parameter values``hyperparameter_values``.

    Parameters
    ----------
    template:
        A template to merge ``hyperparameter_values`` for.
    hyperparameter_values:
        A list of dicts of hyper-parameter name to raw Python value mappings for each step
        in the pipeline.

    Returns
    -------
    A list of dicts of hyper-parameter name to raw Python value mappings for each
    step in the pipeline.
    """

    assert pipelines_utils.last_step_is_placeholder(template)

    # Make mutable.
    hyperparams = list(default_tuner.DefaultValuesPipelineTuner.get_default_values(template.get_free_hyperparams()))

    hyperparams[-1] = hyperparameter_values

    return hyperparams
