import abc
import datetime
import logging
import typing

from d3m import container, utils as d3m_utils  # pylint: disable=unused-import
from d3m.metadata import problem as problem_module

from aika.pipelines import pipeline as pipeline_module
from aika.search.evaluator import base as evaluator_base
from aika.search.searcher import base as searcher_base

logger = logging.getLogger(__name__)


class RunnerBase(metaclass=d3m_utils.AbstractMetaclass):
    """
    Obtains solutions from the solution searcher and evaluates them using an evaluator.

    Parameters
    ----------
    problem_description:
        A problem description to use.
    datasets:
        A sequence of datasets to use.
    searcher:
        A solution searcher to use.
    evaluator:
        An evaluator to use.
    random_seed:
        Random seed to use.
    strict_resolving:
        If resolved primitive does not fully match specified primitive reference, raise an exception?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    volumes_dir:
        Path to a directory with static files required by primitives.
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    deadline:
        Deadline before which the whole run should finish.
    """

    def __init__(
        self,
        problem_description: problem_module.Problem,
        datasets: typing.Sequence[container.Dataset],
        searcher: searcher_base.SolutionSearcherBase,
        evaluator: evaluator_base.EvaluatorBase,
        *,
        random_seed: int,
        strict_resolving: bool,
        strict_digest: bool,
        volumes_dir: str = None,
        scratch_dir: str,
        deadline: datetime.datetime = None,
    ) -> None:
        self.problem_description = problem_description
        self.datasets = datasets
        self.searcher = searcher
        self.evaluator = evaluator
        self.random_seed = random_seed
        self.strict_resolving = strict_resolving
        self.strict_digest = strict_digest
        self.volumes_dir = volumes_dir
        self.scratch_dir = scratch_dir
        self.deadline = deadline

    @abc.abstractmethod
    def start(self) -> None:
        """
        Starts evaluating all solutions found by the searcher.

        It does not block.
        """

    @abc.abstractmethod
    def get_next_evaluated_solutions(
        self,
        block: bool = False,
        timeout: float = None,
    ) -> typing.Optional[typing.Sequence[typing.Tuple[str, pipeline_module.Pipeline, typing.Sequence, float, typing.Sequence[typing.Dict]]]]:
        """
        Get next available evaluated solutions.

        Returns ``None`` if no more solutions can be evaluated.
        If returns an empty sequence if for now no more solutions have been evaluated.

        Parameters
        ----------
        block:
            If no evaluated solutions are available, block until one is available?
        timeout:
            If ``block`` is ``True``, how long to wait before returning an empty list, in seconds?

        Returns
        -------
        A sequence of tuples with: an arbitrary solution ID, a pipeline description,
        hyper-parameter values, a normalized score for this solutions, and a list of
        descriptions of how scores were computed and their raw values. Hyper-parameter
        values are represented as a list of dicts of hyper-parameter name to raw Python
        value mappings for each step in the pipeline.
        """
