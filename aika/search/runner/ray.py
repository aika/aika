import datetime
import logging
import queue
import sys
import threading
import time
import typing
import weakref

import ray
from ray import exceptions as ray_exceptions

from d3m import container
from d3m.metadata import problem as problem_module

from aika.automl import base as automl_base, tasks, utils as automl_utils
from aika.pipelines import pipeline as pipeline_module
from aika.search.evaluator import base as evaluator_base
from aika.search.searcher import base as searcher_base

from . import base

logger = logging.getLogger(__name__)

if sys.version_info >= (3, 7):
    # Available from Python 3.7 on.
    QueueClass = queue.SimpleQueue  # pylint: disable=no-member,invalid-name
else:
    QueueClass = queue.Queue  # pylint: disable=invalid-name


# TODO: Define per-solution resource requirements (GPUs) for the task.
@ray.remote
@tasks.automl_task(task_logger=logger)
def evaluate_solution(
    evaluator: evaluator_base.EvaluatorBase,
    pipeline: pipeline_module.Pipeline,
    problem_description: problem_module.Problem,
    inputs: typing.Sequence[typing.Any],
    *,
    hyperparameter_values: typing.Sequence,
    random_seed: int,
    deadline: datetime.datetime = None,
) -> typing.Optional[typing.Tuple[float, typing.Sequence[typing.Dict]]]:
    """
    Evaluate a pipeline with hyper-parameters using a 0.75 train-test holdout and
    metrics from the problem description.

    Returns ``None`` if evaluation has errored or deadline has reached.

    Parameters
    ----------
    evaluator:
        An instance of an evaluator to use.
    pipeline:
        A pipeline to evaluate.
    problem_description:
        Problem description to use for the pipeline run to set target columns.
    inputs:
        A list of input values to the pipeline.
    hyperparameter_values:
        A list of dicts of hyper-parameter name to raw Python value mappings for each step in the pipeline.
    random_seed:
        Random seed to use.
    deadline:
        Deadline before which the evaluation should finish.

    Returns
    -------
    A tuple with: a normalized score for this solutions, and a list of
    descriptions of how scores were computed and their raw values.
    """

    return evaluator.evaluate_solution(pipeline, problem_description, inputs, hyperparameter_values=hyperparameter_values, random_seed=random_seed, deadline=deadline)


class EvaluateSolutionsThread(threading.Thread):
    """
    A thread running a tight loop of obtaining new solutions, submitting them for evaluation,
    and once they are evaluated, updating the "evaluated_solutions" queue with scores.
    """

    _pending_solutions: typing.List[typing.Tuple[str, pipeline_module.Pipeline, typing.Sequence]]
    _pending_scores: typing.Dict[ray.ObjectID, typing.Tuple[str, pipeline_module.Pipeline, typing.Sequence]]

    def __init__(
        self,
        runner: 'RayRunner',
        evaluated_solutions: QueueClass,
        problem_description: problem_module.Problem,
        datasets: typing.Sequence[container.Dataset],
        searcher: searcher_base.SolutionSearcherBase,
        evaluator: evaluator_base.EvaluatorBase,
        *,
        random_seed: int,
        deadline: datetime.datetime = None,
    ) -> None:
        # Making the thread a daemon causes it to exit when the main thread exits.
        super().__init__(name='EvaluateSolutions', daemon=True)

        self.problem_description = problem_description
        self.datasets = datasets
        self.searcher = searcher
        self.evaluator = evaluator
        self.random_seed = random_seed
        self.deadline = deadline

        # A queue of solutions which yet have to be scored.
        self._pending_solutions = []
        # A map between evaluation ObjectIDs and tuples of "solution_id", "pipeline", and "hyperparameter_values".
        self._pending_scores = {}
        # A queue to which to push scores as tuples of "solution_id", "pipeline", "hyperparameter_values",
        # "normalized_score", and "search_scores"
        self._evaluated_solutions = evaluated_solutions
        self._searching_active = True
        self._terminated = False

        # We make a weak reference to runner, so that when
        # it gets deleted also thread gets terminated.
        self._runner = weakref.ref(runner, lambda ref: self.terminate())

        # We remember the current Ray job ID so that if it changes we know
        # the thread should terminate.
        self._current_job_id = ray.worker.global_worker.current_job_id

        # A workaround to allow scheduling one extra object than current available resources
        # to enable running Aika only on one CPU. This extra object can be scheduled because we suspend
        # this runner itself to free a worker. Will not be necessary once Ray API for waiting
        # for available worker is available. See: https://github.com/ray-project/ray/issues/5243
        self._scheduled_extra_object_id = None

        # The thread inherits current Ray context.
        self._ray_context_object_ids = getattr(automl_utils.ray_context, 'object_ids', None)
        self._ray_context_task_name = getattr(automl_utils.ray_context, 'task_name', None)

    # TODO: Optimize that if some steps between different pipelines are equivalent, we compute them only once.
    def run(self) -> None:
        """
        We check if thread is terminated before any logging or any external call (especially Ray API call).
        """

        # pylint: disable=too-many-return-statements,too-many-branches,too-many-statements

        # The thread inherits current Ray context.
        if self._ray_context_object_ids:
            automl_utils.ray_context.object_ids = self._ray_context_object_ids
        if self._ray_context_task_name:
            automl_utils.ray_context.task_name = self._ray_context_task_name

        try:
            while True:
                if self.is_terminated():
                    return

                # Has time to evaluate solutions expired?
                if self.deadline is not None and datetime.datetime.now(datetime.timezone.utc) > self.deadline:
                    logger.info("Deadline has been reached.")
                    return

                solutions = None

                if self._searching_active:
                    logger.info("Searching for next solutions.")

                    # TODO: We should not get more solutions if we do not have resources currently to schedule evaluation for them.
                    #       Probably we should request one more than we have resources, so that we do not have to wait for it to
                    #       be generated, once resources are available, but not more. This could allow searchers to generate solutions
                    #       given the latest results and not that solutions are generated which might become obsolete with newer results.
                    solutions = self.searcher.get_next_solutions()

                    if self.is_terminated():
                        return

                    if solutions is None:
                        logger.info("No more solutions.")
                        self._searching_active = False

                    # Has time to evaluate solutions expired?
                    if self.deadline is not None and datetime.datetime.now(datetime.timezone.utc) > self.deadline:
                        logger.info("Deadline has been reached.")
                        return

                for solution_id, pipeline, hyperparameter_values in solutions or []:
                    if self.is_terminated():
                        return

                    # Has time to evaluate solutions expired?
                    if self.deadline is not None and datetime.datetime.now(datetime.timezone.utc) > self.deadline:
                        logger.info("Deadline has been reached.")
                        return

                    logger.info(
                        "Found a solution '%(solution_id)s' with pipeline '%(pipeline_id)s'.",
                        {
                            'solution_id': solution_id,
                            'pipeline_id': pipeline.id,
                        },
                    )

                    self._pending_solutions.append((solution_id, pipeline, hyperparameter_values))

                if self.is_terminated():
                    return

                # Has time to evaluate solutions expired?
                if self.deadline is not None and datetime.datetime.now(datetime.timezone.utc) > self.deadline:
                    logger.info("Deadline has been reached.")
                    return

                # Schedule for scoring any pending solutions we can.
                # When a resource has no availability, it seems its key is not present.
                # TODO: Check resources which are really necessary per solution (GPUs)?
                while self._pending_solutions and ray.available_resources().get('CPU', 0.0) >= 1.0:
                    if self.is_terminated():
                        return

                    # Has time to evaluate solutions expired?
                    if self.deadline is not None and datetime.datetime.now(datetime.timezone.utc) > self.deadline:
                        logger.info("Deadline has been reached.")
                        return

                    solution_id, pipeline, hyperparameter_values = self._pending_solutions.pop()

                    object_id = evaluate_solution.remote(
                        self.evaluator,
                        pipeline,
                        self.problem_description,
                        self.datasets,
                        hyperparameter_values=hyperparameter_values,
                        random_seed=self.random_seed,
                        deadline=self.deadline,
                    )

                    self._pending_scores[object_id] = (solution_id, pipeline, hyperparameter_values)

                pending_object_ids = list(self._pending_scores.keys())

                # If there were solutions, we just check if any pending score is now available,
                # and if yes, update the searcher.
                if solutions and pending_object_ids:
                    if self.is_terminated():
                        return

                    ready_ids, remaining_ids = ray.wait(  # pylint: disable=unused-variable
                        pending_object_ids,
                        num_returns=len(pending_object_ids),
                        timeout=0,
                    )

                    self._process_ready_ids(ready_ids)

                # If there are pending ObjectIDs, but there were no solutions, we wait for any of the
                # pending ObjectIDs to be available. There is no point in retrying before updating the
                # searcher with at least one score, because searcher is returning no solutions, arguably,
                # because it is waiting for scores. Or searcher is not active anymore.
                elif pending_object_ids:
                    if self.is_terminated():
                        return

                    if self.deadline is not None:
                        remaining_time: typing.Optional[float] = (self.deadline - datetime.datetime.now(datetime.timezone.utc)).total_seconds()
                    else:
                        remaining_time = None

                    # Has time to evaluate solutions expired?
                    if remaining_time is not None and remaining_time < 0:
                        logger.info("Deadline has been reached.")
                        return

                    # We wait only until the deadline.
                    ready_ids, remaining_ids = ray.wait(  # pylint: disable=unused-variable
                        pending_object_ids,
                        num_returns=1,
                        timeout=remaining_time,
                    )

                    if self.is_terminated():
                        return

                    if not ready_ids:
                        logger.info("Deadline has been reached.")
                        return

                    self._process_ready_ids(ready_ids)

                # We might still have pending solutions to schedule for scoring. This can happen
                # if the cluster is occupied. So we should wait a bit and retry.
                elif self._pending_solutions:
                    # TODO: Instead of sleeping, wait for resources to be available.
                    #       See: https://github.com/ray-project/ray/issues/5243
                    time.sleep(0.1)  # seconds

                elif solutions is not None and not solutions:
                    logger.error("Searcher has not returned next solutions, but there are no pending solutions or scores.")
                    return

                else:
                    # We have done our job: search is not active anymore and there
                    # are no more pending solutions or scores.
                    assert not self._searching_active
                    assert not self._pending_scores
                    assert not self._pending_solutions
                    return

        except Exception as error:
            logger.exception("Exception in evaluate solutions thread.")
            raise error

        finally:
            # We cleanup any ongoing evaluation tasks.
            while self._pending_scores:
                object_id, _ = self._pending_scores.popitem()
                ray.cancel(object_id)

            # To wake up anyone blocked on the queue.
            self._evaluated_solutions.put(None)

    def terminate(self) -> None:
        """
        Mark thread as terminated. We check if thread is terminated regularly inside the `run` method.
        """

        self._terminated = True

    def is_terminated(self) -> bool:
        """
        Is thread marked as terminated? Or has current Ray job changed since the thread started?

        Returns
        -------
        ``True`` if thread is marked as terminated.
        """

        return self._terminated or ray.worker.global_worker.current_job_id != self._current_job_id

    def _process_ready_ids(self, ready_ids: typing.Sequence[ray.ObjectID]) -> None:
        # pylint: disable=too-many-return-statements,too-many-branches

        for ready_id in ready_ids:
            if self.is_terminated():
                return

            # Has time to evaluate solutions expired?
            if self.deadline is not None and datetime.datetime.now(datetime.timezone.utc) > self.deadline:
                return

            solution_id, pipeline, hyperparameter_values = self._pending_scores.pop(ready_id)

            try:
                result = ray.get(ready_id, timeout=automl_base.DEFAULT_RAY_TIMEOUT)

                if result is None:
                    normalized_score = None
                else:
                    normalized_score, search_scores = result

            except ray_exceptions.RayError as error:
                # Evaluation should not really error in this way because it should
                # return "None" if an error happened during evaluation.

                if self.is_terminated():
                    return

                if isinstance(error, ray_exceptions.RayTaskError):
                    # Removing the final newline. This is also what logging does in default "Formatter.formatException".
                    traceback = error.traceback_str  # pylint: disable=no-member
                    if traceback[-1:] == '\n':
                        traceback = traceback[:-1]

                    logger.error(
                        "Unexpected error evaluating solution '%(solution_id)s' with pipeline '%(pipeline_id)s'.",
                        {
                            'solution_id': solution_id,
                            'pipeline_id': pipeline.id,
                        },
                        # Passing a tuple of a string as "exc_info" is possible because we are using
                        # custom "LogRecord" factory from "aika.utils.logging".
                        exc_info=(traceback,),  # type: ignore
                    )
                else:
                    logger.error(
                        "Unexpected error evaluating solution '%(solution_id)s' with pipeline '%(pipeline_id)s': %(error)s",
                        {
                            'solution_id': solution_id,
                            'pipeline_id': pipeline.id,
                            'error': str(error),
                        },
                    )

                normalized_score = None

            if self.is_terminated():
                return

            # Has time to evaluate solutions expired?
            if self.deadline is not None and datetime.datetime.now(datetime.timezone.utc) > self.deadline:
                return

            self.searcher.on_solution_result(solution_id, normalized_score)

            if self.is_terminated():
                return

            # Has time to evaluate solutions expired?
            if self.deadline is not None and datetime.datetime.now(datetime.timezone.utc) > self.deadline:
                return

            if normalized_score is None:
                continue

            self._evaluated_solutions.put((solution_id, pipeline, hyperparameter_values, normalized_score, search_scores))


class RayRunner(base.RunnerBase):
    """
    Evaluate solutions in parallel using Ray.
    """

    _evaluation_thread: typing.Optional[EvaluateSolutionsThread]
    _evaluated_solutions: QueueClass

    def __init__(
        self,
        problem_description: problem_module.Problem,
        datasets: typing.Sequence[container.Dataset],
        searcher: searcher_base.SolutionSearcherBase,
        evaluator: evaluator_base.EvaluatorBase,
        *,
        random_seed: int,
        strict_resolving: bool,
        strict_digest: bool,
        volumes_dir: str = None,
        scratch_dir: str,
        deadline: datetime.datetime = None,
    ) -> None:
        super().__init__(
            problem_description,
            datasets,
            searcher,
            evaluator,
            random_seed=random_seed,
            strict_resolving=strict_resolving,
            strict_digest=strict_digest,
            volumes_dir=volumes_dir,
            scratch_dir=scratch_dir,
            deadline=deadline,
        )

        self._evaluation_thread = None
        self._evaluated_solutions = QueueClass()

    def start(self) -> None:  # noqa: D102
        self._evaluation_thread = EvaluateSolutionsThread(
            self,
            self._evaluated_solutions,
            self.problem_description,
            self.datasets,
            self.searcher,
            self.evaluator,
            random_seed=self.random_seed,
            deadline=self.deadline,
        )
        self._evaluation_thread.start()

    def _is_evaluation_thread_running(self) -> bool:
        return self._evaluation_thread is not None and not self._evaluation_thread.is_terminated() and self._evaluation_thread.is_alive()

    def get_next_evaluated_solutions(
        self,
        block: bool = False,
        timeout: float = None,
    ) -> typing.Optional[typing.Sequence[typing.Tuple[str, pipeline_module.Pipeline, typing.Sequence, float, typing.Sequence[typing.Dict]]]]:  # noqa: D102
        # pylint: disable=too-many-return-statements,too-many-branches

        evaluated_solutions = []
        evaluated_solution_has_been_none = False

        while True:
            try:
                evaluated_solution = self._evaluated_solutions.get_nowait()
            except queue.Empty:
                break

            if evaluated_solution is None:
                evaluated_solution_has_been_none = True
            else:
                evaluated_solutions.append(evaluated_solution)

        if evaluated_solutions:
            return evaluated_solutions

        elif evaluated_solution_has_been_none:
            return None

        elif self._is_evaluation_thread_running():
            if block:
                try:
                    evaluated_solution = self._evaluated_solutions.get(True, timeout)
                except queue.Empty:
                    return []

                # We get "None" if thread has stopped while we waited.
                if evaluated_solution is None:
                    return None
                else:
                    return [evaluated_solution]
            else:
                return []

        # To handle an edge case where queue was empty when we check it for the first
        # time, but then something was pushed into it and thread stops, before we check
        # if thread is still running, so we get here.
        elif not self._evaluated_solutions.empty():
            return self.get_next_evaluated_solutions(block, timeout)

        else:
            return None
