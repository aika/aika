# Required for Bash autocomplete to work: PYTHON_ARGCOMPLETE_OK

import argparse
import importlib
import logging
import os
import os.path
import pkgutil
import sys
import tempfile
import typing

import argcomplete

from aika.utils import arguments as arguments_module, logging as logging_utils

from . import commands

# Possible suffixes for memory units in Kubernetes.
# See: https://github.com/eBay/Kubernetes/blob/master/docs/design/resources.md#resource-quantities
#      https://kubernetes.io/docs/tasks/configure-pod-container/assign-memory-resource/#memory-units
SUFFIXES = {
    'E': 10 ** 18,
    'P': 10 ** 15,
    'T': 10 ** 12,
    'G': 10 ** 9,
    'M': 10 ** 6,
    'K': 10 ** 3,
    'Ei': 2 ** 60,
    'Pi': 2 ** 50,
    'Ti': 2 ** 40,
    'Gi': 2 ** 30,
    'Mi': 2 ** 20,
    'Ki': 2 ** 10,
}

# It should match "DATASETS_DIRECTORY" and "PROBLEMS_DIRECTORY" in "aika.datasets.base" and "aika.problems.base".
DATASETS_DIRECTORY = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..', 'datasets'))

logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser()  # pylint: disable=invalid-name
parser.add_argument(
    '-n',
    '--random-seed',
    type=int,
    default=0,
    action='store',
    metavar='SEED',
    help="random seed to use",
)
parser.add_argument(
    '-p',
    '--pipelines-path',
    action='append',
    metavar='PATH',
    dest='pipeline_search_paths',
    help=("path to a directory with pipelines to resolve from (<pipeline id>.json and <pipeline id>.yml), " "can be specified multiple times, has priority over PIPELINES_PATH environment variable"),
)
parser.add_argument(
    '-v',
    '--volumes',
    action='store',
    dest='volumes_dir',
    help="path to a directory with static files required by primitives, in the standard directory structure (as obtained running \"aika d3m index download\")",
)
parser.add_argument(
    '-s',
    '--scratch',
    action='store',
    dest='scratch_dir',
    help="path to a directory to store any temporary files needed during execution",
)
parser.add_argument(
    '-d',
    '--datasets-path',
    action='append',
    metavar='PATH',
    dest='dataset_search_paths',
    help=("path to a directory with datasets (and problem descriptions), can be specified multiple times, " "if not provided, \"{DATASETS_DIRECTORY}\" directory is used").format(
        DATASETS_DIRECTORY=DATASETS_DIRECTORY,
    ),
)
parser.add_argument(
    '-c',
    '--client-only',
    action='store_true',
    help="connect to background services (like MongoDB and Ray) instead of starting them, when they are necessary",
)
parser.add_argument(
    '-l',
    '--logging-level',
    default='info',
    action='store',
    choices=['debug', 'info', 'warning', 'error', 'critical'],
    help="logging level to use for the console",
)
parser.add_argument(
    '--plasma',
    action='store',
    dest='plasma_dir',
    help="path to a Plasma directory to use by Ray when starting it in the background",
)
parser.add_argument(
    '--huge-pages',
    default=False,
    action='store_true',
    help="does Plasma directory support huge pages",
)
parser.add_argument(
    '--plasma-socket',
    action='store',
    help="path to a Plasma socket to use by Ray when starting in the background",
)
parser.add_argument(
    '--number-of-cpus',
    type=int,
    action='store',
    metavar='CPUS',
    help="number of CPUs to use by Ray when starting it in the background",
)
parser.add_argument(
    '--number-of-gpus',
    type=int,
    action='store',
    metavar='GPUS',
    help="number of GPUs to use by Ray when starting it in the background",
)
# We parse the argument to always be an int in bytes before passing it to command handlers.
parser.add_argument(
    '--object-store-memory',
    action='store',
    dest='object_store_memory_limit',
    metavar='MEMORY',
    help="memory limit to use by Ray object store when starting it in the background",
)
# We do it differently from the d3m core package so that we do not have to
# load it to get all possible values of the enumeration.
parser.add_argument(
    '--always-compute-digest',
    default='ONLY_IF_MISSING',
    action='store_const',
    const='ALWAYS',
    dest='compute_digest',
    help="when loading datasets, always compute their digests, instead of only if missing",
)
parser.add_argument(
    '--strict-resolving',
    default=False,
    action='store_true',
    help="fail resolving if a resolved pipeline or primitive does not fully match specified reference",
)
parser.add_argument(
    '--strict-digest',
    default=False,
    action='store_true',
    help="when loading datasets, pipelines, or primitives, if computed digest does not match the one provided in metadata, raise an exception?",
)

# We use our own "parser_class" which supports lazy initialization.
subparsers = parser.add_subparsers(dest='command', title='commands', parser_class=arguments_module.ArgumentParser)  # pylint: disable=invalid-name
subparsers.required = True


def parse_memory_limit(memory_limit: str) -> int:
    """
    Parse a Kubernetes-compatible memory limit string into number of bytes.

    Parameters
    ----------
    memory_limit:
        A memory limit string to parse.

    Returns
    -------
    Memory limit as integer in bytes.
    """

    for suffix, value in SUFFIXES.items():
        if memory_limit.endswith(suffix):
            # We first parse as float and then convert to int to allow for fixed-point integers.
            return int(float(memory_limit[: -len(suffix)])) * value
    else:  # pylint: disable=useless-else-on-loop
        return int(memory_limit)


def main(argv: typing.Sequence) -> None:
    """
    Main Aika command line parser and handling function.

    Start it with ``aika`` or ``python3 -m aika``.

    All modules from `aika.commands` are first automatically imported and a function ``register``
    is called from them, to allow them to add themselves to the command line parser.

    Parameters
    ----------
    argv:
        CLI arguments to use.
    """

    # Load all modules in the "commands" package and call "register" function on each of them.
    for loader, name, ispkg in pkgutil.walk_packages(commands.__path__, commands.__name__ + '.'):  # type: ignore # pylint: disable=unused-variable
        if ispkg:
            continue

        if name.startswith('aika.commands.utils.'):
            continue

        try:
            importlib.import_module(name).register(subparsers)  # type: ignore
        except Exception:  # pylint: disable=broad-except
            logger.exception(
                "Error importing a command from module '%(module)s'.",
                {
                    'module': name,
                },
            )

    argcomplete.autocomplete(parser)

    arguments = parser.parse_args(argv[1:])

    if arguments.scratch_dir:
        scratch_dir = os.path.abspath(arguments.scratch_dir)
        os.makedirs(scratch_dir, 0o755, exist_ok=True)
        tempfile.tempdir = scratch_dir
    else:
        arguments.scratch_dir = tempfile.gettempdir()

    # Configure console handler logging level.
    logging_utils.get_console_handler().setLevel(arguments.logging_level.upper())

    if arguments.object_store_memory_limit is not None:
        arguments.object_store_memory_limit = parse_memory_limit(arguments.object_store_memory_limit)

    assert parser._subparsers is not None  # pylint: disable=protected-access

    # Dynamically fetch which subparser was used.
    subparser = typing.cast(argparse._SubParsersAction, parser._subparsers._group_actions[0]).choices[arguments.command]  # pylint: disable=protected-access

    try:
        arguments.handler(arguments, subparser)
    except Exception:  # pylint: disable=broad-except
        logger.exception("Exception while running the command.")
        sys.exit(1)
