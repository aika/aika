import copy
import datetime
import functools
import inspect
import json
import logging
import os
import os.path
import typing
import uuid

import frozendict
import ray

from d3m import runtime as runtime_module
from d3m.container import dataset as dataset_module
from d3m.metadata import base as metadata_base

from aika.pipelines import pipeline as pipeline_module, utils as pipelines_utils
from aika.search.evaluator import base as evaluator_base
from aika.search.runner import base as runner_base
from aika.search.searcher import base as searcher_base
from aika.search.tuner import default as default_tuner
from aika.utils import exceptions, resolver as resolver_utils

from . import base, utils

logger = logging.getLogger(__name__)

EXECUTABLE_CONTENT = """#!/bin/bash -e

echo "{pipeline_id}"
"""


def automl_task(*, task_logger: logging.Logger, collection_name: str = None) -> typing.Callable:  # pylint: disable=missing-return-doc,missing-return-type-doc
    """
    A decorator for AutoML tasks which contains initialization logic, exception handling logic, and
    sets logging context.

    It assumes that task function receives as the first positional argument the ID of the document
    in the collection with ``collection_name``. Any error state is then set on this document in the
    case of an exception.

    Parameters
    ----------
    task_logger:
        A logger instance to use.
    collection_name:
        A collection where to find a document to set an error state on.
    """

    def decorator(func: typing.Callable) -> typing.Callable:
        @functools.wraps(func)
        def wrapper(*args: typing.Any, **kwargs: typing.Any) -> typing.Any:
            with utils.set_ray_context(func.__qualname__):
                document_id = args[0]

                try:
                    # Just to make sure. This should already be called automatically on every worker.
                    base.init_worker()

                    assert base.mongo_client is not None

                    return func(*args, **kwargs)

                except Exception as error:
                    if collection_name is not None:
                        timestamp = datetime.datetime.now(datetime.timezone.utc)

                        assert base.mongo_client is not None
                        collection = getattr(base.mongo_client.aika, collection_name)

                        # We first try to set both "start" and "end" if "start" is not set for some reason.
                        collection.update_one(
                            {
                                '_id': document_id,
                                'state': {
                                    '$in': [base.ProgressState.PENDING.name, base.ProgressState.RUNNING.name],  # pylint: disable=no-member
                                },
                                'start': None,
                            },
                            {
                                '$set': {
                                    'state': base.ProgressState.ERRORED.name,  # pylint: disable=no-member
                                    'status': str(error),
                                    'start': timestamp,
                                    'end': timestamp,
                                },
                            },
                        )
                        # Then we try to set just "end". If the previous query succeeded, then this query cannot.
                        collection.update_one(
                            {
                                '_id': document_id,
                                'state': {
                                    '$in': [base.ProgressState.PENDING.name, base.ProgressState.RUNNING.name],  # pylint: disable=no-member
                                },
                            },
                            {
                                '$set': {
                                    'state': base.ProgressState.ERRORED.name,  # pylint: disable=no-member
                                    'status': str(error),
                                    'end': timestamp,
                                },
                            },
                        )

                    task_logger.exception("%(task_name)s failed.", {'task_name': func.__name__})

                    raise error

        return wrapper

    return decorator


def _encode_hyperparameter_values(free_hyperparams: typing.Sequence, hyperparameter_values: typing.Sequence) -> typing.Sequence:
    """
    Given values of hyper-parameters ``hyperparameter_values`` and possible free
    hyper-parameter instances ``free_hyperparams``, it encodes values for JSON.

    ``hyperparameter_values`` have a structure matching pipeline's steps (and sub-pipelines).
    ``free_hyperparams`` as well.

    The reason why hyper-parameter instances are needed is because we have to know
    for which hyper-parameter the value belongs to encode it properly.

    Parameters
    ----------
    free_hyperparams:
        A list of hyper-parameters configuration for free hyper-parameters for each step.
    hyperparameter_values:
        A list of dicts of hyper-parameter name to raw Python value mappings for each step
        in the pipeline.

    Returns
    -------
    A list of dicts of hyper-parameter name to value (encoded for JSON) mappings for each
    step in the pipeline.
    """

    hyperparams = []

    assert len(free_hyperparams) == len(hyperparameter_values), (len(free_hyperparams), len(hyperparameter_values))

    for free_hyperparams_for_step, hyperparameter_values_for_step in zip(free_hyperparams, hyperparameter_values):
        if isinstance(free_hyperparams_for_step, (dict, frozendict.frozendict)):
            assert isinstance(hyperparameter_values_for_step, (dict, frozendict.frozendict))

            values = {}
            for name, hyperparameter in free_hyperparams_for_step.items():
                values[name] = hyperparameter.value_to_json_structure(hyperparameter_values_for_step[name])

            hyperparams.append(values)
        elif isinstance(free_hyperparams_for_step, typing.Sequence):
            assert isinstance(hyperparameter_values_for_step, typing.Sequence)

            hyperparams.append(_encode_hyperparameter_values(free_hyperparams_for_step, hyperparameter_values_for_step))
        else:
            raise exceptions.UnexpectedValueError("Unknown hyper-parameters type: {hyperparams_type}".format(hyperparams_type=type(free_hyperparams_for_step)))

    return hyperparams


def _store_solution(
    search: typing.Dict,
    solution_id: str,
    pipeline: pipeline_module.Pipeline,
    hyperparameter_values: typing.Sequence,
    normalized_score: float,
    search_scores: typing.Sequence[typing.Dict],
    pipelines_scored_dir: typing.Optional[str],
    subpipelines_dir: typing.Optional[str],
) -> None:
    """
    Stores the solution into MongoDB database.

    Parameters
    ----------
    search:
        A ``aika.Searches`` MongoDB document.
    solution_id:
        A solution ID under which to store the solution.
    pipeline:
        A pipeline to evaluate.
    hyperparameter_values:
        A list of dicts of hyper-parameter name to raw Python value mappings for each step in the pipeline.
    normalized_score:
        A normalized score of this solution.
    search_scores:
        A list of descriptions of how scores were computed and their raw values. See MongoDB ``aika.Solutions``
        collection description for expected structure.
    pipelines_scored_dir:
        Where to store all successfully evaluated pipelines?
    subpipelines_dir:
        Where to store sub-pipelines? If not specified, a corresponding pipeline
        output directory is used for sub-pipelines as well.
    """

    assert normalized_score is not None
    assert search_scores

    hyperparameter_values = _encode_hyperparameter_values(pipeline.get_free_hyperparams(), hyperparameter_values)
    search_scores = base.encode_search_scores(search_scores)

    assert base.mongo_client is not None

    base.mongo_client.aika.Solutions.insert_one(
        {
            '_id': solution_id,
            'search': {
                '_id': search['_id'],
            },
            'internal_score': normalized_score,
            'random_seed': search['random_seed'],
            'search_scores': search_scores,
            'problem': base.encode_problem_description(search['problem']),
            'pipeline': pipeline.to_json_structure(nest_subpipelines=True),
            'hyperparameter_values': hyperparameter_values,
        },
    )

    base.mongo_client.aika.Searches.update_one(
        {
            '_id': search['_id'],
            'state': base.ProgressState.RUNNING.name,  # pylint: disable=no-member
        },
        {
            '$push': {
                'solutions': {
                    '_id': solution_id,
                    'internal_score': normalized_score,
                    'random_seed': search['random_seed'],
                    'search_scores': search_scores,
                },
            },
        },
    )

    if pipelines_scored_dir is not None:
        if subpipelines_dir is None:
            subpipelines_dir = pipelines_scored_dir

        # When exporting a pipeline we should export it under the solution ID and not pipeline ID.
        pipeline.id = solution_id
        pipeline.export_to_json(pipelines_scored_dir, subpipelines_dir)


def _search_solutions(
    search: typing.Dict,
    template: typing.Optional[pipeline_module.Pipeline],
    strict_resolving: bool,
    volumes_dir: typing.Optional[str],
    scratch_dir: str,
    pipelines_scored_dir: typing.Optional[str],
    subpipelines_dir: typing.Optional[str],
    compute_digest: dataset_module.ComputeDigest,
    strict_digest: bool,
) -> None:
    """
    Search for solutions (pipelines + hyper-parameters) given a problem description, data inputs,
    and pipeline template.

    Pipeline template is optional, but if it is specified can currently only have one placeholder,
    at the end of the pipeline.

    Parameters
    ----------
    search:
        A ``aika.Searches`` MongoDB document.
    template:
        A pipeline template instance.
    strict_resolving:
        If resolved pipeline or primitive does not fully match specified reference, raise an exception?
    volumes_dir:
        Path to a directory with static files required by primitives.
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    pipelines_scored_dir:
        Where to store all successfully evaluated pipelines?
    subpipelines_dir:
        Where to store sub-pipelines? If not specified, a corresponding pipeline
        output directory is used for sub-pipelines as well.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    """

    # pylint: disable=too-many-locals,too-many-branches,too-many-statements

    problem_description = search['problem']

    if problem_description is None:
        # In this function we are assuming the template is not fully specified.
        raise exceptions.InvalidArgumentValueError("Problem description has to be provided when pipeline template is not fully specified.")

    logger.info("Loading inputs.")
    inputs = [base.load_value(base.retrieve_value_in_ray(input), compute_digest=compute_digest, strict_digest=strict_digest) for input in search['inputs']]

    if template is not None:
        logger.info("Checking pipeline template.")
        template.check(
            allow_placeholders=True,
            standard_pipeline=True,
            input_types={'inputs.{i}'.format(i=i): type(input) for i, input in enumerate(inputs)},
        )

    if not inputs:
        # In this function we are assuming the template is not fully specified.
        raise exceptions.InvalidArgumentValueError("Inputs have to be provided when pipeline template is not fully specified.")

    logger.info("Initializing search.")

    runner_class = typing.cast(typing.Type[runner_base.RunnerBase], resolver_utils.from_path(search['runner_class']))
    evaluator_class = typing.cast(typing.Type[evaluator_base.EvaluatorBase], resolver_utils.from_path(search['evaluator_class']))
    searcher_class = typing.cast(typing.Type[searcher_base.SolutionSearcherBase], resolver_utils.from_path(search['searcher_class']))

    searcher_parameters = inspect.signature(searcher_class).parameters

    searcher_kwargs = copy.copy(search['searcher_arguments'])
    if 'generator_class' in searcher_parameters:
        searcher_kwargs['generator_class'] = search['generator_class']
    if 'generator_arguments' in searcher_parameters:
        searcher_kwargs['generator_arguments'] = search['generator_arguments']
    if 'rewriter_class' in searcher_parameters:
        searcher_kwargs['rewriter_class'] = search['rewriter_class']
    if 'rewriter_arguments' in searcher_parameters:
        searcher_kwargs['rewriter_arguments'] = search['rewriter_arguments']
    if 'tuner_class' in searcher_parameters:
        searcher_kwargs['tuner_class'] = search['tuner_class']
    if 'tuner_arguments' in searcher_parameters:
        searcher_kwargs['tuner_arguments'] = search['tuner_arguments']

    if search['time_bound_search']:
        deadline: typing.Optional[datetime.datetime] = datetime.datetime.now(tz=datetime.timezone.utc) + datetime.timedelta(minutes=search['time_bound_search'])
    else:
        deadline = None

    searcher = searcher_class(  # type: ignore
        problem_description,
        inputs,
        random_seed=search['random_seed'],
        strict_resolving=strict_resolving,
        strict_digest=strict_digest,
        volumes_dir=volumes_dir,
        scratch_dir=scratch_dir,
        template=template,
        deadline=deadline,
        **searcher_kwargs,
    )

    evaluator = evaluator_class(  # type: ignore
        strict_resolving=strict_resolving,
        strict_digest=strict_digest,
        volumes_dir=volumes_dir,
        scratch_dir=scratch_dir,
        pipelines_scored_dir=pipelines_scored_dir,
        subpipelines_dir=subpipelines_dir,
        **search['evaluator_arguments'],
    )

    runner = runner_class(  # type: ignore
        problem_description,
        inputs,
        searcher,
        evaluator,
        random_seed=search['random_seed'],
        strict_resolving=strict_resolving,
        strict_digest=strict_digest,
        volumes_dir=volumes_dir,
        scratch_dir=scratch_dir,
        deadline=deadline,
        **search['runner_arguments'],
    )

    runner.start()

    while True:
        if deadline is not None:
            remaining_time: typing.Optional[float] = (deadline - datetime.datetime.now(datetime.timezone.utc)).total_seconds()
        else:
            remaining_time = None

        if remaining_time is not None and remaining_time < 0:
            break

        evaluated_solutions = runner.get_next_evaluated_solutions(block=True, timeout=remaining_time)

        if evaluated_solutions is None:
            break

        for solution_id, pipeline, hyperparameter_values, normalized_score, search_scores in evaluated_solutions:
            _store_solution(search, solution_id, pipeline, hyperparameter_values, normalized_score, search_scores, pipelines_scored_dir, subpipelines_dir)

    assert base.mongo_client is not None

    base.mongo_client.aika.Searches.update_one(
        {
            '_id': search['_id'],
            'state': base.ProgressState.RUNNING.name,  # pylint: disable=no-member
        },
        {
            '$set': {
                'state': base.ProgressState.COMPLETED.name,  # pylint: disable=no-member
                'end': datetime.datetime.now(datetime.timezone.utc),
            },
        },
    )

    logger.info("Search completed.")


def _check_pipeline(search: typing.Dict, pipeline: pipeline_module.Pipeline, compute_digest: dataset_module.ComputeDigest, strict_digest: bool) -> None:
    """
    For a fully-specified pipeline we just have to check the pipeline that it is a valid
    pipeline given the inputs. The fully-specified pipeline does not have to be a standard
    pipeline and can have any inputs and outputs.

    Parameters
    ----------
    search:
        A ``aika.Searches`` MongoDB document.
    pipeline:
        A fully-specified pipeline instance.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    """

    # We load all inputs to be able to get their types.
    logger.info("Loading inputs.")
    inputs = [base.load_value(base.retrieve_value_in_ray(input), compute_digest=compute_digest, strict_digest=strict_digest) for input in search['inputs']]

    logger.info("Checking pipeline %(pipeline_id)s.", {'pipeline_id': pipeline.id})
    pipeline.check(
        allow_placeholders=False,
        standard_pipeline=False,
        input_types={'inputs.{i}'.format(i=i): type(input) for i, input in enumerate(inputs)},
    )

    # TODO: Do a proper hyper-parameter tuning for the free hyper-parameters in the provided pipeline.
    hyperparameter_values = _encode_hyperparameter_values(pipeline.get_free_hyperparams(), default_tuner.DefaultValuesPipelineTuner.get_default_values(pipeline.get_free_hyperparams()))

    solution_id = str(uuid.uuid4())

    assert base.mongo_client is not None

    base.mongo_client.aika.Solutions.insert_one(
        {
            '_id': solution_id,
            'search': {
                '_id': search['_id'],
            },
            'internal_score': None,
            'random_seed': None,
            'search_scores': [],
            'problem': base.encode_problem_description(search['problem']),
            'pipeline': pipeline.to_json_structure(nest_subpipelines=True),
            'hyperparameter_values': hyperparameter_values,
        },
    )

    base.mongo_client.aika.Searches.update_one(
        {
            '_id': search['_id'],
            'state': base.ProgressState.RUNNING.name,  # pylint: disable=no-member
        },
        {
            '$set': {
                'state': base.ProgressState.COMPLETED.name,  # pylint: disable=no-member
                'end': datetime.datetime.now(datetime.timezone.utc),
                'solutions': [
                    {
                        '_id': solution_id,
                        'internal_score': None,
                        'random_seed': None,
                        'search_scores': [],
                    },
                ],
            },
        },
    )

    logger.info("Pipeline %(pipeline_id)s checked.", {'pipeline_id': pipeline.id})


def _construct_steps(pipeline: pipeline_module.Pipeline, start_timestamp: datetime.datetime, end_timestamp: datetime.datetime) -> typing.Sequence:
    """
    Constructs fake ``steps`` value which has the correct structure based
    on the ``pipeline``, but timing information is always based on
    ``start_timestamp`` and ``end_timestamp``. To be stored in the MongoDB
    document.

    Parameters
    ----------
    pipeline:
        A pipeline instance for which to construct steps.
    start_timestamp:
        A timestamp when the whole pipeline run started.
    end_timestamp:
        A timestamp when the whole pipeline run finished.

    Returns
    -------
    A list of steps.
    """

    steps = []
    for step in pipeline.steps:
        step_dict = {
            'state': base.ProgressState.COMPLETED.name,  # pylint: disable=no-member
            'start': start_timestamp,
            'end': end_timestamp,
            'status': None,
            'steps': [],
        }

        if isinstance(step, pipeline_module.PrimitiveStep):
            steps.append(step_dict)
        elif isinstance(step, pipeline_module.SubpipelineStep):
            if step.pipeline is None:
                raise exceptions.InvalidStateError("Pipeline has not been resolved.")
            step_dict['steps'] = _construct_steps(typing.cast(pipeline_module.Pipeline, step.pipeline), start_timestamp, end_timestamp)
            steps.append(step_dict)
        elif isinstance(step, pipeline_module.PlaceholderStep):
            raise exceptions.UnexpectedValueError("Unexpected placeholder step.")
        else:
            raise exceptions.UnexpectedValueError("Unknown step type: {step_type}".format(step_type=type(step)))

    return steps


def _decode_hyperparameter_values(free_hyperparams: typing.Sequence, hyperparameter_values: typing.Sequence) -> typing.Sequence:
    """
    Given values encoded for JSON of hyper-parameters ``hyperparameter_values`` and possible free
    hyper-parameter instances ``free_hyperparams``, it decodes values into raw Python values.

    ``hyperparameter_values`` have a structure matching pipeline's steps (and sub-pipelines).
    ``free_hyperparams`` as well.

    The reason why hyper-parameter instances are needed is because we have to know
    for which hyper-parameter the value belongs to decode it properly.

    Parameters
    ----------
    free_hyperparams:
        A list of hyper-parameters configuration for free hyper-parameters for each step.
    hyperparameter_values:
        A list of dicts of hyper-parameter name to value (encoded for JSON) mappings for each step
        in the pipeline.

    Returns
    -------
    A list of dicts of hyper-parameter name to raw Python value mappings for each
    step in the pipeline.
    """

    hyperparams = []

    assert len(free_hyperparams) == len(hyperparameter_values), (len(free_hyperparams), len(hyperparameter_values))

    for free_hyperparams_for_step, hyperparameter_values_for_step in zip(free_hyperparams, hyperparameter_values):
        if isinstance(free_hyperparams_for_step, (dict, frozendict.frozendict)):
            assert isinstance(hyperparameter_values_for_step, (dict, frozendict.frozendict))

            values = {}
            for name, hyperparameter in free_hyperparams_for_step.items():
                values[name] = hyperparameter.value_from_json_structure(hyperparameter_values_for_step[name])

            hyperparams.append(values)
        elif isinstance(free_hyperparams_for_step, typing.Sequence):
            assert isinstance(hyperparameter_values_for_step, typing.Sequence)

            hyperparams.append(_decode_hyperparameter_values(free_hyperparams_for_step, hyperparameter_values_for_step))
        else:
            raise exceptions.UnexpectedValueError("Unknown hyper-parameters type: {hyperparams_type}".format(hyperparams_type=type(free_hyperparams_for_step)))

    return hyperparams


@ray.remote
@automl_task(task_logger=logger, collection_name='Searches')
def search_solutions(
    search_id: str,
    *,
    scratch_dir: str,
    volumes_dir: str = None,
    pipelines_scored_dir: str = None,
    subpipelines_dir: str = None,
    compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING,
    strict_resolving: bool = False,
    strict_digest: bool = False,
) -> None:
    """
    A Ray task for searching solutions (pipelines + hyper-parameters).

    Configuration of the task is stored in ``aika.Searches`` MongoDB collection under ``search_id`` ID.

    Parameters
    ----------
    search_id:
        ID of the configuration document.
    scratch_dir:
        Path to a directory to store any temporary files needed during run.
    volumes_dir:
        Path to a directory with static files required by primitives.
    pipelines_scored_dir:
        Where to store all successfully evaluated pipelines?
    subpipelines_dir:
        Where to store sub-pipelines? If not specified, a corresponding pipeline
        output directory is used for sub-pipelines as well.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_resolving:
        If resolved pipeline or primitive does not fully match specified reference, raise an exception?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    """

    assert base.mongo_client is not None

    result = base.mongo_client.aika.Searches.update_one(
        {
            '_id': search_id,
            'state': base.ProgressState.PENDING.name,  # pylint: disable=no-member
        },
        {
            '$set': {
                'state': base.ProgressState.RUNNING.name,  # pylint: disable=no-member
                'start': datetime.datetime.now(datetime.timezone.utc),
            },
        },
    )

    if not result.matched_count:
        return

    search = base.mongo_client.aika.Searches.find_one({'_id': search_id})

    if search is None:
        return

    search = base.decode_search_document(search)

    if search['template'] is not None:
        logger.info("Loading pipeline template.")
        # The pipeline had to be already resolved and fully nested and here we just load it, without allowing
        # any additional sub-pipelines resolving and expecting that primitives have not changed in meantime.
        template = pipeline_module.Pipeline.from_json_structure(
            search['template'],
            strict_resolving=True,
            respect_environment_variable=False,
            no_pipeline_resolving=True,
        )
    else:
        template = None

    if template is None or template.has_placeholder():
        # Do a search to fill placeholders.
        _search_solutions(search, template, strict_resolving, volumes_dir, scratch_dir, pipelines_scored_dir, subpipelines_dir, compute_digest, strict_digest)
    else:
        # A fully specified pipeline, let's check it.
        _check_pipeline(search, template, compute_digest, strict_digest)


# TODO: Expose all outputs.
@ray.remote
@automl_task(task_logger=logger, collection_name='FittedSolutions')
def fit_solution(
    request_id: str,
    *,
    scratch_dir: str,
    volumes_dir: str = None,
    compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING,
    strict_digest: bool = False,
) -> typing.Optional[runtime_module.Runtime]:
    """
    A Ray task for fitting a solution.

    Configuration of the task is stored in ``aika.FittedSolutions`` MongoDB collection under ``request_id`` ID.

    Parameters
    ----------
    request_id:
        ID of the configuration document.
    scratch_dir:
        Path to a directory to store any temporary files needed during run.
    volumes_dir:
        Path to a directory with static files required by primitives.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?

    Returns
    -------
    A fitted pipeline instance.
    """

    # pylint: disable=too-many-locals

    start_timestamp = datetime.datetime.now(datetime.timezone.utc)

    assert base.mongo_client is not None

    result = base.mongo_client.aika.FittedSolutions.update_one(
        {
            '_id': request_id,
            'state': base.ProgressState.PENDING.name,  # pylint: disable=no-member
        },
        {
            '$set': {
                'state': base.ProgressState.RUNNING.name,  # pylint: disable=no-member
                'start': start_timestamp,
            },
        },
    )

    if not result.matched_count:
        return None

    fitted_solution = base.mongo_client.aika.FittedSolutions.find_one({'_id': request_id})

    if fitted_solution is None:
        return None

    fitted_solution = base.decode_fitted_solution_document(fitted_solution)

    solution = base.mongo_client.aika.Solutions.find_one({'_id': fitted_solution['solution']['_id']})

    if solution is None:
        return None

    solution = base.decode_solution_document(solution)

    logger.info("Loading pipeline.")
    # The pipeline had to be already resolved and fully nested and here we just load it, without allowing
    # any additional sub-pipelines resolving and expecting that primitives have not changed in meantime.
    pipeline = pipeline_module.Pipeline.from_json_structure(
        solution['pipeline'],
        strict_resolving=True,
        respect_environment_variable=False,
        no_pipeline_resolving=True,
    )

    logger.info("Loading hyper-parameters.")
    hyperparameter_values = _decode_hyperparameter_values(pipeline.get_free_hyperparams(), solution['hyperparameter_values'])

    logger.info("Loading inputs.")
    inputs = [base.load_value(base.retrieve_value_in_ray(input), compute_digest=compute_digest, strict_digest=strict_digest) for input in fitted_solution['inputs']]

    # TODO: Correctly set context.
    runtime = runtime_module.Runtime(
        pipeline,
        hyperparameter_values,
        problem_description=solution['problem'],
        random_seed=fitted_solution['random_seed'],
        volumes_dir=volumes_dir,
        scratch_dir=scratch_dir,
        is_standard_pipeline=False,
        context=metadata_base.Context.TESTING,
        environment=pipelines_utils.get_runtime_environment(),
    )

    logger.info("Fitting.")
    result = runtime.fit(inputs, outputs_to_expose=fitted_solution['expose_outputs'])
    result.check_success()

    logger.info("Encoding exposed outputs.")
    exposed_outputs = []
    for data_reference, value in result.values.items():
        assert data_reference in fitted_solution['expose_outputs']
        data = base.store_value_in_ray(base.save_value(value, fitted_solution['expose_value_types'], scratch_dir))
        data['data'] = data_reference
        exposed_outputs.append(data)

    assert len(exposed_outputs) == len(fitted_solution['expose_outputs']), (exposed_outputs, fitted_solution['expose_outputs'])

    end_timestamp = datetime.datetime.now(datetime.timezone.utc)

    base.mongo_client.aika.FittedSolutions.update_one(
        {
            '_id': request_id,
            'state': base.ProgressState.RUNNING.name,  # pylint: disable=no-member
        },
        {
            '$set': {
                'state': base.ProgressState.COMPLETED.name,  # pylint: disable=no-member
                'end': end_timestamp,
                # TODO: Report real progress for every step.
                'steps': _construct_steps(pipeline, start_timestamp, end_timestamp),
                'exposed_outputs': exposed_outputs,
            },
        },
    )

    logger.info("Fitting completed.")

    return runtime


# TODO: Expose all outputs.
@ray.remote
@automl_task(task_logger=logger, collection_name='ProducedSolutions')
def produce_solution(request_id: str, *, scratch_dir: str, compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING, strict_digest: bool = False) -> None:
    """
    A Ray task for production a solution.

    Configuration of the task is stored in ``aika.ProducedSolutions`` MongoDB collection under ``request_id`` ID.

    Parameters
    ----------
    request_id:
        ID of the configuration document.
    scratch_dir:
        Path to a directory to store any temporary files needed during run.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    """

    # pylint: disable=too-many-locals

    start_timestamp = datetime.datetime.now(datetime.timezone.utc)

    assert base.mongo_client is not None

    result = base.mongo_client.aika.ProducedSolutions.update_one(
        {
            '_id': request_id,
            'state': base.ProgressState.PENDING.name,  # pylint: disable=no-member
        },
        {
            '$set': {
                'state': base.ProgressState.RUNNING.name,  # pylint: disable=no-member
                'start': start_timestamp,
            },
        },
    )

    if not result.matched_count:
        return

    produced_solution = base.mongo_client.aika.ProducedSolutions.find_one({'_id': request_id})

    if produced_solution is None:
        return

    produced_solution = base.decode_produced_solution_document(produced_solution)

    fitted_solution = base.mongo_client.aika.FittedSolutions.find_one({'_id': produced_solution['fitted_solution']['_id']})

    if fitted_solution is None:
        return

    fitted_solution = base.decode_fitted_solution_document(fitted_solution)

    if not fitted_solution['object_id']:
        raise exceptions.InvalidStateError("'object_id' field not available in the fitted solution document for request ID '{request_id}'.".format(request_id=request_id))

    # If fitting the solution failed, this will raise that exception as well.
    logger.info("Loading fitted pipeline.")
    fitted_pipeline = ray.get(utils.decode_object_id(fitted_solution['object_id']), timeout=base.DEFAULT_RAY_TIMEOUT)

    logger.info("Loading inputs.")
    inputs = [base.load_value(base.retrieve_value_in_ray(input), compute_digest=compute_digest, strict_digest=strict_digest) for input in produced_solution['inputs']]

    logger.info("Producing.")
    result = fitted_pipeline.produce(inputs, outputs_to_expose=produced_solution['expose_outputs'])
    result.check_success()

    logger.info("Encoding exposed outputs.")
    exposed_outputs = []
    for data_reference, value in result.values.items():
        assert data_reference in produced_solution['expose_outputs']
        data = base.store_value_in_ray(base.save_value(value, produced_solution['expose_value_types'], scratch_dir))
        data['data'] = data_reference
        exposed_outputs.append(data)

    assert len(exposed_outputs) == len(produced_solution['expose_outputs']), (exposed_outputs, produced_solution['expose_outputs'])

    end_timestamp = datetime.datetime.now(datetime.timezone.utc)

    base.mongo_client.aika.ProducedSolutions.update_one(
        {
            '_id': request_id,
            'state': base.ProgressState.RUNNING.name,  # pylint: disable=no-member
        },
        {
            '$set': {
                'state': base.ProgressState.COMPLETED.name,  # pylint: disable=no-member
                'end': end_timestamp,
                # TODO: Report real progress for every step.
                'steps': _construct_steps(fitted_pipeline.pipeline, start_timestamp, end_timestamp),
                'exposed_outputs': exposed_outputs,
            },
        },
    )

    logger.info("Producing completed.")


@ray.remote
@automl_task(task_logger=logger, collection_name='Scores')
def score_solution(
    request_id: str,
    *,
    scratch_dir: str,
    volumes_dir: str = None,
    compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING,
    strict_resolving: bool = False,
    strict_digest: bool = False,
) -> None:
    """
    A Ray task for scoring a solution.

    Configuration of the task is stored in ``aika.Scores`` MongoDB collection under ``request_id`` ID.

    Parameters
    ----------
    request_id:
        ID of the configuration document.
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    volumes_dir:
        Path to a directory with static files required by primitives.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_resolving:
        If resolved pipeline or primitive does not fully match specified reference, raise an exception?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    """

    # pylint: disable=too-many-locals,too-many-branches,too-many-statements

    start_timestamp = datetime.datetime.now(datetime.timezone.utc)

    assert base.mongo_client is not None

    result = base.mongo_client.aika.Scores.update_one(
        {
            '_id': request_id,
            'state': base.ProgressState.PENDING.name,  # pylint: disable=no-member
        },
        {
            '$set': {
                'state': base.ProgressState.RUNNING.name,  # pylint: disable=no-member
                'start': start_timestamp,
            },
        },
    )

    if not result.matched_count:
        return

    score = base.mongo_client.aika.Scores.find_one({'_id': request_id})

    if score is None:
        return

    score = base.decodes_score_document(score)

    solution = base.mongo_client.aika.Solutions.find_one({'_id': score['solution']['_id']})

    if solution is None:
        return

    solution = base.decode_solution_document(solution)

    metrics = score['metrics']

    base.validate_metrics(metrics, score['configuration']['method'])

    if score['configuration']['method'] == base.EvaluationMethod.RANKING:
        logger.info("Ranking.")

        # TODO: Do proper ranking.
        rank = 1 - solution['internal_score']
        normalized = solution['internal_score']

        end_timestamp = datetime.datetime.now(datetime.timezone.utc)

        base.mongo_client.aika.Scores.update_one(
            {
                '_id': request_id,
                'state': base.ProgressState.RUNNING.name,  # pylint: disable=no-member
            },
            {
                '$set': {
                    'state': base.ProgressState.COMPLETED.name,  # pylint: disable=no-member
                    'end': end_timestamp,
                    'scores': base.encode_scores(
                        [
                            {
                                'metric': {
                                    'metric': 'RANK',
                                    'params': {},
                                },
                                'value': rank,
                                'normalized': normalized,
                                # We use random seed from the solution because that one
                                # influenced our internal score which we used for rank.
                                'random_seed': solution['random_seed'],
                                'fold': 0,
                            },
                        ],
                    ),
                },
            },
        )

        return

    logger.info("Loading pipeline.")
    # The pipeline had to be already resolved and fully nested and here we just load it, without allowing
    # any additional sub-pipelines resolving and expecting that primitives have not changed in meantime.
    pipeline = pipeline_module.Pipeline.from_json_structure(
        solution['pipeline'],
        strict_resolving=True,
        respect_environment_variable=False,
        no_pipeline_resolving=True,
    )

    logger.info("Loading hyper-parameters.")
    hyperparameter_values = _decode_hyperparameter_values(pipeline.get_free_hyperparams(), solution['hyperparameter_values'])

    logger.info("Loading inputs.")
    inputs = [base.load_value(base.retrieve_value_in_ray(input), compute_digest=compute_digest, strict_digest=strict_digest) for input in score['inputs']]

    logger.info("Preparing scoring.")

    problem_description = solution['problem']

    # We see the following default data and scoring pipelines as part of the code of this task, just stored in
    # a pipeline form. So we do not respect command line arguments (which could modify pipeline search paths to
    # get these pipelines to resolve to something else; or require strict resolving). Non-default pipelines had
    # to be already resolved and fully nested and here we just load them, without allowing any additional
    # sub-pipelines resolving and expecting that primitives have not changed in meantime.

    pipeline_search_paths = pipelines_utils.get_internal_pipeline_search_paths()

    if score['configuration']['method'] == base.EvaluationMethod.HOLDOUT:
        if score['data_pipeline']:
            data_pipeline = pipeline_module.Pipeline.from_json_structure(
                score['data_pipeline'],
                strict_resolving=True,
                respect_environment_variable=False,
                no_pipeline_resolving=True,
            )
        else:
            data_pipeline = pipelines_utils.get_pipeline(
                'train-test-tabular-split',
                strict_resolving=strict_resolving,
                strict_digest=strict_digest,
                pipeline_search_paths=pipeline_search_paths,
                respect_environment_variable=False,
            )
        data_params = {
            # Values have to be JSON-serialized.
            'train_score_ratio': json.dumps(score['configuration']['train_test_ratio']),
            'shuffle': json.dumps(score['configuration']['shuffle']),
            'stratified': json.dumps(score['configuration']['stratified']),
        }
    elif score['configuration']['method'] == base.EvaluationMethod.K_FOLD:
        if score['data_pipeline']:
            data_pipeline = pipeline_module.Pipeline.from_json_structure(
                score['data_pipeline'],
                strict_resolving=True,
                respect_environment_variable=False,
                no_pipeline_resolving=True,
            )
        else:
            data_pipeline = pipelines_utils.get_pipeline(
                'k-fold-tabular-split',
                strict_resolving=strict_resolving,
                strict_digest=strict_digest,
                pipeline_search_paths=pipeline_search_paths,
                respect_environment_variable=False,
            )
        data_params = {
            # Values have to be JSON-serialized.
            'number_of_folds': json.dumps(score['configuration']['folds']),
            'shuffle': json.dumps(score['configuration']['shuffle']),
            'stratified': json.dumps(score['configuration']['stratified']),
        }
    else:
        raise exceptions.InvalidArgumentValueError("Unknown evaluation method: '{method}'.".format(method=score['configuration']['method']))

    if score['scoring_pipeline']:
        scoring_pipeline = pipeline_module.Pipeline.from_json_structure(
            score['scoring_pipeline'],
            strict_resolving=True,
            respect_environment_variable=False,
            no_pipeline_resolving=True,
        )
    else:
        scoring_pipeline = pipelines_utils.get_pipeline(
            runtime_module.DEFAULT_SCORING_PIPELINE_PATH,
            strict_resolving=strict_resolving,
            strict_digest=strict_digest,
            allow_pipeline_path=True,
            respect_environment_variable=False,
        )

    logger.info("Scoring.")
    # TODO: Update results as folds are finishing, do not need to wait for all folds to finish.
    # TODO: Run each fold in parallel.
    #       Handle then what happens if one of the folds dies but other do succeed.
    # TODO: Correctly set context.
    scores_list, results_list = runtime_module.evaluate(
        pipeline,
        inputs,
        data_pipeline=data_pipeline,
        scoring_pipeline=scoring_pipeline,
        problem_description=problem_description,
        data_params=data_params,
        metrics=metrics,
        hyperparams=hyperparameter_values,
        random_seed=score['random_seed'],
        data_random_seed=score['configuration']['random_seed'],
        scoring_random_seed=score['scoring_random_seed'],
        volumes_dir=volumes_dir,
        scratch_dir=scratch_dir,
        context=metadata_base.Context.TESTING,
        runtime_environment=pipelines_utils.get_runtime_environment(),
    )

    # TODO: Use "results_list" to save pipeline runs.
    results_list.check_success()

    assert scores_list
    scores = runtime_module.combine_folds(scores_list)

    output_scores = base.construct_scores(scores, metrics)

    end_timestamp = datetime.datetime.now(datetime.timezone.utc)

    base.mongo_client.aika.Scores.update_one(
        {
            '_id': request_id,
            'state': base.ProgressState.RUNNING.name,  # pylint: disable=no-member
        },
        {
            '$set': {
                'state': base.ProgressState.COMPLETED.name,  # pylint: disable=no-member
                'end': end_timestamp,
                'scores': base.encode_scores(output_scores),
            },
        },
    )

    logger.info("Scoring completed.")


@ray.remote
@automl_task(task_logger=logger)
def export_solution(solution_id: str, *, pipelines_ranked_dir: str, pipelines_scored_dir: str, subpipelines_dir: str, pipeline_rank: str = None) -> None:
    """
    A ray task for exporting a solution for evaluation purposes.

    Parameters
    ----------
    solution_id:
        A solution ID.
    pipelines_ranked_dir:
        A path to a directory to store ranked pipelines.
    pipelines_scored_dir:
        Where to store all successfully evaluated pipelines?
    subpipelines_dir:
        Where to store sub-pipelines?
    pipeline_rank:
        An unique pipeline rank to be set. If not provided, one is computed using (1 - internal score).
    """

    assert base.mongo_client is not None

    solution = base.mongo_client.aika.Solutions.find_one({'_id': solution_id})

    assert solution is not None

    solution = base.decode_solution_document(solution)

    # The pipeline had to be already resolved and fully nested and here we just load it, without allowing
    # any additional sub-pipelines resolving and expecting that primitives have not changed in meantime.
    pipeline = pipeline_module.Pipeline.from_json_structure(
        solution['pipeline'],
        strict_resolving=True,
        respect_environment_variable=False,
        no_pipeline_resolving=True,
    )

    if pipeline_rank is None:
        # TODO: Make sure that pipeline rank is unique for all exported solutions.
        pipeline_rank = 1 - solution['internal_score']

    # When exporting a pipeline we should export it under the solution ID and not pipeline ID.
    pipeline.id = solution['_id']
    pipeline.export_to_json(pipelines_ranked_dir, subpipelines_dir, pipeline_rank=pipeline_rank)  # pylint: disable=no-member

    try:
        # Every time a solution is found, its pipeline is stored into a "pipelines_scored_dir" directory.
        # When we export a solution we should remove it from "pipelines_scored_dir" because it should
        # be only in one place. It was exported there under solution ID as well.
        os.remove(os.path.join(pipelines_scored_dir, '{solution_id}.json'.format(solution_id=solution['_id'])))
    except FileNotFoundError:
        pass
