import atexit
import copy
import datetime
import faulthandler
import logging
import os
import queue as queue_module
import signal
import socket
import subprocess
import sys
import tempfile
import threading
import time
import typing
from logging import handlers

import prctl
import pymongo
import ray
from bson import objectid
from ray import ray_constants

# Non-module imports are made to make things available directly from this module.
from d3m import utils as d3m_utils
from d3m.container import dataset as dataset_module, list as list_module, numpy as numpy_module, pandas as pandas_module
from d3m.metadata import problem as problem_module
from d3m_automl_rpc import utils as d3m_automl_rpc_utils
from d3m_automl_rpc.utils import ValueType  # pylint: disable=import-only-modules,unused-import

from aika import datasets, primitives, problems
from aika.utils import exceptions, logging as logging_utils, resolver as resolver_utils

from . import utils

try:
    from pip.utils import logging as pip_logging
except ModuleNotFoundError:
    from pip._internal.utils import logging as pip_logging

logger = logging.getLogger(__name__)

RAY_ADDRESS = '127.0.0.1:6379'
# Used when requesting ObjectIDs which should have already been available, but we still
# leave a bit of a buffer just case. In any case, we do not want to block indefinitely
# if there is a bug or something.
DEFAULT_RAY_TIMEOUT = 1.0  # seconds

# No limit on size.
logging_queue: queue_module.Queue = queue_module.Queue()  # pylint: disable=invalid-name

mongo_client = None  # pylint: disable=invalid-name
_mongo_client_lock = threading.Lock()  # pylint: disable=invalid-name

initialized = False  # pylint: disable=invalid-name
_initialized_lock = threading.Lock()  # pylint: disable=invalid-name
initialized_worker = False  # pylint: disable=invalid-name
_initialized_worker_lock = threading.Lock()  # pylint: disable=invalid-name
mongodb_started = False  # pylint: disable=invalid-name
_mongodb_started_lock = threading.Lock()  # pylint: disable=invalid-name
serializers_registered = False  # pylint: disable=invalid-name
_serializers_registered_lock = threading.Lock()  # pylint: disable=invalid-name
logging_prepared = False  # pylint: disable=invalid-name
_logging_prepared_lock = threading.Lock()  # pylint: disable=invalid-name
logging_redirected = False  # pylint: disable=invalid-name
_logging_redirected_lock = threading.Lock()  # pylint: disable=invalid-name


class ProgressState(d3m_utils.Enum):
    """
    Enumeration of possible progress states.

    Values are kept in sync with TA2-TA3 API's ``ProgressState`` enumeration.
    """

    PENDING = 1
    RUNNING = 2
    COMPLETED = 3
    ERRORED = 4


class EvaluationMethod(d3m_utils.Enum):
    """
    Enumeration of supported evaluation methods.
    """

    HOLDOUT = 1
    K_FOLD = 2
    RANKING = 3


def _stop_mongodb(process: subprocess.Popen) -> None:
    """
    Attempts to gracefully stop the MongoDB process. Called when Python process is about to exit.

    If it does not succeed it is OK, because we also sets "pdeathsig" for MongoDB process to be killed
    # if Python process dies for any reason.

    Parameters
    ----------
    process:
        A process instance for MongoDB.
    """

    process.poll()

    if process.returncode is not None:
        return

    process.terminate()

    try:
        # Give one second for MongoDB to terminate, then continue with Python process termination.
        process.wait(timeout=1)
    except subprocess.TimeoutExpired:
        pass


def start_mongodb() -> None:
    """
    Starts MongoDB in the background and configures it so that it is terminated
    once this process finishes.

    It does not restart MongoDB if it fails. This should be used only during development.

    Moreover, we do not reap the MongoDB zombie process until the end of this process itself.
    This is mostly because we do not know how to reap only the MongoDB process without
    interfering with subprocess handling by any other code loaded (for example, setting our
    own SIGCHLD handler might interfere with existing code).
    """

    global mongodb_started  # pylint: disable=invalid-name,global-statement

    with _mongodb_started_lock:
        if mongodb_started:
            return

        logger.info("Starting MongoDB.")

        log_file = open('/var/log/mongodb/current', 'a')

        process = subprocess.Popen(  # pylint: disable=subprocess-popen-preexec-fn,multi-line-arguments-indentation
            ['/etc/service/mongodb/run'],
            stdin=subprocess.DEVNULL,
            stdout=log_file,
            stderr=log_file,
            encoding='utf8',
            # Setting "pdeathsig" will make the MongoDB process be killed if our process dies for any reason.
            preexec_fn=lambda: prctl.set_pdeathsig(signal.SIGKILL),  # pylint: disable=no-member
        )

        atexit.register(_stop_mongodb, process)

        mongodb_started = True


def connect_mongodb() -> None:
    """
    Connects to MongoDB database and sets ``mongo_client`` global variable.

    It waits until the database is ready.
    """

    global mongo_client  # pylint: disable=invalid-name,global-statement

    with _mongo_client_lock:
        if mongo_client is not None:
            return

        utils.wait_for_mongo()

        logger.info("Connecting to MongoDB.")

        # We use non-localhost hostname so that we do not connect to MongoDB while replica set is being setup.
        mongo_client = pymongo.MongoClient(host=socket.gethostname(), tz_aware=True, replicaSet='aika')


# We require "class_id" so that it is consistent across all workers.
def _register_serializer_locally(cls: typing.Type, class_id: str, serializer: typing.Callable = None, deserializer: typing.Callable = None) -> None:
    context = ray.worker.global_worker.get_serialization_context()
    context.register_custom_serializer(cls, serializer, deserializer, class_id=class_id, local=True)


def register_serializers_locally(worker: ray.worker.Worker) -> None:
    """
    Registers serializers for D3M objects on this driver or worker.
    """

    global serializers_registered  # pylint: disable=invalid-name,global-statement

    with _serializers_registered_lock:
        if serializers_registered:
            return

        logger.info("Registering serializers.")

        _register_serializer_locally(list_module.List, class_id='d3m.list', serializer=list_module.list_serializer, deserializer=list_module.list_deserializer)
        _register_serializer_locally(dataset_module.Dataset, class_id='d3m.dataset', serializer=dataset_module.dataset_serializer, deserializer=dataset_module.dataset_deserializer)
        _register_serializer_locally(numpy_module.ndarray, class_id='d3m.ndarray', serializer=numpy_module.ndarray_serializer, deserializer=numpy_module.ndarray_deserializer)
        # Ignoring pylint errors. See: https://github.com/PyCQA/pylint/issues/2392
        _register_serializer_locally(
            pandas_module.DataFrame,
            class_id='d3m.dataframe',
            serializer=pandas_module.dataframe_serializer,
            deserializer=pandas_module.dataframe_deserializer,
        )  # pylint: disable=no-member
        _register_serializer_locally(problem_module.Problem, class_id='d3m.problem', serializer=problem_module.problem_serializer, deserializer=problem_module.problem_deserializer)

        serializers_registered = True


def prepare_logging() -> None:
    """
    Configure logging and prepare redirection to MongoDB.

    We use a queue to collect logging information first, both to allow logging to be collected
    before we are connected to MongoDB, but also to not delay anything by waiting for logging to
    be stored into MongoDB.
    """

    global logging_prepared  # pylint: disable=invalid-name,global-statement
    global logging_queue  # pylint: disable=invalid-name,global-statement

    with _logging_prepared_lock:
        if logging_prepared:
            return

        logger.info("Preparing logging.")

        # Redirect all stdout and stderr.
        d3m_utils.redirect_to_logging().__enter__()

        queue_handler = utils.RawQueueHandler(logging_queue)
        queue_handler.addFilter(utils.RayContextFilter())
        queue_handler.setLevel('DEBUG')

        root = logging.getLogger()
        root.addHandler(queue_handler)

        logging_prepared = True


def redirect_logging() -> None:
    """
    After logging has been configured and redirected to a queue, here
    we redirect the queue to a MongoDB database for storage.

    This also adds to MongoDB any pending records in the queue which
    might be there already.
    """

    global logging_redirected  # pylint: disable=invalid-name,global-statement
    global logging_queue  # pylint: disable=invalid-name,global-statement

    with _logging_redirected_lock:
        if logging_redirected:
            return

        logger.info("Redirecting logging.")

        handler = utils.MongoDBHandler(mongo_client)
        listener = handlers.QueueListener(logging_queue, handler)
        listener.start()

        atexit.register(listener.stop)

        logging_redirected = True


def output_logging(start_timestamp: datetime.datetime, stream: typing.TextIO) -> None:
    """
    Fetches log records from workers from MongoDB and displays them
    to stderr formatted using Python logging.

    Parameters
    ----------
    start_timestamp:
        A timestamp from when to fetch log records from MongoDb.
    stream:
        An IO stream to use to output.
    """

    global mongo_client  # pylint: disable=invalid-name,global-statement

    assert mongo_client is not None

    console_level = logging_utils.get_console_handler().level

    # In MongoDB, ObjectIds contain a timestamp. So we can use it to select only
    # records newer than "start_timestamp".
    start_id = objectid.ObjectId.from_datetime(start_timestamp)

    query = {
        '$or': [
            {
                '_id': {'$gte': start_id},
                'record.worker_mode': ray.worker.WORKER_MODE,
                'record.levelno': {'$gte': console_level},
            },
            {
                'fullDocument._id': {'$gte': start_id},
                'fullDocument.record.worker_mode': ray.worker.WORKER_MODE,
                'fullDocument.record.levelno': {'$gte': console_level},
            },
        ],
    }

    handler = pip_logging.ColorizedStreamHandler(stream)
    formatter = resolver_utils.from_path(logging_utils.FORMATTERS['worker']['class'])
    handler.setFormatter(formatter(logging_utils.FORMATTERS['worker']['format']))

    for document in utils.find_and_watch_only_new(mongo_client.aika.Logs, query):
        record = logging.makeLogRecord(document['record'])
        handler.emit(record)


def init(
    client_only: bool = False,
    *,
    number_of_cpus: int = None,
    number_of_gpus: int = None,
    object_store_memory_limit: int = None,
    plasma_dir: str = None,
    huge_pages: bool = False,
    plasma_socket: str = None,
    dataset_search_paths: typing.Sequence[str] = None,
) -> None:
    """
    Initialize AutoML system.

    If ``client_only`` is ``False`` it starts MongoDB and Ray in the background,
    waits for the connection to MongoDB, establishes the connection, and initializes Ray.
    If ``client_only`` is ``True`` it just connects to and existing MongoDB and
    Ray services.

    When ``client_only`` is ``False``, MongoDB and Ray are not restarted if they fail.
    This should be used only during development.

    Parameters
    ----------
    client_only:
        Only connect as a client to services like MongoDB and Ray and do not start them in the background.
    number_of_cpus:
        Number of CPUs to use by Ray when starting it in the background (``client_only`` set to ``False``).
    number_of_gpus:
        Number of GPUs to use by Ray when starting it in the background (``client_only`` set to ``False``).
    object_store_memory_limit:
        Memory limit in bytes to use by Ray object store when starting it in the background (``client_only`` set to ``False``).
    plasma_dir:
        Path to a Plasma directory to use by Ray when starting it in the background (``client_only`` set to ``False``).
    huge_pages:
        Does Plasma directory support huge pages?
    plasma_socket:
        Path to a Plasma socket to use by Ray when starting in the background (``client_only`` set to ``False``).
    dataset_search_paths:
        Directory paths with datasets (and problem descriptions).
    """

    global initialized  # pylint: disable=invalid-name,global-statement

    with _initialized_lock:
        if initialized:
            return

        if not faulthandler.is_enabled():
            # Setup fault handler before modifying "sys.stderr". Use the original stderr to make sure we have a valid file descriptor.
            faulthandler.enable(file=sys.__stderr__, all_threads=False)

        start_timestamp = datetime.datetime.now(datetime.timezone.utc)

        prepare_logging()

        if dataset_search_paths is not None:
            datasets.store.set_directories(dataset_search_paths)
            problems.store.set_directories(dataset_search_paths)

        if client_only:
            logger.info("Initializing client only.")
        else:
            logger.info("Initializing with services.")

        ray_args = {
            # This is the default password for Ray.
            'redis_password': ray_constants.REDIS_DEFAULT_PASSWORD,
            # We configure logging ourselves.
            'configure_logging': False,
            # We also redirect all logging ourselves.
            'log_to_driver': False,
            # We do not really use dynamic functions, all our workers should have the same code.
            # We had many issues with Ray cloud-pickling and distributing functions to workers
            # in the past, so this should hopefully address those issues for good.
            'load_code_from_local': True,
        }

        if client_only:
            logger.info("Connecting to Ray.")

            ray.init(address=RAY_ADDRESS, **ray_args)
        else:
            # We start MongoDB and Ray.
            start_mongodb()

            ray_args.update(
                {
                    'webui_host': '0.0.0.0',
                    'num_cpus': number_of_cpus,
                    'num_gpus': number_of_gpus,
                    'object_store_memory': object_store_memory_limit,
                    # TODO: Uncomment. Currently it does not work.
                    #       See: https://github.com/ray-project/ray/issues/4641
                    # 'temp_dir': scratch_dir,
                    'plasma_directory': plasma_dir,
                    'huge_pages': huge_pages,
                    'plasma_store_socket_name': plasma_socket,
                },
            )

            logger.info("Starting Ray.")
            ray.init(**ray_args)

        worker = ray.worker.global_worker

        # This should be called only on the driver.
        assert worker.mode != ray.worker.WORKER_MODE

        # We share temp directory with all workers to assure temporary paths between workers and the driver match.
        worker.redis_client.set(b'AIKA_TEMP_DIR', tempfile.gettempdir().encode('utf8'))
        # "init_worker" is called both on workers and the driver.
        worker.run_function_on_all_workers(init_worker)

        # Making the thread a daemon causes it to exit when the main thread exits.
        threading.Thread(target=output_logging, name='MongoDBLogging', args=(start_timestamp, sys.__stderr__), daemon=True).start()

        # We sleep a bit for initialization to get to all workers.
        time.sleep(1)

        logger.info("Initialization done.")

        initialized = True


def init_worker(worker_info: typing.Dict = None) -> None:
    """
    Initializes AutoML worker.

    Despite the name, this is called both on workers and the driver.

    Parameters
    ----------
    worker_info:
        A dict with ``worker`` key.
    """

    global initialized_worker  # pylint: disable=invalid-name,global-statement

    with _initialized_worker_lock:
        if initialized_worker:
            assert mongo_client is not None
            return

        # We have already called "prepare_logging" on the driver, but it will not do anything the second time,
        # so we can call it again so that on workers we call both.
        prepare_logging()

        if worker_info is not None:
            worker = worker_info['worker']
        else:
            worker = ray.worker.global_worker

        if worker.mode == ray.worker.WORKER_MODE:
            logger.info("Initializing the worker.")
        else:
            logger.info("Initializing the driver.")

        register_serializers_locally(worker)

        # We share temp directory between all workers and the driver to assure temporary paths match.
        # On the driver this should not change anything.
        tempfile.tempdir = worker.redis_client.get(b'AIKA_TEMP_DIR').decode('utf8')

        connect_mongodb()

        assert mongo_client is not None

        redirect_logging()

        if worker.mode == ray.worker.WORKER_MODE:
            # We set all worker processes to be the most likely to be killed by OOM.
            with open('/proc/{pid}/oom_score_adj'.format(pid=os.getpid()), 'w') as oom_score_adj_file:
                oom_score_adj_file.write('1000')

            # We preload all primitives on workers. This means that the first task does not have this hit.
            primitives.store.populate()

            logger.info("Initialization of the worker done.")
        else:
            logger.info("Initialization of the driver done.")

        initialized_worker = True


# TODO: Maybe some small values we could encode directly into a dict (pickle it, or even represent it in a MongoDB-compatible way).
# TODO: Ray objects are not persistent. This means that values stored into MongoDB cannot be fully retrieved at a later date.
#       This might not be a problem for exposed output values, but it might be for storing into documents inputs
#       to the pipeline (for what we currently also use this function). It is true that in most cases inputs are
#       "Dataset" objects which would use "dataset_uri" value type to reference them. This does open another issue
#       which is that "dataset_uri" also do not necessary transfer between runs and systems. We should probably store
#       dataset IDs in that case and resolve them using datasets store.
def store_value_in_ray(value: typing.Dict) -> typing.Dict:
    """
    It stores values of type ``object`` into Ray and returns an updated dict
    representing the value. For those ``value`` becomes a hex-encoded Ray's ``ObjectID``.
    Other values are left as-is.

    The input is a dict representation of the value at the *intermediate level* and
    the resulting dict is at the *MongoDB-compatible level*.

    Parameters
    ----------
    value:
        A dict with ``type`` and ``value`` fields. ``type`` can be one of
        ``object``, ``dataset_uri``, ``csv_uri``, ``pickle_uri``, ``plasma_id``, ``error``.
        ``value`` is then a corresponding value for a given type.

    Returns
    -------
    A dict with ``type`` and ``value`` fields. ``type`` can be one of
    ``object_id``, ``dataset_uri``, ``csv_uri``, ``pickle_uri``, ``plasma_id``, ``error``.
    ``value`` is then a corresponding value for a given type.
    """

    assert value['type'] in ['object', 'dataset_uri', 'csv_uri', 'pickle_uri', 'plasma_id', 'error']

    if value['type'] == 'object':
        obj_id = ray.put(value['value'])
        return {
            'type': 'object_id',
            'value': utils.encode_object_id(obj_id),
        }

    return value


def retrieve_value_in_ray(value: typing.Dict) -> typing.Dict:
    """
    It retrieve values of type ``object`` from Ray. For those values it retrieves values using
    a hex-encoded Ray's ``ObjectID`` and replaces ``value`` values with values themselves.
    Other values are left as-is.

    The input is a dict representation of the value at the *MongoDB-compatible level* and
    the resulting dict is at the *intermediate level*.

    Parameters
    ----------
    value:
        A dict with ``type`` and ``value`` fields. ``type`` can be one of
        ``object_id``, ``dataset_uri``, ``csv_uri``, ``pickle_uri``, ``plasma_id``, ``error``.
        ``value`` is then a corresponding value for a given type.

    Returns
    -------
    A dict with ``type`` and ``value`` fields. ``type`` can be one of
    ``object``, ``dataset_uri``, ``csv_uri``, ``pickle_uri``, ``plasma_id``, ``error``.
    ``value`` is then a corresponding value for a given type.
    """

    assert value['type'] in ['object_id', 'dataset_uri', 'csv_uri', 'pickle_uri', 'plasma_id', 'error']

    if value['type'] == 'object_id':
        obj = ray.get(utils.decode_object_id(value['value']), timeout=DEFAULT_RAY_TIMEOUT)
        return {
            'type': 'object',
            'value': obj,
        }

    return value


def plasma_put(value: typing.Any) -> bytes:
    """
    Stores a value into the Plasma store.

    Parameters
    ----------
    value:
        The value to be stored.

    Returns
    -------
    Stored object's ID.
    """

    object_id = ray.put(value)
    return ray.cloudpickle.dumps(object_id)


def plasma_get(object_id: bytes) -> typing.Any:
    """
    Retrieves the value from the Plasma store.

    Parameters
    ----------
    object_id:
        Stored object's ID.

    Returns
    -------
    The stored value.
    """

    return ray.get(ray.cloudpickle.loads(object_id), timeout=DEFAULT_RAY_TIMEOUT)


def load_value(value: typing.Dict, *, compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING, strict_digest: bool = False) -> typing.Any:
    """
    Loads and returns a raw Python value from a dict representing a value
    at the *intermediate level*.

    Parameters
    ----------
    value:
        A dict with ``type`` and ``value`` fields. ``type`` can be one of
        ``object``, ``dataset_uri``, ``csv_uri``, ``pickle_uri``, ``plasma_id``, ``error``.
        ``value`` is then a corresponding value for a given type.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?

    Returns
    -------
    Loaded raw Python value.
    """

    return d3m_automl_rpc_utils.load_value(value, plasma_get=plasma_get, compute_digest=compute_digest, strict_digest=strict_digest)


def save_value(value: typing.Any, allowed_value_types: typing.Sequence[ValueType], scratch_dir: str, *, raise_error: bool = False) -> typing.Dict:
    """
    Saves a raw Python value and returns a dict representing the
    value at the *intermediate level*.

    It tries to save it based on allowed value types, potentially saving it to a disk
    and providing an URI to the location. It uses Python `tempfile` module to generate
    the location.

    Parameters
    ----------
    value:
        A value to save.
    allowed_value_types:
        A list of allowed value types to save this value as. This
        list is tried in order until encoding succeeds.
    scratch_dir:
         Path to a directory to store any temporary files needed during run.
    raise_error:
        If value cannot be encoded, should an exception be raised or
        should it be returned as value type ``error``?

    Returns
    -------
    A dict with ``type`` and ``value`` fields. ``type`` can be one of
    ``object``, ``dataset_uri``, ``csv_uri``, ``pickle_uri``, ``plasma_id``, ``error``.
    ``value`` is then a corresponding value for a given type.
    ``error`` value type is possible only if ``raise_error`` is ``False``.
    """

    return d3m_automl_rpc_utils.save_value(value, allowed_value_types, scratch_dir, plasma_put=plasma_put, raise_error=raise_error)


def encode_problem_description(problem_description: typing.Optional[problem_module.Problem]) -> typing.Optional[typing.Dict]:
    """
    Encodes problem description into a MongoDB-compatible encoding.

    Parameters
    ----------
    problem_description:
        Problem description to encode.

    Returns
    -------
    Encoded problem description.
    """

    if problem_description is None:
        return None

    problem_description_dict = dict(copy.deepcopy(problem_description))

    if 'problem' in problem_description_dict:
        if 'task_keywords' in problem_description_dict['problem']:
            problem_description_dict['problem']['task_keywords'] = [task_keyword.name for task_keyword in problem_description_dict['problem']['task_keywords']]

        if 'performance_metrics' in problem_description_dict['problem']:
            for performance_metric in problem_description_dict['problem']['performance_metrics']:
                if 'metric' in performance_metric:
                    # Only standard metrics are allowed here.
                    performance_metric['metric'] = performance_metric['metric'].name

    return problem_description_dict


def decode_problem_description(problem_description: typing.Optional[typing.Dict]) -> typing.Optional[problem_module.Problem]:
    """
    Decodes problem description from a MongoDB-compatible encoding.

    Parameters
    ----------
    problem_description:
        Problem description to decode.

    Returns
    -------
    Decoded problem description.
    """

    if problem_description is None:
        return None

    problem_description = copy.deepcopy(problem_description)

    if 'problem' in problem_description:
        if 'task_keywords' in problem_description['problem']:
            problem_description['problem']['task_keywords'] = [problem_module.TaskKeyword[task_keyword] for task_keyword in problem_description['problem']['task_keywords']]

        if 'performance_metrics' in problem_description['problem']:
            for performance_metric in problem_description['problem']['performance_metrics']:
                if 'metric' in performance_metric:
                    # Only standard metrics are allowed here.
                    performance_metric['metric'] = problem_module.PerformanceMetric[performance_metric['metric']]

    return problem_module.Problem(problem_description)


def encode_scores(scores: typing.Sequence[typing.Dict]) -> typing.Sequence[typing.Dict]:
    """
    Encodes ``scores`` list into a MongoDB-compatible encoding.

    Parameters
    ----------
    scores:
        Scores to encode.

    Returns
    -------
    Encoded scores.
    """

    scores = copy.deepcopy(scores)

    for score in scores:
        if 'metric' in score and 'metric' in score['metric']:
            # If it is a standard metric we encode it, otherwise we
            # assume custom metric and leave it as string.
            if isinstance(score['metric']['metric'], problem_module.PerformanceMetric):
                score['metric']['metric'] = score['metric']['metric'].name

    return scores


def decode_scores(scores: typing.Sequence[typing.Dict]) -> typing.Sequence[typing.Dict]:
    """
    Decodes ``scores`` list from a MongoDB-compatible encoding.

    Parameters
    ----------
    scores:
        Scores to decode.

    Returns
    -------
    Decoded scores.
    """

    scores = copy.deepcopy(scores)

    for score in scores:
        if 'metric' in score and 'metric' in score['metric']:
            try:
                score['metric']['metric'] = problem_module.PerformanceMetric[score['metric']['metric']]
            except KeyError:
                # We assume a custom metric.
                pass

    return scores


def encode_search_scores(search_scores: typing.Sequence[typing.Dict]) -> typing.Sequence[typing.Dict]:
    """
    Encodes ``search_scores`` list into a MongoDB-compatible encoding.

    Parameters
    ----------
    search_scores:
        Search scores to encode.

    Returns
    -------
    Encoded search scores.
    """

    search_scores = copy.deepcopy(search_scores)

    for search_score in search_scores:
        if 'configuration' in search_score and 'method' in search_score['configuration']:
            search_score['configuration']['method'] = search_score['configuration']['method'].name
        if 'scores' in search_score:
            search_score['scores'] = encode_scores(search_score['scores'])

    return search_scores


def decode_search_scores(search_scores: typing.Sequence[typing.Dict]) -> typing.Sequence[typing.Dict]:
    """
    Decodes ``search_scores`` list from a MongoDB-compatible encoding.

    Parameters
    ----------
    search_scores:
        Search scores to decode.

    Returns
    -------
    Decoded search scores.
    """

    search_scores = copy.deepcopy(search_scores)

    for search_score in search_scores:
        if 'configuration' in search_score and 'method' in search_score['configuration']:
            search_score['configuration']['method'] = EvaluationMethod[search_score['configuration']['method']]
        if 'scores' in search_score:
            search_score['scores'] = decode_scores(search_score['scores'])

    return search_scores


def decode_steps(steps: typing.Sequence[typing.Dict]) -> typing.Sequence[typing.Dict]:
    """
    Decodes ``steps`` list from a MongoDB-compatible encoding.

    Parameters
    ----------
    steps:
        Steps to decode.

    Returns
    -------
    Decoded steps.
    """

    steps = copy.deepcopy(steps)

    for step in steps:
        if 'state' in step:
            step['state'] = ProgressState[step['state']]

        if 'steps' in step:
            step['steps'] = decode_steps(step['steps'])

    return steps


def decode_search_document(document: typing.Dict) -> typing.Dict:
    """
    Decodes from a MongoDB-compatible encoding a document from ``Searches`` collection.

    Parameters
    ----------
    document:
        Document to decode.

    Returns
    -------
    Decoded document.
    """

    document = copy.deepcopy(document)

    if 'problem' in document:
        document['problem'] = decode_problem_description(document['problem'])

    if 'state' in document:
        document['state'] = ProgressState[document['state']]

    if 'solutions' in document:
        for solution in document['solutions']:
            if 'search_scores' in solution:
                solution['search_scores'] = decode_search_scores(solution['search_scores'])

    return document


def decode_solution_document(document: typing.Dict) -> typing.Dict:
    """
    Decodes from a MongoDB-compatible encoding a document from ``Solutions`` collection.

    Parameters
    ----------
    document:
        Document to decode.

    Returns
    -------
    Decoded document.
    """

    document = copy.deepcopy(document)

    if 'search_scores' in document:
        document['search_scores'] = decode_search_scores(document['search_scores'])

    if 'problem' in document:
        document['problem'] = decode_problem_description(document['problem'])

    return document


def decodes_score_document(document: typing.Dict) -> typing.Dict:
    """
    Decodes from a MongoDB-compatible encoding a document from ``Scores`` collection.

    Parameters
    ----------
    document:
        Document to decode.

    Returns
    -------
    Decoded document.
    """

    document = copy.deepcopy(document)

    if 'state' in document:
        document['state'] = ProgressState[document['state']]

    if 'metrics' in document:
        for metric in document['metrics']:
            if 'metric' in metric:
                try:
                    metric['metric'] = problem_module.PerformanceMetric[metric['metric']]
                except KeyError:
                    # We assume a custom metric.
                    pass

    if 'configuration' in document and 'method' in document['configuration']:
        document['configuration']['method'] = EvaluationMethod[document['configuration']['method']]

    if 'scores' in document:
        document['scores'] = decode_scores(document['scores'])

    return document


def decode_fitted_solution_document(document: typing.Dict) -> typing.Dict:
    """
    Decodes from a MongoDB-compatible encoding a document from ``FittedSolutions`` collection.

    Parameters
    ----------
    document:
        Document to decode.

    Returns
    -------
    Decode document.
    """

    document = copy.deepcopy(document)

    if 'state' in document:
        document['state'] = ProgressState[document['state']]

    if 'expose_value_types' in document:
        document['expose_value_types'] = [ValueType[expose_value_type] for expose_value_type in document['expose_value_types']]

    if 'steps' in document:
        document['steps'] = decode_steps(document['steps'])

    return document


def decode_produced_solution_document(document: typing.Dict) -> typing.Dict:
    """
    Decodes from a MongoDB-compatible encoding a document from ``ProducedSolutions`` collection.

    Parameters
    ----------
    document:
        Document to decode.

    Returns
    -------
    Decode document.
    """

    document = copy.deepcopy(document)

    if 'state' in document:
        document['state'] = ProgressState[document['state']]

    if 'expose_value_types' in document:
        document['expose_value_types'] = [ValueType[expose_value_type] for expose_value_type in document['expose_value_types']]

    if 'steps' in document:
        document['steps'] = decode_steps(document['steps'])

    return document


def validate_metrics(metrics: typing.Sequence[typing.Dict], evaluation_method: EvaluationMethod) -> None:
    """
    Validates that ``RANK`` metric is used only with ``RANKING`` evaluation method.

    Raises exception if validation fails.

    Parameters
    ----------
    metrics:
        A list of metrics to use. Dicts are with fields ``metric`` and ``params``,
        where ``metric`` is a ``PerformanceMetric`` enumeration value or a string
        if a custom metric.
    evaluation_method:
        Evaluation method to use.
    """

    if evaluation_method == EvaluationMethod.RANKING:
        for metric in metrics:
            if metric['metric'] != 'RANK':
                if isinstance(metric['metric'], problem_module.PerformanceMetric):
                    raise exceptions.InvalidArgumentValueError("Metric '{metric}' is not supported with 'RANKING' evaluation method.".format(metric=metric['metric'].name))
                else:
                    raise exceptions.InvalidArgumentValueError("Custom metric '{metric}' is not supported with 'RANKING' evaluation method.".format(metric=metric['metric']))
    else:
        for metric in metrics:
            if not isinstance(metric['metric'], problem_module.PerformanceMetric):
                if metric['metric'] == 'RANK':
                    raise exceptions.InvalidArgumentValueError("Metric 'RANK' is supported only with 'RANKING' evaluation method.")
                else:
                    raise exceptions.InvalidArgumentValueError("Custom metric '{metric}' is not supported.".format(metric=metric['metric']))


def _get_metric_description(metric: str, performance_metrics: typing.Sequence[typing.Dict]) -> typing.Dict:
    """
    Returns a metric description from a list of them, given metric.

    Parameters
    ----------
    metric:
        A metric name.
    performance_metrics:
        A list of performance metric descriptions requested for scoring.

    Returns
    -------
    A metric description.
    """

    for performance_metric in performance_metrics:
        if performance_metric['metric'] == metric:
            return {
                'metric': performance_metric['metric'],
                'params': performance_metric.get('params', {}),
            }

    raise KeyError("Cannot find metric '{metric}' among those defined in the problem description.".format(metric=metric))


def construct_scores(scores: pandas_module.DataFrame, metrics: typing.Sequence[typing.Dict]) -> typing.Sequence[typing.Dict]:
    """
    Constructs scores description.

    Parameters
    ----------
    scores:
        A table with ``metric``, ``value``, ``normalized``, ``randomSeed``, and ``fold`` columns.
    metrics:
        A list of performance metric descriptions used.

    Returns
    -------
    A list of dicts describing scores.
    """

    output_scores = []
    for metric, value, normalized, random_seed, fold in scores.loc[:, ['metric', 'value', 'normalized', 'randomSeed', 'fold']].itertuples(index=False, name=None):
        output_scores.append(
            {
                'metric': _get_metric_description(metric, metrics),
                'value': value,
                'normalized': normalized,
                'random_seed': random_seed,
                'fold': fold,
            },
        )

    return output_scores
