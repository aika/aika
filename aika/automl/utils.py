import contextlib
import logging
import socket
import sys
import threading
import time
import traceback as traceback_module
import types
import typing
from logging import handlers

import bson
import pymongo
import ray
from pymongo import collection as collection_module

from d3m import utils as d3m_utils

from aika.utils import encode as encode_utils

logger = logging.getLogger(__name__)

ray_context = threading.local()  # pylint: disable=invalid-name
# A function name of the currently running Ray task, if any.
ray_context.task_name = None


class RayContextFilter(logging.Filter):
    """
    Adds additional context to the logging record.
    """

    def filter(self, record: logging.LogRecord) -> bool:
        """
        We use the filter to modify the record.

        Parameters
        ----------
        record:
            A record to add fields to.

        Returns
        -------
        We return ``True`` to not filter the record out.
        """

        worker = ray.worker.global_worker
        if worker:
            worker_id = getattr(worker, 'worker_id', None)
            if worker_id:  # pylint: disable=using-constant-test
                record.worker_id = encode_utils.binary_to_hex(worker_id)  # type: ignore
            worker_mode = getattr(worker, 'mode', None)
            if worker_mode is not None:
                record.worker_mode = worker_mode  # type: ignore

        if getattr(ray_context, 'task_name', None):
            record.task_name = ray_context.task_name  # type: ignore

        record.hostname = socket.gethostname()  # type: ignore

        return True


class set_ray_context(contextlib.AbstractContextManager):  # pylint: disable=invalid-name
    """
    A Python context manager which sets ``ray_context.object_ids`` and
    ``ray_context.task_name`` variables to new values and then restores them
     back to the original values on exit.
    """

    def __init__(self, task_name: str) -> None:
        self._task_name = task_name
        self._old_task_name: typing.List[str] = []

    def __enter__(self) -> None:
        self._old_task_name.append(getattr(ray_context, 'task_name', None))
        ray_context.task_name = self._task_name

    def __exit__(  # pylint: disable=useless-return
        self,
        exc_type: typing.Optional[typing.Type[BaseException]],
        exc_value: typing.Optional[BaseException],
        traceback: typing.Optional[types.TracebackType],
    ) -> typing.Optional[bool]:
        ray_context.task_name = self._old_task_name.pop()

        return None


class MongoDBHandler(logging.Handler):
    """
    Stores logging records into a MongoDB database.

    It tries to store them mostly as they are, all attributes of the `LogRecord` object,
    but removes or converts those attributes which cannot be directly stored into MongoDB.
    """

    def __init__(self, mongo_client: pymongo.MongoClient) -> None:
        super().__init__(logging.DEBUG)

        self._mongo_client = mongo_client

    def emit(self, record: logging.LogRecord) -> None:
        """
        Stores a record into MongoDB.

        Parameters
        ----------
        record:
            A record to store into MongoDB.
        """

        document = {'record': record.__dict__}

        try:
            # Exceptions cannot be directly stored into MongoDB.
            if 'exc_info' in document['record']:
                if document['record']['exc_info']:
                    if isinstance(document['record']['exc_info'], BaseException):
                        document['record']['exc_type'] = d3m_utils.type_to_str(type(document['record']['exc_info']))
                    else:
                        document['record']['exc_type'] = d3m_utils.type_to_str(type(document['record']['exc_info'][1]))
                del document['record']['exc_info']

            try:
                self._mongo_client.aika.Logs.insert_one(document)
                return
            except bson.InvalidDocument:
                pass

            # Probably arguments cannot be directly stored into MongoDB. Remove them and retry.
            if 'args' in document['record']:
                del document['record']['args']

            self._mongo_client.aika.Logs.insert_one(document)

        except Exception:  # pylint: disable=broad-except
            print("Unable to insert log record to MongoDB: {document}".format(document=document), file=sys.__stderr__)  # noqa: L202
            traceback_module.print_exc(file=sys.__stderr__)


class RawQueueHandler(handlers.QueueHandler):
    """
    Stores logging records into a queue as they are without any conversion
    except for formatting the logging message and adding it to the record object.
    """

    def prepare(self, record: logging.LogRecord) -> logging.LogRecord:
        """
        Leave record as-is but format the logging message.

        Parameters
        ----------
        record:
            A record to format the message for.

        Returns
        -------
        A record to store into a queue.
        """

        self.format(record)
        return record


def wait_for_mongo() -> None:
    """
    Waits (blocks) for MongoDB to be available.

    It waits for the replica set to be configured (which happens at first start of MongoDB) and that the
    server to which we are connected is primary (accepts writes).
    """

    logger.info("Checking MongoDB.")

    # We use non-localhost hostname so that we do not connect to MongoDB while replica set is being setup.
    client = pymongo.MongoClient(host=socket.gethostname(), socketTimeoutMS=5000, connectTimeoutMS=5000, serverSelectionTimeoutMS=5000, connect=False, replicaSet='aika')

    while True:
        try:
            # 1 == PRIMARY replica set member state.
            # See: https://docs.mongodb.com/manual/reference/replica-states/
            if client.admin.command('replSetGetStatus').get('myState', None) == 1:
                break
        except Exception:  # pylint: disable=broad-except
            logger.info("Waiting for MongoDB.")
            time.sleep(0.1)

    client.close()

    logger.info("MongoDB ready.")


def find_and_watch(
    collection: collection_module.Collection,
    query: typing.Dict,
    has_completed: typing.Callable,
    decoder: typing.Callable = None,
    no_initial_documents: typing.Callable = None,
) -> typing.Generator[typing.Dict, None, None]:
    """
    First finds and yields all documents from ``collection`` matching ``query``
    and then blocks and returns new and updated documents matching ``query``.
    For every new or updated document, ``end_condition`` is called and if
    it returns true value, this functions returns.

    ``query`` should be written in a way that it matches both stand-alone documents
    and also change events event documents.

    If no documents match initially, ``no_initial_documents`` is called.

    Parameters
    ----------
    collection:
        Collection on which to do a query.
    query:
        A query to do.
    has_completed:
        A function which should return a true value when this function should return.
    decoder:
        A function which decodes every document before yielding it.
    no_initial_documents:
        A function which is called when there are no documents initially.

    Yields
    ------
    First yields found documents and then updated documents at every change.
    """

    seen_documents = {}

    # We first start watching for changes.
    with collection.watch([{'$match': query}], full_document='updateLookup') as stream:
        completed = False

        # But we then first return existing documents.
        for document in collection.find(query):
            if decoder is not None:
                document = decoder(document)

            seen_documents[document['_id']] = document
            completed = completed or has_completed(seen_documents, document)
            yield document

        if completed:
            return

        if no_initial_documents is not None and not seen_documents:
            no_initial_documents()

        for document_change in stream:
            document_id = document_change['documentKey']['_id']
            document = document_change.get('fullDocument', None)

            if document is not None and decoder is not None:
                document = decoder(document)

            if seen_documents.get(document_id, None) == document:
                continue

            seen_documents[document_id] = document
            completed = completed or has_completed(seen_documents, document)

            if document is not None:
                yield document

            if completed:
                return


def find_and_watch_only_new(collection: collection_module.Collection, query: typing.Dict, decoder: typing.Callable = None) -> typing.Generator[typing.Dict, None, None]:
    """
    First finds and yields all documents from ``collection`` matching ``query``
    and then blocks and returns new documents matching ``query``.

    ``query`` should be written in a way that it matches both stand-alone documents
    and also change events event documents.

    Parameters
    ----------
    collection:
        Collection on which to do a query.
    query:
        A query to do.
    decoder:
        A function which decodes every document before yielding it.

    Yields
    ------
    First yields found documents and then inserted documents as they are inserted.
    """

    initial_ids = set()

    # We first start watching for changes.
    with collection.watch([{'$match': {'operationType': 'insert'}}, {'$match': query}]) as stream:
        # But we then first return existing documents.
        for document in collection.find(query):
            if decoder is not None:
                document = decoder(document)

            initial_ids.add(document['_id'])

            yield document

        for document_change in stream:
            document = document_change['fullDocument']

            if decoder is not None:
                document = decoder(document)

            if document['_id'] in initial_ids:
                continue

            yield document


def encode_object_id(object_id: ray.ObjectID) -> str:
    """
    Encodes ObjectID to hex.

    It uses also cloudpickle to encode it first, so that
    `Ray tracks the object ID correctly <https://github.com/ray-project/ray/issues/9578>`__.

    Parameters
    ----------
    object_id:
        ObjectID to encode.

    Returns
    -------
    Hex-encoded ObjectID.
    """

    return encode_utils.binary_to_hex(ray.cloudpickle.dumps(object_id))


def decode_object_id(object_id: str) -> ray.ObjectID:
    """
    Decodes ObjectID from hex.

    It uses also cloudpickle to decode it, too, because we are using it when encoding.

    Parameters
    ----------
    object_id:
        ObjectID to decode.

    Returns
    -------
    Hex-decoded ObjectID.
    """

    return ray.cloudpickle.loads(encode_utils.hex_to_binary(object_id))
