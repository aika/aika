# AutoML system

This module represents the main entry point for calling high-level
AutoML functions which tie up together many lower level elements we have in Aika.
It uses Ray to distribute the AutoML execution and MongoDB to store its state.

## Values

We represent values in this module at three levels:

* A raw Python value.
* An *intermediate level* with a dict with `type` and `value` fields
  where `type` can be one of the `object`, `dataset_uri`, `csv_uri`,
  `pickle_uri`, `plasma_id`, `error`. All values except for `object` type are strings.
  Values for `object` type are raw Python values.
* A *MongoDB-compatible level* with a dict with `type` and `value` fields
  where `type` can be one of the `object_id`, `dataset_uri`, `csv_uri`,
  `pickle_uri`, `plasma_id`, `error`. All values are strings.

The difference between the intermediate level and a MongoDB-compatible level
is that for MongoDB-compatible level all Python values are stored or
on the disk or in the Ray's object store (using `object_id` to identify them).
The intermediate level allows for Python values to be passed directly.

The reason for the intermediate level is that we want to expose values
through the AutoML API in this way so that heavy serialization to disk
for datasets and dataframes is done as part of a Ray task, but that we
still allow raw Python values if needed. This is useful also in the scope
of the TA2-TA3 API where values of `object` type can be encoded into GRPC
messages in different ways.

## Collections

We are using MongoDB database to store the state of the AutoML system.
This is used internally by the API and Ray tasks so it generally does
not matter when API is used from outside.

Generally each document represents both the request to do something
and stores responses. A general life-cycle of an API call is to prepare
a MongoDB document, store it, start a Ray task, giving the ID of the
stored document, the Ray task reads the document, does the work, and
stores results into the document. It can potentially store results
multiple times, if partial results are available and supported by
the document schema.

`aika.Searches`
* `_id`
* `time_bound_search`
* `time_bound_run`
* `priority`
* `problem`: contains enumerations
* `template`: pipeline encoded into JSON-compatible structure using
              a standard JSON pipeline conversion; e.g., this means
              that timestamps are encoded into strings; all sub-pipelines
              have to be resolved and nested
* `runner_class`
* `runner_arguments`
* `evaluator_class`
* `evaluator_arguments`
* `searcher_class`
* `searcher_arguments`
* `generator_class`
* `generator_arguments`
* `rewriter_class`
* `rewriter_arguments`
* `tuner_class`
* `tuner_arguments`
* `inputs`: list of
  * `type`
  * `value`
* `random_seed`: random seed for search and computing of internal score
* `state`: `ProgressState` enumeration
* `start`
* `end`
* `status`
* `solutions`: list of
  * `_id`
  * `internal_score`
  * `random_seed`: random seed used for search and computing of internal score
  * `search_scores`: scores made during search, list of
    * `configuration`:
      * `method`: `EvaluationMethod` enumeration
      * `folds`
      * `train_test_ratio`
      * `shuffle`
      * `random_seed`
      * `stratified`
    * `scores`: list of
      * `metric`
        * `metric`: `PerformanceMetric` enumeration
        * `params`:
          * `k`
          * `pos_label`
      * `value`
      * `normalized`
      * `random_seed`
      * `fold`
* `object_id`: hex-encoded `ObjectID` of Ray solutions search task

`aika.Solutions`
* `_id`
* `search`:
  * `_id`
* `internal_score`
* `random_seed`: random seed used for search and computing of internal score
* `search_scores`: scores made during search, list of
  * `configuration`:
    * `method`: `EvaluationMethod` enumeration
    * `folds`
    * `train_test_ratio`
    * `shuffle`
    * `random_seed`
    * `stratified`
  * `scores`: list of
    * `metric`
      * `metric`: `PerformanceMetric` enumeration
      * `params`:
        * `k`
        * `pos_label`
    * `value`
    * `normalized`
    * `random_seed`
    * `fold`
* `problem`: contains enumerations
* `pipeline`: pipeline encoded into JSON-compatible structure using
              a standard JSON pipeline conversion; e.g., this means
              that timestamps are encoded into strings; all sub-pipelines
              have to be resolved and nested
* `hyperparameter_values`: hyper-parameter values encoded into JSON-compatible structure
                           using the standard JSON hyper-parameters conversion

`aika.Scores`
* `_id`
* `search`:
  * `_id`
* `solution`:
  * `_id`
* `inputs`: list of
  * `type`
  * `value`
* `metrics`: list of
  * `metric`: `PerformanceMetric` enumeration or a string for custom metric
  * `params`:
    * `k`
    * `pos_label`
* `users`: list of
  * `id`
  * `chosen`
  * `reason`
* `configuration`:
  * `method`: `EvaluationMethod` enumeration
  * `folds`
  * `train_test_ratio`
  * `shuffle`
  * `random_seed`: random seed for data preparation pipeline
  * `stratified`
* `data_pipeline`: pipeline encoded into JSON-compatible structure using
                   a standard JSON pipeline conversion; e.g., this means
                   that timestamps are encoded into strings; all sub-pipelines
                   have to be resolved and nested
* `scoring_pipeline`: pipeline encoded into JSON-compatible structure using
                      a standard JSON pipeline conversion; e.g., this means
                      that timestamps are encoded into strings; all sub-pipelines
                      have to be resolved and nested
* `scoring_random_seed`: random seed for scoring pipeline
* `random_seed`: random seed the pipeline
* `state`: `ProgressState` enumeration
* `start`
* `end`
* `status`
* `scores`: list of
  * `metric`
    * `metric`: `PerformanceMetric` enumeration or a string for custom metric
    * `params`:
      * `k`
      * `pos_label`
  * `value`
  * `normalized`
  * `random_seed`
  * `fold`
* `object_id`: hex-encoded `ObjectID` of Ray scoring task

`aika.FittedSolutions`
* `_id`
* `search`:
  * `_id`
* `solution`:
  * `_id`
* `inputs`: list of
  * `type`
  * `value`
* `expose_outputs`
* `expose_value_types`: list of `ValueType` enumeration
* `users`: list of
  * `id`
  * `chosen`
  * `reason`
* `random_seed`
* `state`: `ProgressState` enumeration
* `start`
* `end`
* `status`
* `steps`: list of
  * `state`: `ProgressState` enumeration
  * `start`
  * `end`
  * `status`
  * `steps`: recursive list of steps
* `exposed_outputs`: list of
  * `data`
  * `type`
  * `value`
* `object_id`: hex-encoded `ObjectID` of Ray fitting task and the resulting
               fitted pipeline (`Runtime` instance)

`aika.ProducedSolutions`
* `_id`
* `search`:
  * `_id`
* `solution`:
  * `_id`
* `fitted_solution`:
  * `_id`
* `inputs`: list of
  * `type`
  * `value`
* `expose_outputs`
* `expose_value_types`: list of `ValueType` enumeration
* `users`: list of
  * `id`
  * `chosen`
  * `reason`
* `state`: `ProgressState` enumeration
* `start`
* `end`
* `status`
* `steps`: list of
  * `state`: `ProgressState` enumeration
  * `start`
  * `end`
  * `status`
  * `steps`: recursive list of steps
* `exposed_outputs`: list of
  * `data`
  * `type`
  * `value`
* `object_id`: hex-encoded `ObjectID` of Ray producing task

`aika.Logs`
* `_id`
* `record`
  * `name`
  * `msg`
  * `args`
  * `levelname`
  * `levelno`
  * `pathname`
  * `filename`
  * `module`
  * `exc_text`
  * `stack_info`
  * `lineno`
  * `funcName`
  * `created`
  * `msecs`
  * `relativeCreated`
  * `thread`
  * `threadName`
  * `processName`
  * `process`
  * `message`
  * `asctime`
  * `worker_id`: a Ray's worker ID, hex-encoded
  * `worker_mode`: a Ray's worker mode
  * `object_ids`: a list of Ray's task `returns` ObjectIDs, hex-encoded 
  * `task_name`: a Ray's task function name
  * `hostname`
  * `exc_type`: a type name of the type from `exc_info`
