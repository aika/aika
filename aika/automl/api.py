import datetime
import typing
import uuid

import ray

from d3m.container import dataset as dataset_module
from d3m.metadata import problem as problem_module

from aika.pipelines import pipeline as pipeline_module
from aika.utils import exceptions

from . import base, tasks, utils


def search_solutions(
    problem: typing.Optional[problem_module.Problem],
    inputs: typing.Sequence[typing.Dict],
    *,
    scratch_dir: str,
    time_bound_search: float = 0,
    time_bound_run: float = 0,
    priority: float = 0,
    template: pipeline_module.Pipeline = None,
    runner_class: str = 'aika.search.runner.ray.RayRunner',
    runner_arguments: typing.Dict[str, typing.Any] = None,
    evaluator_class: str = 'aika.search.evaluator.runtime.RuntimeEvaluator',
    evaluator_arguments: typing.Dict[str, typing.Any] = None,
    searcher_class: str = 'aika.search.searcher.exhaustive.ExhaustiveSolutionSearcher',
    searcher_arguments: typing.Dict[str, typing.Any] = None,
    generator_class: str = 'aika.search.generator.chain.ChainGeneratorsPipelineGenerator',
    generator_arguments: typing.Dict[str, typing.Any] = None,
    rewriter_class: str = 'aika.search.rewriter.passthrough.PassthroughPipelineRewriter',
    rewriter_arguments: typing.Dict[str, typing.Any] = None,
    tuner_class: str = 'aika.search.tuner.default.DefaultValuesPipelineTuner',
    tuner_arguments: typing.Dict[str, typing.Any] = None,
    search_id: str = None,
    strict_resolving: bool = False,
    random_seed: int = 0,
    volumes_dir: str = None,
    pipelines_scored_dir: str = None,
    subpipelines_dir: str = None,
    compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING,
    strict_digest: bool = False,
) -> typing.Tuple[str, ray.ObjectID]:
    """
    It starts searching for problem solutions (pipelines + hyper-parameters for those pipelines).

    Parameters
    ----------
    problem:
        A problem description.
    inputs:
        A list of value dicts (with ``type`` and ``value`` fields) representing inputs.
        Dict representation of the value should be at the *intermediate level*.
    scratch_dir:
        Path to a directory to store any temporary files needed during run.
    time_bound_search:
        An upper limit on time for solution search, in minutes.
    time_bound_run:
        Limit found solutions based on the time for one pass of their run, in minutes.
    priority:
        Search with higher number has higher priority when multiple searches are done in parallel.
    template:
        A pipeline template to use for search or to run. If omitted,
        then a regular solution search is done.
    runner_class:
        A Python path to a runner class.
    runner_arguments:
        A dict containing additional arguments for runner. It should be JSON-compatible.
    evaluator_class:
        A Python path to a solution evaluator class.
    evaluator_arguments:
        A dict containing additional arguments for solution evaluator. It should be JSON-compatible.
    searcher_class:
        A Python path to a solution searcher class.
    searcher_arguments:
        A dict containing additional arguments for solution searcher. It should be JSON-compatible.
    generator_class:
        A Python path to a pipeline generator class.
    generator_arguments:
        A dict containing additional arguments for pipeline generator. It should be JSON-compatible.
    rewriter_class:
        A Python path to a pipeline rewriter class.
    rewriter_arguments:
        A dict containing additional arguments for pipeline rewriter. It should be JSON-compatible.
    tuner_class:
        A Python path to a pipeline tuner class.
    tuner_arguments:
        A dict containing additional arguments for pipeline tuner. It should be JSON-compatible.
    search_id:
        An optional search ID to use. If omitted, a new one is generated.
    strict_resolving:
        If resolved pipeline or primitive does not fully match specified reference, raise an exception?
    random_seed:
        Random seed to use.
    volumes_dir:
        Path to a directory with static files required by primitives.
    pipelines_scored_dir:
        Where to store all successfully evaluated pipelines?
    subpipelines_dir:
        Where to store sub-pipelines? If not specified, a corresponding pipeline
        output directory is used for sub-pipelines as well.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?

    Returns
    -------
    A search ID and Ray task ObjectID.
    """

    # pylint: disable=too-many-locals

    if search_id is None:
        search_id = str(uuid.uuid4())
    if runner_arguments is None:
        runner_arguments = {}
    if evaluator_arguments is None:
        evaluator_arguments = {}
    if searcher_arguments is None:
        searcher_arguments = {}
    if generator_arguments is None:
        generator_arguments = {}
    if rewriter_arguments is None:
        rewriter_arguments = {}
    if tuner_arguments is None:
        tuner_arguments = {}

    inputs = [base.store_value_in_ray(input) for input in inputs]

    assert base.mongo_client is not None

    base.mongo_client.aika.Searches.insert_one(
        {
            '_id': search_id,
            'time_bound_search': time_bound_search,
            'time_bound_run': time_bound_run,
            'priority': priority,
            'problem': base.encode_problem_description(problem),
            'template': template.to_json_structure(nest_subpipelines=True) if template is not None else None,
            'runner_class': runner_class,
            'runner_arguments': runner_arguments,
            'evaluator_class': evaluator_class,
            'evaluator_arguments': evaluator_arguments,
            'searcher_class': searcher_class,
            'searcher_arguments': searcher_arguments,
            'generator_class': generator_class,
            'generator_arguments': generator_arguments,
            'rewriter_class': rewriter_class,
            'rewriter_arguments': rewriter_arguments,
            'tuner_class': tuner_class,
            'tuner_arguments': tuner_arguments,
            'inputs': inputs,
            'random_seed': random_seed,
            'state': base.ProgressState.PENDING.name,  # pylint: disable=no-member
            'start': None,
            'end': None,
            'status': None,
            'solutions': [],
            'object_id': None,
        },
    )

    object_id = tasks.search_solutions.remote(
        search_id,
        scratch_dir=scratch_dir,
        strict_resolving=strict_resolving,
        volumes_dir=volumes_dir,
        pipelines_scored_dir=pipelines_scored_dir,
        subpipelines_dir=subpipelines_dir,
        compute_digest=compute_digest,
        strict_digest=strict_digest,
    )

    base.mongo_client.aika.Searches.update_one(
        {
            '_id': search_id,
            'state': {
                '$in': [base.ProgressState.PENDING.name, base.ProgressState.RUNNING.name],  # pylint: disable=no-member
            },
        },
        {
            '$set': {
                'object_id': utils.encode_object_id(object_id),
            },
        },
    )

    return search_id, object_id


def end_search_solutions(search_id: str) -> None:
    """
    Ends the search and releases all resources associated with the solution search.

    Parameters
    ----------
    search_id:
        A search ID.
    """

    stop_search_solutions(search_id, allow_concluded=True)

    # TODO: Kill all ongoing Ray tasks for this search.
    #       See: https://github.com/ray-project/ray/issues/854
    # TODO: Free objects in Ray.
    #       See: https://github.com/ray-project/ray/pull/2542
    # TODO: Should we also terminate any scoring, fitting, and producing of solutions from this search?


def stop_search_solutions(search_id: str, *, allow_concluded: bool = False) -> None:
    """
    Stops the search but leaves all currently found solutions available.

    Parameters
    ----------
    search_id:
        A search ID.
    allow_concluded:
        Do not raise an exception if search has already concluded.
    """

    assert base.mongo_client is not None

    result = base.mongo_client.aika.Searches.update_one(
        {
            '_id': search_id,
            'state': {
                '$in': [base.ProgressState.PENDING.name, base.ProgressState.RUNNING.name],  # pylint: disable=no-member
            },
        },
        {
            '$set': {
                'state': base.ProgressState.COMPLETED.name,  # pylint: disable=no-member
                'end': datetime.datetime.now(datetime.timezone.utc),
            },
        },
    )

    if not result.matched_count:
        if base.mongo_client.aika.Searches.find_one({'_id': search_id}) is not None:
            if allow_concluded:
                return
            else:
                raise exceptions.InvalidStateError("Search has already concluded.")
        else:
            raise exceptions.InvalidArgumentValueError("Invalid search ID.")

    # TODO: Kill all ongoing Ray tasks for this search.
    #       See: https://github.com/ray-project/ray/issues/854


def _search_solutions_results(seen_solutions: typing.Optional[typing.Sequence[typing.Dict]], document: typing.Dict) -> typing.Generator[typing.Dict, None, typing.Sequence[typing.Dict]]:
    if seen_solutions is None:
        new_solutions = document['solutions']
    else:
        new_solutions = [solution for solution in document['solutions'] if solution not in seen_solutions]

    if not new_solutions:
        yield {
            'state': document['state'],
            'status': document['status'],
            'start': document['start'],
            'end': document['end'],
            'done_ticks': len(document['solutions']),
            'all_ticks': None,
        }
    else:
        for solution in new_solutions:
            yield {
                'state': document['state'],
                'status': document['status'],
                'start': document['start'],
                'end': document['end'],
                'done_ticks': len(document['solutions']),
                'all_ticks': None,
                'solution_id': solution['_id'],
                'internal_score': solution['internal_score'],
                'random_seed': solution['random_seed'],
                'search_scores': solution['search_scores'],
            }

    return document['solutions']


def get_search_solutions_results(search_id: str) -> typing.Generator[typing.Dict, None, None]:
    """
    Get all solutions presently identified by the search and start receiving any
    further solutions also found as well.

    Parameters
    ----------
    search_id:
        A search ID.

    Yields
    ------
    For each found solution and updated solution, a dict with fields is yielded: ``state``,
    ``status``, ``start``, ``end``, ``done_ticks``, ``all_ticks``, ``solution_id``,
    ``internal_score``, ``search_scores``.
    """

    def has_completed(seen_documents: typing.Sequence, document: typing.Dict) -> bool:
        return document is None or document['state'] in [base.ProgressState.COMPLETED, base.ProgressState.ERRORED]

    def no_initial_documents() -> None:
        raise exceptions.InvalidArgumentValueError("Invalid search ID.")

    query = {
        '$or': [
            {
                '_id': search_id,
            },
            {
                'documentKey._id': search_id,
            },
            {
                'fullDocument._id': search_id,
            },
        ],
    }

    assert base.mongo_client is not None

    seen_solutions = None
    for document in utils.find_and_watch(base.mongo_client.aika.Searches, query, has_completed, base.decode_search_document, no_initial_documents):
        seen_solutions = yield from _search_solutions_results(seen_solutions, document)


def describe_solution(solution_id: str) -> typing.Dict:
    """
    Request a detailed description of the found solution.

    Parameters
    ----------
    solution_id:
        A solution ID.

    Returns
    -------
    A dict with fields: ``internal_score``, ``search_scores``, ``pipeline``, ``hyperparams``.
    """

    assert base.mongo_client is not None

    solution = base.mongo_client.aika.Solutions.find_one({'_id': solution_id})

    if solution is None:
        raise exceptions.InvalidArgumentValueError("Invalid solution ID.")

    solution = base.decode_solution_document(solution)

    return solution


def score_solution(
    solution_id: str,
    inputs: typing.Sequence[typing.Dict],
    metrics: typing.Sequence[typing.Dict],
    evaluation_method: base.EvaluationMethod,
    *,
    scratch_dir: str,
    users: typing.Sequence[typing.Dict] = None,
    folds: int = None,
    train_test_ratio: float = 0.75,
    shuffle: bool = False,
    stratified: bool = False,
    data_pipeline: pipeline_module.Pipeline = None,
    data_random_seed: int = 0,
    scoring_pipeline: pipeline_module.Pipeline = None,
    scoring_random_seed: int = 0,
    random_seed: int = 0,
    volumes_dir: str = None,
    compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING,
    strict_resolving: bool = False,
    strict_digest: bool = False,
) -> typing.Tuple[str, ray.ObjectID]:
    """
    Request solution to be scored given inputs.

    Parameters
    ----------
    solution_id:
        A solution ID.
    inputs:
        A list of value dicts (with ``type`` and ``value`` fields) representing inputs.
        Dict representation of the value should be at the *intermediate level*.
    metrics:
        A list of metrics to use. Dicts are with fields ``metric`` and ``params``,
        where ``metric`` is a ``PerformanceMetric`` enumeration value or a string
        if a custom metric.
    evaluation_method:
        Evaluation method to use.
    scratch_dir:
        Path to a directory to store any temporary files needed during run.
    users:
        A list of users associated with this call itself. Dicts are with fields
        ``id``, ``chosen``, ``reason``.
    folds:
        Number of folds for k-fold evaluation method.
    train_test_ratio:
        Ratio of train data to all data for holdout evaluation method.
    shuffle:
        Should sampling for the split be shuffled? Default ``False``.
    stratified:
        Should sampling for the split be stratified? Default ``False``.
    data_pipeline:
        Override default data preparation pipeline to use. Otherwise a default data preparation pipeline based on the evaluation method is used.
    data_random_seed:
        Random seed to use for data preparation.
    scoring_pipeline:
        Override default scoring pipeline to use.
    scoring_random_seed:
        Random seed to use for scoring.
    random_seed:
        Random seed to use.
    volumes_dir:
        Path to a directory with static files required by primitives.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_resolving:
        If resolved pipeline or primitive does not fully match specified reference, raise an exception?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?

    Returns
    -------
    A request ID and Ray task ObjectID.
    """

    # pylint: disable=too-many-locals

    request_id = str(uuid.uuid4())

    assert base.mongo_client is not None

    solution = base.mongo_client.aika.Solutions.find_one({'_id': solution_id})

    if solution is None:
        raise exceptions.InvalidArgumentValueError("Invalid solution ID.")

    solution = base.decode_solution_document(solution)

    if users is None:
        users = []

    if folds is None:
        if evaluation_method == base.EvaluationMethod.HOLDOUT:
            folds = 1
        elif evaluation_method == base.EvaluationMethod.K_FOLD:
            folds = 5
        elif evaluation_method == base.EvaluationMethod.RANKING:
            folds = 1
        else:
            raise exceptions.UnexpectedValueError("Unknown evaluation method: {evaluation_method}".format(evaluation_method=evaluation_method))

    inputs = [base.store_value_in_ray(input) for input in inputs]

    base.mongo_client.aika.Scores.insert_one(
        {
            '_id': request_id,
            'search': {
                '_id': solution['search']['_id'],
            },
            'solution': {
                '_id': solution_id,
            },
            'inputs': inputs,
            'metrics': [
                {
                    'metric': metric['metric'].name if isinstance(metric['metric'], problem_module.PerformanceMetric) else metric['metric'],
                    'params': metric.get('params', {}),
                }
                for metric in metrics
            ],
            'users': users,
            'configuration': {
                'method': evaluation_method.name,
                'folds': folds,
                'train_test_ratio': train_test_ratio,
                'shuffle': shuffle,
                'random_seed': data_random_seed,
                'stratified': stratified,
            },
            'data_pipeline': data_pipeline.to_json_structure(nest_subpipelines=True) if data_pipeline is not None else None,
            'scoring_pipeline': scoring_pipeline.to_json_structure(nest_subpipelines=True) if scoring_pipeline is not None else None,
            'scoring_random_seed': scoring_random_seed,
            'random_seed': random_seed,
            'state': base.ProgressState.PENDING.name,  # pylint: disable=no-member
            'start': None,
            'end': None,
            'status': None,
            'scores': [],
            'object_id': None,
        },
    )

    object_id = tasks.score_solution.remote(
        request_id,
        scratch_dir=scratch_dir,
        volumes_dir=volumes_dir,
        compute_digest=compute_digest,
        strict_resolving=strict_resolving,
        strict_digest=strict_digest,
    )

    base.mongo_client.aika.Scores.update_one(
        {
            '_id': request_id,
            'state': {
                '$in': [base.ProgressState.PENDING.name, base.ProgressState.RUNNING.name],  # pylint: disable=no-member
            },
        },
        {
            '$set': {
                'object_id': utils.encode_object_id(object_id),
            },
        },
    )

    return request_id, object_id


def get_score_solution_results(request_id: str) -> typing.Generator[typing.Dict, None, None]:
    """
    Get all score results computed until now and start receiving any
    new score results computed as well.

    Parameters
    ----------
    request_id:
        A request ID.

    Yields
    ------
    Every time scores information is updated, a dict with fields is yielded: ``state``, ``status``,
    ``start``, ``end``, ``scores``.
    """

    def has_completed(seen_documents: typing.Sequence, document: typing.Dict) -> bool:
        return document is None or document['state'] in [base.ProgressState.COMPLETED, base.ProgressState.ERRORED]

    def no_initial_documents() -> None:
        raise exceptions.InvalidArgumentValueError("Invalid request ID.")

    query = {
        '$or': [
            {
                '_id': request_id,
            },
            {
                'documentKey._id': request_id,
            },
            {
                'fullDocument._id': request_id,
            },
        ],
    }

    assert base.mongo_client is not None

    for document in utils.find_and_watch(base.mongo_client.aika.Scores, query, has_completed, base.decodes_score_document, no_initial_documents):
        yield {
            'state': document['state'],
            'status': document['status'],
            'start': document['start'],
            'end': document['end'],
            'scores': document['scores'],
        }


def fit_solution(
    solution_id: str,
    inputs: typing.Sequence[typing.Dict],
    *,
    scratch_dir: str,
    expose_outputs: typing.Sequence[str] = None,
    expose_value_types: typing.Sequence[base.ValueType] = None,
    users: typing.Sequence[typing.Dict] = None,
    random_seed: int = 0,
    volumes_dir: str = None,
    compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING,
    strict_digest: bool = False,
) -> typing.Tuple[str, ray.ObjectID]:
    """
    Fit the solution on given inputs.

    Parameters
    ----------
    solution_id:
        A solution ID.
    inputs:
        A list of value dicts (with ``type`` and ``value`` fields) representing inputs.
        Dict representation of the value should be at the *intermediate level*.
    scratch_dir:
        Path to a directory to store any temporary files needed during run.
    expose_outputs:
        A list of recursive data references for values which should be exposed during pipeline run.
    expose_value_types:
        A list (in order of priority) of value types in which exposed values should be encoded.
    users:
        A list of users associated with this call itself. Dicts are with fields
        ``id``, ``chosen``, ``reason``.
    random_seed:
        Random seed to use.
    volumes_dir:
        Path to a directory with static files required by primitives.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?

    Returns
    -------
    A request ID and Ray task ObjectID.
    """

    # pylint: disable=too-many-locals

    request_id = str(uuid.uuid4())

    assert base.mongo_client is not None

    solution = base.mongo_client.aika.Solutions.find_one({'_id': solution_id})

    if solution is None:
        raise exceptions.InvalidArgumentValueError("Invalid solution ID.")

    solution = base.decode_solution_document(solution)

    if expose_outputs is None:
        expose_outputs = []
    else:
        expose_outputs = sorted(set(expose_outputs))
    if expose_value_types is None:
        expose_value_types = []
    if users is None:
        users = []

    # The pipeline had to be already resolved and fully nested and here we just load it, without allowing any additional
    # sub-pipelines resolving and not resolving primitives. This speeds up things and prevents triggering loading of all primitives.
    pipeline = pipeline_module.Pipeline.from_json_structure(
        solution['pipeline'],
        respect_environment_variable=False,
        no_primitive_resolving=True,
        no_pipeline_resolving=True,
    )
    exposable_outputs = pipeline.get_producing_outputs()  # pylint: disable=no-member
    expose_outputs_set = set(expose_outputs)

    if not expose_outputs_set <= exposable_outputs:
        raise exceptions.InvalidArgumentValueError(
            "Unknown outputs to expose: {unknown_outputs}".format(
                unknown_outputs=sorted(expose_outputs_set - exposable_outputs),
            ),
        )

    inputs = [base.store_value_in_ray(input) for input in inputs]

    base.mongo_client.aika.FittedSolutions.insert_one(
        {
            '_id': request_id,
            'search': {
                '_id': solution['search']['_id'],
            },
            'solution': {
                '_id': solution_id,
            },
            'inputs': inputs,
            'expose_outputs': expose_outputs,
            'expose_value_types': [expose_value_type.name for expose_value_type in expose_value_types],
            'users': users,
            'random_seed': random_seed,
            'state': base.ProgressState.PENDING.name,  # pylint: disable=no-member
            'start': None,
            'end': None,
            'status': None,
            'steps': [],
            'exposed_outputs': [],
            'object_id': None,
        },
    )

    object_id = tasks.fit_solution.remote(
        request_id,
        scratch_dir=scratch_dir,
        volumes_dir=volumes_dir,
        compute_digest=compute_digest,
        strict_digest=strict_digest,
    )

    base.mongo_client.aika.FittedSolutions.update_one(
        {
            '_id': request_id,
            'state': {
                '$in': [base.ProgressState.PENDING.name, base.ProgressState.RUNNING.name],  # pylint: disable=no-member
            },
        },
        {
            '$set': {
                'object_id': utils.encode_object_id(object_id),
            },
        },
    )

    return request_id, object_id


def get_fit_solution_results(request_id: str) -> typing.Generator[typing.Dict, None, None]:
    """
    Get all fitted results currently available and start receiving any further
    new fitted results as well.

    Values in ``exposed_outputs`` are dicts representation of values at the *intermediate level*.

    Parameters
    ----------
    request_id:
        A request ID.

    Yields
    ------
    Every time fitting information is updated, a dict with fields is yielded: ``state``, ``status``,
    ``start``, ``end``, ``steps``, ``exposed_outputs``, ``object_id``.
    """

    def has_completed(seen_documents: typing.Sequence, document: typing.Dict) -> bool:
        return document is None or document['state'] in [base.ProgressState.COMPLETED, base.ProgressState.ERRORED]

    def no_initial_documents() -> None:
        raise exceptions.InvalidArgumentValueError("Invalid request ID.")

    query = {
        '$or': [
            {
                '_id': request_id,
            },
            {
                'documentKey._id': request_id,
            },
            {
                'fullDocument._id': request_id,
            },
        ],
    }

    assert base.mongo_client is not None

    for document in utils.find_and_watch(base.mongo_client.aika.FittedSolutions, query, has_completed, base.decode_fitted_solution_document, no_initial_documents):
        exposed_outputs = []
        for exposed_output in document['exposed_outputs']:
            exposed_output_dict = {'data': exposed_output['data']}
            exposed_output_dict.update(base.retrieve_value_in_ray({'type': exposed_output['type'], 'value': exposed_output['value']}))
            exposed_outputs.append(exposed_output_dict)

        yield {
            'state': document['state'],
            'status': document['status'],
            'start': document['start'],
            'end': document['end'],
            'steps': document['steps'],
            'exposed_outputs': exposed_outputs,
            'object_id': document['object_id'],
        }


def produce_solution(
    fitted_solution_id: str,
    inputs: typing.Sequence[typing.Dict],
    *,
    scratch_dir: str,
    expose_outputs: typing.Sequence[str] = None,
    expose_value_types: typing.Sequence[base.ValueType] = None,
    users: typing.Sequence[typing.Dict] = None,
    compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING,
    strict_digest: bool = False,
) -> typing.Tuple[str, ray.ObjectID]:
    """
    Produce (execute) the solution on given inputs.

    Parameters
    ----------
    fitted_solution_id:
        A fitted solution ID.
    inputs:
        A list of value dicts (with ``type`` and ``value`` fields) representing inputs.
        Dict representation of the value should be at the *intermediate level*.
    scratch_dir:
        Path to a directory to store any temporary files needed during run.
    expose_outputs:
        A list of recursive data references for values which should be exposed during pipeline run.
    expose_value_types:
        A list (in order of priority) of value types in which exposed values should be encoded.
    users:
        A list of users associated with this call itself. Dicts are with fields
        ``id``, ``chosen``, ``reason``.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?

    Returns
    -------
    A request ID and Ray task ObjectID.
    """

    request_id = str(uuid.uuid4())

    assert base.mongo_client is not None

    fitted_solution = base.mongo_client.aika.FittedSolutions.find_one({'_id': fitted_solution_id})

    if fitted_solution is None:
        raise exceptions.InvalidArgumentValueError("Invalid fitted solution ID.")

    fitted_solution = base.decode_fitted_solution_document(fitted_solution)

    solution = base.mongo_client.aika.Solutions.find_one({'_id': fitted_solution['solution']['_id']})

    assert solution is not None

    solution = base.decode_solution_document(solution)

    if expose_outputs is None:
        expose_outputs = []
    else:
        expose_outputs = sorted(set(expose_outputs))
    if expose_value_types is None:
        expose_value_types = []
    if users is None:
        users = []

    # The pipeline had to be already resolved and fully nested and here we just load it, without allowing any additional
    # sub-pipelines resolving and not resolving primitives. This speeds up things and prevents triggering loading of all primitives.
    pipeline = pipeline_module.Pipeline.from_json_structure(
        solution['pipeline'],
        respect_environment_variable=False,
        no_primitive_resolving=True,
        no_pipeline_resolving=True,
    )
    exposable_outputs = pipeline.get_producing_outputs()  # pylint: disable=no-member
    expose_outputs_set = set(expose_outputs)

    if not expose_outputs_set <= exposable_outputs:
        raise exceptions.InvalidArgumentValueError(
            "Unknown outputs to expose: {unknown_outputs}".format(
                unknown_outputs=sorted(expose_outputs_set - exposable_outputs),
            ),
        )

    inputs = [base.store_value_in_ray(input) for input in inputs]

    base.mongo_client.aika.ProducedSolutions.insert_one(
        {
            '_id': request_id,
            'search': {
                '_id': solution['search']['_id'],
            },
            'solution': {
                '_id': fitted_solution['solution']['_id'],
            },
            'fitted_solution': {
                '_id': fitted_solution['_id'],
            },
            'inputs': inputs,
            'expose_outputs': expose_outputs,
            'expose_value_types': [expose_value_type.name for expose_value_type in expose_value_types],
            'users': users,
            'state': base.ProgressState.PENDING.name,  # pylint: disable=no-member
            'start': None,
            'end': None,
            'status': None,
            'steps': [],
            'exposed_outputs': [],
            'object_id': None,
        },
    )

    object_id = tasks.produce_solution.remote(
        request_id,
        scratch_dir=scratch_dir,
        compute_digest=compute_digest,
        strict_digest=strict_digest,
    )

    base.mongo_client.aika.ProducedSolutions.update_one(
        {
            '_id': request_id,
            'state': {
                '$in': [base.ProgressState.PENDING.name, base.ProgressState.RUNNING.name],  # pylint: disable=no-member
            },
        },
        {
            '$set': {
                'object_id': utils.encode_object_id(object_id),
            },
        },
    )

    return request_id, object_id


def get_produce_solution_results(request_id: str) -> typing.Generator[typing.Dict, None, None]:
    """
    Get all producing results computed until now and start receiving any
    new producing results computed as well.

    Values in ``exposed_outputs`` are dicts representation of values at the *intermediate level*.

    Parameters
    ----------
    request_id:
        A request ID.

    Yields
    ------
    Every time producing information is updated, a dict with fields is yielded: ``state``, ``status``,
    ``start``, ``end``, ``steps``, ``exposed_outputs``.
    """

    def has_completed(seen_documents: typing.Sequence, document: typing.Dict) -> bool:
        return document is None or document['state'] in [base.ProgressState.COMPLETED, base.ProgressState.ERRORED]

    def no_initial_documents() -> None:
        raise exceptions.InvalidArgumentValueError("Invalid request ID.")

    query = {
        '$or': [
            {
                '_id': request_id,
            },
            {
                'documentKey._id': request_id,
            },
            {
                'fullDocument._id': request_id,
            },
        ],
    }

    assert base.mongo_client is not None

    for document in utils.find_and_watch(base.mongo_client.aika.ProducedSolutions, query, has_completed, base.decode_produced_solution_document, no_initial_documents):
        exposed_outputs = []
        for exposed_output in document['exposed_outputs']:
            exposed_output_dict = {'data': exposed_output['data']}
            exposed_output_dict.update(base.retrieve_value_in_ray({'type': exposed_output['type'], 'value': exposed_output['value']}))
            exposed_outputs.append(exposed_output_dict)

        yield {
            'state': document['state'],
            'status': document['status'],
            'start': document['start'],
            'end': document['end'],
            'steps': document['steps'],
            'exposed_outputs': exposed_outputs,
        }


def solution_export(solution_id: str, *, pipelines_ranked_dir: str, pipelines_scored_dir: str, subpipelines_dir: str, pipeline_rank: float = None) -> ray.ObjectID:
    """
    Exports a solution for evaluation purposes.

    Parameters
    ----------
    solution_id:
        A solution ID.
    pipelines_ranked_dir:
        A path to a directory to store ranked pipelines.
    pipelines_scored_dir:
        Where to store all successfully evaluated pipelines?
    subpipelines_dir:
        Where to store sub-pipelines?
    pipeline_rank:
        An unique pipeline rank to be set. If not provided, one is computed using (1 - internal score).

    Returns
    -------
    A Ray task ObjectID.
    """

    return tasks.export_solution.remote(
        solution_id,
        pipelines_ranked_dir=pipelines_ranked_dir,
        pipelines_scored_dir=pipelines_scored_dir,
        subpipelines_dir=subpipelines_dir,
        pipeline_rank=pipeline_rank,
    )
