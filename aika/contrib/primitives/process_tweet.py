import re

from d3m import index
from d3m.metadata import base as metadata_base

from . import base

__all__ = ('ProcessTweetPrimitive',)

PYTHON_PATH = 'd3m.primitives.aika.ProcessTweet'

URL_REGEX = re.compile(r'(www\.\S+)|(https?://\S+)')
USERNAME_REGEX = re.compile(r'@\S+')
HASHTAG_REGEX = re.compile(r'#\S+')


class Hyperparams(base.TextPrimitiveHyperparams):
    pass


# TODO: Implement "can_accept".
class ProcessTweetPrimitive(base.TextPrimitiveBase[Hyperparams]):  # pylint: disable=inherit-non-class
    """
    Processes Tweeter messages in all columns with ``http://schema.org/Text`` semantic
    type and structural type ``str``, by default.
    Converts messages to lower case, removes URLs, usernames, and hashtags.
    """

    __author__ = 'Tin Hang Chui'
    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '399b48c2-dff4-4bd0-a0dd-d5984f64970e',
            'version': '0.1.0',
            'name': "Process tweet",
            'python_path': PYTHON_PATH,
            'keywords': ['text', 'txt', 'twitter'],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_PREPROCESSING,
        },
    )

    @classmethod
    def _process_text(cls, text: str, hyperparams: Hyperparams) -> str:
        # Converts to lower case.
        text = text.lower()
        # Remove URLs.
        text = URL_REGEX.sub('', text)
        # Remove usernames.
        text = USERNAME_REGEX.sub('', text)
        # Remove hashtags.
        text = HASHTAG_REGEX.sub('', text)

        return text


index.register_primitive(PYTHON_PATH, ProcessTweetPrimitive)
