import abc
import typing

from d3m import container
from d3m.base import utils as base_utils
from d3m.metadata import base as metadata_base, hyperparams as hyperparams_module
from d3m.primitive_interfaces import base, transformer

TextPrimitiveInputs = container.DataFrame
TextPrimitiveOutputs = container.DataFrame


class TextPrimitiveHyperparams(hyperparams_module.Hyperparams):
    """
    A base set of hyper-parameters for text primitives.
    """

    use_columns = hyperparams_module.Set(
        elements=hyperparams_module.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description=(
            "A set of column indices to force primitive to operate on. If any specified column does not have semantic " "type \"http://schema.org/Text\" or structural type \"str\", it is skipped."
        ),
    )
    exclude_columns = hyperparams_module.Set(
        elements=hyperparams_module.Hyperparameter[int](-1),
        default=(),
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="A set of column indices to not operate on. Applicable only if \"use_columns\" is not provided.",
    )
    return_result = hyperparams_module.Enumeration(
        values=['append', 'replace', 'new'],
        default='replace',
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Should processed columns be appended, should they replace original columns, or should only processed columns be returned?",
    )
    add_index_columns = hyperparams_module.UniformBool(
        default=True,
        semantic_types=['https://metadata.datadrivendiscovery.org/types/ControlParameter'],
        description="Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\".",
    )


class TextPrimitiveBase(transformer.TransformerPrimitiveBase[TextPrimitiveInputs, TextPrimitiveOutputs, base.Hyperparams]):  # pylint: disable=inherit-non-class
    """
    A base primitive implementation for text primitives.
    """

    def produce(self, *, inputs: TextPrimitiveInputs, timeout: float = None, iterations: int = None) -> base.CallResult[TextPrimitiveOutputs]:  # noqa: D102
        columns_to_use, output_columns = self._produce_columns(inputs, self.hyperparams)

        outputs = base_utils.combine_columns(inputs, columns_to_use, output_columns, return_result=self.hyperparams['return_result'], add_index_columns=self.hyperparams['add_index_columns'])

        return base.CallResult(outputs)

    @classmethod
    @abc.abstractmethod
    def _process_text(cls, text: str, hyperparams: base.Hyperparams) -> str:
        pass

    @classmethod
    def _can_use_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        if column_metadata['structural_type'] != str:
            return False

        return 'http://schema.org/Text' in column_metadata.get('semantic_types', [])

    @classmethod
    def _get_columns(cls, inputs_metadata: metadata_base.DataMetadata, hyperparams: base.Hyperparams) -> typing.List[int]:
        def can_use_column(column_index: int) -> bool:
            return cls._can_use_column(inputs_metadata, column_index)

        columns_to_use, columns_not_to_use = base_utils.get_columns_to_use(inputs_metadata, hyperparams['use_columns'], hyperparams['exclude_columns'], can_use_column)

        # We are OK if no columns ended up having be processed.
        # "base_utils.combine_columns" will throw an error if it cannot work with this.

        if hyperparams['use_columns'] and columns_not_to_use:
            cls.logger.warning(
                "Not all specified columns have semantic type \"http://schema.org/Text\" or structural type \"str\". Skipping columns: %(columns)s",
                {
                    'columns': columns_not_to_use,
                },
            )

        return columns_to_use

    @classmethod
    def _produce_columns(cls, inputs: TextPrimitiveOutputs, hyperparams: base.Hyperparams) -> typing.Tuple[typing.List[int], typing.List[TextPrimitiveOutputs]]:
        columns_to_use = cls._get_columns(inputs.metadata, hyperparams)

        output_columns = []

        for column_index in columns_to_use:
            column = inputs.select_columns([column_index])
            column.iloc[:, 0] = column.iloc[:, 0].map(lambda x: cls._process_text(x, hyperparams))
            output_columns.append(column)

        return columns_to_use, output_columns
