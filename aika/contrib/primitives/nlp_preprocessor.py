import re
import string

import nltk
from nltk import corpus

from d3m import index
from d3m.metadata import base as metadata_base, hyperparams as hyperparams_module

from . import base

__all__ = ('NLPPreprocessorPrimitive',)

PYTHON_PATH = 'd3m.primitives.aika.NLPPreprocessor'

WHITESPACE_REGEX = re.compile(r'\s+')

# TODO: Once this primitive is published, this should be part of post-installation hook in "setup.py".
nltk.download('stopwords')


class Hyperparams(base.TextPrimitiveHyperparams):
    language = hyperparams_module.Enumeration[str](
        values=corpus.stopwords._fileids,  # pylint: disable=protected-access
        default='english',
        semantic_types=[
            'http://schema.org/Language',
            'https://metadata.datadrivendiscovery.org/types/ControlParameter',
        ],
        description="The language to use for stopwords.",
    )


# TODO: Implement "can_accept".
class NLPPreprocessorPrimitive(base.TextPrimitiveBase[Hyperparams]):  # pylint: disable=inherit-non-class
    """
    Processes text in all columns with ``http://schema.org/Text`` semantic
    type and structural type ``str``, by default.
    Converts text to lower case, strips and normalizes white space, removes
    stopwords, and punctuation.
    """

    __author__ = 'Tin Hang Chui'
    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': 'bef3bdc8-ace6-4225-a62b-ec6fe4e6533c',
            'version': '0.1.0',
            'name': "NLP preprocessor",
            'python_path': PYTHON_PATH,
            'keywords': ['text', 'txt', 'stopwords'],
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_PREPROCESSING,
        },
    )

    @classmethod
    def _process_text(cls, text: str, hyperparams: Hyperparams) -> str:
        # Converts to lower case.
        text = text.lower()
        # Removes punctuation.
        text = ''.join(l for l in text if l not in string.punctuation)
        # Strips white space.
        text = text.strip()
        # Removes stopwords and normalizes white space.
        text = ' '.join([word for word in WHITESPACE_REGEX.split(text) if word not in corpus.stopwords.words(hyperparams['language'])])

        return text


index.register_primitive(PYTHON_PATH, NLPPreprocessorPrimitive)
