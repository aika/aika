Implementation of primitives' methods we believe are useful to Aika but are not yet published.

A basic structure of a primitive is the same to regular D3M primitives, but instead of
having `installation` metadata and registering it through Python entry points, we call
`d3m.index.register_primitive` function directly.

All files in this directory are automatically loaded by Aika and this is why
`d3m.index.register_primitive` gets called.

```python
import typing

from d3m import index
from d3m.metadata import base as metadata_base, hyperparams
from d3m.primitive_interfaces import base, transformer

__all__ = ('CustomPrimitive',)


PYTHON_PATH = 'd3m.primitives.aika.CustomPrimitive'

Inputs = ...
Outputs = ...


class Hyperparams(hyperparams.Hyperparams):
    ...


class CustomPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):  # pylint: disable=inherit-non-class
    """
    A custom primitive doing custom stuff.
    """

    metadata = metadata_base.PrimitiveMetadata(
        {
            'id': '218337c9-3b12-4257-b983-908784910cc7',
            'version': '0.1.0',
            'name': "Custom primitive",
            'python_path': PYTHON_PATH,
            'algorithm_types': [
                metadata_base.PrimitiveAlgorithmType.DATA_CONVERSION,
            ],
            'primitive_family': metadata_base.PrimitiveFamily.DATA_TRANSFORMATION,
        },
    )

    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:  # noqa: D102
        ...
        
    @classmethod
    def can_accept(cls, *, method_name: str, arguments: typing.Dict[str, typing.Union[metadata_base.Metadata, type]],
                   hyperparams: Hyperparams) -> typing.Optional[metadata_base.DataMetadata]:  # noqa: D102
        ...


index.register_primitive(PYTHON_PATH, CustomPrimitive)
```
