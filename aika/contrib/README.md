This package contains code which is not necessary required for Aika.
For example, Aika operates over all primitives which are available,
and primitives are generally provided by 3rd parties, but we can also
provide our own primitives to assure their functionality is available to Aika.
