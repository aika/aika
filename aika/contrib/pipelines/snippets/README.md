This directory contains reusable sub-pipelines which are used in other
pipelines. When resolving, this directory is added to the pipeline
search path.
