import functools
import logging
import typing

import grpc

# Non-module imports are made to make things available directly from this module.
from d3m.container import dataset as dataset_module
from d3m_automl_rpc import core_pb2, core_pb2_grpc, pipeline_pb2, utils as d3m_automl_rpc_utils, value_pb2
from d3m_automl_rpc.utils import (  # pylint: disable=import-only-modules,unused-import
    decode_performance_metric,
    decode_primitive,
    decode_problem_description,
    encode_primitive,
    encode_problem_description,
    encode_timestamp,
)

from aika.automl import base as automl_base
from aika.pipelines import pipeline as pipeline_module
from aika.utils import uri as uri_utils

from . import grpc_exceptions


def encode_pipeline_description(
    pipeline: pipeline_module.Pipeline,
    allowed_value_types: typing.Sequence[d3m_automl_rpc_utils.ValueType],
    scratch_dir: str,
    data_directories: typing.Sequence[str],
) -> pipeline_pb2.PipelineDescription:
    """
    Encodes a pipeline into a GRPC message.

    Parameters
    ----------
    pipeline:
        A pipeline instance. Primitives do not have to be resolved. Sub-pipelines should be nested.
    allowed_value_types:
        A list of allowed value types to encode this value as. This
        list is tried in order until encoding succeeds.
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    data_directories:
        A list of directories under which local URIs are allowed to be.
        Used to validate any URI encountered during encoding.

    Returns
    -------
    A GRPC message.
    """

    def validate_uri(uri: str) -> None:
        uri_utils.validate_uri(uri, data_directories)

    return d3m_automl_rpc_utils.encode_pipeline_description(pipeline, allowed_value_types, scratch_dir, plasma_put=automl_base.plasma_put, validate_uri=validate_uri)


def decode_pipeline_description(
    pipeline_description: pipeline_pb2.PipelineDescription,
    strict_resolving: bool,
    pipeline_search_paths: typing.Optional[typing.Sequence[str]],
    data_directories: typing.Sequence[str],
    compute_digest: dataset_module.ComputeDigest,
    strict_digest: bool,
) -> typing.Optional[pipeline_module.Pipeline]:
    """
    Decodes a GRPC message into a pipeline.

    Parameters
    ----------
    pipeline_description:
        A GRPC message.
    strict_resolving:
        If resolved pipeline or primitive does not fully match specified reference, raise an exception?
    pipeline_search_paths:
        A list of paths to directories with pipelines to resolve from.
        Their files should be named ``<pipeline id>.json`` or ``<pipeline id>.yml``.
    data_directories:
        A list of directories under which local URIs are allowed to be.
        Used to validate any URI encountered during decoding.
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?

    Returns
    -------
    A pipeline instance, or ``None`` if pipeline is not defined.
    """

    def validate_uri(uri: str) -> None:
        uri_utils.validate_uri(uri, data_directories)

    # We do not respect environment variables to resolve sub-pipelines from an untrusted source.
    # Nor do we resolve sub-pipelines using internal pipeline search paths.
    # We resolve sub-pipelines using only explicitly provided pipeline search paths.
    resolver = pipeline_module.Resolver(strict_resolving=strict_resolving, pipeline_search_paths=pipeline_search_paths, respect_environment_variable=False)

    return d3m_automl_rpc_utils.decode_pipeline_description(
        pipeline_description,
        resolver,
        pipeline_class=pipeline_module.Pipeline,
        plasma_get=automl_base.plasma_get,
        validate_uri=validate_uri,
        compute_digest=compute_digest,
        strict_digest=strict_digest,
    )


def encode_value(value: typing.Dict, allowed_value_types: typing.Sequence[d3m_automl_rpc_utils.ValueType], scratch_dir: str, data_directories: typing.Sequence[str]) -> value_pb2.Value:
    """
    Encodes a value into a GRPC message.

    The input is a dict representation of the value at the *intermediate level*.

    Parameters
    ----------
    value:
        A dict with ``type`` and ``value`` fields. ``type`` can be one of
        ``object``, ``dataset_uri``, ``csv_uri``, ``pickle_uri``, ``plasma_id``, ``error``.
        ``value`` is then a corresponding value for a given type.
    allowed_value_types:
        A list of allowed value types to encode this value as. This
        list is tried in order until encoding succeeds.
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    data_directories:
        A list of directories under which local URIs are allowed to be.
        Used to validate any URI encountered during encoding.

    Returns
    -------
    A GRPC message.
    """

    def validate_uri(uri: str) -> None:
        uri_utils.validate_uri(uri, data_directories)

    return d3m_automl_rpc_utils.encode_value(value, allowed_value_types, scratch_dir, plasma_put=automl_base.plasma_put, validate_uri=validate_uri)


def decode_value(value: value_pb2.Value, data_directories: typing.Sequence[str], *, raise_error: bool = True) -> typing.Dict:
    """
    Decodes a GRPC message.

    The output is a dict representation of the value at the *intermediate level*.

    Parameters
    ----------
    value:
        A GRPC message to decode.
    data_directories:
        A list of directories under which local URIs are allowed to be.
        Used to validate any URI encountered during decoding.
    raise_error:
        If value is representing an error, should an exception be raised or
        should it be returned as value type ``error``?

    Returns
    -------
    A dict with ``type`` and ``value`` fields. ``type`` can be one of
    ``object``, ``dataset_uri``, ``csv_uri``, ``pickle_uri``, ``plasma_id``, ``error``.
    ``value`` is then a corresponding value for a given type.
    ``error`` value type is possible only if ``raise_error`` is ``False``.
    """

    def validate_uri(uri: str) -> None:
        uri_utils.validate_uri(uri, data_directories)

    return d3m_automl_rpc_utils.decode_value(value, validate_uri=validate_uri, raise_error=raise_error)


def encode_score(score: typing.Dict, allowed_value_types: typing.Sequence[d3m_automl_rpc_utils.ValueType], scratch_dir: str, data_directories: typing.Sequence[str]) -> core_pb2.Score:
    """
    Encodes a score description into a GRPC message.

    Parameters
    ----------
    score:
        A score description is a dict with fields: ``metric``
        ``fold``, ``targets``, ``value``.
    allowed_value_types:
        A list of allowed value types to encode this value as. This
        list is tried in order until encoding succeeds.
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    data_directories:
        A list of directories under which local URIs are allowed to be.
        Used to validate any URI encountered during encoding.

    Returns
    -------
    A GRPC message.
    """

    def validate_uri(uri: str) -> None:
        uri_utils.validate_uri(uri, data_directories)

    return d3m_automl_rpc_utils.encode_score(score, allowed_value_types, scratch_dir, plasma_put=automl_base.plasma_put, validate_uri=validate_uri)


def map_exceptions(logger: logging.Logger, name: str) -> typing.Callable:
    """
    A decorator which handles special exceptions and maps them to proper GRPC status codes.
    Moreover, it logs unexpected exceptions.
    """

    def decorator(func: typing.Callable) -> typing.Callable:
        @functools.wraps(func)
        def wrapper(self: core_pb2_grpc.CoreServicer, request: typing.Any, context: grpc.ServicerContext, *args: typing.Any, **kwargs: typing.Any) -> typing.Any:
            try:
                return func(self, request, context, *args, **kwargs)
            except grpc_exceptions.GrpcCodeError as error:
                context.abort(error.code, str(error))
            except Exception as error:
                logger.exception("[%(name)s] Unexpected exception.", {'name': name})
                raise error

        return wrapper

    return decorator


def map_exceptions_generator(logger: logging.Logger, name: str) -> typing.Callable:
    """
    A decorator which handles special exceptions and maps them to proper GRPC status codes.
    Moreover, it logs unexpected exceptions.

    A version for generators.
    """

    def decorator(func: typing.Callable) -> typing.Callable:
        @functools.wraps(func)
        def wrapper(self: core_pb2_grpc.CoreServicer, request: typing.Any, context: grpc.ServicerContext, *args: typing.Any, **kwargs: typing.Any) -> typing.Any:
            try:
                yield from func(self, request, context, *args, **kwargs)
            except grpc_exceptions.GrpcCodeError as error:
                context.abort(error.code, str(error))
            except Exception as error:
                logger.exception("[%(name)s] Unexpected exception.", {'name': name})
                raise error

        return wrapper

    return decorator
