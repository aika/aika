import grpc


class GrpcCodeError(Exception):
    """
    The base class for GRPC code-based exceptions.
    """

    code = None


class CancelledError(GrpcCodeError):
    """
    The operation was cancelled, typically by the caller.
    """

    code = grpc.StatusCode.CANCELLED


class UnknownError(GrpcCodeError):
    """
    Unknown error. For example, this error may be returned when a Status
    value received from another address space belongs to an error space
    that is not known in this address space. Also errors raised by APIs
    that do not return enough error information may be converted to this error.
    """

    code = grpc.StatusCode.UNKNOWN


class InvalidArgumentError(GrpcCodeError):
    """
    The client specified an invalid argument. Note that this differs
    from ``FailedPreconditionError``. ``InvalidArgumentError`` indicates
    arguments that are problematic regardless of the state of the system
    (e.g., a malformed file name).
    """

    code = grpc.StatusCode.INVALID_ARGUMENT


class DeadlineExceeded(GrpcCodeError):
    """
    The deadline expired before the operation could complete. For
    operations that change the state of the system, this error may be
    returned even if the operation has completed successfully. For
    example, a successful response from a server could have been delayed long.
    """

    code = grpc.StatusCode.DEADLINE_EXCEEDED


class NotFoundError(GrpcCodeError):
    """
    Some requested entity (e.g., file or directory) was not found. Note to
    server developers: if a request is denied for an entire class of users,
    such as gradual feature rollout or undocumented whitelist, ``NotFoundError``
    may be used. If a request is denied for some users within a class of users,
    such as user-based access control, ``PermissionDeniedError`` must be used.
    """

    code = grpc.StatusCode.NOT_FOUND


class AlreadyExistsError(GrpcCodeError):
    """
    The entity that a client attempted to
    create (e.g., file or directory) already exists.
    """

    code = grpc.StatusCode.ALREADY_EXISTS


class PermissionDeniedError(GrpcCodeError):
    """
    The caller does not have permission to execute the specified operation.
    ``PermissionDeniedError`` must not be used for rejections caused by
    exhausting some resource (use ``ResourceExhaustedError`` instead for
    those errors). ``PermissionDeniedError`` must not be used if the
    caller can not be identified (use ``UnauthenticatedError`` instead
    for those errors). This error code does not imply the request is
    valid or the requested entity exists or satisfies other pre-conditions.
    """

    code = grpc.StatusCode.PERMISSION_DENIED


class UnauthenticatedError(GrpcCodeError):
    """
    The request does not have valid authentication credentials for the operation.
    """

    code = grpc.StatusCode.UNAUTHENTICATED


class ResourceExhaustedError(GrpcCodeError):
    """
    Some resource has been exhausted, perhaps a
    per-user quota, or perhaps the entire file system is out of space.
    """

    code = grpc.StatusCode.RESOURCE_EXHAUSTED


class FailedPreconditionError(GrpcCodeError):
    """
    The operation was rejected because the system is not in a state
    required for the operation's execution. For example, the directory
    to be deleted is non-empty, an ``rmdir`` operation is applied to a
    non-directory, etc. Service implementors can use the following
    guidelines to decide between ``FailedPreconditionError``,
    ``AbortedError``, and ``UnavailableError``:

    * Use ``UnavailableError`` if the client can retry just the
      failing call.
    * Use ``AbortedError`` if the client should retry at a higher
      level (e.g., when a client-specified test-and-set fails, indicating
      the client should restart a read-modify-write sequence).
    * Use ``FailedPreconditionError`` if the client should not retry
      until the system state has been explicitly fixed. E.g., if an
      ``rmdir`` fails because the directory is non-empty,
      ``FailedPreconditionError`` should be returned since the client
      should not retry unless the files are deleted from the directory.
    """

    code = grpc.StatusCode.FAILED_PRECONDITION


class AbortedError(GrpcCodeError):
    """
    The operation was aborted, typically due to a concurrency issue
    such as a sequencer check failure or transaction abort. See the
    guidelines under ``FailedPreconditionError`` for deciding between
    ``FailedPreconditionError``, ``AbortedError``, and ``UnavailableError``.
    """

    code = grpc.StatusCode.ABORTED


class OutOfRangeError(GrpcCodeError):
    """
    The operation was attempted past the valid range. E.g., seeking or
    reading past end-of-file. Unlike ``InvalidArgumentError``, this
    error indicates a problem that may be fixed if the system state
    changes. For example, a 32-bit file system will generate
    ``InvalidArgumentError`` if asked to read at an offset that is not
    in the range [0,2^32-1], but it will generate ``OutOfRangeError``
    if asked to read from an offset past the current file size.
    There is a fair bit of overlap between ``FailedPreconditionError``
    and ``OutOfRangeError``. We recommend using ``OutOfRangeError``
    (the more specific error) when it applies so that callers who are
    iterating through a space can easily look for an ``OutOfRangeError``
    error to detect when they are done.
    """

    code = grpc.StatusCode.OUT_OF_RANGE


class UnimplementedError(GrpcCodeError):
    """
    The operation is not implemented or is not supported/enabled in this service.
    """

    code = grpc.StatusCode.UNIMPLEMENTED


class InternalError(GrpcCodeError):
    """
    Internal errors. This means that some invariants expected by the
    underlying system have been broken. This error code is reserved
    for serious errors.
    """

    code = grpc.StatusCode.INTERNAL


class UnavailableError(GrpcCodeError):
    """
    The service is currently unavailable. This is most likely a
    transient condition, which can be corrected by retrying with a
    backoff. Note that it is not always safe to retry non-idempotent
    operations.
    """

    code = grpc.StatusCode.UNAVAILABLE


class DataLossError(GrpcCodeError):
    """
    Unrecoverable data loss or corruption.
    """

    code = grpc.StatusCode.DATA_LOSS
