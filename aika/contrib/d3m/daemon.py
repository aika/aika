import logging
import os
import os.path
import time
import typing
import uuid
from concurrent import futures

import grpc
import ray

from d3m.container import dataset as dataset_module
from d3m.metadata import problem as problem_module
from d3m_automl_rpc import core_pb2, core_pb2_grpc

from aika import primitives
from aika.automl import api as automl_api, base as automl_base
from aika.commands import d3m_daemon
from aika.pipelines import pipeline as pipeline_module

from . import grpc_exceptions, grpc_utils

logger = logging.getLogger(__name__)

ONE_DAY_IN_SECONDS = 60 * 60 * 24
USER_AGENT = 'Aika'


class _CoreServicer(core_pb2_grpc.CoreServicer):
    allowed_value_types: typing.Dict[str, typing.Sequence[automl_base.ValueType]]
    solution_ids: typing.Dict[str, str]
    request_ids: typing.Dict[str, str]
    expose_value_types: typing.Dict[str, typing.Sequence[automl_base.ValueType]]

    def __init__(
        self,
        *,
        data_directories: typing.Sequence[str],
        output_dir: str,
        scratch_dir: str,
        pipeline_search_paths: typing.Sequence[str] = None,
        volumes_dir: str = None,
        compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING,
        strict_resolving: bool = False,
        strict_digest: bool = False,
    ) -> None:
        # pylint: disable=too-many-locals

        super().__init__()

        self.data_directories = list(data_directories)
        self.output_dir = output_dir
        self.scratch_dir = scratch_dir
        self.pipeline_search_paths = pipeline_search_paths
        self.volumes_dir = volumes_dir
        self.compute_digest = compute_digest
        self.strict_resolving = strict_resolving
        self.strict_digest = strict_digest

        if self.scratch_dir not in self.data_directories:
            self.data_directories.append(self.scratch_dir)

        # Allowed value types per search ID.
        self.allowed_value_types = {}

        # Search ID per solution ID.
        self.solution_ids = {}

        # Search ID per request ID.
        self.request_ids = {}

        # Allowed value types for exposed values per request ID.
        self.expose_value_types = {}

    def _get_allowed_value_types(self, search_id: str) -> typing.Sequence[automl_base.ValueType]:
        if search_id not in self.allowed_value_types:
            raise grpc_exceptions.InvalidArgumentError("Invalid search ID.")

        return self.allowed_value_types[search_id]

    def _get_search_id_from_solution_id(self, solution_id: str) -> str:
        if solution_id not in self.solution_ids:
            raise grpc_exceptions.InvalidArgumentError("Invalid solution ID.")

        assert self.solution_ids[solution_id] in self.allowed_value_types

        return self.solution_ids[solution_id]

    def _get_search_id_from_request_id(self, request_id: str) -> str:
        if request_id not in self.request_ids:
            raise grpc_exceptions.InvalidArgumentError("Invalid request ID.")

        assert self.request_ids[request_id] in self.allowed_value_types

        return self.request_ids[request_id]

    @grpc_utils.map_exceptions(logger, 'SearchSolutions')
    def SearchSolutions(self, request: core_pb2.SearchSolutionsRequest, context: grpc.ServicerContext) -> core_pb2.SearchSolutionsResponse:  # noqa: N802 pylint: disable=invalid-name
        # pylint: disable=too-many-locals

        search_id = str(uuid.uuid4())
        logger.info(
            "[%(search_id)s] [SearchSolutions] user_agent='%(user_agent)s' version='%(version)s'",
            {
                'search_id': search_id,
                'user_agent': request.user_agent,
                'version': request.version,
            },
        )

        version = core_pb2.DESCRIPTOR.GetOptions().Extensions[core_pb2.protocol_version]
        if request.version != version:
            logger.warning(
                "[%(search_id)s] [SearchSolutions] Client version (%(client_version)s) is not the same as our version (%(our_version)s).",
                {
                    'search_id': search_id,
                    'client_version': request.version,
                    'our_version': version,
                },
            )

        try:
            allowed_value_types = [automl_base.ValueType[allowed_value_type] for allowed_value_type in request.allowed_value_types]
        except ValueError as error:
            raise grpc_exceptions.InvalidArgumentError("Unknown allowed value type.") from error

        problem = grpc_utils.decode_problem_description(request.problem)
        template = grpc_utils.decode_pipeline_description(request.template, self.strict_resolving, self.pipeline_search_paths, self.data_directories, self.compute_digest, self.strict_digest)
        inputs = [grpc_utils.decode_value(input, self.data_directories) for input in request.inputs]

        # This check is for the future API compatibility (where we will probably be able to do
        # something with just a problem or just inputs). Currently, our implementation is stricter
        # and requires both problem description and inputs for a not fully specified template pipeline.
        if not problem and not inputs:
            raise grpc_exceptions.InvalidArgumentError("At least a problem description or inputs have to be provided.")

        runner_class = os.environ.get('AIKA_RUNNER', 'aika.search.runner.ray.RayRunner')
        evaluator_class = os.environ.get('AIKA_EVALUATOR', 'aika.search.evaluator.runtime.RuntimeEvaluator')
        searcher_class = os.environ.get('AIKA_SEARCHER', 'aika.search.searcher.exhaustive.ExhaustiveSolutionSearcher')
        generator_class = os.environ.get('AIKA_GENERATOR', 'aika.search.generator.chain.ChainGeneratorsPipelineGenerator')
        rewriter_class = os.environ.get('AIKA_REWRITER', 'aika.search.rewriter.passthrough.PassthroughPipelineRewriter')

        # TODO: Implement support for "use_default_values_for_free_hyperparams".
        automl_api.search_solutions(
            problem,
            inputs,
            scratch_dir=self.scratch_dir,
            time_bound_search=request.time_bound_search,
            time_bound_run=request.time_bound_run,
            priority=request.priority,
            template=template,
            runner_class=runner_class,
            evaluator_class=evaluator_class,
            searcher_class=searcher_class,
            generator_class=generator_class,
            rewriter_class=rewriter_class,
            search_id=search_id,
            strict_resolving=self.strict_resolving,
            volumes_dir=self.volumes_dir,
            pipelines_scored_dir=os.path.join(self.output_dir, search_id, 'pipelines_scored'),
            subpipelines_dir=os.path.join(self.output_dir, search_id, 'subpipelines'),
            compute_digest=self.compute_digest,
            strict_digest=self.strict_digest,
        )

        self.allowed_value_types[search_id] = allowed_value_types

        return core_pb2.SearchSolutionsResponse(
            search_id=search_id,
        )

    @grpc_utils.map_exceptions_generator(logger, 'GetSearchSolutionsResults')
    def GetSearchSolutionsResults(  # noqa: N802 pylint: disable=invalid-name
        self,
        request: core_pb2.GetSearchSolutionsResultsRequest,
        context: grpc.ServicerContext,
    ) -> typing.Generator[core_pb2.GetSearchSolutionsResultsResponse, None, None]:
        if not request.search_id:
            raise grpc_exceptions.InvalidArgumentError("Search ID is missing.")

        logger.info(
            "[%(search_id)s] [GetSearchSolutionsResults]",
            {
                'search_id': request.search_id,
            },
        )

        for document in automl_api.get_search_solutions_results(request.search_id):
            scores = []
            for search_score in document.get('search_scores', []):
                scores.append(
                    core_pb2.SolutionSearchScore(
                        scoring_configuration=core_pb2.ScoringConfiguration(
                            method=search_score['configuration']['method'].name,
                            folds=search_score['configuration']['folds'],
                            train_test_ratio=search_score['configuration']['train_test_ratio'],
                            shuffle=search_score['configuration']['shuffle'],
                            random_seed=search_score['configuration']['random_seed'],
                            stratified=search_score['configuration']['stratified'],
                        ),
                        scores=[grpc_utils.encode_score(score, self._get_allowed_value_types(request.search_id), self.scratch_dir, self.data_directories) for score in search_score['scores']],
                    ),
                )

            # TODO: Properly implement support for "rank_solutions_limit".
            #       We currently always return a rank based on internal score.
            if document.get('internal_score', None) is not None:
                # TODO: Do proper ranking.
                rank = 1 - document['internal_score']
                normalized = document['internal_score']

                score = {
                    'metric': {
                        'metric': 'RANK',
                    },
                    'value': rank,
                    'normalized': normalized,
                    # We use random seed from the solution because that one
                    # influenced our internal score which we used for rank.
                    'random_seed': document['random_seed'],
                    'fold': 0,
                }

                scores.append(
                    core_pb2.SolutionSearchScore(
                        scoring_configuration=core_pb2.ScoringConfiguration(
                            method=automl_base.EvaluationMethod.RANKING.name,  # pylint: disable=no-member
                        ),
                        scores=[grpc_utils.encode_score(score, self._get_allowed_value_types(request.search_id), self.scratch_dir, self.data_directories)],
                    ),
                )

            if document.get('solution_id', None):
                self.solution_ids[document['solution_id']] = request.search_id

            yield core_pb2.GetSearchSolutionsResultsResponse(
                progress=core_pb2.Progress(
                    state=document['state'].value,
                    status=document['status'],
                    start=grpc_utils.encode_timestamp(document['start']),
                    end=grpc_utils.encode_timestamp(document['end']),
                ),
                done_ticks=document['done_ticks'],
                all_ticks=document['all_ticks'],
                solution_id=document.get('solution_id', None),
                internal_score=document.get('internal_score', None),
                scores=scores,
            )

    @grpc_utils.map_exceptions(logger, 'EndSearchSolutions')
    def EndSearchSolutions(self, request: core_pb2.EndSearchSolutionsRequest, context: grpc.ServicerContext) -> core_pb2.EndSearchSolutionsResponse:  # noqa: N802 pylint: disable=invalid-name
        if not request.search_id:
            raise grpc_exceptions.InvalidArgumentError("Search ID is missing.")

        logger.info(
            "[%(search_id)s] [EndSearchSolutions]",
            {
                'search_id': request.search_id,
            },
        )

        automl_api.end_search_solutions(request.search_id)

        # We iterate over a list so that we can change dict while iterating.
        for solution_id, search_id in list(self.solution_ids.items()):
            if search_id == request.search_id:
                del self.solution_ids[solution_id]

        # We iterate over a list so that we can change dict while iterating.
        for request_id, search_id in list(self.request_ids.items()):
            if search_id == request.search_id:
                del self.request_ids[request_id]

                if request_id in self.expose_value_types:
                    del self.expose_value_types[request_id]

        del self.allowed_value_types[request.search_id]

        return core_pb2.EndSearchSolutionsResponse()

    @grpc_utils.map_exceptions(logger, 'StopSearchSolutions')
    def StopSearchSolutions(self, request: core_pb2.StopSearchSolutionsRequest, context: grpc.ServicerContext) -> core_pb2.StopSearchSolutionsResponse:  # noqa: N802 pylint: disable=invalid-name
        if not request.search_id:
            raise grpc_exceptions.InvalidArgumentError("Search ID is missing.")

        logger.info(
            "[%(search_id)s] [StopSearchSolutions]",
            {
                'search_id': request.search_id,
            },
        )

        automl_api.stop_search_solutions(request.search_id)

        return core_pb2.StopSearchSolutionsResponse()

    @grpc_utils.map_exceptions(logger, 'DescribeSolution')
    def DescribeSolution(self, request: core_pb2.DescribeSolutionRequest, context: grpc.ServicerContext) -> core_pb2.DescribeSolutionResponse:  # noqa: N802 pylint: disable=invalid-name
        if not request.solution_id:
            raise grpc_exceptions.InvalidArgumentError("Solution ID is missing.")

        search_id = self._get_search_id_from_solution_id(request.solution_id)

        logger.info(
            "[%(search_id)s] [DescribeSolution] solution_id='%(solution_id)s'",
            {
                'search_id': search_id,
                'solution_id': request.solution_id,
            },
        )

        solution = automl_api.describe_solution(request.solution_id)

        assert solution['search']['_id'] == search_id, (solution['search']['_id'], search_id)

        # The pipeline had to be already resolved and fully nested and here we just load it, without allowing
        # any additional sub-pipelines resolving and expecting that primitives have not changed in meantime.
        pipeline = pipeline_module.Pipeline.from_json_structure(
            solution['pipeline'],
            strict_resolving=True,
            respect_environment_variable=False,
            no_pipeline_resolving=True,
        )

        return core_pb2.DescribeSolutionResponse(
            pipeline=grpc_utils.encode_pipeline_description(pipeline, self._get_allowed_value_types(search_id), self.scratch_dir, self.data_directories),
            steps=self._construct_solution_steps(search_id, pipeline, solution['hyperparameter_values']),
        )

    def _construct_solution_steps(self, search_id: str, pipeline: pipeline_module.Pipeline, hyperparams: typing.Sequence) -> typing.Sequence[core_pb2.StepDescription]:
        assert len(hyperparams) == len(pipeline.steps), (len(hyperparams), len(pipeline.steps))

        steps = []
        for hyperparams_for_step, step in zip(hyperparams, pipeline.steps):
            if isinstance(step, pipeline_module.PrimitiveStep):
                free_hyperparams_for_step = step.get_free_hyperparams()

                assert isinstance(hyperparams_for_step, dict)
                assert isinstance(free_hyperparams_for_step, dict)

                values = {}
                for name, hyperparameter in free_hyperparams_for_step.items():
                    values[name] = hyperparameter.value_from_json_structure(hyperparams_for_step[name])

                steps.append(
                    core_pb2.StepDescription(
                        primitive=core_pb2.PrimitiveStepDescription(
                            hyperparams={
                                key: grpc_utils.encode_value({'type': 'object', 'value': value}, self._get_allowed_value_types(search_id), self.scratch_dir, self.data_directories)
                                for key, value in values.items()
                            },
                        ),
                    ),
                )

            elif isinstance(step, pipeline_module.SubpipelineStep):
                assert isinstance(hyperparams_for_step, typing.Sequence)

                if step.pipeline is None:
                    raise grpc_exceptions.InternalError("Pipeline has not been resolved.")

                steps.append(
                    core_pb2.StepDescription(
                        pipeline=core_pb2.SubpipelineStepDescription(
                            steps=self._construct_solution_steps(search_id, typing.cast(pipeline_module.Pipeline, step.pipeline), hyperparams_for_step),
                        ),
                    ),
                )

            else:
                raise grpc_exceptions.InternalError("Unknown step type: {step_type}".format(step_type=type(step)))

        return steps

    @grpc_utils.map_exceptions(logger, 'ScoreSolution')
    def ScoreSolution(self, request: core_pb2.ScoreSolutionRequest, context: grpc.ServicerContext) -> core_pb2.ScoreSolutionResponse:  # noqa: N802 pylint: disable=invalid-name
        if not request.solution_id:
            raise grpc_exceptions.InvalidArgumentError("Solution ID is missing.")

        search_id = self._get_search_id_from_solution_id(request.solution_id)

        logger.info(
            "[%(search_id)s] [ScoreSolution] solution_id='%(solution_id)s'",
            {
                'search_id': search_id,
                'solution_id': request.solution_id,
            },
        )

        metrics = [grpc_utils.decode_performance_metric(metric) for metric in request.performance_metrics]

        if not metrics:
            raise grpc_exceptions.InvalidArgumentError("No metrics provided.")

        try:
            evaluation_method = automl_base.EvaluationMethod[request.configuration.method]
        except ValueError as error:
            raise grpc_exceptions.InvalidArgumentError("Unknown evaluation method.") from error

        if evaluation_method == automl_base.EvaluationMethod.HOLDOUT and request.configuration.train_test_ratio in [0.0, 1.0]:
            raise grpc_exceptions.InvalidArgumentError(
                "Invalid train-test ratio: {ratio}".format(
                    ratio=request.configuration.train_test_ratio,
                ),
            )
        if evaluation_method == automl_base.EvaluationMethod.K_FOLD and request.configuration.folds < 2:
            raise grpc_exceptions.InvalidArgumentError(
                "Invalid number of folds: {folds}".format(
                    folds=request.configuration.folds,
                ),
            )

        automl_base.validate_metrics(metrics, evaluation_method)

        users = [{'id': user.id, 'chosen': user.chosen, 'reason': user.reason} for user in request.users]
        inputs = [grpc_utils.decode_value(input, self.data_directories) for input in request.inputs]

        request_id, object_id = automl_api.score_solution(  # pylint: disable=unused-variable
            request.solution_id,
            inputs,
            metrics,
            evaluation_method,
            scratch_dir=self.scratch_dir,
            users=users,
            folds=request.configuration.folds,
            train_test_ratio=request.configuration.train_test_ratio,
            shuffle=request.configuration.shuffle,
            data_random_seed=request.configuration.random_seed,
            stratified=request.configuration.stratified,
            strict_resolving=self.strict_resolving,
            volumes_dir=self.volumes_dir,
            compute_digest=self.compute_digest,
            strict_digest=self.strict_digest,
        )

        self.request_ids[request_id] = search_id

        return core_pb2.ScoreSolutionResponse(
            request_id=request_id,
        )

    @grpc_utils.map_exceptions_generator(logger, 'GetScoreSolutionResults')
    def GetScoreSolutionResults(  # noqa: N802 pylint: disable=invalid-name
        self,
        request: core_pb2.GetScoreSolutionResultsRequest,
        context: grpc.ServicerContext,
    ) -> typing.Generator[core_pb2.GetScoreSolutionResultsResponse, None, None]:
        if not request.request_id:
            raise grpc_exceptions.InvalidArgumentError("Request ID is missing.")

        search_id = self._get_search_id_from_request_id(request.request_id)

        logger.info(
            "[%(search_id)s] [GetScoreSolutionResults] request_id='%(request_id)s'",
            {
                'search_id': search_id,
                'request_id': request.request_id,
            },
        )

        for document in automl_api.get_score_solution_results(request.request_id):
            yield core_pb2.GetScoreSolutionResultsResponse(
                progress=core_pb2.Progress(
                    state=document['state'].value,
                    status=document['status'],
                    start=grpc_utils.encode_timestamp(document['start']),
                    end=grpc_utils.encode_timestamp(document['end']),
                ),
                scores=[grpc_utils.encode_score(score, self._get_allowed_value_types(search_id), self.scratch_dir, self.data_directories) for score in document['scores']],
            )

    @grpc_utils.map_exceptions(logger, 'FitSolution')
    def FitSolution(self, request: core_pb2.FitSolutionRequest, context: grpc.ServicerContext) -> core_pb2.FitSolutionResponse:  # noqa: N802 pylint: disable=invalid-name
        if not request.solution_id:
            raise grpc_exceptions.InvalidArgumentError("Solution ID is missing.")

        search_id = self._get_search_id_from_solution_id(request.solution_id)

        logger.info(
            "[%(search_id)s] [FitSolution] solution_id='%(solution_id)s'",
            {
                'search_id': search_id,
                'solution_id': request.solution_id,
            },
        )

        if request.expose_value_types:
            try:
                expose_value_types: typing.Sequence[automl_base.ValueType] = [automl_base.ValueType[expose_value_type] for expose_value_type in request.expose_value_types]
            except ValueError as error:
                raise grpc_exceptions.InvalidArgumentError("Unknown allowed value type.") from error
        else:
            expose_value_types = self._get_allowed_value_types(search_id)
        users = [{'id': user.id, 'chosen': user.chosen, 'reason': user.reason} for user in request.users]
        inputs = [grpc_utils.decode_value(input, self.data_directories) for input in request.inputs]

        request_id, object_id = automl_api.fit_solution(  # pylint: disable=unused-variable
            request.solution_id,
            inputs,
            scratch_dir=self.scratch_dir,
            expose_outputs=request.expose_outputs,
            expose_value_types=expose_value_types,
            users=users,
            volumes_dir=self.volumes_dir,
            compute_digest=self.compute_digest,
            strict_digest=self.strict_digest,
        )

        self.request_ids[request_id] = search_id
        self.expose_value_types[request_id] = expose_value_types

        return core_pb2.FitSolutionResponse(
            request_id=request_id,
        )

    @grpc_utils.map_exceptions_generator(logger, 'GetFitSolutionResults')
    def GetFitSolutionResults(  # noqa: N802 pylint: disable=invalid-name
        self,
        request: core_pb2.GetFitSolutionResultsRequest,
        context: grpc.ServicerContext,
    ) -> typing.Generator[core_pb2.GetFitSolutionResultsResponse, None, None]:
        if not request.request_id:
            raise grpc_exceptions.InvalidArgumentError("Request ID is missing.")

        search_id = self._get_search_id_from_request_id(request.request_id)

        logger.info(
            "[%(search_id)s] [GetFitSolutionResults] request_id='%(request_id)s'",
            {
                'search_id': search_id,
                'request_id': request.request_id,
            },
        )

        for document in automl_api.get_fit_solution_results(request.request_id):
            yield core_pb2.GetFitSolutionResultsResponse(
                progress=core_pb2.Progress(
                    state=document['state'].value,
                    status=document['status'],
                    start=grpc_utils.encode_timestamp(document['start']),
                    end=grpc_utils.encode_timestamp(document['end']),
                ),
                steps=self._construct_ran_solution_steps(document['steps']),
                # We do not have to check if "request_id" exists in "expose_value_types" because we
                # know that "request_id" is valid because "get_fit_solution_results" returned a document.
                exposed_outputs={
                    exposed_output['data']: grpc_utils.encode_value(
                        {'type': exposed_output['type'], 'value': exposed_output['value']},
                        self.expose_value_types[request.request_id],
                        self.scratch_dir,
                        self.data_directories,
                    )
                    for exposed_output in document['exposed_outputs']
                },
                # If ObjectID is available, we return the request ID for the fitted solution ID,
                # because they are the same in our system. We exposed it even before fitting is
                # completed so that TA3 clients can streamline fit and produce requests.
                fitted_solution_id=request.request_id if document['object_id'] and document['state'] != automl_base.ProgressState.ERRORED else None,
            )

    def _construct_ran_solution_steps(self, steps: typing.Sequence) -> typing.Sequence[core_pb2.StepProgress]:
        return [
            core_pb2.StepProgress(
                progress=core_pb2.Progress(
                    state=step['state'].value,
                    status=step['status'],
                    start=grpc_utils.encode_timestamp(step['start']),
                    end=grpc_utils.encode_timestamp(step['end']),
                ),
                steps=self._construct_ran_solution_steps(step['steps']),
            )
            for step in steps
        ]

    @grpc_utils.map_exceptions(logger, 'ProduceSolution')
    def ProduceSolution(self, request: core_pb2.ProduceSolutionRequest, context: grpc.ServicerContext) -> core_pb2.ProduceSolutionResponse:  # noqa: N802 pylint: disable=invalid-name
        if not request.fitted_solution_id:
            raise grpc_exceptions.InvalidArgumentError("Fitted solution ID is missing.")

        # Our "fitted_solution_id" is the same as "request_id" of the task fitting the solution.
        search_id = self._get_search_id_from_request_id(request.fitted_solution_id)

        logger.info(
            "[%(search_id)s] [ProduceSolution] fitted_solution_id='%(fitted_solution_id)s'",
            {
                'search_id': search_id,
                'fitted_solution_id': request.fitted_solution_id,
            },
        )

        if request.expose_value_types:
            try:
                expose_value_types: typing.Sequence[automl_base.ValueType] = [automl_base.ValueType[expose_value_type] for expose_value_type in request.expose_value_types]
            except ValueError as error:
                raise grpc_exceptions.InvalidArgumentError("Unknown allowed value type.") from error
        else:
            expose_value_types = self._get_allowed_value_types(search_id)
        users = [{'id': user.id, 'chosen': user.chosen, 'reason': user.reason} for user in request.users]
        inputs = [grpc_utils.decode_value(input, self.data_directories) for input in request.inputs]

        request_id, object_id = automl_api.produce_solution(  # pylint: disable=unused-variable
            request.fitted_solution_id,
            inputs,
            scratch_dir=self.scratch_dir,
            expose_outputs=request.expose_outputs,
            expose_value_types=expose_value_types,
            users=users,
            compute_digest=self.compute_digest,
            strict_digest=self.strict_digest,
        )

        self.request_ids[request_id] = search_id
        self.expose_value_types[request_id] = expose_value_types

        return core_pb2.ProduceSolutionResponse(
            request_id=request_id,
        )

    @grpc_utils.map_exceptions_generator(logger, 'GetProduceSolutionResults')
    def GetProduceSolutionResults(  # noqa: N802 pylint: disable=invalid-name
        self,
        request: core_pb2.GetProduceSolutionResultsRequest,
        context: grpc.ServicerContext,
    ) -> typing.Generator[core_pb2.GetProduceSolutionResultsResponse, None, None]:
        if not request.request_id:
            raise grpc_exceptions.InvalidArgumentError("Request ID is missing.")

        search_id = self._get_search_id_from_request_id(request.request_id)

        logger.info(
            "[%(search_id)s] [GetProduceSolutionResults] request_id='%(request_id)s'",
            {
                'search_id': search_id,
                'request_id': request.request_id,
            },
        )

        for document in automl_api.get_produce_solution_results(request.request_id):
            yield core_pb2.GetProduceSolutionResultsResponse(
                progress=core_pb2.Progress(
                    state=document['state'].value,
                    status=document['status'],
                    start=grpc_utils.encode_timestamp(document['start']),
                    end=grpc_utils.encode_timestamp(document['end']),
                ),
                steps=self._construct_ran_solution_steps(document['steps']),
                # We do not have to check if "request_id" exists in "expose_value_types" because we
                # know that "request_id" is valid because "get_fit_solution_results" returned a document.
                exposed_outputs={
                    exposed_output['data']: grpc_utils.encode_value(
                        {'type': exposed_output['type'], 'value': exposed_output['value']},
                        self.expose_value_types[request.request_id],
                        self.scratch_dir,
                        self.data_directories,
                    )
                    for exposed_output in document['exposed_outputs']
                },
            )

    @grpc_utils.map_exceptions(logger, 'SolutionExport')
    def SolutionExport(self, request: core_pb2.SolutionExportRequest, context: grpc.ServicerContext) -> core_pb2.SolutionExportResponse:  # noqa: N802 pylint: disable=invalid-name
        if not request.solution_id:
            raise grpc_exceptions.InvalidArgumentError("Solution ID is missing.")

        search_id = self._get_search_id_from_solution_id(request.solution_id)

        logger.info(
            "[%(search_id)s] [SolutionExport] solution_id='%(solution_id)s'",
            {
                'search_id': search_id,
                'solution_id': request.solution_id,
            },
        )

        object_id = automl_api.solution_export(
            request.solution_id,
            pipelines_ranked_dir=os.path.join(self.output_dir, search_id, 'pipelines_ranked'),
            pipelines_scored_dir=os.path.join(self.output_dir, search_id, 'pipelines_scored'),
            subpipelines_dir=os.path.join(self.output_dir, search_id, 'subpipelines'),
            pipeline_rank=request.rank,
        )

        # Wait for task to finish before returning. Raise an exception if it fails.
        ray.get(object_id)

        return core_pb2.SolutionExportResponse()

    @grpc_utils.map_exceptions(logger, 'ListPrimitives')
    def ListPrimitives(self, request: core_pb2.ListPrimitivesRequest, context: grpc.ServicerContext) -> core_pb2.ListPrimitivesResponse:  # noqa: N802 pylint: disable=invalid-name
        logger.info("[ListPrimitives]")

        primitives.store.is_populated(block=True)  # pylint: disable=no-member

        return core_pb2.ListPrimitivesResponse(
            primitives=[grpc_utils.encode_primitive(primitive) for primitive in primitives.store.list()],  # pylint: disable=no-member
        )

    @grpc_utils.map_exceptions(logger, 'Hello')
    def Hello(self, request: core_pb2.HelloRequest, context: grpc.ServicerContext) -> core_pb2.HelloResponse:  # noqa: N802 pylint: disable=invalid-name
        logger.info("[Hello]")

        primitives.store.is_populated(block=True)  # pylint: disable=no-member

        version = core_pb2.DESCRIPTOR.GetOptions().Extensions[core_pb2.protocol_version]

        return core_pb2.HelloResponse(
            user_agent=USER_AGENT,
            version=version,
            allowed_value_types=[
                automl_base.ValueType.RAW.name,  # pylint: disable=no-member
                automl_base.ValueType.DATASET_URI.name,  # pylint: disable=no-member
                automl_base.ValueType.PICKLE_BLOB.name,  # pylint: disable=no-member
                automl_base.ValueType.PICKLE_URI.name,  # pylint: disable=no-member
                automl_base.ValueType.CSV_URI.name,  # pylint: disable=no-member
            ],
            supported_extensions=[],
            supported_task_keywords=[keyword.name for keyword in problem_module.TaskKeyword],
            supported_performance_metrics=[metric.name for metric in problem_module.PerformanceMetric],
            supported_evaluation_methods=[method.name for method in automl_base.EvaluationMethod],
            supported_search_features=[],
        )

    @grpc_utils.map_exceptions(logger, 'DataAvailable')
    def DataAvailable(self, request: core_pb2.DataAvailableRequest, context: grpc.ServicerContext) -> core_pb2.DataAvailableResponse:  # noqa: N802 pylint: disable=invalid-name
        logger.info("[DataAvailable]")

        # TODO: Do something better.
        return core_pb2.DataAvailableResponse()


def start_server(
    *,
    executor: futures.Executor,
    input_dir: str,
    output_dir: str,
    scratch_dir: str,
    address: str = d3m_daemon.ADDRESS,
    client_only: bool = True,
    number_of_cpus: int = None,
    number_of_gpus: int = None,
    object_store_memory_limit: int = None,
    plasma_dir: str = None,
    huge_pages: bool = False,
    plasma_socket: str = None,
    pipeline_search_paths: typing.Sequence[str] = None,
    volumes_dir: str = None,
    dataset_search_paths: typing.Sequence[str] = None,
    compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING,
    strict_resolving: bool = False,
    strict_digest: bool = False,
) -> grpc.Server:
    """
    Starts a GRPC server.

    Server has to be manually stopped.

    Parameters
    ----------
    executor:
        Asynchronous executor to use.
    input_dir:
        Path to a base input directory.
    output_dir:
        Path to a base output directory.
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    address:
        Address at which to listen. By default on all addresses and port 45042.
    client_only:
        Only connect as a client to services like MongoDB and Ray and do not start them in the background.
    number_of_cpus:
        Number of CPUs to use by Ray when starting it in the background (``client_only`` set to ``False``).
    number_of_gpus:
        Number of GPUs to use by Ray when starting it in the background (``client_only`` set to ``False``).
    object_store_memory_limit:
        Memory limit in bytes to use by Ray object store when starting it in the background (``client_only`` set to ``False``).
    plasma_dir:
        Path to a Plasma directory to use by Ray when starting it in the background (``client_only`` set to ``False``).
    huge_pages:
        Does Plasma directory support huge pages?
    plasma_socket:
        Path to a Plasma socket to use by Ray when starting in the background (``client_only`` set to ``False``).
    pipeline_search_paths:
        A list of paths to directories with pipelines to resolve from.
        Their files should be named ``<pipeline id>.json`` or ``<pipeline id>.yml``.
    volumes_dir:
        Path to a directory with static files required by primitives.
    dataset_search_paths:
        Directory paths with datasets (and problem descriptions).
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_resolving:
        If resolved pipeline or primitive does not fully match specified reference, raise an exception?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?

    Returns
    -------
    A GRPC server instance.
    """

    # pylint: disable=too-many-locals

    logger.info(
        (
            "Starting GRPC server: "
            "input_dir=%(input_dir)s, "
            "output_dir=%(output_dir)s, "
            "scratch_dir=%(scratch_dir)s, "
            "address=%(address)s, "
            "client_only=%(client_only)s, "
            "number_of_cpus=%(number_of_cpus)s, "
            "number_of_gpus=%(number_of_gpus)s, "
            "object_store_memory_limit=%(object_store_memory_limit)s, "
            "plasma_dir=%(plasma_dir)s, "
            "huge_pages=%(huge_pages)s, "
            "plasma_socket=%(plasma_socket)s, "
            "pipeline_search_paths=%(pipeline_search_paths)s, "
            "volumes_dir=%(volumes_dir)s, "
            "dataset_search_paths=%(dataset_search_paths)s, "
            "compute_digest=%(compute_digest)s, "
            "strict_resolving=%(strict_resolving)s, "
            "strict_digest=%(strict_digest)s"
        ),
        {
            'input_dir': input_dir,
            'output_dir': output_dir,
            'scratch_dir': scratch_dir,
            'address': address,
            'client_only': client_only,
            'number_of_cpus': number_of_cpus,
            'number_of_gpus': number_of_gpus,
            'object_store_memory_limit': object_store_memory_limit,
            'plasma_dir': plasma_dir,
            'huge_pages': huge_pages,
            'plasma_socket': plasma_socket,
            'pipeline_search_paths': pipeline_search_paths,
            'volumes_dir': volumes_dir,
            'dataset_search_paths': dataset_search_paths,
            'compute_digest': compute_digest,
            'strict_resolving': strict_resolving,
            'strict_digest': strict_digest,
        },
    )

    # We can call "populate" multiple times and it will populate primitives only once.
    primitives.store.populate()

    automl_base.init(
        client_only,
        number_of_cpus=number_of_cpus,
        number_of_gpus=number_of_gpus,
        object_store_memory_limit=object_store_memory_limit,
        plasma_dir=plasma_dir,
        huge_pages=huge_pages,
        plasma_socket=plasma_socket,
        dataset_search_paths=dataset_search_paths,
    )

    server = grpc.server(executor)
    core_pb2_grpc.add_CoreServicer_to_server(
        _CoreServicer(
            data_directories=[input_dir, output_dir],
            output_dir=output_dir,
            scratch_dir=scratch_dir,
            pipeline_search_paths=pipeline_search_paths,
            volumes_dir=volumes_dir,
            compute_digest=compute_digest,
            strict_resolving=strict_resolving,
            strict_digest=strict_digest,
        ),
        server,
    )
    server.add_insecure_port(address)
    server.start()

    logger.info("GRPC server started.")

    return server


def handler(
    *,
    input_dir: str,
    output_dir: str,
    scratch_dir: str,
    address: str = d3m_daemon.ADDRESS,
    client_only: bool = False,
    number_of_cpus: int = None,
    number_of_gpus: int = None,
    object_store_memory_limit: int = None,
    plasma_dir: str = None,
    huge_pages: bool = False,
    plasma_socket: str = None,
    pipeline_search_paths: typing.Sequence[str] = None,
    volumes_dir: str = None,
    dataset_search_paths: typing.Sequence[str] = None,
    compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING,
    strict_resolving: bool = False,
    strict_digest: bool = False,
) -> None:
    """
    Starts listening for GRPC clients and responding to their requests.

    Parameters
    ----------
    input_dir:
        Base input directory.
    output_dir:
        Base output directory.
    scratch_dir:
        Path to a directory to store any temporary files needed during execution.
    address:
        Address at which to listen. By default on all addresses and port 45042.
    client_only:
        Only connect as a client to services like MongoDB and Ray and do not start them in the background.
    number_of_cpus:
        Number of CPUs to use by Ray when starting it in the background (``client_only`` set to ``False``).
    number_of_gpus:
        Number of GPUs to use by Ray when starting it in the background (``client_only`` set to ``False``).
    object_store_memory_limit:
        Memory limit in bytes to use by Ray object store when starting it in the background (``client_only`` set to ``False``).
    plasma_dir:
        Path to a Plasma directory to use by Ray when starting it in the background (``client_only`` set to ``False``).
    huge_pages:
        Does Plasma directory support huge pages?
    plasma_socket:
        Path to a Plasma socket to use by Ray when starting in the background (``client_only`` set to ``False``).
    pipeline_search_paths:
        A list of paths to directories with pipelines to resolve from.
        Their files should be named ``<pipeline id>.json`` or ``<pipeline id>.yml``.
    volumes_dir:
        Path to a directory with static files required by primitives.
    dataset_search_paths:
        Directory paths with datasets (and problem descriptions).
    compute_digest:
        When loading datasets, compute a digest over the data?
    strict_resolving:
        If resolved pipeline or primitive does not fully match specified reference, raise an exception?
    strict_digest:
        If computed digest does not match the one provided in metadata, raise an exception?
    """

    # pylint: disable=too-many-locals

    with futures.ThreadPoolExecutor(max_workers=1000) as executor:
        server = start_server(
            executor=executor,
            input_dir=input_dir,
            output_dir=output_dir,
            scratch_dir=scratch_dir,
            address=address,
            client_only=client_only,
            number_of_cpus=number_of_cpus,
            number_of_gpus=number_of_gpus,
            object_store_memory_limit=object_store_memory_limit,
            plasma_dir=plasma_dir,
            huge_pages=huge_pages,
            plasma_socket=plasma_socket,
            pipeline_search_paths=pipeline_search_paths,
            volumes_dir=volumes_dir,
            dataset_search_paths=dataset_search_paths,
            compute_digest=compute_digest,
            strict_resolving=strict_resolving,
            strict_digest=strict_digest,
        )

        logger.info("Listening on %(address)s.", {'address': address})

        try:
            while True:
                time.sleep(ONE_DAY_IN_SECONDS)
        except KeyboardInterrupt:
            logger.info("Terminating.")
            server.stop(5)
