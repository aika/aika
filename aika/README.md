# Aika

Aika automatically builds best machine learning pipelines and deep learning
architectures. It consists of the following components:

* [Datasets](./datasets/) provides datasets available to Aika.
* [Problems](./problems/) provides problem descriptions available to Aika.
* [Primitives](./primitives/) are functions available to Aika for use in its pipelines.
* [Pipelines](./pipelines/) are pipelines themselves with their execution logic.
