import typing

from d3m import runtime
from d3m.container import dataset as dataset_module

from aika.datasets import utils as datasets_utils
from aika.pipelines import pipeline as pipeline_module, utils as pipelines_utils
from aika.problems import utils as problems_utils


def get_pipeline(pipeline_path: str, *, strict_resolving: bool = False, strict_digest: bool = False, pipeline_search_paths: typing.Sequence[str] = None) -> typing.Optional[pipeline_module.Pipeline]:
    """
    A pipeline resolver function which is integrated with Aika.

    Can be used with d3m core package top-level runtime function which expect
    a pipeline resolver function.
    """

    pipeline_search_paths = pipelines_utils.get_internal_pipeline_search_paths(pipeline_search_paths)
    return pipelines_utils.get_pipeline(
        pipeline_path,
        allow_pipeline_path=True,
        strict_resolving=strict_resolving,
        strict_digest=strict_digest,
        pipeline_search_paths=pipeline_search_paths,
    )


def parse_pipeline_run(
    pipeline_run_file: typing.TextIO,
    pipeline_search_paths: typing.Sequence[str],
    datasets_dir: typing.Optional[str],
    *,
    pipeline_resolver: typing.Callable = None,
    dataset_resolver: typing.Callable = None,
    problem_resolver: typing.Callable = None,
    strict_resolving: bool = False,
    compute_digest: dataset_module.ComputeDigest = dataset_module.ComputeDigest.ONLY_IF_MISSING,
    strict_digest: bool = False,
    handle_score_split: bool = True,
) -> typing.Sequence[typing.Dict[str, typing.Any]]:
    """
    A pipeline run parser which is integrated with Aika.

    Can be used with d3m core package top-level runtime function which expect
    a pipeline run parser.
    """

    # In Aika codebase this parameter is never passed.
    assert datasets_dir is None

    if pipeline_resolver is None:
        pipeline_resolver = pipelines_utils.get_pipeline
    if dataset_resolver is None:
        dataset_resolver = datasets_utils.get_dataset
    if problem_resolver is None:
        problem_resolver = problems_utils.get_problem

    return runtime.parse_pipeline_run(
        pipeline_run_file,
        pipeline_search_paths,
        None,
        pipeline_resolver=pipeline_resolver,
        dataset_resolver=dataset_resolver,
        problem_resolver=problem_resolver,
        strict_resolving=strict_resolving,
        compute_digest=compute_digest,
        strict_digest=strict_digest,
    )
