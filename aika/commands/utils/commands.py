# Arguments required for these commands are in "arguments.py"
# so that we do not load all modules required here.

import argparse
import json
import logging
import typing

import ray

from d3m import utils as d3m_utils
from d3m.container import dataset as dataset_module, pandas as pandas_module
from d3m.metadata import problem as problem_module

from aika.automl import api, base
from aika.pipelines import utils as pipelines_utils
from aika.utils import exceptions

logger = logging.getLogger(__name__)


def search_for_best_solution(problem_description: typing.Optional[problem_module.Problem], inputs: typing.Sequence[typing.Dict], arguments: argparse.Namespace) -> typing.Optional[str]:
    """
    Command utility which contains command logic for searching solutions.

    Parameters
    ----------
    problem_description:
        A problem description.
    inputs:
        A list of value dicts (with ``type`` and ``value`` fields) representing inputs.
        Dict representation of the value should be at the *intermediate level*.
    arguments:
        Arguments parsed from the command line.

    Returns
    -------
    ID of the best solution. ``None`` on error.
    """

    # pylint: disable=too-many-locals,too-many-branches,too-many-statements

    if arguments.template is not None:
        # When resolving a template from a trusted source (command line arguments we see as trusted)
        # we use also an internal pipeline search path to allow for experimentation using those pipelines.
        pipeline_search_paths = pipelines_utils.get_internal_pipeline_search_paths(arguments.pipeline_search_paths)
        template = pipelines_utils.get_pipeline(
            arguments.template,
            allow_pipeline_path=True,
            strict_resolving=arguments.strict_resolving,
            strict_digest=arguments.strict_digest,
            pipeline_search_paths=pipeline_search_paths,
        )
    else:
        template = None

    if arguments.runner_arguments is not None:
        runner_arguments = {name: json.loads(value) for name, value in arguments.runner_arguments}
    else:
        runner_arguments = {}

    if arguments.evaluator_arguments is not None:
        evaluator_arguments = {name: json.loads(value) for name, value in arguments.evaluator_arguments}
    else:
        evaluator_arguments = {}

    if arguments.searcher_arguments is not None:
        searcher_arguments = {name: json.loads(value) for name, value in arguments.searcher_arguments}
    else:
        searcher_arguments = {}

    if arguments.generator_arguments is not None:
        generator_arguments = {name: json.loads(value) for name, value in arguments.generator_arguments}
    else:
        generator_arguments = {}

    if arguments.rewriter_arguments is not None:
        rewriter_arguments = {name: json.loads(value) for name, value in arguments.rewriter_arguments}
    else:
        rewriter_arguments = {}

    if arguments.tuner_arguments is not None:
        tuner_arguments = {name: json.loads(value) for name, value in arguments.tuner_arguments}
    else:
        tuner_arguments = {}

    logger.info("Starting search.")

    search_id, object_id = api.search_solutions(
        problem_description,
        inputs,
        scratch_dir=arguments.scratch_dir,
        time_bound_search=arguments.time_bound_search,
        time_bound_run=arguments.time_bound_run,
        priority=arguments.priority,
        template=template,
        runner_class=arguments.runner_class,
        runner_arguments=runner_arguments,
        evaluator_class=arguments.evaluator_class,
        evaluator_arguments=evaluator_arguments,
        searcher_class=arguments.searcher_class,
        searcher_arguments=searcher_arguments,
        generator_class=arguments.generator_class,
        generator_arguments=generator_arguments,
        rewriter_class=arguments.rewriter_class,
        rewriter_arguments=rewriter_arguments,
        tuner_class=arguments.tuner_class,
        tuner_arguments=tuner_arguments,
        random_seed=arguments.random_seed,
        volumes_dir=arguments.volumes_dir,
        compute_digest=dataset_module.ComputeDigest[arguments.compute_digest],
        strict_resolving=arguments.strict_resolving,
        strict_digest=arguments.strict_digest,
    )

    logger.info("Search ID: %(search_id)s", {'search_id': search_id})

    best_solution_id = None
    best_solution_internal_score = None
    seen_solution_ids = set()

    search_solutions_result = None
    for search_solutions_result in api.get_search_solutions_results(search_id):
        solution_id = search_solutions_result.get('solution_id', None)

        if not solution_id:
            continue

        if solution_id in seen_solution_ids:
            continue
        seen_solution_ids.add(solution_id)

        if best_solution_internal_score is None or search_solutions_result['internal_score'] > best_solution_internal_score:
            logger.info(
                "Found a better solution '%(solution_id)s' with internal score %(internal_score)s.",
                {
                    'solution_id': solution_id,
                    'internal_score': search_solutions_result['internal_score'],
                },
            )

            best_solution_id = solution_id
            best_solution_internal_score = search_solutions_result['internal_score']
        else:
            logger.info(
                "Found a solution '%(solution_id)s' with internal score %(internal_score)s.",
                {
                    'solution_id': solution_id,
                    'internal_score': search_solutions_result['internal_score'],
                },
            )

        solution_result_document = {
            'solution_id': search_solutions_result['solution_id'],
            'internal_score': search_solutions_result['internal_score'],
            'search_scores': search_solutions_result['search_scores'],
        }

        logger.debug("Solution result: %(solution_result)s", {'solution_result': json.dumps(solution_result_document, indent=4, cls=d3m_utils.JsonEncoder)})

    if search_solutions_result is None:
        # This should not really happen. There should always be a document available because it is inserted with "search_solutions".
        raise exceptions.InvalidStateError("No search solutions result.")

    try:
        # We block until the request finishes.
        ray.get(object_id)
    except Exception:  # pylint: disable=broad-except
        assert search_solutions_result['state'] == base.ProgressState.ERRORED, search_solutions_result['state']
        logger.exception("Search failed: %(status)s", {'status': search_solutions_result['status']})
        return None
    else:
        assert search_solutions_result['state'] == base.ProgressState.COMPLETED, search_solutions_result['state']
        logger.info("Search finished.")

    return best_solution_id


def fit_solution(
    solution_id: str,
    inputs: typing.Sequence[typing.Dict],
    return_predictions: bool,
    arguments: argparse.Namespace,
) -> typing.Union[typing.Optional[str], typing.Tuple[typing.Optional[str], typing.Optional[pandas_module.DataFrame]]]:
    """
    Command utility which contains command logic for fitting a solution.

    Parameters
    ----------
    solution_id:
        A solution ID.
    inputs:
        A list of value dicts (with ``type`` and ``value`` fields) representing inputs.
        Dict representation of the value should be at the *intermediate level*.
    return_predictions:
        Should the function also return predictions produced during fitting.
    arguments:
        Arguments parsed from the command line.

    Returns
    -------
    Fitted solution ID, or a tuple of one and predictions produced during fitting
    if ``return_predictions`` is set to ``True``. ``None`` or tuple of two ``None``
    on error.
    """

    logger.info("Starting fitting.")

    if return_predictions:
        kwargs = {
            'expose_outputs': ['outputs.0'],
            'expose_value_types': [base.ValueType.PLASMA_ID],
        }
    else:
        kwargs = {}

    request_id, object_id = api.fit_solution(
        solution_id,
        inputs,
        scratch_dir=arguments.scratch_dir,
        random_seed=arguments.random_seed,
        volumes_dir=arguments.volumes_dir,
        compute_digest=dataset_module.ComputeDigest[arguments.compute_digest],
        strict_digest=arguments.strict_digest,
        **kwargs,
    )

    logger.info("Fitted solution ID: %(request_id)s", {'request_id': request_id})

    try:
        # We block until the request finishes.
        ray.get(object_id)
    except Exception:  # pylint: disable=broad-except
        logger.exception("Fitting failed.")
        if return_predictions:
            return None, None
        else:
            return None
    else:
        logger.info("Fitting finished.")

    if return_predictions:
        predictions = None
        for document in api.get_fit_solution_results(request_id):
            assert document['state'] == base.ProgressState.COMPLETED, document['state']

            for exposed_output in document['exposed_outputs']:
                if exposed_output['data'] == 'outputs.0':
                    assert predictions is None, predictions
                    predictions = base.load_value(exposed_output, compute_digest=dataset_module.ComputeDigest[arguments.compute_digest], strict_digest=arguments.strict_digest)
                    break

        assert predictions is not None

        return request_id, predictions
    else:
        return request_id


def produce_solution(fitted_solution_id: str, inputs: typing.Sequence[typing.Dict], arguments: argparse.Namespace) -> typing.Optional[pandas_module.DataFrame]:
    """
    Command utility which contains command logic for producing a fitted solution.

    Parameters
    ----------
    fitted_solution_id:
        A fitted solution ID.
    inputs:
        A list of value dicts (with ``type`` and ``value`` fields) representing inputs.
        Dict representation of the value should be at the *intermediate level*.
    arguments:
        Arguments parsed from the command line.

    Returns
    -------
    Produced predictions. ``None`` on error.
    """

    logger.info("Starting producing.")

    request_id, object_id = api.produce_solution(
        fitted_solution_id,
        inputs,
        scratch_dir=arguments.scratch_dir,
        expose_outputs=['outputs.0'],
        expose_value_types=[base.ValueType.PLASMA_ID],
        compute_digest=dataset_module.ComputeDigest[arguments.compute_digest],
        strict_digest=arguments.strict_digest,
    )

    logger.info("Request ID: %(request_id)s", {'request_id': request_id})

    try:
        # We block until the request finishes.
        ray.get(object_id)
    except Exception:  # pylint: disable=broad-except
        logger.exception("Producing failed.")
        return None
    else:
        logger.info("Producing finished.")

    predictions = None
    for document in api.get_produce_solution_results(request_id):
        assert document['state'] == base.ProgressState.COMPLETED, document['state']

        for exposed_output in document['exposed_outputs']:
            if exposed_output['data'] == 'outputs.0':
                assert predictions is None, predictions
                predictions = base.load_value(exposed_output, compute_digest=dataset_module.ComputeDigest[arguments.compute_digest], strict_digest=arguments.strict_digest)
                break

    assert predictions is not None

    return predictions
