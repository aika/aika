import argparse

# This is in a separate file and not in "commands.py" so that
# we do not load all modules required there.


def search_for_best_solution(parser: argparse.ArgumentParser) -> None:
    """
    Add arguments required by the `search_for_best_solution` command utility.

    Parameters
    ----------
    parser:
        Parser to which to add arguments.
    """

    parser.add_argument(
        '--time-bound-search',
        type=float,
        action='store',
        default=0,
        help="upper limit on time for solution search, in minutes",
    )
    parser.add_argument(
        '--time-bound-run',
        type=float,
        action='store',
        default=0,
        help="limit found solutions based on the time for one pass of their run, in minutes",
    )
    parser.add_argument(
        '--priority',
        type=float,
        action='store',
        default=0,
        help="search with higher number has higher priority when multiple searches are done in parallel",
    )
    parser.add_argument(
        '--template',
        action='store',
        help="pipeline template to use for search or to run, by default a regular solution search is done",
    )
    parser.add_argument(
        '--runner',
        action='store',
        default='aika.search.runner.ray.RayRunner',
        dest='runner_class',
        help="Python path to a runner class, default \"aika.search.runner.ray.RayRunner\"",
    )
    parser.add_argument(
        '--runner-argument',
        nargs=2,
        action='append',
        metavar=('NAME', 'VALUE'),
        dest='runner_arguments',
        help="runner argument name and its value, can be specified multiple times, value should be JSON-serialized",
    )
    parser.add_argument(
        '--evaluator',
        action='store',
        default='aika.search.evaluator.runtime.RuntimeEvaluator',
        dest='evaluator_class',
        help="Python path to a solution evaluator class, default \"aika.search.evaluator.runtime.RuntimeEvaluator\"",
    )
    parser.add_argument(
        '--evaluator-argument',
        nargs=2,
        action='append',
        metavar=('NAME', 'VALUE'),
        dest='evaluator_arguments',
        help="evaluator argument name and its value, can be specified multiple times, value should be JSON-serialized",
    )
    parser.add_argument(
        '--searcher',
        action='store',
        default='aika.search.searcher.exhaustive.ExhaustiveSolutionSearcher',
        dest='searcher_class',
        help="Python path to a solution searcher class, default \"aika.search.searcher.exhaustive.ExhaustiveSolutionSearcher\"",
    )
    parser.add_argument(
        '--searcher-argument',
        nargs=2,
        action='append',
        metavar=('NAME', 'VALUE'),
        dest='searcher_arguments',
        help="searcher argument name and its value, can be specified multiple times, value should be JSON-serialized",
    )
    parser.add_argument(
        '--generator',
        action='store',
        default='aika.search.generator.chain.ChainGeneratorsPipelineGenerator',
        dest='generator_class',
        help="Python path to a pipeline generator class, default \"aika.search.generator.chain.ChainGeneratorsPipelineGenerator\"",
    )
    parser.add_argument(
        '--generator-argument',
        nargs=2,
        action='append',
        metavar=('NAME', 'VALUE'),
        dest='generator_arguments',
        help="generator argument name and its value, can be specified multiple times, value should be JSON-serialized",
    )
    parser.add_argument(
        '--rewriter',
        action='store',
        default='aika.search.rewriter.passthrough.PassthroughPipelineRewriter',
        dest='rewriter_class',
        help="Python path to a pipeline rewriter class, default \"aika.search.rewriter.passthrough.PassthroughPipelineRewriter\"",
    )
    parser.add_argument(
        '--rewriter-argument',
        nargs=2,
        action='append',
        metavar=('NAME', 'VALUE'),
        dest='rewriter_arguments',
        help="rewriter argument name and its value, can be specified multiple times, value should be JSON-serialized",
    )
    parser.add_argument(
        '--tuner',
        action='store',
        default='aika.search.tuner.default.DefaultValuesPipelineTuner',
        dest='tuner_class',
        help="Python path to a pipeline tuner class, default \"aika.search.tuner.default.DefaultValuesPipelineTuner\"",
    )
    parser.add_argument(
        '--tuner-argument',
        nargs=2,
        action='append',
        metavar=('NAME', 'VALUE'),
        dest='tuner_arguments',
        help="tuner argument name and its value, can be specified multiple times, value should be JSON-serialized",
    )


def fit_solution(parser: argparse.ArgumentParser) -> None:
    """
    Add arguments required by the `fit_solution` command utility.

    Note: Currently none, but exists for future compatibility.

    Parameters
    ----------
    parser:
        Parser to which to add arguments.
    """


def produce_solution(parser: argparse.ArgumentParser) -> None:
    """
    Add arguments required by the `produce_solution` command utility.

    Note: Currently none, but exists for future compatibility.

    Parameters
    ----------
    parser:
        Parser to which to add arguments.
    """
