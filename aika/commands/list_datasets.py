import argparse


def handler(arguments: argparse.Namespace, subparser: argparse.ArgumentParser) -> None:
    """
    Handle the command.

    Parameters
    ----------
    arguments:
        Arguments parsed from the command line.
    subparser:
        A sub-parser for this command.
    """

    # Importing here so that loading of this module is quick for Bash autocomplete to be quick.

    import json

    from d3m import utils
    from d3m.container import dataset as dataset_module

    from aika import datasets

    datasets.store.set_directories(arguments.dataset_search_paths)
    datasets.store.populate(compute_digest=dataset_module.ComputeDigest[arguments.compute_digest], strict_digest=arguments.strict_digest)

    for dataset in datasets.store.list():  # pylint: disable=no-member
        if arguments.metadata:
            print(json.dumps(dataset.metadata.query(()), indent=1, cls=utils.JsonEncoder))  # noqa: L202
        elif arguments.name:
            print('{id}: {name}'.format(id=dataset.metadata.query(())['id'], name=dataset.metadata.query(())['name']))  # noqa: L202
        else:
            print(dataset.metadata.query(())['id'])  # noqa: L202


def register(parsers: argparse._SubParsersAction) -> None:  # pylint: disable=protected-access
    """
    Registers a command into the main command line parser.

    Parameters
    ----------
    parsers:
        Subparsers to add a command to.
    """

    parser = parsers.add_parser(
        'list-datasets',
        help="list datasets",
        description="List datasets in the datasets store.",
    )

    output_types_group = parser.add_mutually_exclusive_group()
    output_types_group.add_argument(
        '-m',
        '--metadata',
        action='store_true',
        help="print top-level metadata of a dataset instead of just its ID",
    )
    output_types_group.add_argument(
        '-n',
        '--name',
        action='store_true',
        help="print the name of a dataset alongside its ID",
    )

    # Register a handler.
    parser.set_defaults(handler=handler)
