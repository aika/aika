import argparse


def handler(arguments: argparse.Namespace, subparser: argparse.ArgumentParser) -> None:
    """
    Handle the command.

    Parameters
    ----------
    arguments:
        Arguments parsed from the command line.
    subparser:
        A sub-parser for this command.
    """

    # pylint: disable=too-many-locals

    # Importing here so that loading of this module is quick for Bash autocomplete to be quick.
    import json
    import logging

    import ray

    from d3m import runtime, utils as d3m_utils
    from d3m.container import dataset as dataset_module

    from aika.automl import api, base
    from aika.datasets import utils as datasets_utils
    from aika.pipelines import utils as pipelines_utils

    logger = logging.getLogger(__name__)

    base.init(
        arguments.client_only,
        number_of_cpus=arguments.number_of_cpus,
        number_of_gpus=arguments.number_of_gpus,
        object_store_memory_limit=arguments.object_store_memory_limit,
        plasma_dir=arguments.plasma_dir,
        huge_pages=arguments.huge_pages,
        plasma_socket=arguments.plasma_socket,
        dataset_search_paths=arguments.dataset_search_paths,
    )

    if arguments.metrics:
        metrics = runtime.get_metrics_from_list(arguments.metrics)
    else:
        solution = api.describe_solution(arguments.solution)
        metrics = runtime.get_metrics_from_problem_description(solution['problem'])

    inputs = [
        datasets_utils.get_dataset_value(dataset_id, compute_digest=dataset_module.ComputeDigest[arguments.compute_digest], strict_digest=arguments.strict_digest) for dataset_id in arguments.inputs
    ]

    if arguments.data_pipeline is not None:
        # When resolving a pipeline from a trusted source (command line arguments we see as trusted)
        # we use also an internal pipeline search path to allow for experimentation using those pipelines.
        pipeline_search_paths = pipelines_utils.get_internal_pipeline_search_paths(arguments.pipeline_search_paths)
        data_pipeline = pipelines_utils.get_pipeline(
            arguments.data_pipeline,
            allow_pipeline_path=True,
            strict_resolving=arguments.strict_resolving,
            strict_digest=arguments.strict_digest,
            pipeline_search_paths=pipeline_search_paths,
        )
    else:
        data_pipeline = None

    if arguments.scoring_pipeline is not None:
        # When resolving a pipeline from a trusted source (command line arguments we see as trusted)
        # we use also an internal pipeline search path to allow for experimentation using those pipelines.
        pipeline_search_paths = pipelines_utils.get_internal_pipeline_search_paths(arguments.pipeline_search_paths)
        scoring_pipeline = pipelines_utils.get_pipeline(
            arguments.scoring_pipeline,
            allow_pipeline_path=True,
            strict_resolving=arguments.strict_resolving,
            strict_digest=arguments.strict_digest,
            pipeline_search_paths=pipeline_search_paths,
        )
    else:
        scoring_pipeline = None

    logger.info("Starting scoring.")

    request_id, object_id = api.score_solution(
        arguments.solution,
        inputs,
        metrics,
        base.EvaluationMethod[arguments.evaluation_method],
        scratch_dir=arguments.scratch_dir,
        folds=arguments.folds,
        train_test_ratio=arguments.train_test_ratio,
        shuffle=arguments.shuffle,
        stratified=arguments.stratified,
        data_pipeline=data_pipeline,
        data_random_seed=arguments.data_random_seed,
        scoring_pipeline=scoring_pipeline,
        scoring_random_seed=arguments.scoring_random_seed,
        random_seed=arguments.random_seed,
        volumes_dir=arguments.volumes_dir,
        compute_digest=dataset_module.ComputeDigest[arguments.compute_digest],
        strict_resolving=arguments.strict_resolving,
        strict_digest=arguments.strict_digest,
    )

    logger.info("Request ID: %(request_id)s", {'request_id': request_id})

    try:
        # We block until the request finishes.
        ray.get(object_id)
    except Exception:  # pylint: disable=broad-except
        logger.exception("Scoring failed.")
        return
    else:
        logger.info("Scoring finished.")

    scores = None
    for document in api.get_score_solution_results(request_id):
        assert document['state'] == base.ProgressState.COMPLETED, document['state']

        scores = document['scores']

    assert scores is not None

    json.dump(scores, arguments.scores, indent=4, cls=d3m_utils.JsonEncoder)
    arguments.scores.write('\n')


def register(parsers: argparse._SubParsersAction) -> None:  # pylint: disable=protected-access
    """
    Registers a command into the main command line parser.

    Parameters
    ----------
    parsers:
        Subparsers to add a command to.
    """

    parser = parsers.add_parser(
        'score-solution',
        help="score the solution",
        description="Score the solution given inputs.",
    )

    # We initialize arguments lazily because we have to do expensive imports.
    # These arguments will be loaded only when this command is selected.
    def lazy_init() -> None:
        from d3m.metadata import problem

        from aika.automl import base

        parser.add_argument(
            '-s',
            '--solution',
            action='store',
            required=True,
            help="ID of a solution",
        )
        parser.add_argument(
            '-i',
            '--input',
            action='append',
            required=True,
            metavar='INPUT',
            dest='inputs',
            help="ID, path, or URI of an input dataset",
        )
        parser.add_argument(
            '-m',
            '--metric',
            choices=[metric.name for metric in problem.PerformanceMetric],
            action='append',
            metavar='METRIC',
            dest='metrics',
            help="metric to use, can be specified multiple times, default from problem description",
        )
        parser.add_argument(
            '-e',
            '--evaluation-method',
            choices=[evaluation_method.name for evaluation_method in base.EvaluationMethod],
            action='store',
            required=True,
            help="evaluation method to use for scoring",
        )
        parser.add_argument(
            '-c',
            '--scores',
            type=argparse.FileType('w', encoding='utf8'),
            default='-',
            action='store',
            help="save scores to a file, default stdout",
        )
        parser.add_argument(
            '--folds',
            type=int,
            action='store',
            help="number of folds for k-fold evaluation method",
        )
        parser.add_argument(
            '--train-test-ratio',
            type=float,
            action='store',
            default=0.75,
            help="ratio of train data to all data for holdout evaluation method",
        )
        parser.add_argument(
            '--shuffle',
            action='store_true',
            help="should sampling for the split be shuffled?",
        )
        parser.add_argument(
            '--stratified',
            action='store_true',
            help="should sampling for the split be stratified?",
        )
        parser.add_argument(
            '--data-pipeline',
            action='store',
            help="override default data preparation pipeline to use",
        )
        parser.add_argument(
            '--data-random-seed',
            type=int,
            action='store',
            default=0,
            help="random seed to use for data preparation",
        )
        parser.add_argument(
            '--scoring-pipeline',
            action='store',
            help="override default scoring pipeline to use",
        )
        parser.add_argument(
            '--scoring-random-seed',
            type=int,
            action='store',
            default=0,
            help="random seed to use for scoring",
        )

    parser.lazy_init = lazy_init  # type: ignore

    # Register a handler.
    parser.set_defaults(handler=handler)
