import argparse


def handler(arguments: argparse.Namespace, subparser: argparse.ArgumentParser) -> None:
    """
    Handle the command.

    Parameters
    ----------
    arguments:
        Arguments parsed from the command line.
    subparser:
        A sub-parser for this command.
    """

    # Importing here so that loading of this module is quick for Bash autocomplete to be quick.

    import json

    from d3m import utils

    from aika import primitives

    primitives.store.populate()

    for primitive in primitives.store.list():  # pylint: disable=no-member
        if arguments.metadata:
            print(json.dumps(primitive.metadata.query(), indent=1, cls=utils.JsonEncoder))  # noqa: L202
        elif arguments.name:
            print('{id}: {name}'.format(id=primitive.metadata.query()['id'], name=primitive.metadata.query()['name']))  # noqa: L202
        elif arguments.path:
            print('{id}: {path}'.format(id=primitive.metadata.query()['id'], path=primitive.metadata.query()['python_path']))  # noqa: L202
        else:
            print(primitive.metadata.query()['id'])  # noqa: L202


def register(parsers: argparse._SubParsersAction) -> None:  # pylint: disable=protected-access
    """
    Registers a command into the main command line parser.

    Parameters
    ----------
    parsers:
        Subparsers to add a command to.
    """

    parser = parsers.add_parser(
        'list-primitives',
        help="list primitives",
        description="List primitives in the primitives index.",
    )

    output_types_group = parser.add_mutually_exclusive_group()
    output_types_group.add_argument(
        '-m',
        '--metadata',
        action='store_true',
        help="print metadata of a primitive instead of just its ID",
    )
    output_types_group.add_argument(
        '-n',
        '--name',
        action='store_true',
        help="print the name of a primitive alongside its ID",
    )
    output_types_group.add_argument(
        '-p',
        '--path',
        action='store_true',
        help="print the Python path of a primitive alongside its ID",
    )

    # Register a handler.
    parser.set_defaults(handler=handler)
