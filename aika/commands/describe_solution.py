import argparse


def handler(arguments: argparse.Namespace, subparser: argparse.ArgumentParser) -> None:
    """
    Handle the command.

    Parameters
    ----------
    arguments:
        Arguments parsed from the command line.
    subparser:
        A sub-parser for this command.
    """

    # Importing here so that loading of this module is quick for Bash autocomplete to be quick.
    import json
    import sys

    from d3m import utils as d3m_utils

    from aika.automl import api, base
    from aika.pipelines import pipeline as pipeline_module

    original_stdout = sys.stdout

    base.init(
        arguments.client_only,
        number_of_cpus=arguments.number_of_cpus,
        number_of_gpus=arguments.number_of_gpus,
        object_store_memory_limit=arguments.object_store_memory_limit,
        plasma_dir=arguments.plasma_dir,
        huge_pages=arguments.huge_pages,
        plasma_socket=arguments.plasma_socket,
        dataset_search_paths=arguments.dataset_search_paths,
    )

    solution = api.describe_solution(arguments.solution)

    if arguments.yaml or arguments.json:
        # The pipeline had to be already resolved and fully nested and here we just load it, without allowing any additional
        # sub-pipelines resolving and not resolving primitives. This speeds up things and prevents triggering loading of all primitives.
        pipeline = pipeline_module.Pipeline.from_json_structure(
            solution['pipeline'],
            respect_environment_variable=False,
            no_primitive_resolving=True,
            no_pipeline_resolving=True,
        )
    else:
        pipeline = None

    if arguments.internal_score:
        print("Internal score: {internal_score}".format(internal_score=solution['internal_score']), file=original_stdout)  # noqa: L202

    if arguments.search_scores:
        print("Search scores:", file=original_stdout)  # noqa: L202
        json.dump(solution['search_scores'], original_stdout, indent=4, cls=d3m_utils.JsonEncoder)
        original_stdout.write('\n')

    if arguments.hyperparams:
        # TODO: Should we decode hyper-parameter values and display their Python string representation?
        #       This would probably require us to resolve primitives in the pipeline, to be able to decode hyper-parameter values.
        print("Hyper-parameters:", file=original_stdout)  # noqa: L202
        json.dump(solution['hyperparameter_values'], original_stdout, indent=4, cls=d3m_utils.JsonEncoder)
        original_stdout.write('\n')

    if pipeline is not None:
        if arguments.yaml:
            pipeline.to_yaml(original_stdout, nest_subpipelines=arguments.nest_subpipelines)
        if arguments.json:
            pipeline.to_json(original_stdout, nest_subpipelines=arguments.nest_subpipelines, indent=4)
        original_stdout.write('\n')


def register(parsers: argparse._SubParsersAction) -> None:  # pylint: disable=protected-access
    """
    Registers a command into the main command line parser.

    Parameters
    ----------
    parsers:
        Subparsers to add a command to.
    """

    parser = parsers.add_parser(
        'describe-solution',
        help="describe the solution",
        description="Request a detailed description of the found solution.",
    )
    parser.add_argument(
        '-s',
        '--solution',
        action='store',
        required=True,
        help="ID of a solution",
    )
    parser.add_argument(
        '--internal-score',
        action='store_true',
        help="display internal score of the solution",
    )
    parser.add_argument(
        '--search-scores',
        action='store_true',
        help="display solution's scores made during search",
    )
    parser.add_argument(
        '--hyperparams',
        action='store_true',
        help="display hyper-parameters of the solution",
    )

    pipeline_group = parser.add_mutually_exclusive_group()
    pipeline_group.add_argument(
        '-y',
        '--yaml',
        action='store_true',
        help="display solution's pipeline as YAML",
    )
    pipeline_group.add_argument(
        '-j',
        '--json',
        action='store_true',
        help="display solution's pipeline as JSON",
    )

    parser.add_argument(
        '--nest-subpipelines',
        action='store_true',
        help="when displaying the pipeline, nest sub-pipelines?",
    )

    # Register a handler.
    parser.set_defaults(handler=handler)
