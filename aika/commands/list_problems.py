import argparse


def handler(arguments: argparse.Namespace, subparser: argparse.ArgumentParser) -> None:
    """
    Handle the command.

    Parameters
    ----------
    arguments:
        Arguments parsed from the command line.
    subparser:
        A sub-parser for this command.
    """

    # Importing here so that loading of this module is quick for Bash autocomplete to be quick.

    import json

    from d3m import utils

    from aika import problems

    problems.store.set_directories(arguments.dataset_search_paths)
    problems.store.populate(strict_digest=arguments.strict_digest)

    for problem in problems.store.list():  # pylint: disable=no-member
        if arguments.metadata:
            print(json.dumps(problem, indent=1, cls=utils.JsonEncoder))  # noqa: L202
        else:
            print(problem['problem']['id'])  # noqa: L202


def register(parsers: argparse._SubParsersAction) -> None:  # pylint: disable=protected-access
    """
    Registers a command into the main command line parser.

    Parameters
    ----------
    parsers:
        Subparsers to add a command to.
    """

    parser = parsers.add_parser(
        'list-problems',
        help="list problems",
        description="List problems in the problems store.",
    )
    parser.add_argument(
        '-m',
        '--metadata',
        action='store_true',
        help="print problem description instead of just its ID",
    )

    # Register a handler.
    parser.set_defaults(handler=handler)
