import argparse


def handler(arguments: argparse.Namespace, subparser: argparse.ArgumentParser) -> None:
    """
    Handle the command.

    Parameters
    ----------
    arguments:
        Arguments parsed from the command line.
    subparser:
        A sub-parser for this command.
    """

    from d3m import cli

    from aika import datasets, problems
    from aika.datasets import utils as datasets_utils
    from aika.primitives import store as primitives_store  # pylint: disable=import-only-modules
    from aika.problems import utils as problems_utils
    from aika.pipelines import pipeline

    from .utils import runtime as runtime_utils

    if arguments.dataset_search_paths is not None:
        datasets.store.set_directories(arguments.dataset_search_paths)
        problems.store.set_directories(arguments.dataset_search_paths)

    if arguments.d3m_command == 'index':
        # So that all local primitives are available to D3M index commands.
        primitives_store.populate_local()

    cli.handler(
        arguments,
        subparser,
        pipeline_resolver=runtime_utils.get_pipeline,
        pipeline_run_parser=runtime_utils.parse_pipeline_run,
        dataset_resolver=datasets_utils.get_dataset,
        problem_resolver=problems_utils.get_problem,
        resolver_class=pipeline.Resolver,
        pipeline_class=pipeline.Pipeline,
    )


def register(parsers: argparse._SubParsersAction) -> None:  # pylint: disable=protected-access
    """
    Registers a command into the main command line parser.

    Parameters
    ----------
    parsers:
        Subparsers to add a command to.
    """

    parser = parsers.add_parser(
        'd3m',
        help="run a D3M command",
        description="Run a D3M core package command.",
    )

    # We initialize arguments lazily because we have to do expensive imports.
    # These arguments will be loaded only when this command is selected.
    def lazy_init() -> None:
        from d3m import cli

        cli.configure_parser(
            parser,
            skip_arguments=(
                'random_seed',
                'pipeline_search_paths',
                'volumes_dir',
                'datasets_dir',
                'scratch_dir',
                'pipeline_search_paths',
                'compute_digest',
                'strict_resolving',
                'strict_digest',
                'logging_level',
            ),
        )

    parser.lazy_init = lazy_init  # type: ignore

    # Register a handler.
    parser.set_defaults(handler=handler)
