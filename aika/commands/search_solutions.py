import argparse


def handler(arguments: argparse.Namespace, subparser: argparse.ArgumentParser) -> None:
    """
    Handle the command.

    Parameters
    ----------
    arguments:
        Arguments parsed from the command line.
    subparser:
        A sub-parser for this command.
    """

    # pylint: disable=too-many-locals

    # Importing here so that loading of this module is quick for Bash autocomplete to be quick.
    import logging

    from d3m.container import dataset as dataset_module

    from aika.automl import base
    from aika.datasets import utils as datasets_utils
    from aika.problems import utils as problems_utils

    from .utils import commands

    # This check is for the future API compatibility (where we will probably be able to do
    # something with just a problem or just inputs). Currently, our implementation is stricter
    # and requires both problem description and inputs for a not fully specified template pipeline.
    if not arguments.problem and not arguments.inputs:
        subparser.error("the following arguments are required: -p/--problem or -i/--input")

    logger = logging.getLogger(__name__)

    base.init(
        arguments.client_only,
        number_of_cpus=arguments.number_of_cpus,
        number_of_gpus=arguments.number_of_gpus,
        object_store_memory_limit=arguments.object_store_memory_limit,
        plasma_dir=arguments.plasma_dir,
        huge_pages=arguments.huge_pages,
        plasma_socket=arguments.plasma_socket,
        dataset_search_paths=arguments.dataset_search_paths,
    )

    logger.info("Loading problem and dataset(s).")

    problem_description = problems_utils.get_problem(arguments.problem)
    inputs = [
        datasets_utils.get_dataset_value(dataset_id, compute_digest=dataset_module.ComputeDigest[arguments.compute_digest], strict_digest=arguments.strict_digest) for dataset_id in arguments.inputs
    ]

    commands.search_for_best_solution(problem_description, inputs, arguments)


def register(parsers: argparse._SubParsersAction) -> None:  # pylint: disable=protected-access
    """
    Registers a command into the main command line parser.

    Parameters
    ----------
    parsers:
        Subparsers to add a command to.
    """

    from .utils import arguments

    parser = parsers.add_parser(
        'search-solutions',
        help="searches for problem solutions",
        description="Searches for problem solutions (pipelines + hyper-parameters for those pipelines).",
    )
    parser.add_argument(
        '-p',
        '--problem',
        action='store',
        help="ID, path, or URI of a problem description",
    )
    parser.add_argument(
        '-i',
        '--input',
        action='append',
        metavar='INPUT',
        dest='inputs',
        help="ID, path, or URI of an input dataset",
    )

    arguments.search_for_best_solution(parser)

    # Register a handler.
    parser.set_defaults(handler=handler)
