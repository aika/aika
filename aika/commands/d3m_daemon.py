import argparse

ADDRESS = '[::]:45042'


def handler(arguments: argparse.Namespace, subparser: argparse.ArgumentParser) -> None:
    """
    Handle the command.

    Parameters
    ----------
    arguments:
        Arguments parsed from the command line.
    subparser:
        A sub-parser for this command.
    """

    # Importing here so that loading of this module is quick for Bash autocomplete to be quick.
    from d3m.container import dataset as dataset_module

    from aika.contrib.d3m import daemon  # pylint: disable=cyclic-import

    daemon.handler(
        input_dir=arguments.input_dir,
        output_dir=arguments.output_dir,
        scratch_dir=arguments.scratch_dir,
        address=arguments.address,
        client_only=arguments.client_only,
        number_of_cpus=arguments.number_of_cpus,
        number_of_gpus=arguments.number_of_gpus,
        object_store_memory_limit=arguments.object_store_memory_limit,
        plasma_dir=arguments.plasma_dir,
        huge_pages=arguments.huge_pages,
        plasma_socket=arguments.plasma_socket,
        pipeline_search_paths=arguments.pipeline_search_paths,
        volumes_dir=arguments.volumes_dir,
        dataset_search_paths=arguments.dataset_search_paths,
        compute_digest=dataset_module.ComputeDigest[arguments.compute_digest],
        strict_resolving=arguments.strict_resolving,
        strict_digest=arguments.strict_digest,
    )


def register(parsers: argparse._SubParsersAction) -> None:  # pylint: disable=protected-access
    """
    Registers a command into the main command line parser.

    Parameters
    ----------
    parsers:
        Subparsers to add a command to.
    """

    parser = parsers.add_parser(
        'd3m-daemon',
        help="start a D3M TA2-TA3 API daemon",
        description="Start a D3M TA2-TA3 API daemon.",
    )
    parser.add_argument(
        '-i',
        '--input',
        action='store',
        required=True,
        dest='input_dir',
        help="a base input directory to use",
    )
    parser.add_argument(
        '-o',
        '--output',
        action='store',
        required=True,
        dest='output_dir',
        help="a base output directory to use",
    )
    parser.add_argument(
        '-a',
        '--address',
        default=ADDRESS,
        action='store',
        help="an address and port on which daemon should listen, by default {address}".format(address=ADDRESS),
    )

    # Register a handler.
    parser.set_defaults(handler=handler)
