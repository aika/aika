Modules in this package provide various command-line commands for Aika.

You can list all of them using:

```
$ aika -h
```

And get help for one of them:

```
$ aika <command> -h
```

All files in this directory are automatically loaded by Aika and `register`
function is called, which should then register a command.

Command handlers should import all modules inside its body and not
top-level of the source code file. This allows much faster loading
of commands for Bash autocomplete.

