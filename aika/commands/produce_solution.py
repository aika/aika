import argparse


def handler(arguments: argparse.Namespace, subparser: argparse.ArgumentParser) -> None:
    """
    Handle the command.

    Parameters
    ----------
    arguments:
        Arguments parsed from the command line.
    subparser:
        A sub-parser for this command.
    """

    # Importing here so that loading of this module is quick for Bash autocomplete to be quick.
    import logging

    from d3m.container import dataset as dataset_module

    from aika.automl import base
    from aika.datasets import utils

    from .utils import commands

    logger = logging.getLogger(__name__)

    base.init(
        arguments.client_only,
        number_of_cpus=arguments.number_of_cpus,
        number_of_gpus=arguments.number_of_gpus,
        object_store_memory_limit=arguments.object_store_memory_limit,
        plasma_dir=arguments.plasma_dir,
        huge_pages=arguments.huge_pages,
        plasma_socket=arguments.plasma_socket,
        dataset_search_paths=arguments.dataset_search_paths,
    )

    logger.info("Loading dataset(s).")

    inputs = [utils.get_dataset_value(dataset_id, compute_digest=dataset_module.ComputeDigest[arguments.compute_digest], strict_digest=arguments.strict_digest) for dataset_id in arguments.inputs]

    predictions = commands.produce_solution(arguments.fitted_solution, inputs, arguments)

    if predictions is None:
        return

    predictions.to_csv(arguments.output)


def register(parsers: argparse._SubParsersAction) -> None:  # pylint: disable=protected-access
    """
    Registers a command into the main command line parser.

    Parameters
    ----------
    parsers:
        Subparsers to add a command to.
    """

    from .utils import arguments

    parser = parsers.add_parser(
        'produce-solution',
        help="produce (execute) the solution",
        description="Produce (execute) the solution on given inputs.",
    )
    parser.add_argument(
        '-f',
        '--fitted-solution',
        action='store',
        required=True,
        help="ID of a fitted solution",
    )
    parser.add_argument(
        '-i',
        '--input',
        action='append',
        required=True,
        metavar='INPUT',
        dest='inputs',
        help="ID, path, or URI of an input dataset",
    )
    parser.add_argument(
        '-o',
        '--output',
        type=argparse.FileType('w', encoding='utf8'),
        default='-',
        action='store',
        help="save produced predictions to a file, default stdout",
    )

    arguments.produce_solution(parser)

    # Register a handler.
    parser.set_defaults(handler=handler)
