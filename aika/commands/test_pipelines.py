import argparse


def _test_pipeline(meta_path: str, other_arguments: argparse.Namespace) -> None:
    # pylint: disable=too-many-locals

    # Importing here so that loading of this module is quick for Bash autocomplete to be quick.
    import io
    import os.path

    import pandas
    from pandas import testing as pandas_testing

    from d3m import exceptions, runtime
    from d3m.metadata import base as metadata_base

    from aika.datasets import utils as datasets_utils
    from aika.problems import utils as problems_utils

    from .utils import runtime as runtime_utils

    assert meta_path.endswith('.meta'), meta_path

    for extension in ['.yaml', '.yml', '.json']:
        if os.path.exists(meta_path[:-5] + extension):
            pipeline_path = meta_path[:-5] + extension
            break
    else:
        raise exceptions.NotFoundError("Pipeline for meta file '{meta_path}' cannot be found.".format(meta_path=meta_path))

    existing_output_path = meta_path[:-5] + '.csv'

    with open(existing_output_path, 'r', encoding='utf8') as existing_output_file:
        existing_output_dataframe = pandas.read_csv(existing_output_file)

    output = io.StringIO()
    scores = io.StringIO()

    with open(meta_path, 'r', encoding='utf8') as meta_file:
        arguments = dict(other_arguments.__dict__)
        arguments.update(
            {
                'context': metadata_base.Context.TESTING.name,
                'pipeline': pipeline_path,
                'meta': meta_file,
                'scoring_pipeline': runtime.DEFAULT_SCORING_PIPELINE_PATH,
                'output': output,
                'scores': scores,
            },
        )

        runtime.fit_score_handler(
            argparse.Namespace(**arguments),
            pipeline_resolver=runtime_utils.get_pipeline,
            pipeline_run_parser=runtime_utils.parse_pipeline_run,
            dataset_resolver=datasets_utils.get_dataset,
            problem_resolver=problems_utils.get_problem,
        )

    output.seek(0)

    output_dataframe = pandas.read_csv(output)

    pandas_testing.assert_frame_equal(existing_output_dataframe, output_dataframe)


def handler(arguments: argparse.Namespace, subparser: argparse.ArgumentParser) -> None:
    """
    Handle the command.

    Parameters
    ----------
    arguments:
        Arguments parsed from the command line.
    subparser:
        A sub-parser for this command.
    """

    # pylint: disable=too-many-locals

    # Importing here so that loading of this module is quick for Bash autocomplete to be quick.
    import fnmatch
    import logging
    import os
    import sys

    from aika import datasets, problems

    logger = logging.getLogger(__name__)

    if arguments.dataset_search_paths is not None:
        datasets.store.set_directories(arguments.dataset_search_paths)
        problems.store.set_directories(arguments.dataset_search_paths)

    failed = False

    for pipelines_dir in arguments.pipelines:
        for dirpath, dirnames, filenames in os.walk(pipelines_dir, followlinks=True):  # pylint: disable=unused-variable
            for filename in filenames:
                if filename.endswith('.meta'):
                    meta_path = os.path.join(dirpath, filename)

                    if any(fnmatch.fnmatch(meta_path, pattern) for pattern in arguments.ignored_patterns or []):
                        logger.info("Ignoring pipeline '%(meta_path)s'.", {'meta_path': meta_path})
                        continue

                    try:
                        logger.debug("Testing pipeline '%(meta_path)s'.", {'meta_path': meta_path})
                        _test_pipeline(meta_path, arguments)
                        logger.info("Pipeline '%(meta_path)s' test successful.", {'meta_path': meta_path})
                    except Exception:  # pylint: disable=broad-except
                        failed = True
                        logger.exception("Pipeline '%(meta_path)s' test failed.", {'meta_path': meta_path})

    sys.exit(failed)


def register(parsers: argparse._SubParsersAction) -> None:  # pylint: disable=protected-access
    """
    Registers a command into the main command line parser.

    Parameters
    ----------
    parsers:
        Subparsers to add a command to.
    """

    parser = parsers.add_parser(
        'test-pipelines',
        help="test all pipelines",
        description="Test all pipelines using their meta files.",
    )
    parser.add_argument(
        '-i',
        '--ignore',
        metavar='PATTERN',
        action='append',
        dest='ignored_patterns',
        help="glob patterns of meta file paths (and their pipelines) to ignore",
    )
    parser.add_argument(
        'pipelines',
        action='store',
        nargs='+',
        help="directory with pipelines to test",
    )

    # Register a handler.
    parser.set_defaults(handler=handler)
