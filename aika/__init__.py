import logging
import os
import sys
import typing

# If not a Bash autocomplete invocation. For Bash autocomplete to be quick.
if not os.environ.get('_ARGCOMPLETE', None):
    from aika.utils import logging as logging_utils

    logger = logging.getLogger(__name__)

    REQUIRED_PYTHON_VERSION = (3, 6, 9)

    logging_utils.configure()

    def _version_string(parts: typing.Sequence[typing.Any]) -> str:  # noqa: L205
        return '.'.join(str(part) for part in parts)

    if REQUIRED_PYTHON_VERSION != tuple(sys.version_info[:3]):
        logging.warning(
            "Aika requires Python %(required_version)s, but it is running on %(current_version)s.",
            {
                'required_version': _version_string(REQUIRED_PYTHON_VERSION),
                'current_version': _version_string(sys.version_info[:3]),
            },
        )
