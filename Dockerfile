# syntax=docker/dockerfile:1.2.1
# REMEMBER: When updating the base Docker image, consider also updating requirements.txt because
#           some packages there override those from the base Docker image.
FROM registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-devel-20210301-112938

# We record the tag of the base Docker image.
ENV D3M_BASE_IMAGE_NAME=registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-devel-20210301-112938

# We record the digest of the base Docker image.
# You can obtain it by running:
# docker inspect --format='{{index .RepoDigests 0}}' <IMAGE_NAME> | cut -d '@' -f 2
ENV D3M_BASE_IMAGE_DIGEST=sha256:95f9bbd6726a276a0b1f9ce7a442b1b540523e52a7ec7da91b0e05b3766e46d0

# Install process supervisor.
RUN \
 --mount=target=/var/lib/apt,type=cache \
 --mount=target=/var/cache/apt,type=cache \
 apt-get update -q -q && \
 apt-get install --yes runit && \
 rm -rf /tmp/* /var/tmp/*

# MongoDB.
RUN \
 --mount=target=/var/lib/apt,type=cache \
 --mount=target=/var/cache/apt,type=cache \
 apt-get update -q -q && \
 apt-get install --yes dirmngr && \
 apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4 && \
 echo "deb https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" > /etc/apt/sources.list.d/mongodb-org-4.0.list && \
 apt-get update -q -q && \
 apt-get install --yes mongodb-org=4.0.8 mongodb-org-server=4.0.8 mongodb-org-shell=4.0.8 mongodb-org-mongos=4.0.8 mongodb-org-tools=4.0.8 && \
 rm -rf /tmp/* /var/tmp/*

# System depdendencies.
RUN \
 --mount=target=/var/lib/apt,type=cache \
 --mount=target=/var/cache/apt,type=cache \
 --mount=target=/packages.txt,type=bind,source=./packages.txt \
 apt-get update -q -q && \
 apt-get install --yes --no-install-recommends $(cat /packages.txt) && \
 rm -rf /tmp/* /var/tmp/*

# Python dependencies.
RUN \
 --mount=target=/var/lib/apt,type=cache \
 --mount=target=/var/cache/apt,type=cache \
 --mount=target=/requirements.txt,type=bind,source=./requirements.txt \
 pip3 install --no-cache-dir --upgrade --upgrade-strategy only-if-needed --requirement /requirements.txt && \
 rm -rf /tmp/* /var/tmp/*

# Bash autocomplete.
RUN activate-global-python-argcomplete

# Patching.
RUN \
 --mount=target=/var/lib/apt,type=cache \
 --mount=target=/var/cache/apt,type=cache \
 --mount=target=/patches,type=bind,source=./patches \
 apt-get update -q -q && \
 apt-get install --yes patch && \
 for patch in /patches/*; do patch --directory=/ --version-control=never -p0 --force "--input=$patch" || exit 1; done && \
 rm -rf /tmp/* /var/tmp/*

# Make dependencies on d3m core package not be strict. This minimizes warnings.
RUN \
 find /src /usr/local/lib/python3.6/dist-packages/ -name requires.txt -print0 | xargs -r -0 sed -i -E 's/^d3m[<>=]{2}.*$/d3m/g' && \
 find /src /usr/local/lib/python3.6/dist-packages/ -path '*dist-info/METADATA' -print0 | xargs -r -0 sed -i -E 's/^Requires-Dist: d3m .*$/Requires-Dist: d3m/g' && \
 pip3 --disable-pip-version-check check

# AutoML RPC API port.
EXPOSE 45042/tcp
# Ray Dashboard port.
EXPOSE 8265/tcp

# Datasets should be mounted here.
VOLUME /datasets
# Primitves' static files should be mounted here.
VOLUME /volumes
# MongoDB database.
VOLUME /var/lib/mongodb

# Workaround. See: https://github.com/ray-project/ray/issues/4638
ENV RAY_DEBUG_DISABLE_MEMORY_MONITOR 1

COPY ./etc/service /etc/service
COPY ./etc/mongodb.conf /etc/mongodb.conf
COPY ./bin/runsvdir-start /usr/local/sbin/runsvdir-start
COPY ./bin/entrypoint.sh /usr/local/bin/entrypoint.sh
COPY ./bin/aika /usr/local/bin/aika
COPY ./aika.pth /usr/local/lib/python3.6/dist-packages/aika.pth
COPY . /code

WORKDIR /code

# This argument should be provided during Docker build process.
# You can obtain it by running:
# git rev-parse HEAD
ARG aika_commit_sha
ENV AIKA_COMMIT_SHA=$aika_commit_sha

ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
