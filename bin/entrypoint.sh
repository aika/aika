#!/bin/bash -e

cd /code

function daemons() {
  # Useful for debugging.
  export
  # Run service supervisor.
  exec /usr/local/sbin/runsvdir-start
}

# D3M evaluation.
case "$D3MRUN" in
  ta2)
    daemons
    ;;
  ta2ta3)
    daemons
    ;;
esac

if [ $# -eq 0 ]; then
  # No arguments passed to Docker run. Use defaults.
  export D3MINPUTDIR=/datasets
  export D3MOUTPUTDIR=/datasets
  export D3MSTATICDIR=/volumes
  daemons
else
  # Just execute. Useful for debugging.
  "$@"
fi
