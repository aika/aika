import tokenize

from astroid import decorators

from pylint import checkers, interfaces, utils
from pylint.checkers import utils as checkers_utils

CLOSING_PARENTHESIS = {
    '(': ')',
    '{': '}',
    '[': ']',
}


class MultiLineArgumentsChecker(checkers.BaseTokenChecker):
    __implements__ = interfaces.ITokenChecker

    name = 'multi-line-arguments'
    priority = -1
    msgs = {
        'W5511': (
            "Closing parenthesis of multi-line arguments is not indented correctly.",
            'multi-line-arguments-start-end',
            "Closing parenthesis of multi-line arguments should be indented the same as beginning of the opening line.",
        ),
        'W5512': (
            "Multi-line arguments are not indented correctly.",
            'multi-line-arguments-indentation',
            "Multi-line arguments should use hanging indentation.",
        ),
        'W5513': (
            "Closing parenthesis of multi-line arguments is not on its own line.",
            'multi-line-arguments-end',
            "Closing parenthesis of multi-line arguments should be on its own line.",
        ),
    }

    @checkers_utils.check_messages('multi-line-arguments-start-end', 'multi-line-arguments-indentation', 'multi-line-arguments-end')
    def process_tokens(self, tokens):
        self._process_tokens(tokens, None, None)

    def _process_tokens(self, tokens, start_line, previous_token):
        open_start = None
        args_tokens = []
        result_tokens = []
        parenthesis = 0
        parenthesis_open_string = None
        parenthesis_close_string = None

        for token in tokens:
            result_tokens.append(token)

            # We skip dummy tokens.
            if token.type in (tokenize.ENCODING, tokenize.INDENT):
                continue

            # We are not inside arguments.
            if open_start is None:
                # We want to remember the first token which comes after the new line.
                if not previous_token or token.start[0] > previous_token.end[0]:
                    start_line = token.start

                # Do arguments start?
                if token.type == tokenize.OP and token.string in ['(', '{', '[']:
                    open_start = token.start
                    args_tokens = []
                    parenthesis = 1
                    parenthesis_open_string = token.string
                    parenthesis_close_string = CLOSING_PARENTHESIS[token.string]

            # We are inside arguments.
            elif open_start is not None:
                if token.type == tokenize.OP and token.string == parenthesis_open_string:
                    parenthesis += 1
                elif token.type == tokenize.OP and token.string == parenthesis_close_string:
                    parenthesis -= 1

                if token.type == tokenize.OP and token.string == parenthesis_close_string and parenthesis == 0:
                    self._process_args_tokens(start_line, previous_token, open_start, token.start, args_tokens)
                    # When current close parenthesis ended on the same line it was opened, we leave start_line
                    # as it is, because we require that the next close parenthesis match its indentation.
                    # Otherwise, we update star_line to where close parenthesis ended and to require
                    # next close parenthesis to match this indentation.
                    if start_line[0] != token.start[0]:
                        start_line = token.start
                    open_start = None
                    args_tokens = []
                    parenthesis_open_string = None
                    parenthesis_close_string = None
                else:
                    args_tokens.append(token)
                    result_tokens.pop()

            previous_token = token

        return result_tokens

    @decorators.cachedproperty
    def _indent(self):
        indent = utils.get_global_option(self, 'indent-string', default='        ')
        if indent == '\\t':  # \t is not interpreted in the configuration file.
            indent = '\t'
        return len(indent)

    def _process_args_tokens(self, start_line, previous_token, open_start, close_start, args_tokens):
        # We recurse first. This removes tokens at a nested level.
        args_tokens = self._process_tokens(args_tokens, start_line, previous_token)

        assert start_line[0] == open_start[0]

        # If arguments start and end on the same line, we do not have to do anything.
        if open_start[0] == close_start[0]:
            return

        # Closing arguments parenthesis should be indented the same as start of the opening line.
        if start_line[1] != close_start[1]:
            self.add_message(
                'multi-line-arguments-start-end', line=close_start[0],
            )

        current_line = None
        for token in args_tokens:
            if token.type == tokenize.COMMENT:
                continue

            if token.type in (tokenize.NL, tokenize.NEWLINE):
                current_line = None
            elif current_line is None:
                current_line = token.start[0]

                # Every new line of arguments should be indented once more than closing indentation.
                if start_line[1] + self._indent != token.start[1]:
                    self.add_message(
                        'multi-line-arguments-indentation', line=token.start[0],
                    )
            # If some tokens were removed when we recursed (including new line tokens),
            # the current token might be on a different line, so we update the current line.
            elif current_line != token.start[0]:
                current_line = token.start[0]

        if current_line is not None:
            self.add_message(
                'multi-line-arguments-end', line=token.start[0],
            )


def register(linter):
    linter.register_checker(MultiLineArgumentsChecker(linter))
