# Run by installing pytest and then run pytest in this directory.

from pylint import testutils

import multi_line_arguments


class TestMultiLineArgumentsChecker(testutils.CheckerTestCase):
    CHECKER_CLASS = multi_line_arguments.MultiLineArgumentsChecker
    CONFIG = {
        'indent_string': '    ',
    }

    def _assert_valid(self, code):
        with self.assertNoMessages():
            self.checker.process_tokens(testutils._tokenize_str(code))

    def test_valid_indentation_1(self):
        self._assert_valid(
            """
            call_long_function_name(
                var_two, var_three,
                var_four,
            )
            """
        )

    def test_valid_indentation_2(self):
        self._assert_valid(
            """
            call_long_function_name(var_two, var_three, var_four)
            """
        )

    def test_valid_indentation_3(self):
        self._assert_valid(
            """
            call_long_function_name(
                var_two, var_three,
                var_four=value,
            )
            """
        )

    def test_valid_indentation_4(self):
        self._assert_valid(
            """
            call_long_function_name(
                var_two,
                var_three={'test': 'value'},
                var_four=value,
            )
            """
        )

    def test_valid_indentation_5(self):
        self._assert_valid(
            """
            call_long_function_name(
                var_two,
                var_three={
                    'test': 'value',
                },
                var_four=value,
            )
            """
        )

    def test_valid_indentation_6(self):
        self._assert_valid(
            """
            call_long_function_name(
                var_zero,
                var_one(
                    foobar, test,
                    more, test,
                ), var_two, var_three,
                var_four,
            )
            """
        )

    def test_valid_indentation_7(self):
        self._assert_valid(
            """
            def call_long_function_name(var_two, var_three, var_four, a=var_one(foobar, test, more, test)):
                pass
            """
        )

    def test_valid_indentation_8(self):
        self._assert_valid(
            """
            def call_long_function_name(
                var_two, var_three,
                var_four,
                a=var_one(
                    foobar, test,
                    more, test,
                ),
            ):
                pass
            """
        )

    def test_valid_indentation_9(self):
        self._assert_valid(
            """
            logging.config.dictConfig(
                {
                    'version': 1,
                    'disable_existing_loggers': False,
                    'formatters': {
                        'indent': {
                            '()': pip_logging.IndentingFormatter,
                            'format': '%(message)s',
                        },
                    },
                    'handlers': {
                        'console': {
                            'level': 'INFO',
                            'class': 'pip.utils.logging.ColorizedStreamHandler',
                            'formatter': 'indent',
                        },
                    },
                    'root': {
                        'level': 'INFO',
                        'handlers': [
                            'console',
                        ],
                    },
                },
                {
                    'test': 'foobar',
                },
                arg3, arg4,
            )
            """
        )

    def test_valid_indentation_11(self):
        self._assert_valid(
            """
            {
                'version': 1,
                'disable_existing_loggers': False,
                'formatters': {
                    'indent': {
                        '()': pip_logging.IndentingFormatter,
                        'format': '%(message)s',
                    },
                },
                'handlers': {
                    'console': {
                        'level': 'INFO',
                        'class': 'pip.utils.logging.ColorizedStreamHandler',
                        'formatter': 'indent',
                    },
                },
                'root': {
                    'level': 'INFO',
                    'handlers': [
                        'console',
                    ],
                },
            }
            """
        )

    def test_valid_indentation_12(self):
        self._assert_valid(
            """
            call_long_function_name(
                '''
                    test
                '''
            )
            """
        )

    def test_valid_indentation_13(self):
        self._assert_valid(
            '''
            call_long_function_name(
                """
                    test
                """
            )
            '''
        )

    def test_valid_indentation_14(self):
        self._assert_valid(
            '''
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    def long_function_name(
                        var_one, var_two, var_three,
                        var_four):
                        print(var_one)
                    """
                )
            )
            '''
        )

    def test_valid_indentation_15(self):
        self._assert_valid(
            """
            with self.assertAddsMessages(
                pylint.testutils.Message(
                    msg_id='non-unique-returns',
                    node=return_node_b,
                ),
            ):
                self.checker.visit_return(return_node_b)
            """
        )

    def test_valid_indentation_16(self):
        self._assert_valid(
            """
            (name in meta) and \\
            (foo is None) and \\
            (bar is None)
            """
        )

    def test_valid_indentation_17(self):
        self._assert_valid(
            """
            data['key'].append(
                {
                    'foo': 'bar',
                }
            )
            """
        )

    def test_valid_indentation_18(self):
        self._assert_valid(
            """
            a = test.foobar(  # comment
                1,
            )
            """
        )

    def test_valid_indentation_19(self):
        self._assert_valid(
            """
            a, b = test.foobar(  # comment
                1,
            )
            """
        )

    def test_invalid_indentation_1(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=3,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=2,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=3,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-end',
                line=3,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    foo = long_function_name(var_one, var_two,
                                                                     var_three, var_four)
                    """
                )
            )

    def test_invalid_indentation_2(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=4,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=3,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=4,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-end',
                line=4,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    def long_function_name(
                            var_one, var_two, var_three,
                            var_four):
                        print(var_one)
                    """
                )
            )

    def test_invalid_indentation_3(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=4,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-end',
                line=4,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    foo = long_function_name(
                        var_one, var_two,
                        var_three, var_four)
                    """
                )
            )

    def test_invalid_indentation_4(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=3,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=2,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-end',
                line=3,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    foo = long_function_name(var_one, var_two,
                        var_three, var_four)
                    """
                )
            )

    def test_invalid_indentation_5(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=2,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    foo = long_function_name(var_one, var_two,
                        var_three, var_four
                    )
                    """
                )
            )

    def test_invalid_indentation_6(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=4,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-end',
                line=4,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    def long_function_name(
                        var_one, var_two, var_three,
                        var_four):
                        print(var_one)
                    """
                )
            )

    def test_invalid_indentation_7(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=26,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=2,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-end',
                line=26,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    logging.config.dictConfig({
                        'version': 1,
                        'disable_existing_loggers': False,
                        'formatters': {
                            'indent': {
                                '()': pip_logging.IndentingFormatter,
                                'format': '%(message)s',
                            },
                        },
                        'handlers': {
                            'console': {
                                'level': 'INFO',
                                'class': 'pip.utils.logging.ColorizedStreamHandler',
                                'formatter': 'indent',
                            },
                        },
                        'root': {
                            'level': 'INFO',
                            'handlers': [
                                'console',
                            ],
                        },
                    }, {
                        'test': 'foobar',
                    }, arg3, arg4)
                    """
                )
            )

    def test_invalid_indentation_8(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=9,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=2,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-end',
                line=9,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    '''
                    self.checker.process_tokens(testutils._tokenize_str(
                        """
                        def long_function_name(
                            var_one, var_two, var_three,
                            var_four):
                            print(var_one)
                        """
                    ))
                    '''
                )
            )

    def test_invalid_indentation_9(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=5,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    call_long_function_name(
                        var_two,
                        var_three={
                        'test': 'value',
                        },
                        var_four=value,
                    )
                    """
                )
            )

    def test_invalid_indentation_10(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=4,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    call_long_function_name(
                        var_zero,
                        var_one(foobar, test,
                            more, test,
                        ), var_two, var_three,
                        var_four,
                    )
                    """
                )
            )

    def test_invalid_indentation_11(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=7,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=8,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=6,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=13,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=14,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=15,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=12,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=21,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=19,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=20,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=3,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=4,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=5,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=11,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=18,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    {
                    'version': 1,
                    'disable_existing_loggers': False,
                    'formatters': {
                    'indent': {
                    '()': pip_logging.IndentingFormatter,
                    'format': '%(message)s',
                    },
                    },
                    'handlers': {
                    'console': {
                    'level': 'INFO',
                    'class': 'pip.utils.logging.ColorizedStreamHandler',
                    'formatter': 'indent',
                    },
                    },
                    'root': {
                    'level': 'INFO',
                    'handlers': [
                    'console',
                    ],
                    },
                    }
                    """
                )
            )

    def test_invalid_indentation_12(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=7,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=3,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    with self.assertAddsMessages(
                            pylint.testutils.Message(
                                msg_id='non-unique-returns',
                                node=return_node_b,
                            ),
                        ):
                        self.checker.visit_return(return_node_b)
                    """
                )
            )

    def test_invalid_indentation_13(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=6,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=3,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-end',
                line=6,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    with self.assertAddsMessages(
                            pylint.testutils.Message(
                                msg_id='non-unique-returns',
                                node=return_node_b,
                            )):
                        self.checker.visit_return(return_node_b)
                    """
                )
            )

    def test_invalid_indentation_14(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=6,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-end',
                line=6,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    with self.assertAddsMessages(
                        pylint.testutils.Message(
                            msg_id='non-unique-returns',
                            node=return_node_b,
                        )):
                        self.checker.visit_return(return_node_b)
                    """
                )
            )

    def test_invalid_indentation_15(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=6,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=3,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-end',
                line=6,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    with self.assertAddsMessages(
                    pylint.testutils.Message(
                        msg_id='non-unique-returns',
                        node=return_node_b,
                    )):
                        self.checker.visit_return(return_node_b)
                    """
                )
            )

    def test_invalid_indentation_16(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=4,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=2,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-end',
                line=2,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    call_long_function_name('''
                        test
                    ''')
                    """
                )
            )

    def test_invalid_indentation_17(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-start-end',
                line=4,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=2,
            ),
            testutils.Message(
                msg_id='multi-line-arguments-end',
                line=2,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    '''
                    call_long_function_name("""
                        test
                    """)
                    '''
                )
            )

    def test_invalid_indentation_18(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=2,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    """
                    call_long_function_name('''
                            test
                        '''
                    )
                    """
                )
            )

    def test_invalid_indentation_19(self):
        with self.assertAddsMessages(
            testutils.Message(
                msg_id='multi-line-arguments-indentation',
                line=2,
            ),
        ):
            self.checker.process_tokens(
                testutils._tokenize_str(
                    '''
                    call_long_function_name("""
                            test
                        """
                    )
                    '''
                )
            )
