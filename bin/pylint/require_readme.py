import os.path

from pylint import checkers, interfaces
from pylint.checkers import utils


class RequireReadmeChecker(checkers.BaseChecker):
    __implements__ = interfaces.IAstroidChecker

    name = 'require-readme'
    priority = -1
    msgs = {
        'W5501': (
            "README.md is missing in the package.",
            'require-readme',
            "All packages should have a README.md.",
        ),
    }
    options = (
        (
            'no-readme',
            {
                'default': [],
                'type':'csv',
                'metavar': '<modules without readme>',
                'help': "List of modules which are allowed to not have a readme, separated by a comma",
            }
        ),
    )

    @utils.check_messages('require-readme')
    def visit_module(self, node):
        if not node.package:
            return

        if node.name in self.config.no_readme:
            return

        if any(not os.path.exists(os.path.join(os.path.dirname(path), 'README.md')) for path in node.path):
            self.add_message(
                'require-readme', node=node,
            )


def register(linter):
    linter.register_checker(RequireReadmeChecker(linter))
