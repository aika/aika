import json
import os
import os.path
import tempfile
import unittest
from concurrent import futures

import grpc
import ray
from google.protobuf import json_format

from d3m import container
from d3m.container import dataset as dataset_module
from d3m.metadata import base as metadata_base
from d3m_automl_rpc import core_pb2, core_pb2_grpc, problem_pb2
from test_primitives import fail

from aika.automl import base as automl_base
from aika.contrib.d3m import daemon, grpc_utils
from aika.pipelines import pipeline as pipeline_module
from aika.problems import utils as problems_utils
from aika.utils import uri as uri_utils

TESTS_DATA_DIRECTORY = os.path.join(os.path.dirname(__file__), 'data')

# We have to set PYTHONPATH so that Ray workers can import this
# file to run "register_fail_primitive".
os.environ['PYTHONPATH'] = os.path.dirname(__file__) + (':' + os.environ['PYTHONPATH'] if 'PYTHONPATH' in os.environ else '')


def register_fail_primitive(worker_info):
    from d3m import index
    from test_primitives import fail

    index.register_primitive('d3m.primitives.operator.null.FailTest', fail.FailPrimitive)


@unittest.skipUnless(os.path.exists(TESTS_DATA_DIRECTORY), "Tests data not available.")
class TestTA2TA3Service(unittest.TestCase):
    def setUp(self):
        self.working_directory = tempfile.TemporaryDirectory()
        self.scratch_directory = os.path.join(self.working_directory.name, 'tmp')
        self.executor = futures.ThreadPoolExecutor(max_workers=1)

        os.makedirs(self.scratch_directory, 0o755, exist_ok=True)

        TESTING_ADDRESS = '127.0.0.1:40000'

        os.environ['AIKA_GENERATOR'] = 'aika.search.generator.templates.TemplatesBasedPipelineGenerator'

        self.server = daemon.start_server(
            executor=self.executor,
            input_dir=os.path.abspath(TESTS_DATA_DIRECTORY),
            output_dir=self.working_directory.name,
            scratch_dir=self.scratch_directory,
            address=TESTING_ADDRESS,
            client_only=os.environ.get('TEST_CLIENT_ONLY', False),
            number_of_cpus=4,
            number_of_gpus=0,
            volumes_dir='/tmp',
            compute_digest=dataset_module.ComputeDigest.ONLY_IF_MISSING,
            strict_digest=False,
        )

        self.data_directories = [os.path.abspath(TESTS_DATA_DIRECTORY), self.working_directory.name, self.scratch_directory]
        self.allowed_value_types = [
            automl_base.ValueType.RAW,
            automl_base.ValueType.DATASET_URI,
            automl_base.ValueType.PICKLE_BLOB,
            automl_base.ValueType.PICKLE_URI,
            automl_base.ValueType.CSV_URI,
        ]

        self.allowed_value_types_names = [value.name for value in self.allowed_value_types]

        ray.worker.global_worker.run_function_on_all_workers(register_fail_primitive)

        self.channel = grpc.insecure_channel(TESTING_ADDRESS)
        self.stub = core_pb2_grpc.CoreStub(self.channel)

    def tearDown(self):
        self.server.stop(5)
        self.executor.shutdown()
        self.channel.close()
        self.working_directory.cleanup()

    def test_hello(self):
        request = core_pb2.HelloRequest()
        response = self.stub.Hello(request)

        version = core_pb2.DESCRIPTOR.GetOptions().Extensions[core_pb2.protocol_version]

        self.assertEqual(response.version, version)
        self.assertEqual(response.allowed_value_types, self.allowed_value_types_names)

    def test_list_primitives(self):
        request = core_pb2.ListPrimitivesRequest()
        response = self.stub.ListPrimitives(request)

        self.assertTrue(response.primitives)

        primitives = [grpc_utils.decode_primitive(primitive) for primitive in response.primitives]

        denormalize_primitive = [primitive for primitive in primitives if primitive['id'] == 'f31f8c1f-d1c5-43e5-a4b2-2ae4a761ef2e']

        # We require denormalize primitive to be available for these tests.
        self.assertEqual(len(denormalize_primitive), 1)
        denormalize_primitive = denormalize_primitive[0]

        self.assertEqual(denormalize_primitive['id'], 'f31f8c1f-d1c5-43e5-a4b2-2ae4a761ef2e')
        self.assertEqual(denormalize_primitive['version'], '0.2.0')
        self.assertEqual(denormalize_primitive['python_path'], 'd3m.primitives.data_transformation.denormalize.Common')
        self.assertEqual(denormalize_primitive['name'], 'Denormalize datasets')

        remove_columns_primitive = [primitive for primitive in primitives if primitive['id'] == '3b09ba74-cc90-4f22-9e0a-0cf4f29a7e28']

        # We require remove columns primitive to be available for these tests.
        self.assertEqual(len(remove_columns_primitive), 1)
        remove_columns_primitive = remove_columns_primitive[0]

        self.assertEqual(remove_columns_primitive['id'], '3b09ba74-cc90-4f22-9e0a-0cf4f29a7e28')
        self.assertEqual(remove_columns_primitive['version'], '0.1.0')
        self.assertEqual(remove_columns_primitive['python_path'], 'd3m.primitives.data_transformation.remove_columns.Common')
        self.assertEqual(remove_columns_primitive['name'], 'Removes columns')

    def test_run_pipeline(self):
        version = core_pb2.DESCRIPTOR.GetOptions().Extensions[core_pb2.protocol_version]

        for allowed_value_type in [automl_base.ValueType.PICKLE_BLOB, automl_base.ValueType.PICKLE_URI, automl_base.ValueType.PLASMA_ID]:
            with open(os.path.join(TESTS_DATA_DIRECTORY, 'pipelines', 'random-sample.yml'), 'r', encoding='utf8') as pipeline_file:
                pipeline = pipeline_module.Pipeline.from_yaml(pipeline_file)

            pipeline.check(standard_pipeline=False)

            inputs = [container.List([0, 1, 42])]
            encoded_inputs = [grpc_utils.encode_value({'type': 'object', 'value': input}, [allowed_value_type], self.scratch_directory, self.data_directories) for input in inputs]

            request = core_pb2.SearchSolutionsRequest(
                user_agent='test client',
                version=version,
                time_bound_search=5,
                allowed_value_types=[automl_base.ValueType.RAW.name, allowed_value_type.name],
                template=grpc_utils.encode_pipeline_description(pipeline, [allowed_value_type], self.scratch_directory, self.data_directories),
                inputs=encoded_inputs,
            )
            response = self.stub.SearchSolutions(request)

            search_id = response.search_id

            self.assertTrue(search_id, allowed_value_type)

            request = core_pb2.GetSearchSolutionsResultsRequest(search_id=search_id)
            responses = list(self.stub.GetSearchSolutionsResults(request))

            # TODO: Check that for state "PENDING", "start" is not yet set.
            self.assertTrue(responses, allowed_value_type)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value, allowed_value_type)
            self.assertTrue(responses[-1].progress.start, allowed_value_type)
            self.assertTrue(responses[-1].progress.end, allowed_value_type)
            self.assertEqual(responses[-1].done_ticks, 1.0, allowed_value_type)

            solution_id = responses[-1].solution_id

            self.assertTrue(solution_id, allowed_value_type)

            request = core_pb2.DescribeSolutionRequest(solution_id=solution_id)
            response = self.stub.DescribeSolution(request)

            pipeline2 = grpc_utils.decode_pipeline_description(response.pipeline, False, None, self.data_directories, dataset_module.ComputeDigest.ONLY_IF_MISSING, False)

            self.assertEqual(pipeline.to_json_structure(nest_subpipelines=True), pipeline2.to_json_structure(nest_subpipelines=True), allowed_value_type)

            self.assertEqual(response.steps[0].primitive.hyperparams['mu'].raw.double, 0.0, allowed_value_type)
            self.assertEqual(response.steps[0].primitive.hyperparams['sigma'].raw.double, 1.0, allowed_value_type)
            self.assertEqual(response.steps[1].primitive.hyperparams['amount'].raw.double, 1.0, allowed_value_type)

            request = core_pb2.FitSolutionRequest(solution_id=solution_id, inputs=encoded_inputs, expose_outputs=['outputs.0'])
            response = self.stub.FitSolution(request)

            request_id = response.request_id

            self.assertTrue(request_id, allowed_value_type)

            request = core_pb2.GetFitSolutionResultsRequest(request_id=request_id)
            responses = list(self.stub.GetFitSolutionResults(request))

            # TODO: Check that for state "PENDING", "start" is not yet set.
            self.assertTrue(responses, allowed_value_type)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value, allowed_value_type)
            self.assertTrue(responses[-1].progress.start, allowed_value_type)
            self.assertTrue(responses[-1].progress.end, allowed_value_type)

            self.assertTrue('outputs.0' in responses[-1].exposed_outputs, allowed_value_type)
            dataframe = automl_base.load_value(grpc_utils.decode_value(responses[-1].exposed_outputs['outputs.0'], self.data_directories))

            self.assertEqual(
                dataframe.values.tolist(),
                [
                    [1.764052345967664 + 1],
                    [0.4001572083672233 + 1],
                    [-1.7062701906250126 + 1],
                ],
                allowed_value_type,
            )

            self.assertEqual(dataframe.metadata.query(())['dimension']['length'], 3, allowed_value_type)

            fitted_solution_id = responses[-1].fitted_solution_id

            # We do not really have to produce (we could just take outputs from fitting),
            # because pipeline is just transforming, but to test APIs we do.
            request = core_pb2.ProduceSolutionRequest(fitted_solution_id=fitted_solution_id, inputs=encoded_inputs, expose_outputs=['outputs.0'])
            response = self.stub.ProduceSolution(request)

            request_id = response.request_id

            self.assertTrue(request_id, allowed_value_type)

            request = core_pb2.GetProduceSolutionResultsRequest(request_id=request_id)
            responses = list(self.stub.GetProduceSolutionResults(request))

            # TODO: Check that for state "PENDING", "start" is not yet set.
            self.assertTrue(responses, allowed_value_type)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value, allowed_value_type)
            self.assertTrue(responses[-1].progress.start, allowed_value_type)
            self.assertTrue(responses[-1].progress.end, allowed_value_type)

            self.assertTrue('outputs.0' in responses[-1].exposed_outputs, allowed_value_type)
            dataframe = automl_base.load_value(grpc_utils.decode_value(responses[-1].exposed_outputs['outputs.0'], self.data_directories))

            self.assertEqual(
                dataframe.values.tolist(),
                [
                    [1.764052345967664 + 1],
                    [0.4001572083672233 + 1],
                    [-1.7062701906250126 + 1],
                ],
                allowed_value_type,
            )

            self.assertEqual(dataframe.metadata.query(())['dimension']['length'], 3, allowed_value_type)

            # Now we change a hyper-parameter in the template.
            pipeline.steps[1].add_hyperparameter('amount', metadata_base.ArgumentType.VALUE, 10)

            request = core_pb2.SearchSolutionsRequest(
                user_agent='test client',
                version=version,
                time_bound_search=5,
                allowed_value_types=[automl_base.ValueType.RAW.name, allowed_value_type.name],
                template=grpc_utils.encode_pipeline_description(pipeline, [allowed_value_type], self.scratch_directory, self.data_directories),
                inputs=encoded_inputs,
            )
            response = self.stub.SearchSolutions(request)

            search_id = response.search_id

            self.assertTrue(search_id, allowed_value_type)

            request = core_pb2.GetSearchSolutionsResultsRequest(search_id=search_id)
            responses = list(self.stub.GetSearchSolutionsResults(request))

            # TODO: Check that for state "PENDING", "start" is not yet set.
            self.assertTrue(responses, allowed_value_type)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value, allowed_value_type)
            self.assertTrue(responses[-1].progress.start, allowed_value_type)
            self.assertTrue(responses[-1].progress.end, allowed_value_type)
            self.assertEqual(responses[-1].done_ticks, 1.0, allowed_value_type)

            solution_id = responses[-1].solution_id

            self.assertTrue(solution_id, allowed_value_type)

            request = core_pb2.DescribeSolutionRequest(solution_id=solution_id)
            response = self.stub.DescribeSolution(request)

            pipeline2 = grpc_utils.decode_pipeline_description(response.pipeline, False, None, self.data_directories, dataset_module.ComputeDigest.ONLY_IF_MISSING, False)

            self.assertEqual(pipeline.to_json_structure(nest_subpipelines=True), pipeline2.to_json_structure(nest_subpipelines=True), allowed_value_type)

            self.assertEqual(response.steps[0].primitive.hyperparams['mu'].raw.double, 0.0, allowed_value_type)
            self.assertEqual(response.steps[0].primitive.hyperparams['sigma'].raw.double, 1.0, allowed_value_type)
            self.assertEqual(len(response.steps[1].primitive.hyperparams), 0)

            request = core_pb2.FitSolutionRequest(solution_id=solution_id, inputs=encoded_inputs, expose_outputs=['outputs.0'])
            response = self.stub.FitSolution(request)

            request_id = response.request_id

            self.assertTrue(request_id, allowed_value_type)

            request = core_pb2.GetFitSolutionResultsRequest(request_id=request_id)
            responses = list(self.stub.GetFitSolutionResults(request))

            # TODO: Check that for state "PENDING", "start" is not yet set.
            self.assertTrue(responses, allowed_value_type)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value, allowed_value_type)
            self.assertTrue(responses[-1].progress.start, allowed_value_type)
            self.assertTrue(responses[-1].progress.end, allowed_value_type)

            self.assertTrue('outputs.0' in responses[-1].exposed_outputs, allowed_value_type)
            dataframe = automl_base.load_value(grpc_utils.decode_value(responses[-1].exposed_outputs['outputs.0'], self.data_directories))

            self.assertEqual(
                dataframe.values.tolist(),
                [
                    [1.764052345967664 + 10],
                    [0.4001572083672233 + 10],
                    [-1.7062701906250126 + 10],
                ],
                allowed_value_type,
            )

            self.assertEqual(dataframe.metadata.query(())['dimension']['length'], 3, allowed_value_type)

    def test_run_pipeline_dataframe(self):
        version = core_pb2.DESCRIPTOR.GetOptions().Extensions[core_pb2.protocol_version]

        csv_file_path = os.path.abspath(os.path.join(self.scratch_directory, 'increment-dataframe-input.csv'))
        with open(csv_file_path, 'x', encoding='utf8') as csv_file:
            csv_file.write('index\n0\n1\n42')

        with open(os.path.join(TESTS_DATA_DIRECTORY, 'pipelines', 'increment-dataframe.yml'), 'r', encoding='utf8') as pipeline_file:
            pipeline = pipeline_module.Pipeline.from_yaml(pipeline_file)

        pipeline.check(standard_pipeline=False)

        encoded_inputs = [
            grpc_utils.encode_value(
                {'type': 'csv_uri', 'value': 'file://{csv_file_path}'.format(csv_file_path=csv_file_path)},
                [automl_base.ValueType.CSV_URI],
                self.scratch_directory,
                self.data_directories,
            ),
        ]

        request = core_pb2.SearchSolutionsRequest(
            user_agent='test client',
            version=version,
            time_bound_search=5,
            allowed_value_types=[automl_base.ValueType.RAW.name, automl_base.ValueType.CSV_URI.name],
            template=grpc_utils.encode_pipeline_description(pipeline, [automl_base.ValueType.RAW], self.scratch_directory, self.data_directories),
            inputs=encoded_inputs,
        )
        response = self.stub.SearchSolutions(request)

        search_id = response.search_id

        self.assertTrue(search_id)

        request = core_pb2.GetSearchSolutionsResultsRequest(search_id=search_id)
        responses = list(self.stub.GetSearchSolutionsResults(request))

        # TODO: Check that for state "PENDING", "start" is not yet set.
        self.assertTrue(responses)
        self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value)
        self.assertTrue(responses[-1].progress.start)
        self.assertTrue(responses[-1].progress.end)
        self.assertEqual(responses[-1].done_ticks, 1.0)

        solution_id = responses[-1].solution_id

        self.assertTrue(solution_id)

        request = core_pb2.DescribeSolutionRequest(solution_id=solution_id)
        response = self.stub.DescribeSolution(request)

        pipeline2 = grpc_utils.decode_pipeline_description(response.pipeline, False, None, self.data_directories, dataset_module.ComputeDigest.ONLY_IF_MISSING, False)

        self.assertEqual(pipeline.to_json_structure(nest_subpipelines=True), pipeline2.to_json_structure(nest_subpipelines=True))

        request = core_pb2.FitSolutionRequest(solution_id=solution_id, inputs=encoded_inputs, expose_outputs=['outputs.0'])
        response = self.stub.FitSolution(request)

        request_id = response.request_id

        self.assertTrue(request_id)

        request = core_pb2.GetFitSolutionResultsRequest(request_id=request_id)
        responses = list(self.stub.GetFitSolutionResults(request))

        # TODO: Check that for state "PENDING", "start" is not yet set.
        self.assertTrue(responses)
        self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value)
        self.assertTrue(responses[-1].progress.start)
        self.assertTrue(responses[-1].progress.end)

        self.assertTrue('outputs.0' in responses[-1].exposed_outputs)
        dataframe = automl_base.load_value(grpc_utils.decode_value(responses[-1].exposed_outputs['outputs.0'], self.data_directories))

        self.assertEqual(
            dataframe.values.tolist(),
            [
                ['1.0'],
                ['2.0'],
                ['43.0'],
            ],
        )

        self.assertEqual(dataframe.metadata.query(())['dimension']['length'], 3)

        fitted_solution_id = responses[-1].fitted_solution_id

        # We do not really have to produce (we could just take outputs from fitting),
        # because pipeline is just transforming, but to test APIs we do.
        request = core_pb2.ProduceSolutionRequest(fitted_solution_id=fitted_solution_id, inputs=encoded_inputs, expose_outputs=['outputs.0'])
        response = self.stub.ProduceSolution(request)

        request_id = response.request_id

        self.assertTrue(request_id)

        request = core_pb2.GetProduceSolutionResultsRequest(request_id=request_id)
        responses = list(self.stub.GetProduceSolutionResults(request))

        # TODO: Check that for state "PENDING", "start" is not yet set.
        self.assertTrue(responses)
        self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value)
        self.assertTrue(responses[-1].progress.start)
        self.assertTrue(responses[-1].progress.end)

        self.assertTrue('outputs.0' in responses[-1].exposed_outputs)
        dataframe = automl_base.load_value(grpc_utils.decode_value(responses[-1].exposed_outputs['outputs.0'], self.data_directories))

        self.assertEqual(
            dataframe.values.tolist(),
            [
                ['1.0'],
                ['2.0'],
                ['43.0'],
            ],
        )

        self.assertEqual(dataframe.metadata.query(())['dimension']['length'], 3)

    def test_run_pipeline_failure(self):
        version = core_pb2.DESCRIPTOR.GetOptions().Extensions[core_pb2.protocol_version]

        csv_file_path = os.path.abspath(os.path.join(self.scratch_directory, 'dummy-input.csv'))
        with open(csv_file_path, 'x', encoding='utf8') as csv_file:
            csv_file.write('index\n0\n1\n42')

        for method_to_fail in ['__init__', 'set_training_data', 'fit', 'produce', 'none']:
            pipeline = pipeline_module.Pipeline()
            pipeline.add_input(name='inputs')
            step = pipeline_module.PrimitiveStep(
                primitive=fail.FailPrimitive,
            )
            step.add_argument('inputs', metadata_base.ArgumentType.CONTAINER, 'inputs.0')
            step.add_output('produce')
            step.add_hyperparameter('method_to_fail', metadata_base.ArgumentType.VALUE, method_to_fail)
            pipeline.add_step(step)
            pipeline.add_output(name='output', data_reference='steps.0.produce')

            pipeline.check(standard_pipeline=False)

            encoded_inputs = [
                grpc_utils.encode_value(
                    {'type': 'csv_uri', 'value': 'file://{csv_file_path}'.format(csv_file_path=csv_file_path)},
                    [automl_base.ValueType.CSV_URI],
                    self.scratch_directory,
                    self.data_directories,
                ),
            ]

            request = core_pb2.SearchSolutionsRequest(
                user_agent='test client',
                version=version,
                time_bound_search=5,
                allowed_value_types=[automl_base.ValueType.RAW.name, automl_base.ValueType.CSV_URI.name],
                template=grpc_utils.encode_pipeline_description(pipeline, [automl_base.ValueType.RAW], self.scratch_directory, self.data_directories),
                inputs=encoded_inputs,
            )
            response = self.stub.SearchSolutions(request)

            search_id = response.search_id

            self.assertTrue(search_id, method_to_fail)

            request = core_pb2.GetSearchSolutionsResultsRequest(search_id=search_id)
            responses = list(self.stub.GetSearchSolutionsResults(request))

            # TODO: Check that for state "PENDING", "start" is not yet set.
            self.assertTrue(responses, method_to_fail)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value, method_to_fail)
            self.assertTrue(responses[-1].progress.start, method_to_fail)
            self.assertTrue(responses[-1].progress.end, method_to_fail)
            self.assertEqual(responses[-1].done_ticks, 1.0, method_to_fail)

            solution_id = responses[-1].solution_id

            self.assertTrue(solution_id, method_to_fail)

            request = core_pb2.DescribeSolutionRequest(solution_id=solution_id)
            response = self.stub.DescribeSolution(request)

            pipeline2 = grpc_utils.decode_pipeline_description(response.pipeline, False, None, self.data_directories, dataset_module.ComputeDigest.ONLY_IF_MISSING, False)

            self.assertEqual(pipeline.to_json_structure(nest_subpipelines=True), pipeline2.to_json_structure(nest_subpipelines=True), method_to_fail)

            request = core_pb2.FitSolutionRequest(solution_id=solution_id, inputs=encoded_inputs, expose_outputs=['outputs.0'])
            response = self.stub.FitSolution(request)

            request_id = response.request_id

            self.assertTrue(request_id, method_to_fail)

            request = core_pb2.GetFitSolutionResultsRequest(request_id=request_id)
            responses = list(self.stub.GetFitSolutionResults(request))

            if method_to_fail in ['__init__', 'set_training_data', 'fit', 'produce']:
                self.assertTrue(responses, method_to_fail)
                self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.ERRORED.value, method_to_fail)
                continue

            # TODO: Check that for state "PENDING", "start" is not yet set.
            self.assertTrue(responses, method_to_fail)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value, method_to_fail)
            self.assertTrue(responses[-1].progress.start, method_to_fail)
            self.assertTrue(responses[-1].progress.end, method_to_fail)

            self.assertTrue('outputs.0' in responses[-1].exposed_outputs, method_to_fail)
            dataframe = automl_base.load_value(grpc_utils.decode_value(responses[-1].exposed_outputs['outputs.0'], self.data_directories))

            self.assertEqual(
                dataframe.values.tolist(),
                [
                    ['0'],
                    ['1'],
                    ['42'],
                ],
            )

            self.assertEqual(dataframe.metadata.query(())['dimension']['length'], 3)

            fitted_solution_id = responses[-1].fitted_solution_id

            # We do not really have to produce (we could just take outputs from fitting),
            # because pipeline is just transforming, but to test APIs we do.
            request = core_pb2.ProduceSolutionRequest(fitted_solution_id=fitted_solution_id, inputs=encoded_inputs, expose_outputs=['outputs.0'])
            response = self.stub.ProduceSolution(request)

            request_id = response.request_id

            self.assertTrue(request_id, method_to_fail)

            request = core_pb2.GetProduceSolutionResultsRequest(request_id=request_id)
            responses = list(self.stub.GetProduceSolutionResults(request))

            # TODO: Check that for state "PENDING", "start" is not yet set.
            self.assertTrue(responses, method_to_fail)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.ERRORED.value, method_to_fail)

    def test_search(self):
        self.maxDiff = None

        version = core_pb2.DESCRIPTOR.GetOptions().Extensions[core_pb2.protocol_version]

        template = pipeline_module.Pipeline()
        template.add_input('dataset_foo')

        step = pipeline_module.PlaceholderStep()
        step.add_input('inputs.0')
        step.add_output('predictions_bar')

        template.add_step(step)
        template.add_output('steps.0.predictions_bar', 'predictions_foo')

        template.check(allow_placeholders=True, standard_pipeline=True, input_types={'inputs.0': container.Dataset})

        dataset_uri = uri_utils.fix_uri(os.path.join(TESTS_DATA_DIRECTORY, 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        problem_description = problems_utils.get_problem(os.path.join(TESTS_DATA_DIRECTORY, 'problems', 'iris_problem_1', 'problemDoc.json'))

        for template_name, template in [('none', None), ('placeholder', template)]:
            encoded_inputs = [grpc_utils.encode_value({'type': 'dataset_uri', 'value': dataset_uri}, self.allowed_value_types, self.scratch_directory, self.data_directories)]

            request = core_pb2.SearchSolutionsRequest(
                user_agent='test client',
                version=version,
                time_bound_search=15,
                allowed_value_types=self.allowed_value_types_names,
                problem=grpc_utils.encode_problem_description(problem_description),
                template=grpc_utils.encode_pipeline_description(template, self.allowed_value_types, self.scratch_directory, self.data_directories) if template is not None else None,
                inputs=encoded_inputs,
            )
            response = self.stub.SearchSolutions(request)

            search_id = response.search_id

            pipelines_scored_dir = os.path.join(self.working_directory.name, search_id, 'pipelines_scored')
            subpipelines_dir = os.path.join(self.working_directory.name, search_id, 'subpipelines')
            pipelines_ranked_dir = os.path.join(self.working_directory.name, search_id, 'pipelines_ranked')

            self.assertTrue(search_id, template_name)

            request = core_pb2.GetSearchSolutionsResultsRequest(search_id=search_id)
            responses = list(self.stub.GetSearchSolutionsResults(request))

            # TODO: Check that for state "PENDING", "start" is not yet set.
            self.assertTrue(responses, template_name)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value, template_name)
            self.assertTrue(responses[-1].progress.start, template_name)
            self.assertTrue(responses[-1].progress.end, template_name)
            self.assertGreaterEqual(responses[-1].done_ticks, 1.0, template_name)

            solution_id = None
            for response in responses:
                # This assumes that the first solution found is one using the template with a random forest.
                if response.solution_id:
                    solution_id = response.solution_id

                    self.assertEqual(response.internal_score, 0.9736842105263158, template_name)
                    self.assertEqual(
                        json_format.MessageToDict(response, including_default_value_fields=True, preserving_proto_field_name=True)['scores'],
                        [
                            {
                                'scoring_configuration': {
                                    'method': 'HOLDOUT',
                                    'folds': 1,
                                    'train_test_ratio': 0.75,
                                    'shuffle': True,
                                    'random_seed': 0,
                                    'stratified': False,
                                },
                                'scores': [
                                    {
                                        'metric': {
                                            'metric': 'ACCURACY',
                                            'k': 0,
                                            'pos_label': '',
                                        },
                                        'value': {
                                            'raw': {
                                                'double': 0.9736842105263158,
                                            },
                                        },
                                        'normalized': {
                                            'raw': {
                                                'double': 0.9736842105263158,
                                            },
                                        },
                                        'random_seed': 0,
                                        'fold': 0,
                                    },
                                ],
                            },
                            {
                                'scoring_configuration': {
                                    'method': 'RANKING',
                                    'folds': 0,
                                    'train_test_ratio': 0.0,
                                    'shuffle': False,
                                    'random_seed': 0,
                                    'stratified': False,
                                },
                                'scores': [
                                    {
                                        'metric': {
                                            'metric': 'RANK',
                                            'k': 0,
                                            'pos_label': '',
                                        },
                                        'value': {
                                            'raw': {
                                                'double': 0.02631578947368418,
                                            },
                                        },
                                        'normalized': {
                                            'raw': {
                                                'double': 0.9736842105263158,
                                            },
                                        },
                                        'random_seed': 0,
                                        'fold': 0,
                                    },
                                ],
                            },
                        ],
                        template_name,
                    )

                    break

            self.assertTrue(solution_id, template_name)

            request = core_pb2.DescribeSolutionRequest(solution_id=solution_id)
            response = self.stub.DescribeSolution(request)

            pipeline = grpc_utils.decode_pipeline_description(response.pipeline, False, None, self.data_directories, dataset_module.ComputeDigest.ONLY_IF_MISSING, False)

            self.assertFalse(pipeline.has_placeholder(), template_name)

            pipeline_structure = pipeline.to_json_structure()

            if template_name == 'placeholder':
                # With template, result should be replacing the placeholder.
                self.assertEqual(len(pipeline_structure['steps']), 1, template_name)
                # Names should be preserved.
                self.assertEqual(pipeline_structure['inputs'], [{'name': 'dataset_foo'}], template_name)
                self.assertEqual(pipeline_structure['outputs'], [{'data': 'steps.0.predictions_bar', 'name': 'predictions_foo'}], template_name)
                self.assertEqual(pipeline_structure['steps'][0]['type'], 'SUBPIPELINE', template_name)
                self.assertEqual(pipeline_structure['steps'][0]['inputs'], [{'data': 'inputs.0'}], template_name)
                self.assertEqual(pipeline_structure['steps'][0]['outputs'], [{'id': 'predictions_bar'}], template_name)

                # Sub-pipeline should match the pipeline found when there is no template.
                pipeline_structure = pipeline.steps[0].pipeline.to_json_structure()

            # When there is no template, we directly return found pipeline which has standard input and output names.
            self.assertEqual(pipeline_structure['inputs'], [{'name': 'dataset'}], template_name)
            self.assertEqual(len(pipeline_structure['outputs']), 1, template_name)
            self.assertEqual(pipeline_structure['outputs'][0]['name'], 'predictions', template_name)

            scored_pipeline_path = os.path.join(pipelines_scored_dir, '{solution_id}.json'.format(solution_id=solution_id))

            # After a solution is found, its pipeline should be exported to "pipelines_scored_dir".
            with open(scored_pipeline_path, encoding='utf8') as scored_pipeline_file:
                scored_pipeline = pipeline_module.Pipeline.from_json(scored_pipeline_file, resolver=pipeline_module.Resolver(pipeline_search_paths=[subpipelines_dir]))

            pipeline.id = solution_id
            expected_pipeline_structure = pipeline.to_json_structure(nest_subpipelines=True)

            self.assertEqual(expected_pipeline_structure, scored_pipeline.to_json_structure(nest_subpipelines=True), template_name)

            request = core_pb2.ScoreSolutionRequest(
                solution_id=solution_id,
                inputs=encoded_inputs,
                performance_metrics=[
                    problem_pb2.ProblemPerformanceMetric(
                        metric='ACCURACY',
                    ),
                ],
                # Repeating configuration from during search to make sure results are equal.
                configuration=core_pb2.ScoringConfiguration(
                    method='HOLDOUT',
                    train_test_ratio=0.75,
                    shuffle=True,
                    random_seed=0,
                    stratified=False,
                ),
            )
            response = self.stub.ScoreSolution(request)

            request_id = response.request_id

            self.assertTrue(request_id, template_name)

            request = core_pb2.GetScoreSolutionResultsRequest(request_id=request_id)
            responses = list(self.stub.GetScoreSolutionResults(request))

            # TODO: Check that for state "PENDING", "start" is not yet set.
            self.assertTrue(responses, template_name)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value, template_name)
            self.assertTrue(responses[-1].progress.start, template_name)
            self.assertTrue(responses[-1].progress.end, template_name)

            self.assertEqual(
                json_format.MessageToDict(responses[-1], including_default_value_fields=True, preserving_proto_field_name=True)['scores'],
                [
                    {
                        'metric': {
                            'metric': 'ACCURACY',
                            'k': 0,
                            'pos_label': '',
                        },
                        'value': {
                            'raw': {
                                'double': 0.9736842105263158,
                            },
                        },
                        'normalized': {
                            'raw': {
                                'double': 0.9736842105263158,
                            },
                        },
                        'fold': 0,
                        'random_seed': 0,
                    },
                ],
                template_name,
            )

            request = core_pb2.ScoreSolutionRequest(
                solution_id=solution_id,
                inputs=encoded_inputs,
                performance_metrics=[
                    problem_pb2.ProblemPerformanceMetric(
                        metric='ACCURACY',
                    ),
                    problem_pb2.ProblemPerformanceMetric(
                        metric='F1_MICRO',
                    ),
                ],
                configuration=core_pb2.ScoringConfiguration(
                    method='K_FOLD',
                    folds=10,
                    shuffle=False,
                    random_seed=0,
                    stratified=False,
                ),
            )
            response = self.stub.ScoreSolution(request)

            request_id = response.request_id

            self.assertTrue(request_id, template_name)

            request = core_pb2.GetScoreSolutionResultsRequest(request_id=request_id)
            responses = list(self.stub.GetScoreSolutionResults(request))

            # TODO: Check that for state "PENDING", "start" is not yet set.
            self.assertTrue(responses, template_name)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value, template_name)
            self.assertTrue(responses[-1].progress.start, template_name)
            self.assertTrue(responses[-1].progress.end, template_name)

            self.assertEqual(
                json_format.MessageToDict(responses[-1], including_default_value_fields=True, preserving_proto_field_name=True)['scores'],
                [
                    {
                        'metric': {'k': 0, 'metric': 'ACCURACY', 'pos_label': ''},
                        'value': {'raw': {'double': 1.0}},
                        'normalized': {'raw': {'double': 1.0}},
                        'random_seed': 0,
                        'fold': 0,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'F1_MICRO', 'pos_label': ''},
                        'value': {'raw': {'double': 1.0}},
                        'normalized': {'raw': {'double': 1.0}},
                        'random_seed': 0,
                        'fold': 0,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'ACCURACY', 'pos_label': ''},
                        'value': {'raw': {'double': 1.0}},
                        'normalized': {'raw': {'double': 1.0}},
                        'random_seed': 0,
                        'fold': 1,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'F1_MICRO', 'pos_label': ''},
                        'value': {'raw': {'double': 1.0}},
                        'normalized': {'raw': {'double': 1.0}},
                        'random_seed': 0,
                        'fold': 1,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'ACCURACY', 'pos_label': ''},
                        'value': {'raw': {'double': 1.0}},
                        'normalized': {'raw': {'double': 1.0}},
                        'random_seed': 0,
                        'fold': 2,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'F1_MICRO', 'pos_label': ''},
                        'value': {'raw': {'double': 1.0}},
                        'normalized': {'raw': {'double': 1.0}},
                        'random_seed': 0,
                        'fold': 2,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'ACCURACY', 'pos_label': ''},
                        'value': {'raw': {'double': 1.0}},
                        'normalized': {'raw': {'double': 1.0}},
                        'random_seed': 0,
                        'fold': 3,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'F1_MICRO', 'pos_label': ''},
                        'value': {'raw': {'double': 1.0}},
                        'normalized': {'raw': {'double': 1.0}},
                        'random_seed': 0,
                        'fold': 3,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'ACCURACY', 'pos_label': ''},
                        'value': {'raw': {'double': 0.9333333333333333}},
                        'normalized': {'raw': {'double': 0.9333333333333333}},
                        'random_seed': 0,
                        'fold': 4,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'F1_MICRO', 'pos_label': ''},
                        'value': {'raw': {'double': 0.9333333333333333}},
                        'normalized': {'raw': {'double': 0.9333333333333333}},
                        'random_seed': 0,
                        'fold': 4,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'ACCURACY', 'pos_label': ''},
                        'value': {'raw': {'double': 0.8666666666666667}},
                        'normalized': {'raw': {'double': 0.8666666666666667}},
                        'random_seed': 0,
                        'fold': 5,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'F1_MICRO', 'pos_label': ''},
                        'value': {'raw': {'double': 0.8666666666666667}},
                        'normalized': {'raw': {'double': 0.8666666666666667}},
                        'random_seed': 0,
                        'fold': 5,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'ACCURACY', 'pos_label': ''},
                        'value': {'raw': {'double': 1.0}},
                        'normalized': {'raw': {'double': 1.0}},
                        'random_seed': 0,
                        'fold': 6,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'F1_MICRO', 'pos_label': ''},
                        'value': {'raw': {'double': 1.0}},
                        'normalized': {'raw': {'double': 1.0}},
                        'random_seed': 0,
                        'fold': 6,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'ACCURACY', 'pos_label': ''},
                        'value': {'raw': {'double': 0.8666666666666667}},
                        'normalized': {'raw': {'double': 0.8666666666666667}},
                        'random_seed': 0,
                        'fold': 7,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'F1_MICRO', 'pos_label': ''},
                        'value': {'raw': {'double': 0.8666666666666667}},
                        'normalized': {'raw': {'double': 0.8666666666666667}},
                        'random_seed': 0,
                        'fold': 7,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'ACCURACY', 'pos_label': ''},
                        'value': {'raw': {'double': 0.8}},
                        'normalized': {'raw': {'double': 0.8}},
                        'random_seed': 0,
                        'fold': 8,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'F1_MICRO', 'pos_label': ''},
                        'value': {'raw': {'double': 0.8000000000000002}},
                        'normalized': {'raw': {'double': 0.8000000000000002}},
                        'random_seed': 0,
                        'fold': 8,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'ACCURACY', 'pos_label': ''},
                        'value': {'raw': {'double': 1.0}},
                        'normalized': {'raw': {'double': 1.0}},
                        'random_seed': 0,
                        'fold': 9,
                    },
                    {
                        'metric': {'k': 0, 'metric': 'F1_MICRO', 'pos_label': ''},
                        'value': {'raw': {'double': 1.0}},
                        'normalized': {'raw': {'double': 1.0}},
                        'random_seed': 0,
                        'fold': 9,
                    },
                ],
                template_name,
            )

            request = core_pb2.ScoreSolutionRequest(
                solution_id=solution_id,
                inputs=encoded_inputs,
                performance_metrics=[
                    problem_pb2.ProblemPerformanceMetric(
                        metric='RANK',
                    ),
                ],
                configuration=core_pb2.ScoringConfiguration(
                    method='RANKING',
                ),
            )
            response = self.stub.ScoreSolution(request)

            request_id = response.request_id

            self.assertTrue(request_id, template_name)

            request = core_pb2.GetScoreSolutionResultsRequest(request_id=request_id)
            responses = list(self.stub.GetScoreSolutionResults(request))

            # TODO: Check that for state "PENDING", "start" is not yet set.
            self.assertTrue(responses, template_name)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value, template_name)
            self.assertTrue(responses[-1].progress.start, template_name)
            self.assertTrue(responses[-1].progress.end, template_name)

            self.assertEqual(
                json_format.MessageToDict(responses[-1], including_default_value_fields=True, preserving_proto_field_name=True)['scores'],
                [
                    {
                        'metric': {
                            'metric': 'RANK',
                            'k': 0,
                            'pos_label': '',
                        },
                        'value': {
                            'raw': {
                                'double': 0.02631578947368418,
                            },
                        },
                        'normalized': {
                            'raw': {
                                'double': 0.9736842105263158,
                            },
                        },
                        'fold': 0,
                        'random_seed': 0,
                    },
                ],
                template_name,
            )

            request = core_pb2.FitSolutionRequest(solution_id=solution_id, inputs=encoded_inputs, expose_outputs=['outputs.0'])
            response = self.stub.FitSolution(request)

            request_id = response.request_id

            self.assertTrue(request_id, template_name)

            request = core_pb2.GetFitSolutionResultsRequest(request_id=request_id)
            responses = list(self.stub.GetFitSolutionResults(request))

            self.assertTrue(responses, template_name)
            self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value, template_name)
            self.assertTrue(responses[-1].progress.start, template_name)
            self.assertTrue(responses[-1].progress.end, template_name)

            self.assertTrue('outputs.0' in responses[-1].exposed_outputs, template_name)

            request = core_pb2.SolutionExportRequest(solution_id=solution_id, rank=4.2)
            self.stub.SolutionExport(request)

            # Scored pipeline should be removed after export.
            self.assertFalse(os.path.exists(scored_pipeline_path), template_name)

            with open(os.path.join(pipelines_ranked_dir, '{solution_id}.json'.format(solution_id=solution_id)), encoding='utf8') as exported_pipeline_file:
                exported_pipeline_json = json.load(exported_pipeline_file)
                exported_pipeline_file.seek(0)
                exported_pipeline = pipeline_module.Pipeline.from_json(exported_pipeline_file, resolver=pipeline_module.Resolver(pipeline_search_paths=[subpipelines_dir]))

            self.assertEqual(expected_pipeline_structure, exported_pipeline.to_json_structure(nest_subpipelines=True), template_name)

            with open(os.path.join(pipelines_ranked_dir, '{solution_id}.rank'.format(solution_id=solution_id)), encoding='utf8') as exported_pipeline_rank:
                self.assertEqual(exported_pipeline_rank.read(), '4.2', template_name)

    def test_preprocessing(self):
        version = core_pb2.DESCRIPTOR.GetOptions().Extensions[core_pb2.protocol_version]

        template = pipeline_module.Pipeline()
        template.add_input()

        step = pipeline_module.PrimitiveStep(
            {
                'id': '3b09ba74-cc90-4f22-9e0a-0cf4f29a7e28',
                'version': '0.1.0',
                'name': "Removes columns",
                'python_path': 'd3m.primitives.data_transformation.remove_columns.Common',
            },
            resolver=pipeline_module.Resolver(),
        )
        step.add_hyperparameter('columns', metadata_base.ArgumentType.VALUE, [3])

        template.add_step(step)

        step = pipeline_module.PrimitiveStep(
            {
                'id': '5bef5738-1638-48d6-9935-72445f0eecdc',
                'version': '0.1.0',
                'name': "Map DataFrame resources to new resources using provided primitive",
                'python_path': 'd3m.primitives.operator.dataset_map.DataFrameCommon',
            },
            resolver=pipeline_module.Resolver(),
        )
        step.add_argument('inputs', metadata_base.ArgumentType.CONTAINER, 'inputs.0')
        step.add_output('produce')
        step.add_hyperparameter('primitive', metadata_base.ArgumentType.PRIMITIVE, 0)

        template.add_step(step)

        step = pipeline_module.PlaceholderStep()
        step.add_input('steps.1.produce')
        step.add_output('predictions')

        template.add_step(step)

        template.add_output('steps.2.predictions')

        template.check(allow_placeholders=True, standard_pipeline=True, input_types={'inputs.0': container.Dataset})

        dataset_uri = uri_utils.fix_uri(os.path.join(TESTS_DATA_DIRECTORY, 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        problem_description = problems_utils.get_problem(os.path.join(TESTS_DATA_DIRECTORY, 'problems', 'iris_problem_1', 'problemDoc.json'))

        encoded_inputs = [grpc_utils.encode_value({'type': 'dataset_uri', 'value': dataset_uri}, self.allowed_value_types, self.scratch_directory, self.data_directories)]

        request = core_pb2.SearchSolutionsRequest(
            user_agent='test client',
            version=version,
            time_bound_search=15,
            allowed_value_types=self.allowed_value_types_names,
            problem=grpc_utils.encode_problem_description(problem_description),
            template=grpc_utils.encode_pipeline_description(template, self.allowed_value_types, self.scratch_directory, self.data_directories),
            inputs=encoded_inputs,
        )
        response = self.stub.SearchSolutions(request)

        search_id = response.search_id

        self.assertTrue(search_id)

        request = core_pb2.GetSearchSolutionsResultsRequest(search_id=search_id)
        responses = list(self.stub.GetSearchSolutionsResults(request))

        # TODO: Check that for state "PENDING", "start" is not yet set.
        self.assertTrue(responses)
        self.assertEqual(responses[-1].progress.state, automl_base.ProgressState.COMPLETED.value)
        self.assertTrue(responses[-1].progress.start)
        self.assertTrue(responses[-1].progress.end)
        self.assertGreaterEqual(responses[-1].done_ticks, 1.0)

        solution_id = None
        max_internal_score = None
        for response in reversed(responses):
            if response.solution_id:
                solution_id = response.solution_id

                if max_internal_score is None or response.internal_score > max_internal_score:
                    max_internal_score = response.internal_score

        self.assertTrue(solution_id)
        self.assertGreaterEqual(max_internal_score, 0.9210526315789473)

        request = core_pb2.DescribeSolutionRequest(solution_id=solution_id)
        response = self.stub.DescribeSolution(request)

        pipeline = grpc_utils.decode_pipeline_description(response.pipeline, False, None, self.data_directories, dataset_module.ComputeDigest.ONLY_IF_MISSING, False)

        self.assertIsInstance(pipeline.steps[2], pipeline_module.SubpipelineStep)

    def test_score_invalid_arguments(self):
        version = core_pb2.DESCRIPTOR.GetOptions().Extensions[core_pb2.protocol_version]

        dataset_uri = uri_utils.fix_uri(os.path.join(TESTS_DATA_DIRECTORY, 'datasets', 'iris_dataset_1', 'datasetDoc.json'))

        problem_description = problems_utils.get_problem(os.path.join(TESTS_DATA_DIRECTORY, 'problems', 'iris_problem_1', 'problemDoc.json'))

        encoded_inputs = [grpc_utils.encode_value({'type': 'dataset_uri', 'value': dataset_uri}, self.allowed_value_types, self.scratch_directory, self.data_directories)]

        request = core_pb2.SearchSolutionsRequest(
            user_agent='test client',
            version=version,
            time_bound_search=15,
            allowed_value_types=self.allowed_value_types_names,
            problem=grpc_utils.encode_problem_description(problem_description),
            inputs=encoded_inputs,
        )
        response = self.stub.SearchSolutions(request)

        search_id = response.search_id

        self.assertTrue(search_id)

        request = core_pb2.GetSearchSolutionsResultsRequest(search_id=search_id)
        responses = list(self.stub.GetSearchSolutionsResults(request))

        solution_id = None
        for response in reversed(responses):
            if response.solution_id:
                solution_id = response.solution_id
                break

        self.assertTrue(solution_id)

        request = core_pb2.ScoreSolutionRequest(
            solution_id=solution_id,
            inputs=encoded_inputs,
            performance_metrics=[],
            configuration=core_pb2.ScoringConfiguration(
                method='HOLDOUT',
                train_test_ratio=0.75,
                shuffle=True,
                random_seed=0,
                stratified=False,
            ),
        )

        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.ScoreSolution(request)

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        request = core_pb2.ScoreSolutionRequest(
            solution_id=solution_id,
            inputs=encoded_inputs,
            performance_metrics=[
                problem_pb2.ProblemPerformanceMetric(
                    metric='ACCURACY',
                ),
            ],
            configuration=core_pb2.ScoringConfiguration(
                method='HOLDOUT',
                train_test_ratio=0,
                shuffle=True,
                random_seed=0,
                stratified=False,
            ),
        )

        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.ScoreSolution(request)

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        request = core_pb2.ScoreSolutionRequest(
            solution_id=solution_id,
            inputs=encoded_inputs,
            performance_metrics=[
                problem_pb2.ProblemPerformanceMetric(
                    metric='ACCURACY',
                ),
            ],
            configuration=core_pb2.ScoringConfiguration(
                method='HOLDOUT',
                train_test_ratio=1,
                shuffle=True,
                random_seed=0,
                stratified=False,
            ),
        )

        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.ScoreSolution(request)

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        request = core_pb2.ScoreSolutionRequest(
            solution_id=solution_id,
            inputs=encoded_inputs,
            performance_metrics=[
                problem_pb2.ProblemPerformanceMetric(
                    metric='ACCURACY',
                ),
            ],
            configuration=core_pb2.ScoringConfiguration(
                method='K_FOLD',
                folds=0,
                shuffle=True,
                random_seed=0,
                stratified=False,
            ),
        )

        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.ScoreSolution(request)

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        request = core_pb2.ScoreSolutionRequest(
            solution_id=solution_id,
            inputs=encoded_inputs,
            performance_metrics=[
                problem_pb2.ProblemPerformanceMetric(
                    metric='ACCURACY',
                ),
            ],
            configuration=core_pb2.ScoringConfiguration(
                method='K_FOLD',
                folds=1,
                shuffle=True,
                random_seed=0,
                stratified=False,
            ),
        )

        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.ScoreSolution(request)

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

    def test_empty_requests(self):
        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.SearchSolutions(core_pb2.SearchSolutionsRequest())

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        with self.assertRaises(grpc.RpcError) as cm:
            list(self.stub.GetSearchSolutionsResults(core_pb2.GetSearchSolutionsResultsRequest()))

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.EndSearchSolutions(core_pb2.EndSearchSolutionsRequest())

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.StopSearchSolutions(core_pb2.StopSearchSolutionsRequest())

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.DescribeSolution(core_pb2.DescribeSolutionRequest())

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.ScoreSolution(core_pb2.ScoreSolutionRequest())

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        with self.assertRaises(grpc.RpcError) as cm:
            list(self.stub.GetScoreSolutionResults(core_pb2.GetScoreSolutionResultsRequest()))

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.FitSolution(core_pb2.FitSolutionRequest())

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        with self.assertRaises(grpc.RpcError) as cm:
            list(self.stub.GetFitSolutionResults(core_pb2.GetFitSolutionResultsRequest()))

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.ProduceSolution(core_pb2.ProduceSolutionRequest())

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        with self.assertRaises(grpc.RpcError) as cm:
            list(self.stub.GetProduceSolutionResults(core_pb2.GetProduceSolutionResultsRequest()))

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)

        with self.assertRaises(grpc.RpcError) as cm:
            self.stub.SolutionExport(core_pb2.SolutionExportRequest())

        self.assertEqual(cm.exception.code(), grpc.StatusCode.INVALID_ARGUMENT)


if __name__ == '__main__':
    unittest.main()
