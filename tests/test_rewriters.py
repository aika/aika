import os
import unittest

from aika.pipelines import pipeline as aika_pipeline, utils as pipelines_utils
from aika.search.rewriter import hyperparams


class TestRewriters(unittest.TestCase):
    def setUp(self):
        self.pipeline = pipelines_utils.get_pipeline(
            os.path.join(aika_pipeline.TEMPLATES_DIRECTORY, 'random-forest-classification.yml'),
            allow_pipeline_path=True,
        )
        # We make "n_estimators" a free hyper-parameter.
        del self.pipeline.steps[-2].hyperparams['n_estimators']

    def test_rules_based_rewriter(self):
        rewriter = hyperparams.RulesBasedRewriter(None, None)
        pipeline, free_hyperparams = rewriter.rewrite(self.pipeline)
        hyperparams_configuration = free_hyperparams[-2]
        self.assertEqual(hyperparams_configuration['n_estimators'].get_default(), hyperparams.N_ESTIMATORS_DEFAULT)
        self.assertEqual(hyperparams_configuration['min_impurity_decrease'].upper, hyperparams.MIN_IMPURITY_DECREASE_UPPER)
        hyperparams_configuration = self.pipeline.get_free_hyperparams()[-2]
        self.assertEqual(hyperparams_configuration['n_estimators'].get_default(), 10)
        self.assertEqual(hyperparams_configuration['min_impurity_decrease'].upper, None)


if __name__ == '__main__':
    unittest.main()
