#!/usr/bin/env python3

import os
import sys
import unittest

if __name__ == '__main__':
    if os.environ.get('GITLAB_CI'):
        import xmlrunner

        runner = xmlrunner.XMLTestRunner(output='test-reports')
    else:
        runner = unittest.TextTestRunner(verbosity=1)

    tests = unittest.TestLoader().discover('tests')

    if not runner.run(tests).wasSuccessful():
        sys.exit(1)
