# Contributing

First, thank you for considering contributing to Aika.

## Cloning

Make sure you clone this repository recursively to get all git submodules:

```
$ git clone --recursive https://gitlab.com/aika/aika.git
```

## Docker Images

Docker images [are available here](https://gitlab.com/aika/aika/container_registry). If you want the latest stable
image, use `registry.gitlab.com/aika/aika/branch/main:latest`. Docker images are also built for every branch and
merge request made, so that it is easy to share your progress or debugging with others. See [Dockerfile](./Dockerfile)
for details about contents of Docker images.

## Working Inside Docker Container

While developing it is useful to run [Aika Docker image](./README.md#docker-images) in the foreground with shell and
having code and datasets  mounted into the container.
In this way when you change the code, you are also changing code available inside
the container and you can through shell run various commands.

```
$ docker run --rm -t -i --name aika \
 --volume "$(pwd):/code" \
 --volume "/path/to/datasets:/datasets" \
 registry.gitlab.com/aika/aika/branch/main:latest /bin/bash
```

The `--volume` argument to [`docker run`](https://docs.docker.com/engine/reference/commandline/run/)
instructs Docker to mount the listed outside directory inside a running container at the given location.
While there is already `/code` directory inside
it contains code which was added there at the image built time.
But while developing locally you would probably prefer to have your latest code from your machine
available inside, so the command above mounts your current working directory with code over the
existing `/code` directory.

`/bin/bash` at the end tells that you want to run just a shell inside the container and not
the default set of services (like MongoDB and Ray). If you omit `/bin/bash`, the container
will start all services and the `docker run` command will not give you a shell by itself
(you are not even able to stop the container using ctrl-c, you have to use
[`docker stop`](https://docs.docker.com/engine/reference/commandline/stop/)). If you want to attach
a shell to a running container in this case, so that you can run commands while having services
running, use [`docker exec`](https://docs.docker.com/engine/reference/commandline/exec/). Also, run
the container in the detached mode:

```
$ docker run --rm --detach --name aika \
 --volume "/path/to/datasets:/datasets" \
 registry.gitlab.com/aika/aika/branch/main:latest
$ docker exec -t -i aika /bin/bash
```

`--name` names this container so that it is easier to reference it (as seen above in
the `docker exec` example). But if you want to run multiple containers with Aika at
the same time, you should or change the name every time, or not use the argument
to let Docker pick one at random.

You can update the image at a later time to a newer one by running:

```
$ docker pull registry.gitlab.com/aika/aika/branch/main:latest
```

This will get a new image which has all dependencies installed from the current `main`
branch. You have to rerun the container to use the new image.

### Working Inside Docker Container From Your IDE

IDEs like PyCharm and VSCode (with support of addons) allow you to run and debug code in the
Docker container directly from your main system. Running the code step by step or by using
breakpoints can help you better understand what are the entry points and return values of
methods of your interest. This also comes with the benefit of having autocomplete and
documentation of all modules used by Aika available inside your IDE without having to
install them on your main system.

Following are the setup instructions for either VSCode or PyCharm. After completing the setup, the `534_cps_85_wages.yml` pipeline will be run in both cases, but behavior can be easily changed with use of different arguments.

#### VSCode Configuration

To enable debugging in VSCode, first download and install
[Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack) extension.
Before you are able to use a debugger for Aika, you have open a folder from
Docker container in your workspace and set up debugger configuration file
(`launch.json`).

Example entry in `launch.json` file:
```json
{
    "name": "27_word_levels",
    "type": "python",
    "request": "launch",
    "program": "/usr/local/bin/aika",
    "cwd": "/code/aika/contrib/pipelines/d3m-baseline-pipelines/",
    "args": ["-c", "d3m", "runtime", "fit-score", "--pipeline", "534_cps_85_wages.yml", "--meta", "534_cps_85_wages.meta"],
    "console": "integratedTerminal"
}
```

#### PyCharm Configuration

Note that remote development is supported only in PyCharm Professional edition. First, you need to
open your preferred workspace folder and configure Python interpreter inside Docker container.
You can see a step by step guide [here](https://www.jetbrains.com/help/pycharm/using-docker-as-a-remote-interpreter.html).

After you set your Python interpreter, create a new debug configuration. Add a new Python configuration
and, for example, set the following fields:

- Script path: `/usr/local/bin/aika`
- Parameters: `-c d3m runtime fit-score --pipeline 534_cps_85_wages.yml --meta 534_cps_85_wages.meta`
- Working directory: depending on your folder structure, set it to your local `d3m-baseline-pipelines` folder.
- Docker container settings: map `aika` and `datasets` local folders to corresponding folders in the running container.
  You have to do this even if you already mapped folders when starting the container.

## Running

Most commands require services like MongoDB and Ray to be running in the background. To achieve
this you have three options:

1. You `docker run` the container with `/bin/bash` at the end to start the shell in the container.
Every time you run any Aika command, when necessary, services will start in the background
automatically, and will be stopped when the command finishes.
2. You `docker run` the container without specifying `/bin/bash` at the end, so that all services
start automatically and then you `docker exec` into a running container to run Aika commands.
In all Aika commands use `--client-only` argument to configure them to just connect to the
services instead of starting them, like `aika --client-only <command>`. A plus side of this
approach is that you will not have to wait for services to start and initialize for every
command. The downside is that if you are changing code used by Ray, the code changes will
not be picked up without restarting the container (or just services).
3. A hybrid option, where you `docker run` the container with `/bin/bash` at the end to start
the shell in the container. Then you `docker exec` into a running container twice, to
run services manually by running `/etc/service/ray/run` and `/etc/service/mongodb/run`.
A good side of this approach is that you can easier restart the Ray service to update the
changing code used by Ray.

In the end, you have to assure services run when you run commands. Or commands start and stop
services automatically, or you run them yourself and use `--client-only` argument.

Running `aika -h` inside the container is equivalent to `python3 -m aika -h`.

## Connecting to MongoDB

If you want to inspect contents of the MongoDB database, MongoDB service has to run, and then
you can connect to it using, inside the container:

```
$ mongo aika
```

See [documentation for available collections and their structure](./aika/automl/README.md#collections).

## Updating Dependencies

Most of the time you can just use the latest image available from GitLab. But if you changed
any Python or system dependencies or want to use updated base image (with potentially updated primitives),
you might have to build a new image locally. This way you can test how it works before you commit your changes,
push to GitLab, and have your changes built there.

If you update any dependencies (`requirements.txt` or `packages.txt`) you can rebuild
the image locally. You can give it a new name, e.g., `aika:latest`.
When you build a Docker image you want Docker 18.09 or newer installed and to enable
[buildkit](https://docs.docker.com/develop/develop-images/build_enhancements/):

```
$ DOCKER_BUILDKIT=1 docker build -t aika:latest .
```

This will use the version of the base image listed in the [`Dockerfile`](./Dockerfile).
If you want to use an updated
version of the base image, you have to update `FROM`, `ENV D3M_BASE_IMAGE_NAME`, and
`ENV D3M_BASE_IMAGE_DIGEST` in the `Dockerfile`.

You can use your local version of `aika:latest` image as you would the one
from GitLab.

## Checks and Tests

We use CI to run a [series of checks and tests](./.gitlab-ci.yml) on every git push to GtiLab.

You can run checks and tests also locally. First, install dependencies and testing tools locally:

```
$ pip3 install --use-deprecated=legacy-resolver --upgrade --upgrade-strategy only-if-needed --requirement -r requirements.txt
$ pip3 install -r https://gitlab.com/aika/images/-/raw/main/requirements-testing.txt
```

To run [linting tools](https://en.wikipedia.org/wiki/Lint_(software)) to check that the code
adheres to [our code style](./CODE_STYLE.md):

```
$ black --color aika
```

```
$ flake8 aika/
```

```
$ export "PYTHONPATH=bin/pylint:$PYTHONPATH"
$ pylint aika/
```

Type checking:

```
$ mypy aika
```

To check that imports are properly ordered and structured:

```
$ cd /code
$ isort --recursive --check-only --diff aika/
```

To run various tests Aika comes with:

```
$ ./run_tests.py
```

While CI will run all these checks and tests for you, running them locally
allows much faster development than waiting on CI to run them.

## Code Style

Our code style is [documented here](./CODE_STYLE.md). Please try to stick to it.
We use various tools to help you with this (see above).

## License

Aika is licensed under [Apache License, version 2.0](./LICENSE). Everything you contribute
is licensed under it as well.
