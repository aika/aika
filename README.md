# Aika

## Docker Images

Aika uses many dependencies which can be tricky to install all. For this reason the best way to
both run Aika is to use a Docker image which contains all dependencies already installed.
Docker images [are available here](https://gitlab.com/aika/aika/container_registry). If you want the latest stable
image, use `registry.gitlab.com/aika/aika/branch/main:latest`. 

## Quickstart

First run Aika locally in the background:

```
$ docker run --rm --detach --name aika \
 --publish 45042:45042 \
 --volume "/path/to/datasets:/datasets" \
 registry.gitlab.com/aika/aika/branch/main:latest
```

Aika has a command line interface. You can access more information about it by running inside
the container:

```
$ docker exec -t -i aika /bin/bash
$ aika -h
```

There are multiple commands available. Each of them has its own help information as well. For example:

```
$ aika list-primitives -h
```

Or connect to port 45042 using [AutoML RPC](https://gitlab.com/datadrivendiscovery/automl-rpc).

To stop Aika, run on the outside of the container:

```
$ docker stop aika
```

## Contributing

See [Contributing](./CONTRIBUTING.md) guide for more details.

## License

Aika is licensed under [Apache License, version 2.0](./LICENSE).
